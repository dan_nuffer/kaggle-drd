#! /usr/bin/python
import importlib
import numpy as np
import xgboost as xgb
import pandas as pd
from clint.textui import progress, indent, puts, colored
import skll
import utils
import sys
from scipy.stats import randint as sp_randint
from scipy.stats import uniform as sp_rand
from sklearn.grid_search import RandomizedSearchCV


class XGBoostClassifier:
    def __init__(self, num_boost_round=10, **params):
        self.clf = None
        self.num_boost_round = num_boost_round
        self.params = params
        self.params.update({'objective': 'multi:softmax'})

    def fit(self, X, y, num_boost_round=None):
        num_boost_round = num_boost_round or self.num_boost_round
        dtrain = xgb.DMatrix(X, label=y)
        self.clf = xgb.train(params=self.params, dtrain=dtrain, num_boost_round=num_boost_round)

    def predict(self, X):
        return self.clf.predict(xgb.DMatrix(X))

    def score(self, X, y):
        Y = self.predict(X)
        # positive values are better than negative, so we don't need to negate
        return skll.kappa(Y, y, 'quadratic')

    def get_params(self, deep=True):
        return self.params

    def set_params(self, **params):
        if 'num_boost_round' in params:
            self.num_boost_round = params.pop('num_boost_round')
        if 'objective' in params:
            del params['objective']
        self.params.update(params)
        return self

def main():
    config_name = sys.argv[1]
    train_predictions_path = sys.argv[2]
    valid_predictions_path = sys.argv[3]

    config = importlib.import_module("nets.%s" % config_name)

    train_X = np.load(train_predictions_path)
    train_Y = utils.predict_ordinal(config.data_loader.labels_train, thresh=0.5)

    test_X = np.load(valid_predictions_path)
    test_Y = utils.predict_ordinal(config.data_loader.labels_valid, thresh=0.5)

    #xg_train = xgb.DMatrix( train_X, label=train_Y)
    #xg_test = xgb.DMatrix(test_X, label=test_Y)


    xgb_clf = XGBoostClassifier(
        num_class = 5,
        nthread = 4,
        silent = 1,
        )

    def sp_unif_range(low, high):
        return sp_rand(low, high - low)

    # colsample_bytree: 0.3544459180620564
    # eta: 0.021317902988929494
    # max_depth: 6
    # num_boost_round: 121
    # subsample: 0.15302735359515365

    # colsample_bytree: 0.38615840855791594
    # eta: 0.030739032962849726
    # max_depth: 4
    # num_boost_round: 76
    # subsample: 0.056143876591567644


    parameters = {
        'num_boost_round': sp_randint(20, 100),
        'eta': sp_unif_range(0.001, 0.1),
        'max_depth': sp_randint(2, 7),
        'subsample': sp_unif_range(0.01, 0.2),
        'colsample_bytree': sp_unif_range(0.25, 0.45), # Can't make this smaller than 1/# features
    }
    clf = RandomizedSearchCV(xgb_clf, parameters, n_jobs=1, cv=3, n_iter=300, verbose=2)

    clf.fit(train_X, train_Y)
    best_parameters, score, _ = max(clf.grid_scores_, key=lambda x: x[1])
    print('score:', score)
    for param_name in sorted(best_parameters.keys()):
        print("%s: %r" % (param_name, best_parameters[param_name]))
    # print('best score:', clf.best_score_)
    # for param_name in sorted(clf.best_params_.keys()):
    #     print("%s: %r" % (param_name, clf.best_params_[param_name]))

    # # setup parameters for xgboost
    # param = {}
    # # use softmax multi-class classification
    # param['objective'] = 'multi:softmax'
    # # scale weight of positive examples
    # param['eta'] = 0.1
    # param['max_depth'] = 6
    # param['silent'] = 1
    # param['nthread'] = 4
    # param['num_class'] = 5
    #
    # watchlist = [ (xg_train,'train'), (xg_test, 'test') ]
    # num_round = 5
    # bst = xgb.train(param, xg_train, num_round, watchlist )
    # # get prediction
    # pred = bst.predict( xg_test )
    #
    def class_error(predicted, expected):
        return sum( int(predicted[i]) != expected[i] for i in range(len(expected))) / float(len(expected))

    def print_prediction_stats(data_set, cat_outputs, cat_labels):
        puts()
        puts("Results for %s" % (data_set,))
        puts("error: %f" % (class_error(cat_outputs, cat_labels)))
        puts("kappa: %f" % (skll.kappa(cat_outputs, cat_labels, 'quadratic')))
        puts("Confusion Matrix:")
        puts(str(utils.conf_matrix(cat_outputs, cat_labels, 5)))

    print_prediction_stats("train xgboost", clf.predict(train_X), train_Y)
    print_prediction_stats("train thresh", utils.predict_ordinal(train_X, thresh=0.5), train_Y)
    print_prediction_stats("valid xgboost", clf.predict(test_X), test_Y)
    print_prediction_stats("valid thresh", utils.predict_ordinal(test_X, thresh=0.5), test_Y)


    # TODO: use the best parameters, retrain on the whole training set and predict the actual test set.
    #clf.fit(np.vstack((train_X, test_X)), np.concatenate((train_Y, test_Y)))
    return 0

if __name__ == '__main__':
    sys.exit(main())
