import glob
import os
import sys

import numpy as np
import skimage.io
import skimage.transform
import skimage.util
import pandas as pd
from multiprocessing.dummy import Pool
import time

size = int(sys.argv[1])
output_shape = (size, size)

save_dir = "data-%dx%d" % (size, size)
os.system('mkdir -p %s' % (save_dir))

# do the training images
def load(path):
    print 'Loading upsampled per-image training labels from %s' % (path)
    return pd.read_csv(path)


train_labels_all_us = load('data/trainLabels_all_US.csv')
train_labels_train_us = load('data/trainLabels_train_US.csv')
train_labels_val = load('data/trainLabels_val.csv')

paths_train = glob.glob('data/train/*.jpeg')
num_workers = 3  # any more than 3 doesn't do any good because of the GIL
print "Loading %d train images using %d worker threads" % (len(paths_train), num_workers)
pool = Pool(processes=num_workers)  # really threads because imported multiprocessing.dummy.Pool


def load_image(k, file_path):
    name = os.path.basename(file_path)[:-5]
    print "Loading %s (%d/%d)" % (name, k, len(paths_train))
    full_image = skimage.io.imread(file_path, as_grey=False)
    # crop to square, the edges just contain mostly worthless black
    rows, cols, channels = full_image.shape
    if rows < cols:
        cropped_image = full_image[:, (cols-rows)/2:-(cols-rows)/2, :]
    elif rows > cols:
        print "UNEXPECTED!!! rows > cols"
        cropped_image = full_image[(rows-cols)/2:-(rows-cols)/2, :, :]
    else:
        print "UNEXPECTED!!! square image rows == cols"
        cropped_image = full_image

    resized_image = skimage.util.dtype.img_as_ubyte(skimage.transform.resize(cropped_image, output_shape=output_shape, order=3))
    del full_image
    del cropped_image
    return [name, resized_image]


t1 = time.time()

images = dict(pool.map(lambda (k, file_path): load_image(k, file_path),
                       enumerate(paths_train)))

print "Time to load %d images: %g" % (len(paths_train), time.time() - t1)


for train_labels, name in zip([train_labels_all_us, train_labels_train_us, train_labels_val],
                              ["all_us", "train_us", "val"]):
    labels = train_labels.iloc[:, 1].values

    # These don't change for resize, so stick them in the main dir
    print "Saving %d train labels to data/labels_%s.npy" % (len(labels), name)
    np.save("%s/labels_%s.npy" % (save_dir, name), labels)

    train_images = np.empty(len(train_labels), dtype='object')
    for k, image in enumerate(train_labels['image']):
        train_images[k] = images[image]

    save_path = "%s/images_%s.npy" % (save_dir, name)
    print "Saving %d train images to %s" % (len(train_images), save_path)
    np.save(save_path, train_images)
    del train_images

del images

# do the test images
paths_test = glob.glob("data/test/*.jpeg")
paths_test.sort()

print "Loading %d test images" % (len(paths_test))
images_test = np.empty(len(paths_test), dtype='object')

t1 = time.time()

images_list = pool.map(lambda (k, file_path): load_image(k, file_path)[1],
                       enumerate(paths_test))

for k in xrange(len(images_list)):
    images_test[k] = images_list[k]
    images_list[k] = None

del images_list

print "Time to load %d images: %g" % (len(paths_test), time.time() - t1)

print "Saving %d test images" % (len(images_test))
np.save("%s/images_test.npy" % (save_dir), images_test)

print "Done"
