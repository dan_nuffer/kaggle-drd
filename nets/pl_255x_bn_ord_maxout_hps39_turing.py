#job_id = 39
hyperparameters = \
{u'batch_size': 689,
 u'conv10_num_filters': 170,
 u'conv1_num_filters': 49,
 u'conv2_num_filters': 18,
 u'conv3_num_filters': 123,
 u'conv4_num_filters': 85,
 u'conv5_num_filters': 238,
 u'conv6_num_filters': 182,
 u'conv7_num_filters': 115,
 u'conv8_num_filters': 340,
 u'conv9_num_filters': 340,
 u'fc1_dropout_p': 0.6875,
 u'fc1_max_col_norm': 10.9828125,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3272,
 u'fc2_dropout_p': 0.8125,
 u'fc2_max_col_norm': 4.1421874999999995,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2792,
 u'fc3_dropout_p': 0.3375,
 u'fc3_max_col_norm': 7.873437499999999,
 u'learning_rate': 0.039123437500000004,
 u'learning_rate_jitter': 2.3125,
 u'train_sample_rate': 0.20468749999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
