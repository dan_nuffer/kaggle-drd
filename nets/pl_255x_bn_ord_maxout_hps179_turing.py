#job_id = 179
hyperparameters = \
{u'batch_size': 80,
 u'conv10_num_filters': 147,
 u'conv1_num_filters': 63,
 u'conv2_num_filters': 62,
 u'conv3_num_filters': 80,
 u'conv4_num_filters': 57,
 u'conv5_num_filters': 155,
 u'conv6_num_filters': 247,
 u'conv7_num_filters': 119,
 u'conv8_num_filters': 505,
 u'conv9_num_filters': 373,
 u'fc1_dropout_p': 0.546875,
 u'fc1_max_col_norm': 16.035546874999998,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2458,
 u'fc2_dropout_p': 0.28437500000000004,
 u'fc2_max_col_norm': 17.434765625,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2878,
 u'fc3_dropout_p': 0.596875,
 u'fc3_max_col_norm': 9.661328124999999,
 u'learning_rate': 0.037952734375000005,
 u'learning_rate_jitter': 4.734375,
 u'train_sample_rate': 0.43320312499999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
