#job_id = 93
hyperparameters = \
{u'batch_size': 40,
 u'conv10_num_filters': 317,
 u'conv1_num_filters': 35,
 u'conv2_num_filters': 63,
 u'conv3_num_filters': 76,
 u'conv4_num_filters': 128,
 u'conv5_num_filters': 235,
 u'conv6_num_filters': 227,
 u'conv7_num_filters': 204,
 u'conv8_num_filters': 290,
 u'conv9_num_filters': 342,
 u'fc1_dropout_p': 0.58125,
 u'fc1_max_col_norm': 12.07109375,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1044,
 u'fc2_dropout_p': 0.53125,
 u'fc2_max_col_norm': 2.43203125,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2332,
 u'fc3_dropout_p': 0.71875,
 u'fc3_max_col_norm': 16.42421875,
 u'learning_rate': 0.018050781249999998,
 u'learning_rate_jitter': 4.90625,
 u'train_sample_rate': 0.88671875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
