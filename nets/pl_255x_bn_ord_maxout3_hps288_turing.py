#job_id = 288
hyperparameters = \
{u'batch_size': 6,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 70,
 u'conv2_num_filters': 32,
 u'conv3_num_filters': 32,
 u'conv4_num_filters': 93,
 u'conv5_num_filters': 320,
 u'conv6_num_filters': 108,
 u'conv7_num_filters': 512,
 u'conv8_num_filters': 528,
 u'conv9_num_filters': 352,
 u'fc1_dropout_p': 0.893690529497367,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 6.9040593052554895,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.46607043760029476,
 u'fc3_max_col_norm': 14.308913735625367,
 u'learning_rate': 0.011470489735932047,
 u'learning_rate_jitter': 1.057968207163339,
 u'train_sample_rate': 0.5,
 u'training_time': 24.37448418598694}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
