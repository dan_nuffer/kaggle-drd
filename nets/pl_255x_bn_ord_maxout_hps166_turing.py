#job_id = 166
hyperparameters = \
{u'batch_size': 72,
 u'conv10_num_filters': 364,
 u'conv1_num_filters': 40,
 u'conv2_num_filters': 52,
 u'conv3_num_filters': 100,
 u'conv4_num_filters': 39,
 u'conv5_num_filters': 179,
 u'conv6_num_filters': 174,
 u'conv7_num_filters': 131,
 u'conv8_num_filters': 457,
 u'conv9_num_filters': 293,
 u'fc1_dropout_p': 0.396875,
 u'fc1_max_col_norm': 4.841796874999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2234,
 u'fc2_dropout_p': 0.534375,
 u'fc2_max_col_norm': 1.266015625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3998,
 u'fc3_dropout_p': 0.146875,
 u'fc3_max_col_norm': 13.392578124999998,
 u'learning_rate': 0.056683984375,
 u'learning_rate_jitter': 2.984375,
 u'train_sample_rate': 0.26445312499999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
