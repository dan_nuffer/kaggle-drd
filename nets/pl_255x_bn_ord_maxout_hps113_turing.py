#job_id = 113
hyperparameters = \
{u'batch_size': 46,
 u'conv10_num_filters': 191,
 u'conv1_num_filters': 46,
 u'conv2_num_filters': 61,
 u'conv3_num_filters': 93,
 u'conv4_num_filters': 84,
 u'conv5_num_filters': 169,
 u'conv6_num_filters': 241,
 u'conv7_num_filters': 213,
 u'conv8_num_filters': 446,
 u'conv9_num_filters': 394,
 u'fc1_dropout_p': 0.5437500000000001,
 u'fc1_max_col_norm': 9.894531249999998,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2332,
 u'fc2_dropout_p': 0.11875000000000001,
 u'fc2_max_col_norm': 18.91171875,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1044,
 u'fc3_dropout_p': 0.25625,
 u'fc3_max_col_norm': 11.138281249999999,
 u'learning_rate': 0.00712421875,
 u'learning_rate_jitter': 1.84375,
 u'train_sample_rate': 0.76015625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
