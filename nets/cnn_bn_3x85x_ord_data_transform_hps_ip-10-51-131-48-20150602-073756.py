hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 66,
                          'per_pixel_gaussian_noise_sigma': 0.12362851481685874,
                          'rotation_range': (0, 150.2262042838178),
                          'shear_range': (-21.55146407587426,
                                          21.55146407587426),
                          'translation_range': (-5.238485299137464,
                                                5.238485299137464),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 52,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-15.807075768779542,
                                           15.807075768779542),
                           'translation_range': (-7.548617593355617,
                                                 7.548617593355617),
                           'zoom_range': (0.6893000398386566,
                                          1.4507470509272977)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
