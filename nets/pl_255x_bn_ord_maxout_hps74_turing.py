#job_id = 74
hyperparameters = \
{u'batch_size': 92,
 u'conv10_num_filters': 497,
 u'conv1_num_filters': 55,
 u'conv2_num_filters': 47,
 u'conv3_num_filters': 86,
 u'conv4_num_filters': 58,
 u'conv5_num_filters': 231,
 u'conv6_num_filters': 231,
 u'conv7_num_filters': 210,
 u'conv8_num_filters': 426,
 u'conv9_num_filters': 286,
 u'fc1_dropout_p': 0.8562500000000001,
 u'fc1_max_col_norm': 13.936718749999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2724,
 u'fc2_dropout_p': 0.20625000000000002,
 u'fc2_max_col_norm': 0.56640625,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1100,
 u'fc3_dropout_p': 0.69375,
 u'fc3_max_col_norm': 3.36484375,
 u'learning_rate': 0.07112265625,
 u'learning_rate_jitter': 3.53125,
 u'train_sample_rate': 0.18359375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
