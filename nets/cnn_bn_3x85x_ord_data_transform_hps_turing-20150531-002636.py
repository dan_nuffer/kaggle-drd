hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.6848128441699854,
                          'color_shift_sigma': 0.10964168425896109,
                          'contrast_noise_sigma': 0.12099042492992831,
                          'do_flip': True,
                          'num_transforms': 76,
                          'per_pixel_gaussian_noise_sigma': 0.0,
                          'rotation_range': (0, 267.6335976731126),
                          'shear_range': (-0.0, 0.0),
                          'translation_range': (-3.1055057856405943,
                                                3.1055057856405943),
                          'zoom_range': (0.7161010912681236,
                                         1.3964508812982364)},
 'train_data_processing': {'allow_stretch': 1.6161942526128577,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 73,
                           'per_pixel_gaussian_noise_sigma': 0.04142352468525341,
                           'rotation_range': (0, 75.49289346173208),
                           'shear_range': (-0.0, 0.0),
                           'translation_range': (-1.9037120109370487,
                                                 1.9037120109370487),
                           'zoom_range': (1.0, 1.0)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
