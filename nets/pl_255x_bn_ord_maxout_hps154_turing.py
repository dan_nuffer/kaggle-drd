#job_id = 154
hyperparameters = \
{u'batch_size': 126,
 u'conv10_num_filters': 285,
 u'conv1_num_filters': 42,
 u'conv2_num_filters': 62,
 u'conv3_num_filters': 109,
 u'conv4_num_filters': 83,
 u'conv5_num_filters': 129,
 u'conv6_num_filters': 237,
 u'conv7_num_filters': 212,
 u'conv8_num_filters': 309,
 u'conv9_num_filters': 409,
 u'fc1_dropout_p': 0.634375,
 u'fc1_max_col_norm': 14.480859374999998,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3186,
 u'fc2_dropout_p': 0.22187500000000002,
 u'fc2_max_col_norm': 15.258203124999998,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3158,
 u'fc3_dropout_p': 0.584375,
 u'fc3_max_col_norm': 1.887890625,
 u'learning_rate': 0.08009804687500001,
 u'learning_rate_jitter': 1.296875,
 u'train_sample_rate': 0.9253906249999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
