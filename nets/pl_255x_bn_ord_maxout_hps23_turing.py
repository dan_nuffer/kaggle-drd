#job_id = 23
hyperparameters = \
{u'batch_size': 737,
 u'conv10_num_filters': 236,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 20,
 u'conv3_num_filters': 106,
 u'conv4_num_filters': 125,
 u'conv5_num_filters': 172,
 u'conv6_num_filters': 164,
 u'conv7_num_filters': 106,
 u'conv8_num_filters': 456,
 u'conv9_num_filters': 392,
 u'fc1_dropout_p': 0.625,
 u'fc1_max_col_norm': 8.184375,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 7952,
 u'fc2_dropout_p': 0.42500000000000004,
 u'fc2_max_col_norm': 16.890625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 7472,
 u'fc3_dropout_p': 0.675,
 u'fc3_max_col_norm': 4.453124999999999,
 u'learning_rate': 0.034440625,
 u'learning_rate_jitter': 3.375,
 u'train_sample_rate': 0.078125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
