#job_id = 20
hyperparameters = \
{u'batch_size': 353,
 u'conv10_num_filters': 284,
 u'conv1_num_filters': 33,
 u'conv2_num_filters': 51,
 u'conv3_num_filters': 82,
 u'conv4_num_filters': 89,
 u'conv5_num_filters': 156,
 u'conv6_num_filters': 180,
 u'conv7_num_filters': 130,
 u'conv8_num_filters': 296,
 u'conv9_num_filters': 360,
 u'fc1_dropout_p': 0.725,
 u'fc1_max_col_norm': 10.671874999999998,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1232,
 u'fc2_dropout_p': 0.725,
 u'fc2_max_col_norm': 9.428125,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 4592,
 u'fc3_dropout_p': 0.375,
 u'fc3_max_col_norm': 6.940624999999999,
 u'learning_rate': 0.071903125,
 u'learning_rate_jitter': 3.875,
 u'train_sample_rate': 0.41562499999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
