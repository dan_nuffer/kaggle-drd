#job_id = 118
hyperparameters = \
{u'batch_size': 74,
 u'conv10_num_filters': 251,
 u'conv1_num_filters': 41,
 u'conv2_num_filters': 60,
 u'conv3_num_filters': 91,
 u'conv4_num_filters': 105,
 u'conv5_num_filters': 221,
 u'conv6_num_filters': 221,
 u'conv7_num_filters': 74,
 u'conv8_num_filters': 294,
 u'conv9_num_filters': 306,
 u'fc1_dropout_p': 0.16875,
 u'fc1_max_col_norm': 10.51640625,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3788,
 u'fc2_dropout_p': 0.89375,
 u'fc2_max_col_norm': 17.04609375,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2276,
 u'fc3_dropout_p': 0.48125000000000007,
 u'fc3_max_col_norm': 10.51640625,
 u'learning_rate': 0.08517109375000001,
 u'learning_rate_jitter': 3.46875,
 u'train_sample_rate': 0.45078124999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
