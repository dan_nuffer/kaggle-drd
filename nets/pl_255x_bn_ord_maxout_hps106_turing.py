#job_id = 106
hyperparameters = \
{u'batch_size': 94,
 u'conv10_num_filters': 143,
 u'conv1_num_filters': 50,
 u'conv2_num_filters': 30,
 u'conv3_num_filters': 102,
 u'conv4_num_filters': 120,
 u'conv5_num_filters': 153,
 u'conv6_num_filters': 225,
 u'conv7_num_filters': 189,
 u'conv8_num_filters': 350,
 u'conv9_num_filters': 362,
 u'fc1_dropout_p': 0.84375,
 u'fc1_max_col_norm': 12.382031249999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1884,
 u'fc2_dropout_p': 0.61875,
 u'fc2_max_col_norm': 6.4742187499999995,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1492,
 u'fc3_dropout_p': 0.75625,
 u'fc3_max_col_norm': 18.60078125,
 u'learning_rate': 0.09453671875000001,
 u'learning_rate_jitter': 1.34375,
 u'train_sample_rate': 0.6476562499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
