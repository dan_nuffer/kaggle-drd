#job_id = 164
hyperparameters = \
{u'batch_size': 7,
 u'conv10_num_filters': 454,
 u'conv1_num_filters': 35,
 u'conv2_num_filters': 35,
 u'conv3_num_filters': 97,
 u'conv4_num_filters': 101,
 u'conv5_num_filters': 153,
 u'conv6_num_filters': 164,
 u'conv7_num_filters': 200,
 u'conv8_num_filters': 485,
 u'conv9_num_filters': 297,
 u'fc1_dropout_p': 0.184375,
 u'fc1_max_col_norm': 13.237109375,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2962,
 u'fc2_dropout_p': 0.271875,
 u'fc2_max_col_norm': 14.014453125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2486,
 u'fc3_dropout_p': 0.834375,
 u'fc3_max_col_norm': 13.081640624999999,
 u'learning_rate': 0.086341796875,
 u'learning_rate_jitter': 3.546875,
 u'train_sample_rate': 0.756640625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
