#job_id = 218
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 166,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 70,
 u'conv4_num_filters': 85,
 u'conv5_num_filters': 245,
 u'conv6_num_filters': 243,
 u'conv7_num_filters': 236,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 418,
 u'fc1_dropout_p': 0.8767809844937724,
 u'fc1_max_col_norm': 15.88991950173523,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4018,
 u'fc2_dropout_p': 0.11578381391081402,
 u'fc2_max_col_norm': 11.311032896109726,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2245,
 u'fc3_dropout_p': 0.5368593594308418,
 u'fc3_max_col_norm': 3.429517885630238,
 u'learning_rate': 0.00447193492217261,
 u'learning_rate_jitter': 4.58242473226728,
 u'train_sample_rate': 0.7163372860673841}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
