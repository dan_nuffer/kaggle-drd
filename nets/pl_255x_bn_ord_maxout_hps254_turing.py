#job_id = 254
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 161,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 31,
 u'conv3_num_filters': 53,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 253,
 u'conv6_num_filters': 380,
 u'conv7_num_filters': 306,
 u'conv8_num_filters': 253,
 u'conv9_num_filters': 425,
 u'fc1_dropout_p': 0.8902321532430916,
 u'fc1_max_col_norm': 18.278255852189538,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4821,
 u'fc2_dropout_p': 0.1673973413082791,
 u'fc2_max_col_norm': 2.4233185059545477,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1743,
 u'fc3_dropout_p': 0.8973409039941512,
 u'fc3_max_col_norm': 2.1545941208248074,
 u'learning_rate': 0.05614738610293805,
 u'learning_rate_jitter': 6.0,
 u'train_sample_rate': 0.020596380640959977}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
