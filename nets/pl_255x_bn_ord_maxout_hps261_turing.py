#job_id = 261
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 97,
 u'conv1_num_filters': 34,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 61,
 u'conv4_num_filters': 120,
 u'conv5_num_filters': 243,
 u'conv6_num_filters': 394,
 u'conv7_num_filters': 359,
 u'conv8_num_filters': 418,
 u'conv9_num_filters': 473,
 u'fc1_dropout_p': 0.7208465648291817,
 u'fc1_max_col_norm': 18.600483077738392,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 256,
 u'fc2_dropout_p': 0.23248382735810186,
 u'fc2_max_col_norm': 25.0,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 256,
 u'fc3_dropout_p': 0.6102842756207066,
 u'fc3_max_col_norm': 20.0,
 u'learning_rate': 0.09147976941960612,
 u'learning_rate_jitter': 6.0,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
