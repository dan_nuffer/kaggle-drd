#job_id = 190
hyperparameters = \
{u'batch_size': 84,
 u'conv10_num_filters': 424,
 u'conv1_num_filters': 51,
 u'conv2_num_filters': 36,
 u'conv3_num_filters': 90,
 u'conv4_num_filters': 54,
 u'conv5_num_filters': 248,
 u'conv6_num_filters': 146,
 u'conv7_num_filters': 245,
 u'conv8_num_filters': 433,
 u'conv9_num_filters': 381,
 u'fc1_dropout_p': 0.621875,
 u'fc1_max_col_norm': 2.976171875,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3914,
 u'fc2_dropout_p': 0.759375,
 u'fc2_max_col_norm': 10.594140625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3214,
 u'fc3_dropout_p': 0.621875,
 u'fc3_max_col_norm': 15.258203124999998,
 u'learning_rate': 0.072293359375,
 u'learning_rate_jitter': 4.859375,
 u'train_sample_rate': 0.8550781249999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
