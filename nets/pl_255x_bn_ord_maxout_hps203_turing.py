#job_id = 203
hyperparameters = \
{u'batch_size': 100,
 u'conv10_num_filters': 279,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 29,
 u'conv3_num_filters': 66,
 u'conv4_num_filters': 114,
 u'conv5_num_filters': 135,
 u'conv6_num_filters': 195,
 u'conv7_num_filters': 76,
 u'conv8_num_filters': 465,
 u'conv9_num_filters': 285,
 u'fc1_dropout_p': 0.321875,
 u'fc1_max_col_norm': 5.463671874999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2570,
 u'fc2_dropout_p': 0.859375,
 u'fc2_max_col_norm': 13.081640624999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 974,
 u'fc3_dropout_p': 0.721875,
 u'fc3_max_col_norm': 2.820703125,
 u'learning_rate': 0.084780859375,
 u'learning_rate_jitter': 2.359375,
 u'train_sample_rate': 0.067578125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
