#job_id = 298
hyperparameters = \
{u'batch_size': 2,
 u'conv10_num_filters': 120,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 72,
 u'conv4_num_filters': 104,
 u'conv5_num_filters': 253,
 u'conv6_num_filters': 253,
 u'conv7_num_filters': 304,
 u'conv8_num_filters': 278,
 u'conv9_num_filters': 381,
 u'fc1_dropout_p': 0.8707330404126753,
 u'fc1_max_col_norm': 10.047640836249238,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4149,
 u'fc2_dropout_p': 0.19180690446671828,
 u'fc2_max_col_norm': 3.716121063584133,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 4147,
 u'fc3_dropout_p': 0.26550810429831884,
 u'fc3_max_col_norm': 14.194256934638855,
 u'learning_rate': 0.0002577387865701163,
 u'learning_rate_jitter': 5.080626590907349,
 u'train_sample_rate': 0.95,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
