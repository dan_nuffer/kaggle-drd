#job_id = 87
hyperparameters = \
{u'batch_size': 96,
 u'conv10_num_filters': 437,
 u'conv1_num_filters': 42,
 u'conv2_num_filters': 36,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 110,
 u'conv5_num_filters': 243,
 u'conv6_num_filters': 171,
 u'conv7_num_filters': 216,
 u'conv8_num_filters': 498,
 u'conv9_num_filters': 486,
 u'fc1_dropout_p': 0.23125,
 u'fc1_max_col_norm': 10.827343749999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1268,
 u'fc2_dropout_p': 0.58125,
 u'fc2_max_col_norm': 8.65078125,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3004,
 u'fc3_dropout_p': 0.66875,
 u'fc3_max_col_norm': 7.717968749999999,
 u'learning_rate': 0.02429453125,
 u'learning_rate_jitter': 2.15625,
 u'train_sample_rate': 0.83046875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
