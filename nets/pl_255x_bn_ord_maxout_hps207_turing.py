#job_id = 207
hyperparameters = \
{u'batch_size': 67,
 u'conv10_num_filters': 475,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 34,
 u'conv3_num_filters': 126,
 u'conv4_num_filters': 83,
 u'conv5_num_filters': 146,
 u'conv6_num_filters': 167,
 u'conv7_num_filters': 220,
 u'conv8_num_filters': 263,
 u'conv9_num_filters': 319,
 u'fc1_dropout_p': 0.703125,
 u'fc1_max_col_norm': 18.989453125,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1534,
 u'fc2_dropout_p': 0.890625,
 u'fc2_max_col_norm': 18.523046875,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3410,
 u'fc3_dropout_p': 0.44062500000000004,
 u'fc3_max_col_norm': 4.219921874999999,
 u'learning_rate': 0.06995195312500001,
 u'learning_rate_jitter': 1.390625,
 u'train_sample_rate': 0.41210937499999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
