#job_id = 182
hyperparameters = \
{u'batch_size': 76,
 u'conv10_num_filters': 207,
 u'conv1_num_filters': 33,
 u'conv2_num_filters': 26,
 u'conv3_num_filters': 70,
 u'conv4_num_filters': 108,
 u'conv5_num_filters': 143,
 u'conv6_num_filters': 186,
 u'conv7_num_filters': 113,
 u'conv8_num_filters': 417,
 u'conv9_num_filters': 397,
 u'fc1_dropout_p': 0.37187500000000007,
 u'fc1_max_col_norm': 19.144921875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1002,
 u'fc2_dropout_p': 0.509375,
 u'fc2_max_col_norm': 14.325390624999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1198,
 u'fc3_dropout_p': 0.571875,
 u'fc3_max_col_norm': 4.064453125,
 u'learning_rate': 0.09102460937500001,
 u'learning_rate_jitter': 1.109375,
 u'train_sample_rate': 0.573828125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
