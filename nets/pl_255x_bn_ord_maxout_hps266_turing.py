#job_id = 266
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 33,
 u'conv4_num_filters': 62,
 u'conv5_num_filters': 291,
 u'conv6_num_filters': 128,
 u'conv7_num_filters': 405,
 u'conv8_num_filters': 161,
 u'conv9_num_filters': 632,
 u'fc1_dropout_p': 0.8987433346078687,
 u'fc1_max_col_norm': 23.496355783098586,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 256,
 u'fc2_dropout_p': 0.05366470388373359,
 u'fc2_max_col_norm': 23.60410554413612,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 2519,
 u'fc3_dropout_p': 0.3075405414794402,
 u'fc3_max_col_norm': 20.0,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.949104595099687,
 u'train_sample_rate': 0.5}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
