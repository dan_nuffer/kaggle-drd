#job_id = 102
hyperparameters = \
{u'batch_size': 118,
 u'conv10_num_filters': 263,
 u'conv1_num_filters': 44,
 u'conv2_num_filters': 27,
 u'conv3_num_filters': 98,
 u'conv4_num_filters': 126,
 u'conv5_num_filters': 145,
 u'conv6_num_filters': 153,
 u'conv7_num_filters': 249,
 u'conv8_num_filters': 302,
 u'conv9_num_filters': 506,
 u'fc1_dropout_p': 0.89375,
 u'fc1_max_col_norm': 3.67578125,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3452,
 u'fc2_dropout_p': 0.76875,
 u'fc2_max_col_norm': 5.230468749999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1716,
 u'fc3_dropout_p': 0.50625,
 u'fc3_max_col_norm': 19.84453125,
 u'learning_rate': 0.07580546875000001,
 u'learning_rate_jitter': 2.09375,
 u'train_sample_rate': 0.25390625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
