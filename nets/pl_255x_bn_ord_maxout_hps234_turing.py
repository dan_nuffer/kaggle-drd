#job_id = 234
hyperparameters = \
{u'batch_size': 8,
 u'conv10_num_filters': 351,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 78,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 133,
 u'conv6_num_filters': 170,
 u'conv7_num_filters': 256,
 u'conv8_num_filters': 512,
 u'conv9_num_filters': 464,
 u'fc1_dropout_p': 0.8018020819710798,
 u'fc1_max_col_norm': 17.074075525872747,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4096,
 u'fc2_dropout_p': 0.1,
 u'fc2_max_col_norm': 7.841874352500031,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 4096,
 u'fc3_dropout_p': 0.5884178231305969,
 u'fc3_max_col_norm': 3.117520606310681,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 4.086457535292192,
 u'train_sample_rate': 0.95}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
