#job_id = 65
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 329,
 u'conv1_num_filters': 53,
 u'conv2_num_filters': 37,
 u'conv3_num_filters': 107,
 u'conv4_num_filters': 52,
 u'conv5_num_filters': 223,
 u'conv6_num_filters': 143,
 u'conv7_num_filters': 246,
 u'conv8_num_filters': 314,
 u'conv9_num_filters': 366,
 u'fc1_dropout_p': 0.50625,
 u'fc1_max_col_norm': 17.66796875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3396,
 u'fc2_dropout_p': 0.8562500000000001,
 u'fc2_max_col_norm': 14.247656249999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1772,
 u'fc3_dropout_p': 0.14375000000000002,
 u'fc3_max_col_norm': 7.096093749999999,
 u'learning_rate': 0.03990390625,
 u'learning_rate_jitter': 4.28125,
 u'train_sample_rate': 0.80234375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
