#job_id = 132
hyperparameters = \
{u'batch_size': 2,
 u'conv10_num_filters': 227,
 u'conv1_num_filters': 47,
 u'conv2_num_filters': 32,
 u'conv3_num_filters': 79,
 u'conv4_num_filters': 38,
 u'conv5_num_filters': 181,
 u'conv6_num_filters': 197,
 u'conv7_num_filters': 183,
 u'conv8_num_filters': 406,
 u'conv9_num_filters': 482,
 u'fc1_dropout_p': 0.31875,
 u'fc1_max_col_norm': 19.22265625,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2668,
 u'fc2_dropout_p': 0.74375,
 u'fc2_max_col_norm': 10.827343749999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2948,
 u'fc3_dropout_p': 0.43125,
 u'fc3_max_col_norm': 16.73515625,
 u'learning_rate': 0.06643984375,
 u'learning_rate_jitter': 3.21875,
 u'train_sample_rate': 0.61953125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
