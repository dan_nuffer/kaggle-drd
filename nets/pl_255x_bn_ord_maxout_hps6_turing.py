#job_id = 6
hyperparameters = \
{u'batch_size': 897,
 u'conv10_num_filters': 176,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 46,
 u'conv3_num_filters': 120,
 u'conv4_num_filters': 68,
 u'conv5_num_filters': 176,
 u'conv6_num_filters': 144,
 u'conv7_num_filters': 136,
 u'conv8_num_filters': 480,
 u'conv9_num_filters': 480,
 u'fc1_dropout_p': 0.4,
 u'fc1_max_col_norm': 17.5125,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 7232,
 u'fc2_dropout_p': 0.6,
 u'fc2_max_col_norm': 12.5375,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3392,
 u'fc3_dropout_p': 0.8,
 u'fc3_max_col_norm': 7.562499999999999,
 u'learning_rate': 0.08751250000000001,
 u'learning_rate_jitter': 1.5,
 u'train_sample_rate': 0.38749999999999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
