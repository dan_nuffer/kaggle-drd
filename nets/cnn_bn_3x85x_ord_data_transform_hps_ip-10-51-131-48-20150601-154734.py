hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.07873314738886714,
                          'contrast_noise_sigma': 0.021760908425324292,
                          'do_flip': True,
                          'num_transforms': 55,
                          'per_pixel_gaussian_noise_sigma': 0.17100671294182868,
                          'rotation_range': (0, 210.08425235718232),
                          'shear_range': (-34.240727877116434,
                                          34.240727877116434),
                          'translation_range': (-12.226922578481922,
                                                12.226922578481922),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.6877848675995313,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.14132696974307538,
                           'do_flip': True,
                           'num_transforms': 19,
                           'per_pixel_gaussian_noise_sigma': 0.15554860831047887,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-13.379490777493913,
                                           13.379490777493913),
                           'translation_range': (-14.146112349920056,
                                                 14.146112349920056),
                           'zoom_range': (0.6821728703034958,
                                          1.4659040890252704)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
