#job_id = 295
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 32,
 u'conv1_num_filters': 76,
 u'conv2_num_filters': 23,
 u'conv3_num_filters': 32,
 u'conv4_num_filters': 39,
 u'conv5_num_filters': 201,
 u'conv6_num_filters': 64,
 u'conv7_num_filters': 245,
 u'conv8_num_filters': 495,
 u'conv9_num_filters': 266,
 u'fc1_dropout_p': 0.8379402854706679,
 u'fc1_max_col_norm': 29.362070828736556,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 32,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 8.472661151845447,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.4140992070431717,
 u'fc3_max_col_norm': 12.119317617103754,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 1.5510254780762365,
 u'train_sample_rate': 0.25,
 u'training_time': 24.098183897846862}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
