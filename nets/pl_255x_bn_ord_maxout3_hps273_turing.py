#job_id = 273
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 40,
 u'conv2_num_filters': 32,
 u'conv3_num_filters': 32,
 u'conv4_num_filters': 94,
 u'conv5_num_filters': 219,
 u'conv6_num_filters': 394,
 u'conv7_num_filters': 292,
 u'conv8_num_filters': 305,
 u'conv9_num_filters': 702,
 u'fc1_dropout_p': 0.7754113139508575,
 u'fc1_max_col_norm': 29.196392300842604,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 430,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 23.246476463244164,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 4745,
 u'fc3_dropout_p': 0.4161813187699055,
 u'fc3_max_col_norm': 22.097143836697573,
 u'learning_rate': 0.014217969288110368,
 u'learning_rate_jitter': 5.031418787846826,
 u'train_sample_rate': 0.5,
 u'training_time': 36.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
