hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.07591656553347509,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 245.93617818361778),
                          'shear_range': (-31.62966048758695,
                                          31.62966048758695),
                          'translation_range': (-12.837460879929695,
                                                12.837460879929695),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.011107107382031658,
              'momentum': 0.8504823388877485},
 'regularization': {'dropout_p1': 0.15478996793751068,
                    'dropout_p2': 0.506498169190844,
                    'dropout_p3': 0.7609594678584526,
                    'l1': 0.0,
                    'l2': 0.0,
                    'max_col_norm1': 7.86994341295962,
                    'max_col_norm2': 4.803856614026963,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-11.682854351476736,
                                           11.682854351476736),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.5988862928175833,
                                          1.66976605073944)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
