hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.032731482198234296,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 220.28305540376934),
                          'shear_range': (-30.12307591295219,
                                          30.12307591295219),
                          'translation_range': (-14.97483155636623,
                                                14.97483155636623),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 2.0978286086916006e-05,
              'momentum': 0.6474215944832014},
 'regularization': {'dropout_p1': 0.7839488788166192,
                    'dropout_p2': 0.8058553645875389,
                    'dropout_p3': 0.33598297029518037,
                    'l1': 0.0005467062298185338,
                    'l2': 2.4882463205637687e-06,
                    'max_col_norm1': 4.991728456576365,
                    'max_col_norm2': 0.0,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.17041336737481752,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-24.95268255263869,
                                           24.95268255263869),
                           'translation_range': (-19.955515247331167,
                                                 19.955515247331167),
                           'zoom_range': (0.6866797736054909,
                                          1.4562828824116738)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
