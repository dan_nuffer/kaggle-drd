#job_id = 71
hyperparameters = \
{u'batch_size': 84,
 u'conv10_num_filters': 281,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 43,
 u'conv3_num_filters': 99,
 u'conv4_num_filters': 40,
 u'conv5_num_filters': 207,
 u'conv6_num_filters': 191,
 u'conv7_num_filters': 222,
 u'conv8_num_filters': 410,
 u'conv9_num_filters': 334,
 u'fc1_dropout_p': 0.20625000000000002,
 u'fc1_max_col_norm': 5.230468749999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2052,
 u'fc2_dropout_p': 0.55625,
 u'fc2_max_col_norm': 16.73515625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2220,
 u'fc3_dropout_p': 0.24375000000000002,
 u'fc3_max_col_norm': 19.53359375,
 u'learning_rate': 0.02741640625,
 u'learning_rate_jitter': 1.78125,
 u'train_sample_rate': 0.46484374999999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
