#job_id = 14
hyperparameters = \
{u'batch_size': 833,
 u'conv10_num_filters': 440,
 u'conv1_num_filters': 46,
 u'conv2_num_filters': 61,
 u'conv3_num_filters': 100,
 u'conv4_num_filters': 86,
 u'conv5_num_filters': 200,
 u'conv6_num_filters': 184,
 u'conv7_num_filters': 220,
 u'conv8_num_filters': 496,
 u'conv9_num_filters': 272,
 u'fc1_dropout_p': 0.55,
 u'fc1_max_col_norm': 3.83125,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1952,
 u'fc2_dropout_p': 0.85,
 u'fc2_max_col_norm': 11.29375,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 4832,
 u'fc3_dropout_p': 0.75,
 u'fc3_max_col_norm': 11.29375,
 u'learning_rate': 0.06878125,
 u'learning_rate_jitter': 4.25,
 u'train_sample_rate': 0.10625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
