#job_id = 177
hyperparameters = \
{u'batch_size': 49,
 u'conv10_num_filters': 243,
 u'conv1_num_filters': 38,
 u'conv2_num_filters': 25,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 81,
 u'conv5_num_filters': 252,
 u'conv6_num_filters': 150,
 u'conv7_num_filters': 167,
 u'conv8_num_filters': 313,
 u'conv9_num_filters': 309,
 u'fc1_dropout_p': 0.7468750000000001,
 u'fc1_max_col_norm': 1.110546875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1562,
 u'fc2_dropout_p': 0.484375,
 u'fc2_max_col_norm': 12.459765625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 1982,
 u'fc3_dropout_p': 0.796875,
 u'fc3_max_col_norm': 4.686328124999999,
 u'learning_rate': 0.012977734375,
 u'learning_rate_jitter': 3.734375,
 u'train_sample_rate': 0.208203125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
