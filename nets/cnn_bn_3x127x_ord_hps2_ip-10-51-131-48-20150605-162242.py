hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.2435868561135324,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 110.05801587833551),
                          'shear_range': (-25.32864834619878,
                                          25.32864834619878),
                          'translation_range': (-4.837765962099435,
                                                4.837765962099435),
                          'zoom_range': (0.8019472457995535,
                                         1.2469648162492097)},
 'learning': {'learning_rate': 0.03725343683108454,
              'momentum': 0.7309051026887771},
 'regularization': {'dropout_p1': 0.19901174241651964,
                    'dropout_p2': 0.5996748927649366,
                    'dropout_p3': 0.49990636084174883,
                    'l1': 0.0,
                    'l2': 9.840577943225882e-05,
                    'max_col_norm1': 3.9555254786502467,
                    'max_col_norm2': 0.0,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-15.742164845874097,
                                           15.742164845874097),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.5549856829025355,
                                          1.80184828331079)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
