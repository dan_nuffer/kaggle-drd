from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import BatchNormalizationLayer, batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import dihedral
import tmp_dnn
import tta
from utils import polynomial_decay

hyperparameters = None
data_dir = None
input_side_len = None
patch_size = None
augmentation_params = None
batch_size = None
chunk_size = None
momentum = None
learning_rate = None
learning_rate_decay = None
learning_rate_patience = None
learning_rate_rolling_mean_window = None
learning_rate_improvement = None
num_chunks_train = None
divergence_threshold = None
validate_every = None
save_every = None
augmentation_transforms_test = None
data_loader = None

def set_hyperparameters(hyperparameters_arg):
    global hyperparameters
    hyperparameters = hyperparameters_arg
    global data_dir
    data_dir = "data-128x128"
    global input_side_len
    input_side_len = hyperparameters['data_params']['input_side_len']
    global patch_size
    patch_size = (input_side_len, input_side_len, 3)
    global augmentation_params
    augmentation_params = hyperparameters['train_data_processing']
    global batch_size
    batch_size = hyperparameters['data_params']['batch_size']
    global chunk_size
    chunk_size = hyperparameters['data_params']['chunk_size']
    global momentum
    momentum = 0.9
    global learning_rate
    learning_rate = 0.2
    global learning_rate_decay
    learning_rate_decay = 0.90
    global learning_rate_patience
    learning_rate_patience = 20
    global learning_rate_rolling_mean_window
    learning_rate_rolling_mean_window = 30
    global learning_rate_improvement
    learning_rate_improvement = 0.999
    global num_chunks_train
    num_chunks_train = hyperparameters['data_params']['num_chunks_train']
    global divergence_threshold
    divergence_threshold = 26.0
    global validate_every
    validate_every = 30
    global save_every
    save_every = 1
    global augmentation_transforms_test
    augmentation_transforms_test = tta.build_quasirandom_transforms(
        num_transforms = hyperparameters['eval_data_processing']['num_transforms'],
        zoom_range = hyperparameters['eval_data_processing']['zoom_range'],
        rotation_range = hyperparameters['eval_data_processing']['rotation_range'],
        shear_range = hyperparameters['eval_data_processing']['shear_range'],
        translation_range = hyperparameters['eval_data_processing']['translation_range'],
        do_flip=hyperparameters['eval_data_processing']['do_flip'],
        allow_stretch=hyperparameters['eval_data_processing']['allow_stretch'])
    global data_loader

    def estimate_scale(img):
        return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]

    data_loader = load.ZmuvRescaledDataLoader(estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
        patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
        augmentation_transforms_test=augmentation_transforms_test,
        predict_mode="ordinal_regression")


def build_model():
    Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
    MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0g = nn.layers.GaussianNoiseLayer(l0, sigma=hyperparameters['train_data_processing']['per_pixel_gaussian_noise_sigma'])
    l0c = nn_plankton.ContrastNoiseLayer(l0g, sigma=hyperparameters['train_data_processing']['contrast_noise_sigma'],
                                         test_sigma=hyperparameters['eval_data_processing']['contrast_noise_sigma'])
    l0cs = nn_plankton.ColorShiftNoiseLayer(l0c, sigma=hyperparameters['train_data_processing']['color_shift_sigma'],
                                            test_sigma=hyperparameters['eval_data_processing']['color_shift_sigma'])

    l1a = batch_norm(Conv2DLayer(l0cs, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=0.5), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0)))

    return [l0], l7


def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy)
