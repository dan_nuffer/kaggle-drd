#job_id = 193
hyperparameters = \
{u'batch_size': 37,
 u'conv10_num_filters': 472,
 u'conv1_num_filters': 47,
 u'conv2_num_filters': 54,
 u'conv3_num_filters': 98,
 u'conv4_num_filters': 66,
 u'conv5_num_filters': 200,
 u'conv6_num_filters': 130,
 u'conv7_num_filters': 173,
 u'conv8_num_filters': 337,
 u'conv9_num_filters': 413,
 u'fc1_dropout_p': 0.721875,
 u'fc1_max_col_norm': 15.413671874999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 778,
 u'fc2_dropout_p': 0.459375,
 u'fc2_max_col_norm': 3.1316406249999997,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2766,
 u'fc3_dropout_p': 0.321875,
 u'fc3_max_col_norm': 12.770703124999999,
 u'learning_rate': 0.034830859375,
 u'learning_rate_jitter': 4.359375,
 u'train_sample_rate': 0.517578125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
