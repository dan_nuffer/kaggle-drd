#job_id = 107
hyperparameters = \
{u'batch_size': 126,
 u'conv10_num_filters': 431,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 18,
 u'conv3_num_filters': 85,
 u'conv4_num_filters': 96,
 u'conv5_num_filters': 185,
 u'conv6_num_filters': 193,
 u'conv7_num_filters': 237,
 u'conv8_num_filters': 286,
 u'conv9_num_filters': 426,
 u'fc1_dropout_p': 0.24375000000000002,
 u'fc1_max_col_norm': 17.35703125,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 988,
 u'fc2_dropout_p': 0.41875000000000007,
 u'fc2_max_col_norm': 11.449218749999998,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 596,
 u'fc3_dropout_p': 0.15625,
 u'fc3_max_col_norm': 3.67578125,
 u'learning_rate': 0.01961171875,
 u'learning_rate_jitter': 4.34375,
 u'train_sample_rate': 0.42265624999999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
