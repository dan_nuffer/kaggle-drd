#job_id = 167
hyperparameters = \
{u'batch_size': 104,
 u'conv10_num_filters': 267,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 40,
 u'conv3_num_filters': 84,
 u'conv4_num_filters': 63,
 u'conv5_num_filters': 147,
 u'conv6_num_filters': 142,
 u'conv7_num_filters': 82,
 u'conv8_num_filters': 393,
 u'conv9_num_filters': 485,
 u'fc1_dropout_p': 0.596875,
 u'fc1_max_col_norm': 9.816796875,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1338,
 u'fc2_dropout_p': 0.334375,
 u'fc2_max_col_norm': 16.191015625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3102,
 u'fc3_dropout_p': 0.7468750000000001,
 u'fc3_max_col_norm': 8.417578124999999,
 u'learning_rate': 0.031708984375000006,
 u'learning_rate_jitter': 3.984375,
 u'train_sample_rate': 0.939453125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
