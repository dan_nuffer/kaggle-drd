#job_id = 19
hyperparameters = \
{u'batch_size': 865,
 u'conv10_num_filters': 476,
 u'conv1_num_filters': 49,
 u'conv2_num_filters': 26,
 u'conv3_num_filters': 114,
 u'conv4_num_filters': 41,
 u'conv5_num_filters': 220,
 u'conv6_num_filters': 244,
 u'conv7_num_filters': 226,
 u'conv8_num_filters': 424,
 u'conv9_num_filters': 488,
 u'fc1_dropout_p': 0.325,
 u'fc1_max_col_norm': 0.7218749999999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 5072,
 u'fc2_dropout_p': 0.325,
 u'fc2_max_col_norm': 19.378125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 752,
 u'fc3_dropout_p': 0.775,
 u'fc3_max_col_norm': 16.890625,
 u'learning_rate': 0.021953125,
 u'learning_rate_jitter': 1.875,
 u'train_sample_rate': 0.865625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
