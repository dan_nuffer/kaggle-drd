#job_id = 274
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 16,
 u'conv2_num_filters': 30,
 u'conv3_num_filters': 33,
 u'conv4_num_filters': 34,
 u'conv5_num_filters': 218,
 u'conv6_num_filters': 371,
 u'conv7_num_filters': 302,
 u'conv8_num_filters': 128,
 u'conv9_num_filters': 572,
 u'fc1_dropout_p': 0.8373627981293015,
 u'fc1_max_col_norm': 22.55555196448424,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 310,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 24.394243883362037,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 5040,
 u'fc3_dropout_p': 0.5125337920164821,
 u'fc3_max_col_norm': 12.189480517294948,
 u'learning_rate': 0.058713902782102166,
 u'learning_rate_jitter': 5.229273058921079,
 u'train_sample_rate': 0.5003628693848646,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
