#job_id = 235
hyperparameters = \
{u'batch_size': 3,
 u'conv10_num_filters': 395,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 40,
 u'conv5_num_filters': 255,
 u'conv6_num_filters': 255,
 u'conv7_num_filters': 256,
 u'conv8_num_filters': 303,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 3.202012691851241,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4083,
 u'fc2_dropout_p': 0.20999498899097377,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 4096,
 u'fc3_dropout_p': 0.5181568408421081,
 u'fc3_max_col_norm': 10.58559894122592,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 4.969384093487006,
 u'train_sample_rate': 0.95}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
