#job_id = 143
hyperparameters = \
{u'batch_size': 122,
 u'conv10_num_filters': 249,
 u'conv1_num_filters': 55,
 u'conv2_num_filters': 27,
 u'conv3_num_filters': 124,
 u'conv4_num_filters': 37,
 u'conv5_num_filters': 149,
 u'conv6_num_filters': 160,
 u'conv7_num_filters': 218,
 u'conv8_num_filters': 365,
 u'conv9_num_filters': 353,
 u'fc1_dropout_p': 0.459375,
 u'fc1_max_col_norm': 10.127734375,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 834,
 u'fc2_dropout_p': 0.596875,
 u'fc2_max_col_norm': 13.392578124999998,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1030,
 u'fc3_dropout_p': 0.5593750000000001,
 u'fc3_max_col_norm': 7.484765624999999,
 u'learning_rate': 0.027026171875,
 u'learning_rate_jitter': 4.921875,
 u'train_sample_rate': 0.053515625000000004}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
