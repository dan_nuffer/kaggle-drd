#job_id = 86
hyperparameters = \
{u'batch_size': 128,
 u'conv10_num_filters': 149,
 u'conv1_num_filters': 33,
 u'conv2_num_filters': 24,
 u'conv3_num_filters': 113,
 u'conv4_num_filters': 85,
 u'conv5_num_filters': 211,
 u'conv6_num_filters': 139,
 u'conv7_num_filters': 168,
 u'conv8_num_filters': 434,
 u'conv9_num_filters': 294,
 u'fc1_dropout_p': 0.83125,
 u'fc1_max_col_norm': 15.802343749999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2164,
 u'fc2_dropout_p': 0.38125,
 u'fc2_max_col_norm': 13.62578125,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3900,
 u'fc3_dropout_p': 0.46875,
 u'fc3_max_col_norm': 12.692968749999999,
 u'learning_rate': 0.09921953125,
 u'learning_rate_jitter': 3.15625,
 u'train_sample_rate': 0.15546875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
