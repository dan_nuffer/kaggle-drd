#job_id = 135
hyperparameters = \
{u'batch_size': 98,
 u'conv10_num_filters': 370,
 u'conv1_num_filters': 37,
 u'conv2_num_filters': 36,
 u'conv3_num_filters': 103,
 u'conv4_num_filters': 128,
 u'conv5_num_filters': 238,
 u'conv6_num_filters': 136,
 u'conv7_num_filters': 158,
 u'conv8_num_filters': 381,
 u'conv9_num_filters': 401,
 u'fc1_dropout_p': 0.509375,
 u'fc1_max_col_norm': 6.396484374999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3298,
 u'fc2_dropout_p': 0.7468750000000001,
 u'fc2_max_col_norm': 12.148828125,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3494,
 u'fc3_dropout_p': 0.609375,
 u'fc3_max_col_norm': 13.703515624999998,
 u'learning_rate': 0.008294921875,
 u'learning_rate_jitter': 1.171875,
 u'train_sample_rate': 0.44726562499999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
