import numpy as np
import theano.tensor as T

from lasagne.layers import DenseLayer
import lasagne as nn
from batch_normalization_layer import batch_norm
import data
import load
import nn_plankton
import tta

# 'expt_dir': 'exp/drd-maxout1/'
# 'id': 4
hyperparameters = \
{'conv10_num_filters': 416,
 'conv1_num_filters': 56,
 'conv2_num_filters': 28,
 'conv3_num_filters': 80,
 'conv4_num_filters': 104,
 'conv5_num_filters': 160,
 'conv6_num_filters': 160,
 'conv7_num_filters': 112,
 'conv8_num_filters': 320,
 'conv9_num_filters': 320,
 'fc1_dropout_p': 0.30000000000000004,
 'fc1_max_col_norm': 15.024999999999999,
 'fc1_maxout_pool_size': 2,
 'fc1_num_units': 4272,
 'fc2_dropout_p': 0.30000000000000004,
 'fc2_max_col_norm': 15.024999999999999,
 'fc2_maxout_pool_size': 4,
 'fc2_num_units': 4272,
 'fc3_dropout_p': 0.30000000000000004,
 'fc3_max_col_norm': 15.024999999999999,
 'learning_rate': 0.025075,
 'learning_rate_jitter': 4.0,
 'train_sample_rate': 0.725}

data_dir = "data-362x362"
patch_size = (255, 255, 3)
augmentation_params = {
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-12.39, 12.38),
    'translation_range': (-20 * patch_size[0] / 127., 20 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
}

batch_size = 3
chunk_size = batch_size * 50
predict_batch_size = 4

momentum = 0.88

num_examples = 90335 + 10535 + 53575  # train (upsampled) + val (pseudo) + test (pseudo)
train_sample_weight = hyperparameters['train_sample_rate']

learning_rate = hyperparameters['learning_rate']
learning_rate_decay = 0.50
learning_rate_patience = num_examples // chunk_size
learning_rate_rolling_mean_window = num_examples // chunk_size
learning_rate_improvement = 0.999
learning_rate_jitter = hyperparameters['learning_rate_jitter']

num_chunks_train = num_examples // chunk_size * 100 # stop after 100 epochs
divergence_threshold = 26.0

validate_every = 1 * num_examples // chunk_size # about once per 1 epoch

save_every = 1

test_pred_file = "predictions/test--weighted_blend-turing-20150618-194139.npy"
valid_pred_file = "predictions/valid--weighted_blend-turing-20150618-194140.npy"

def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-10, 10),
    'translation_range': (-8 * patch_size[0] / 127., 8 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
})



data_loader = load.PseudolabelingZmuvRescaledDataLoader(
    test_pred_file=test_pred_file,
    train_sample_weight=train_sample_weight,
    valid_pred_file=valid_pred_file,
    data_dir=data_dir,
    estimate_scale=estimate_scale,
    num_chunks_train=num_chunks_train,
    patch_size=patch_size,
    chunk_size=chunk_size,
    augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))

    l1a = batch_norm(Conv2DLayer(l0, num_filters=hyperparameters['conv1_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=hyperparameters['conv2_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=hyperparameters['conv3_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=hyperparameters['conv4_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=hyperparameters['conv5_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=hyperparameters['conv6_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=hyperparameters['conv7_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=hyperparameters['conv8_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=hyperparameters['conv9_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=hyperparameters['conv10_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    # the num_units has to be a multiple of the maxout_pool_size
    fc1_num_units = hyperparameters['fc1_num_units'] // hyperparameters['fc1_maxout_pool_size'] * hyperparameters['fc1_maxout_pool_size']
    l5 = nn.layers.DenseLayer(nn.layers.dropout(l4f, p=hyperparameters['fc1_dropout_p']),
                                         num_units=fc1_num_units, W=nn_plankton.Orthogonal(1.0), 
                                         b=nn.init.Constant(0.1), nonlinearity=None, 
                                         max_col_norm=hyperparameters['fc1_max_col_norm'])
    l5fp = batch_norm(nn.layers.FeaturePoolLayer(l5, pool_size=hyperparameters['fc1_maxout_pool_size']))

    fc2_num_units = hyperparameters['fc2_num_units'] // hyperparameters['fc2_maxout_pool_size'] * hyperparameters['fc2_maxout_pool_size']
    l6 = nn.layers.DenseLayer(nn.layers.dropout(l5fp, p=hyperparameters['fc2_dropout_p']),
                                         num_units=fc2_num_units, W=nn_plankton.Orthogonal(1.0),
                                         b=nn.init.Constant(0.1), nonlinearity=None,
                                         max_col_norm=hyperparameters['fc2_max_col_norm'])
    l6fp = batch_norm(nn.layers.FeaturePoolLayer(l6, pool_size=hyperparameters['fc2_maxout_pool_size']))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6fp, p=hyperparameters['fc2_dropout_p']), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0), max_col_norm=hyperparameters['fc3_max_col_norm']))

    return [l0], l7

def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy)

