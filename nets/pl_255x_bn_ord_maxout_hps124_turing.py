#job_id = 124
hyperparameters = \
{u'batch_size': 26,
 u'conv10_num_filters': 395,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 83,
 u'conv4_num_filters': 117,
 u'conv5_num_filters': 205,
 u'conv6_num_filters': 237,
 u'conv7_num_filters': 98,
 u'conv8_num_filters': 390,
 u'conv9_num_filters': 274,
 u'fc1_dropout_p': 0.66875,
 u'fc1_max_col_norm': 3.05390625,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1548,
 u'fc2_dropout_p': 0.59375,
 u'fc2_max_col_norm': 14.558593749999998,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1828,
 u'fc3_dropout_p': 0.38125,
 u'fc3_max_col_norm': 3.05390625,
 u'learning_rate': 0.09765859375000001,
 u'learning_rate_jitter': 2.96875,
 u'train_sample_rate': 0.7882812499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
