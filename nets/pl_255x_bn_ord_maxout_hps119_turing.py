#job_id = 119
hyperparameters = \
{u'batch_size': 106,
 u'conv10_num_filters': 347,
 u'conv1_num_filters': 33,
 u'conv2_num_filters': 47,
 u'conv3_num_filters': 108,
 u'conv4_num_filters': 81,
 u'conv5_num_filters': 253,
 u'conv6_num_filters': 253,
 u'conv7_num_filters': 122,
 u'conv8_num_filters': 358,
 u'conv9_num_filters': 498,
 u'fc1_dropout_p': 0.76875,
 u'fc1_max_col_norm': 15.491406249999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2892,
 u'fc2_dropout_p': 0.29375,
 u'fc2_max_col_norm': 2.12109375,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1380,
 u'fc3_dropout_p': 0.68125,
 u'fc3_max_col_norm': 5.54140625,
 u'learning_rate': 0.010246093749999999,
 u'learning_rate_jitter': 2.46875,
 u'train_sample_rate': 0.67578125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
