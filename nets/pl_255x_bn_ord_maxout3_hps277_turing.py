#job_id = 277
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 85,
 u'conv1_num_filters': 47,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 40,
 u'conv4_num_filters': 80,
 u'conv5_num_filters': 253,
 u'conv6_num_filters': 412,
 u'conv7_num_filters': 271,
 u'conv8_num_filters': 570,
 u'conv9_num_filters': 526,
 u'fc1_dropout_p': 0.7075153828318833,
 u'fc1_max_col_norm': 25.097081586221087,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 700,
 u'fc2_dropout_p': 0.07645380190524516,
 u'fc2_max_col_norm': 16.885799929538134,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2525,
 u'fc3_dropout_p': 0.27419830779578186,
 u'fc3_max_col_norm': 20.089490990344707,
 u'learning_rate': 0.013782460080044875,
 u'learning_rate_jitter': 1.11570231923019,
 u'train_sample_rate': 0.5,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
