hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.1734231951967976,
                          'contrast_noise_sigma': 0.1823381159401407,
                          'do_flip': True,
                          'num_transforms': 89,
                          'per_pixel_gaussian_noise_sigma': 0.16566994957815198,
                          'rotation_range': (0, 341.0818089474471),
                          'shear_range': (-36.56505013194216,
                                          36.56505013194216),
                          'translation_range': (-17.11954559515533,
                                                17.11954559515533),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.11533395123951928,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 39,
                           'per_pixel_gaussian_noise_sigma': 0.162861080587571,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-18.761915574534804,
                                           18.761915574534804),
                           'translation_range': (-14.112706831134135,
                                                 14.112706831134135),
                           'zoom_range': (0.9778301360842944,
                                          1.0226725103855814)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
