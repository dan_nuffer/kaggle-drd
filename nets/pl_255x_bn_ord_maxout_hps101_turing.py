#job_id = 101
hyperparameters = \
{u'batch_size': 54,
 u'conv10_num_filters': 455,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 52,
 u'conv3_num_filters': 65,
 u'conv4_num_filters': 78,
 u'conv5_num_filters': 209,
 u'conv6_num_filters': 217,
 u'conv7_num_filters': 152,
 u'conv8_num_filters': 430,
 u'conv9_num_filters': 378,
 u'fc1_dropout_p': 0.49375,
 u'fc1_max_col_norm': 13.62578125,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1660,
 u'fc2_dropout_p': 0.36875,
 u'fc2_max_col_norm': 15.18046875,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3508,
 u'fc3_dropout_p': 0.10625000000000001,
 u'fc3_max_col_norm': 9.894531249999998,
 u'learning_rate': 0.02585546875,
 u'learning_rate_jitter': 4.09375,
 u'train_sample_rate': 0.70390625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
