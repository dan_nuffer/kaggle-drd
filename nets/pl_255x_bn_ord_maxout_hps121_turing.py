#job_id = 121
hyperparameters = \
{u'batch_size': 58,
 u'conv10_num_filters': 299,
 u'conv1_num_filters': 53,
 u'conv2_num_filters': 29,
 u'conv3_num_filters': 100,
 u'conv4_num_filters': 93,
 u'conv5_num_filters': 237,
 u'conv6_num_filters': 205,
 u'conv7_num_filters': 146,
 u'conv8_num_filters': 454,
 u'conv9_num_filters': 466,
 u'fc1_dropout_p': 0.46875,
 u'fc1_max_col_norm': 8.02890625,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 652,
 u'fc2_dropout_p': 0.39375000000000004,
 u'fc2_max_col_norm': 9.583593749999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 932,
 u'fc3_dropout_p': 0.58125,
 u'fc3_max_col_norm': 17.97890625,
 u'learning_rate': 0.02273359375,
 u'learning_rate_jitter': 3.96875,
 u'train_sample_rate': 0.11328125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
