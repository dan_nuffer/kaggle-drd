#job_id = 32
hyperparameters = \
{u'batch_size': 33,
 u'conv10_num_filters': 212,
 u'conv1_num_filters': 63,
 u'conv2_num_filters': 54,
 u'conv3_num_filters': 86,
 u'conv4_num_filters': 83,
 u'conv5_num_filters': 148,
 u'conv6_num_filters': 204,
 u'conv7_num_filters': 70,
 u'conv8_num_filters': 344,
 u'conv9_num_filters': 504,
 u'fc1_dropout_p': 0.775,
 u'fc1_max_col_norm': 4.453124999999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 5552,
 u'fc2_dropout_p': 0.675,
 u'fc2_max_col_norm': 8.184375,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 5072,
 u'fc3_dropout_p': 0.125,
 u'fc3_max_col_norm': 5.6968749999999995,
 u'learning_rate': 0.05317187500000001,
 u'learning_rate_jitter': 4.625,
 u'train_sample_rate': 0.921875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
