hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.02761772039506624,
                          'do_flip': True,
                          'num_transforms': 79,
                          'per_pixel_gaussian_noise_sigma': 0.16775442735916515,
                          'rotation_range': (0, 0.0),
                          'shear_range': (-35.79756140172352,
                                          35.79756140172352),
                          'translation_range': (-12.589481262955497,
                                                12.589481262955497),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.1047050173625145,
                           'do_flip': True,
                           'num_transforms': 35,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-12.393942615888157,
                                           12.393942615888157),
                           'translation_range': (-6.676575030451364,
                                                 6.676575030451364),
                           'zoom_range': (0.6272334193280636,
                                          1.594302805279843)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
