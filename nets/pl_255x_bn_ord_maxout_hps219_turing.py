#job_id = 219
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 295,
 u'conv1_num_filters': 43,
 u'conv2_num_filters': 46,
 u'conv3_num_filters': 67,
 u'conv4_num_filters': 74,
 u'conv5_num_filters': 165,
 u'conv6_num_filters': 176,
 u'conv7_num_filters': 197,
 u'conv8_num_filters': 397,
 u'conv9_num_filters': 384,
 u'fc1_dropout_p': 0.15163850881461752,
 u'fc1_max_col_norm': 7.716999793547693,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3988,
 u'fc2_dropout_p': 0.30312357285756525,
 u'fc2_max_col_norm': 0.8960147640266561,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1477,
 u'fc3_dropout_p': 0.4594705943923908,
 u'fc3_max_col_norm': 4.953890736422105,
 u'learning_rate': 0.06453667407900439,
 u'learning_rate_jitter': 4.4285432309127675,
 u'train_sample_rate': 0.3903097768861875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
