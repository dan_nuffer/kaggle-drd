#job_id = 70
hyperparameters = \
{u'batch_size': 116,
 u'conv10_num_filters': 377,
 u'conv1_num_filters': 41,
 u'conv2_num_filters': 56,
 u'conv3_num_filters': 82,
 u'conv4_num_filters': 64,
 u'conv5_num_filters': 239,
 u'conv6_num_filters': 159,
 u'conv7_num_filters': 174,
 u'conv8_num_filters': 474,
 u'conv9_num_filters': 398,
 u'fc1_dropout_p': 0.80625,
 u'fc1_max_col_norm': 0.25546875,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1156,
 u'fc2_dropout_p': 0.35625000000000007,
 u'fc2_max_col_norm': 1.81015625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1324,
 u'fc3_dropout_p': 0.84375,
 u'fc3_max_col_norm': 4.608593749999999,
 u'learning_rate': 0.05239140625,
 u'learning_rate_jitter': 4.78125,
 u'train_sample_rate': 0.68984375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
