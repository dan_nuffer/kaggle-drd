#job_id = 51
hyperparameters = \
{u'batch_size': 107,
 u'conv10_num_filters': 158,
 u'conv1_num_filters': 54,
 u'conv2_num_filters': 56,
 u'conv3_num_filters': 65,
 u'conv4_num_filters': 100,
 u'conv5_num_filters': 162,
 u'conv6_num_filters': 242,
 u'conv7_num_filters': 205,
 u'conv8_num_filters': 348,
 u'conv9_num_filters': 412,
 u'fc1_dropout_p': 0.36250000000000004,
 u'fc1_max_col_norm': 2.8984375,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1912,
 u'fc2_dropout_p': 0.7375,
 u'fc2_max_col_norm': 13.470312499999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 1912,
 u'fc3_dropout_p': 0.6625,
 u'fc3_max_col_norm': 2.2765625,
 u'learning_rate': 0.048489062500000006,
 u'learning_rate_jitter': 3.1875,
 u'train_sample_rate': 0.40156249999999993}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
