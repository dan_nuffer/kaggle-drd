#job_id = 122
hyperparameters = \
{u'batch_size': 122,
 u'conv10_num_filters': 491,
 u'conv1_num_filters': 37,
 u'conv2_num_filters': 53,
 u'conv3_num_filters': 67,
 u'conv4_num_filters': 44,
 u'conv5_num_filters': 173,
 u'conv6_num_filters': 141,
 u'conv7_num_filters': 243,
 u'conv8_num_filters': 326,
 u'conv9_num_filters': 338,
 u'fc1_dropout_p': 0.86875,
 u'fc1_max_col_norm': 17.97890625,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2444,
 u'fc2_dropout_p': 0.7937500000000001,
 u'fc2_max_col_norm': 19.53359375,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2724,
 u'fc3_dropout_p': 0.18125000000000002,
 u'fc3_max_col_norm': 8.02890625,
 u'learning_rate': 0.07268359375000001,
 u'learning_rate_jitter': 1.96875,
 u'train_sample_rate': 0.56328125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
