#job_id = 276
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 76,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 38,
 u'conv3_num_filters': 74,
 u'conv4_num_filters': 77,
 u'conv5_num_filters': 231,
 u'conv6_num_filters': 136,
 u'conv7_num_filters': 423,
 u'conv8_num_filters': 142,
 u'conv9_num_filters': 438,
 u'fc1_dropout_p': 0.9499999999999988,
 u'fc1_max_col_norm': 29.468434448912596,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 326,
 u'fc2_dropout_p': 0.9,
 u'fc2_max_col_norm': 23.346616098013836,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 4696,
 u'fc3_dropout_p': 0.539037879021802,
 u'fc3_max_col_norm': 26.674317722409384,
 u'learning_rate': 0.027137888145752125,
 u'learning_rate_jitter': 4.745935610624601,
 u'train_sample_rate': 0.5000744502841796,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
