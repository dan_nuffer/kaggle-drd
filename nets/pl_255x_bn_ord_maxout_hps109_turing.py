#job_id = 109
hyperparameters = \
{u'batch_size': 30,
 u'conv10_num_filters': 335,
 u'conv1_num_filters': 34,
 u'conv2_num_filters': 55,
 u'conv3_num_filters': 69,
 u'conv4_num_filters': 72,
 u'conv5_num_filters': 217,
 u'conv6_num_filters': 161,
 u'conv7_num_filters': 92,
 u'conv8_num_filters': 478,
 u'conv9_num_filters': 490,
 u'fc1_dropout_p': 0.44375,
 u'fc1_max_col_norm': 2.43203125,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3676,
 u'fc2_dropout_p': 0.21875,
 u'fc2_max_col_norm': 16.42421875,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3284,
 u'fc3_dropout_p': 0.35625000000000007,
 u'fc3_max_col_norm': 8.65078125,
 u'learning_rate': 0.044586718750000004,
 u'learning_rate_jitter': 3.34375,
 u'train_sample_rate': 0.19765624999999998}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
