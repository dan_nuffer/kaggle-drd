#job_id = 280
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 48,
 u'conv3_num_filters': 73,
 u'conv4_num_filters': 55,
 u'conv5_num_filters': 320,
 u'conv6_num_filters': 140,
 u'conv7_num_filters': 423,
 u'conv8_num_filters': 423,
 u'conv9_num_filters': 666,
 u'fc1_dropout_p': 0.11991773176756496,
 u'fc1_max_col_norm': 28.18341753471804,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 178,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 11.703769137570154,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 5002,
 u'fc3_dropout_p': 0.2603401648991336,
 u'fc3_max_col_norm': 20.82111580019036,
 u'learning_rate': 0.0007227633145380631,
 u'learning_rate_jitter': 5.891696661422772,
 u'train_sample_rate': 0.5,
 u'training_time': 35.8721682238886}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
