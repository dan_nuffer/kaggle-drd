import numpy as np
import theano.tensor as T

import dihedral_fast
from lasagne.layers import DenseLayer
import lasagne as nn
from batch_normalization_layer import batch_norm
import data
import load
import nn_plankton
import dihedral
import tta
import utils

pre_init_path = "metadata/pl_cr_255x_bn_ord_maxout512_randlr-turing-20150617-201037_best.pkl"

data_dir = "data-362x362"
patch_size = (255, 255, 3)
augmentation_params = {
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-12.39, 12.38),
    'translation_range': (-20 * patch_size[0] / 127., 20 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
}

batch_size = 6
chunk_size = batch_size * 50

momentum = 0.88

num_examples = 90335 + 10535 + 53575  # train (upsampled) + val (pseudo) + test (pseudo)
train_sample_weight = 1. / 3. # TODO: make this somthing else? like 0.5? 2/3? hyperoptimize it?
# learning_rate_schedule = utils.make_full_triangle2_schedule(num_examples, chunk_size,
#                                                             init_min_lr=3.7e-5, init_max_lr=5.6e-4, iter_scale=1./3.)
# num_chunks_train = len(learning_rate_schedule)

learning_rate = 1e-3
learning_rate_decay = 0.50
learning_rate_patience = num_examples // chunk_size
learning_rate_rolling_mean_window = 300
learning_rate_improvement = 0.99999
learning_rate_jitter = 2.0

num_chunks_train = num_examples // chunk_size * 100 # stop after 100 epochs
divergence_threshold = 26.0

validate_every = 1 * num_examples // chunk_size # about once per 1 epoch

save_every = 1

test_pred_file = "predictions/test--weighted_blend-turing-20150618-194139.npy"
valid_pred_file = "predictions/valid--weighted_blend-turing-20150618-194140.npy"

def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-10, 10),
    'translation_range': (-8 * patch_size[0] / 127., 8 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
})



data_loader = load.PseudolabelingZmuvRescaledDataLoader(
    test_pred_file=test_pred_file,
    train_sample_weight=1. / 3., # TODO: make this somthing else? like 0.5? 2/3? hyperoptimize it?
    valid_pred_file=valid_pred_file,
    data_dir=data_dir,
    estimate_scale=estimate_scale,
    num_chunks_train=num_chunks_train,
    patch_size=patch_size,
    chunk_size=chunk_size,
    augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0c = dihedral.CyclicSliceLayer(l0)

    l1a = batch_norm(Conv2DLayer(l0c, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))
    l1r = dihedral_fast.CyclicConvRollLayer(l1)

    l2a = batch_norm(Conv2DLayer(l1r, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))
    l2r = dihedral_fast.CyclicConvRollLayer(l2)

    l3a = batch_norm(Conv2DLayer(l2r, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))
    l3r = dihedral_fast.CyclicConvRollLayer(l3)

    l4a = batch_norm(Conv2DLayer(l3r, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4r = dihedral_fast.CyclicConvRollLayer(l4)
    l4f = nn.layers.flatten(l4r)

    l5 = batch_norm(nn.layers.DenseLayer(nn.layers.dropout(l4f, p=0.5), num_units=1024, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=None))
    l5fp = nn.layers.FeaturePoolLayer(l5, pool_size=2)
    l5r = dihedral_fast.CyclicRollLayer(l5fp)

    l6 = batch_norm(nn.layers.DenseLayer(nn.layers.dropout(l5r, p=0.5), num_units=1024, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=None))
    l6fp = nn.layers.FeaturePoolLayer(l6, pool_size=2)
    l6m = dihedral.CyclicPoolLayer(l6fp, pool_function=nn_plankton.rms)

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6m, p=0.5), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0), max_col_norm=None))

    return [l0], l7

def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy)

