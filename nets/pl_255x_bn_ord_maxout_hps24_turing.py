#job_id = 24
hyperparameters = \
{u'batch_size': 225,
 u'conv10_num_filters': 428,
 u'conv1_num_filters': 45,
 u'conv2_num_filters': 45,
 u'conv3_num_filters': 74,
 u'conv4_num_filters': 77,
 u'conv5_num_filters': 236,
 u'conv6_num_filters': 228,
 u'conv7_num_filters': 202,
 u'conv8_num_filters': 328,
 u'conv9_num_filters': 264,
 u'fc1_dropout_p': 0.225,
 u'fc1_max_col_norm': 18.134375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 4112,
 u'fc2_dropout_p': 0.8250000000000001,
 u'fc2_max_col_norm': 6.940624999999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3632,
 u'fc3_dropout_p': 0.275,
 u'fc3_max_col_norm': 14.403125,
 u'learning_rate': 0.08439062500000001,
 u'learning_rate_jitter': 1.375,
 u'train_sample_rate': 0.528125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
