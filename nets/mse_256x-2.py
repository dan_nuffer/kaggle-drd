from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import tta

data_dir = "unused"
patch_size = (255, 255, 3)
augmentation_params = {
    'zoom_range': (1 / 1.6, 1.6),
    'rotation_range': (0, 360),
    'shear_range': (-20, 20),
    'translation_range': (-20 * patch_size[0] / 127., 20 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.3,
}

batch_size = 16
chunk_size = batch_size * 100
predict_batch_size = 4

num_examples = int(35162 * 3 * .9) # * 3 because of upsampling class imbalance. .9 because of training/validation split
momentum = 0.9
learning_rate = 0.010
learning_rate_decay = 0.50
learning_rate_patience = 6.0 * num_examples // chunk_size
learning_rate_rolling_mean_window = 6.0 * num_examples // chunk_size
learning_rate_improvement = 1.0
learning_rate_jitter = 5.0
num_chunks_train = num_examples // chunk_size * 10000 # stop after 10000 epochs
divergence_threshold = 26.0
failed_training_patience = 25

validate_every = 2 * num_examples // chunk_size # about once per 2 epochs
validate_train = False
save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 360),
    'shear_range': (-10, 10),
    'translation_range': (-16 * patch_size[0] / 127., 16 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.2,
})


data_loader = load.CachingDataLoader(
    estimate_scale=estimate_scale,
    num_chunks_train=num_chunks_train,
    patch_size=patch_size,
    chunk_size=chunk_size,
    augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    test_size=0.1,
    image_yx=(256,256),
    cache_dir="unsup-data-256x256",
    predict_mode="regression",
    random_state=2562 # 256x-2, change this for other nets
)


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer


def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))

    l1a = batch_norm(Conv2DLayer(l0, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))
   
    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    #
    # l5a = batch_norm(Conv2DLayer(l4, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    # l5b = batch_norm(Conv2DLayer(l5a, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    # l5c = batch_norm(Conv2DLayer(l5b, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    # l5 = MaxPool2DLayer(l5c, pool_size=(3, 3), stride=(2, 2))

    l5f = nn.layers.flatten(l4)

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5f, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu, max_col_norm=25.0))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu, max_col_norm=25.0))

    l8 = batch_norm(DenseLayer(nn.layers.dropout(l7, p=0.5), num_units=1, nonlinearity=None, W=nn_plankton.Orthogonal(1.0), max_col_norm=25.0))

    return [l0], l8, l3


def build_objective(l_ins, l_out):
    reg_lambda = .00001
    reg_f = reg_lambda * l2(l_out)
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.mean_squared_error, regularization_function=reg_f)


#pre_init_path = "metadata/mse_64x-turing-20150802-154401_best.pkl"
pre_init_path = "metadata/mse_128x-2-turing-20150805-194003.pkl"
