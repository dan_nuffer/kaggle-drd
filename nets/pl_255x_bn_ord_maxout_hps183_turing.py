#job_id = 183
hyperparameters = \
{u'batch_size': 108,
 u'conv10_num_filters': 496,
 u'conv1_num_filters': 41,
 u'conv2_num_filters': 39,
 u'conv3_num_filters': 119,
 u'conv4_num_filters': 84,
 u'conv5_num_filters': 175,
 u'conv6_num_filters': 154,
 u'conv7_num_filters': 64,
 u'conv8_num_filters': 481,
 u'conv9_num_filters': 333,
 u'fc1_dropout_p': 0.571875,
 u'fc1_max_col_norm': 14.169921874999998,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1898,
 u'fc2_dropout_p': 0.309375,
 u'fc2_max_col_norm': 9.350390625,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2094,
 u'fc3_dropout_p': 0.37187500000000007,
 u'fc3_max_col_norm': 18.989453125,
 u'learning_rate': 0.016099609375,
 u'learning_rate_jitter': 4.109375,
 u'train_sample_rate': 0.34882812499999993}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
