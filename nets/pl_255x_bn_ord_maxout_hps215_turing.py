#job_id = 215
hyperparameters = \
{u'batch_size': 91,
 u'conv10_num_filters': 162,
 u'conv1_num_filters': 42,
 u'conv2_num_filters': 18,
 u'conv3_num_filters': 98,
 u'conv4_num_filters': 77,
 u'conv5_num_filters': 235,
 u'conv6_num_filters': 143,
 u'conv7_num_filters': 159,
 u'conv8_num_filters': 279,
 u'conv9_num_filters': 463,
 u'fc1_dropout_p': 0.25312500000000004,
 u'fc1_max_col_norm': 2.820703125,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2654,
 u'fc2_dropout_p': 0.640625,
 u'fc2_max_col_norm': 17.279296875,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1394,
 u'fc3_dropout_p': 0.390625,
 u'fc3_max_col_norm': 15.413671874999999,
 u'learning_rate': 0.088683203125,
 u'learning_rate_jitter': 4.640625,
 u'train_sample_rate': 0.130859375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
