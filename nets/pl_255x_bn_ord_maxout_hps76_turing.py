#job_id = 76
hyperparameters = \
{u'batch_size': 60,
 u'conv10_num_filters': 401,
 u'conv1_num_filters': 47,
 u'conv2_num_filters': 34,
 u'conv3_num_filters': 70,
 u'conv4_num_filters': 82,
 u'conv5_num_filters': 135,
 u'conv6_num_filters': 135,
 u'conv7_num_filters': 65,
 u'conv8_num_filters': 362,
 u'conv9_num_filters': 350,
 u'fc1_dropout_p': 0.65625,
 u'fc1_max_col_norm': 8.96171875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1828,
 u'fc2_dropout_p': 0.40625,
 u'fc2_max_col_norm': 5.54140625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3788,
 u'fc3_dropout_p': 0.89375,
 u'fc3_max_col_norm': 8.339843749999998,
 u'learning_rate': 0.09609765625000001,
 u'learning_rate_jitter': 4.53125,
 u'train_sample_rate': 0.40859375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
