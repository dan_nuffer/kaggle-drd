#job_id = 68
hyperparameters = \
{u'batch_size': 36,
 u'conv10_num_filters': 233,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 25,
 u'conv3_num_filters': 90,
 u'conv4_num_filters': 76,
 u'conv5_num_filters': 255,
 u'conv6_num_filters': 175,
 u'conv7_num_filters': 198,
 u'conv8_num_filters': 378,
 u'conv9_num_filters': 430,
 u'fc1_dropout_p': 0.30625,
 u'fc1_max_col_norm': 12.692968749999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2500,
 u'fc2_dropout_p': 0.25625,
 u'fc2_max_col_norm': 9.272656249999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 876,
 u'fc3_dropout_p': 0.74375,
 u'fc3_max_col_norm': 12.07109375,
 u'learning_rate': 0.06487890625,
 u'learning_rate_jitter': 1.28125,
 u'train_sample_rate': 0.12734374999999998}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
