#job_id = 33
hyperparameters = \
{u'batch_size': 49,
 u'conv10_num_filters': 506,
 u'conv1_num_filters': 36,
 u'conv2_num_filters': 61,
 u'conv3_num_filters': 115,
 u'conv4_num_filters': 97,
 u'conv5_num_filters': 254,
 u'conv6_num_filters': 134,
 u'conv7_num_filters': 139,
 u'conv8_num_filters': 500,
 u'conv9_num_filters': 372,
 u'fc1_dropout_p': 0.1875,
 u'fc1_max_col_norm': 3.5203124999999997,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 8072,
 u'fc2_dropout_p': 0.5125000000000001,
 u'fc2_max_col_norm': 6.629687499999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3752,
 u'fc3_dropout_p': 0.4375,
 u'fc3_max_col_norm': 15.335937499999998,
 u'learning_rate': 0.0266359375,
 u'learning_rate_jitter': 3.8125,
 u'train_sample_rate': 0.5421874999999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
