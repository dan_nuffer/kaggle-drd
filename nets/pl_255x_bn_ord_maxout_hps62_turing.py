#job_id = 62
hyperparameters = \
{u'batch_size': 99,
 u'conv10_num_filters': 326,
 u'conv1_num_filters': 35,
 u'conv2_num_filters': 59,
 u'conv3_num_filters': 117,
 u'conv4_num_filters': 118,
 u'conv5_num_filters': 138,
 u'conv6_num_filters': 170,
 u'conv7_num_filters': 193,
 u'conv8_num_filters': 364,
 u'conv9_num_filters': 460,
 u'fc1_dropout_p': 0.5125000000000001,
 u'fc1_max_col_norm': 16.5796875,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2584,
 u'fc2_dropout_p': 0.48750000000000004,
 u'fc2_max_col_norm': 9.7390625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1240,
 u'fc3_dropout_p': 0.21250000000000002,
 u'fc3_max_col_norm': 15.9578125,
 u'learning_rate': 0.054732812500000005,
 u'learning_rate_jitter': 1.4375,
 u'train_sample_rate': 0.23281249999999998}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
