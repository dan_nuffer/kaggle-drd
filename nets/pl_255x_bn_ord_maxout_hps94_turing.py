#job_id = 94
hyperparameters = \
{u'batch_size': 104,
 u'conv10_num_filters': 509,
 u'conv1_num_filters': 52,
 u'conv2_num_filters': 39,
 u'conv3_num_filters': 109,
 u'conv4_num_filters': 79,
 u'conv5_num_filters': 171,
 u'conv6_num_filters': 163,
 u'conv7_num_filters': 107,
 u'conv8_num_filters': 418,
 u'conv9_num_filters': 470,
 u'fc1_dropout_p': 0.18125000000000002,
 u'fc1_max_col_norm': 2.12109375,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2836,
 u'fc2_dropout_p': 0.13125,
 u'fc2_max_col_norm': 12.382031249999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 540,
 u'fc3_dropout_p': 0.31875,
 u'fc3_max_col_norm': 6.4742187499999995,
 u'learning_rate': 0.06800078125,
 u'learning_rate_jitter': 2.90625,
 u'train_sample_rate': 0.43671874999999993}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
