#job_id = 35
hyperparameters = \
{u'batch_size': 817,
 u'conv10_num_filters': 410,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 24,
 u'conv3_num_filters': 99,
 u'conv4_num_filters': 72,
 u'conv5_num_filters': 158,
 u'conv6_num_filters': 230,
 u'conv7_num_filters': 187,
 u'conv8_num_filters': 308,
 u'conv9_num_filters': 308,
 u'fc1_dropout_p': 0.38750000000000007,
 u'fc1_max_col_norm': 18.4453125,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2312,
 u'fc2_dropout_p': 0.7125,
 u'fc2_max_col_norm': 1.6546875,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 5672,
 u'fc3_dropout_p': 0.23750000000000002,
 u'fc3_max_col_norm': 10.360937499999999,
 u'learning_rate': 0.0016609375,
 u'learning_rate_jitter': 4.8125,
 u'train_sample_rate': 0.7671875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
