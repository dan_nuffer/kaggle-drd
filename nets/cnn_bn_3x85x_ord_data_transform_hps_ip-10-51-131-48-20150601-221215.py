hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.13999216487528343,
                          'do_flip': True,
                          'num_transforms': 94,
                          'per_pixel_gaussian_noise_sigma': 0.1056107203486547,
                          'rotation_range': (0, 175.99307539041078),
                          'shear_range': (-19.765378583175355,
                                          19.765378583175355),
                          'translation_range': (-0.0, 0.0),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.1520817984481913,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 35,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-13.330368473701212,
                                           13.330368473701212),
                           'translation_range': (-9.180934408757238,
                                                 9.180934408757238),
                           'zoom_range': (0.8315095926237562,
                                          1.2026319466075994)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
