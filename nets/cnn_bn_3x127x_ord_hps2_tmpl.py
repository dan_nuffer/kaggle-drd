from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import BatchNormalizationLayer, batch_norm

import data
from lasagne.regularization import l1, l2
import load
import nn_plankton
import dihedral
import tmp_dnn
import tta
from utils import polynomial_decay

validate_train = False

hyperparameters = None
data_dir = None
input_side_len = None
patch_size = None
augmentation_params = None
batch_size = None
chunk_size = None
momentum = None
learning_rate = None
learning_rate_decay = None
learning_rate_patience = None
learning_rate_rolling_mean_window = None
learning_rate_improvement = None
num_chunks_train = None
divergence_threshold = None
validate_every = None
save_every = None
augmentation_transforms_test = None
data_loader = None

def set_hyperparameters(hyperparameters_arg):
    global hyperparameters
    hyperparameters = hyperparameters_arg
    global data_dir
    data_dir = "data-182x182"
    global input_side_len
    input_side_len = hyperparameters['data_params']['input_side_len']
    global patch_size
    patch_size = (input_side_len, input_side_len, 3)
    global augmentation_params
    augmentation_params = hyperparameters['train_data_processing']
    global batch_size
    batch_size = hyperparameters['data_params']['batch_size']
    global chunk_size
    chunk_size = hyperparameters['data_params']['chunk_size']
    global momentum
    momentum = hyperparameters['learning']['momentum']
    global learning_rate
    learning_rate = hyperparameters['learning']['learning_rate']
    global learning_rate_decay
    learning_rate_decay = 0.90
    global learning_rate_patience
    learning_rate_patience = 20 * (64 * 4096) // (batch_size * chunk_size)
    global learning_rate_rolling_mean_window
    learning_rate_rolling_mean_window = 30 * (64 * 4096) // (batch_size * chunk_size)
    global learning_rate_improvement
    learning_rate_improvement = 0.999
    global num_chunks_train
    num_chunks_train = hyperparameters['data_params']['num_chunks_train']
    global divergence_threshold
    divergence_threshold = 26.0
    global validate_every
    validate_every = num_chunks_train // 10
    global save_every
    save_every = 1
    global augmentation_transforms_test
    augmentation_transforms_test = tta.build_quasirandom_transforms(
        num_transforms = hyperparameters['eval_data_processing']['num_transforms'],
        zoom_range = hyperparameters['eval_data_processing']['zoom_range'],
        rotation_range = hyperparameters['eval_data_processing']['rotation_range'],
        shear_range = hyperparameters['eval_data_processing']['shear_range'],
        translation_range = hyperparameters['eval_data_processing']['translation_range'],
        do_flip=hyperparameters['eval_data_processing']['do_flip'],
        allow_stretch=hyperparameters['eval_data_processing']['allow_stretch'])
    global data_loader

    def estimate_scale(img):
        return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]

    data_loader = load.ZmuvRescaledDataLoader(estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
        patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
        augmentation_transforms_test=augmentation_transforms_test,
        predict_mode="ordinal_regression")


def build_model():
    Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
    MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0c = nn_plankton.ContrastNoiseLayer(l0, sigma=hyperparameters['train_data_processing']['contrast_noise_sigma'],
                                         test_sigma=hyperparameters['eval_data_processing']['contrast_noise_sigma'])

    l1a = batch_norm(Conv2DLayer(l0c, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    def none_for_zero(x):
        return x if x != 0. else None

    max_col_norm1 = none_for_zero(hyperparameters['regularization']['max_col_norm1'])
    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=hyperparameters['regularization']['dropout_p1']), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu, max_col_norm=max_col_norm1))

    max_col_norm2 = none_for_zero(hyperparameters['regularization']['max_col_norm2'])
    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=hyperparameters['regularization']['dropout_p2']), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu, max_col_norm=max_col_norm2))

    max_col_norm3 = none_for_zero(hyperparameters['regularization']['max_col_norm3'])
    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=hyperparameters['regularization']['dropout_p3']), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0), max_col_norm=max_col_norm3))

    return [l0], l7


def build_objective(l_ins, l_out):
    reg_f = hyperparameters['regularization']['l1'] * l1(l_out) + hyperparameters['regularization']['l2'] * l2(l_out)
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy, regularization_function=reg_f)
