#job_id = 260
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 158,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 57,
 u'conv3_num_filters': 32,
 u'conv4_num_filters': 72,
 u'conv5_num_filters': 309,
 u'conv6_num_filters': 377,
 u'conv7_num_filters': 326,
 u'conv8_num_filters': 195,
 u'conv9_num_filters': 402,
 u'fc1_dropout_p': 0.605030248728611,
 u'fc1_max_col_norm': 15.23786354010219,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 256,
 u'fc2_dropout_p': 0.3040495676513933,
 u'fc2_max_col_norm': 13.812879287913063,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 269,
 u'fc3_dropout_p': 0.5988210219244084,
 u'fc3_max_col_norm': 18.5610781599793,
 u'learning_rate': 0.022699146200405806,
 u'learning_rate_jitter': 5.077924374551728,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
