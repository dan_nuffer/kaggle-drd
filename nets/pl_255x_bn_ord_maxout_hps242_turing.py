#job_id = 242
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 212,
 u'conv1_num_filters': 75,
 u'conv2_num_filters': 14,
 u'conv3_num_filters': 59,
 u'conv4_num_filters': 101,
 u'conv5_num_filters': 177,
 u'conv6_num_filters': 202,
 u'conv7_num_filters': 382,
 u'conv8_num_filters': 294,
 u'conv9_num_filters': 446,
 u'fc1_dropout_p': 0.806931661446834,
 u'fc1_max_col_norm': 15.47670520818733,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 4679,
 u'fc2_dropout_p': 0.1,
 u'fc2_max_col_norm': 19.79999855987379,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 908,
 u'fc3_dropout_p': 0.38669231946241844,
 u'fc3_max_col_norm': 3.554912044379356,
 u'learning_rate': 0.00631332624369514,
 u'learning_rate_jitter': 5.702994464724838,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
