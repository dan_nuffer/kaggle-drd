#job_id = 282
hyperparameters = \
{u'batch_size': 6,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 124,
 u'conv4_num_filters': 160,
 u'conv5_num_filters': 279,
 u'conv6_num_filters': 83,
 u'conv7_num_filters': 266,
 u'conv8_num_filters': 583,
 u'conv9_num_filters': 387,
 u'fc1_dropout_p': 0.24350955978990285,
 u'fc1_max_col_norm': 21.83050105352819,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 24.50235531709883,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 5105,
 u'fc3_dropout_p': 0.4032017715935069,
 u'fc3_max_col_norm': 21.03003843771127,
 u'learning_rate': 0.00047413907417780945,
 u'learning_rate_jitter': 5.652280011947864,
 u'train_sample_rate': 0.5,
 u'training_time': 28.819236785469855}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
