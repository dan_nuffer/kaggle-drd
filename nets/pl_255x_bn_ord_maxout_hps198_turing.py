#job_id = 198
hyperparameters = \
{u'batch_size': 21,
 u'conv10_num_filters': 231,
 u'conv1_num_filters': 35,
 u'conv2_num_filters': 60,
 u'conv3_num_filters': 123,
 u'conv4_num_filters': 102,
 u'conv5_num_filters': 183,
 u'conv6_num_filters': 211,
 u'conv7_num_filters': 149,
 u'conv8_num_filters': 305,
 u'conv9_num_filters': 509,
 u'fc1_dropout_p': 0.22187500000000002,
 u'fc1_max_col_norm': 12.926171874999998,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2122,
 u'fc2_dropout_p': 0.359375,
 u'fc2_max_col_norm': 0.6441406249999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1422,
 u'fc3_dropout_p': 0.22187500000000002,
 u'fc3_max_col_norm': 5.3082031249999995,
 u'learning_rate': 0.022343359375,
 u'learning_rate_jitter': 2.859375,
 u'train_sample_rate': 0.40507812499999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
