#job_id = 1
hyperparameters = \
{u'batch_size': 2,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 128,
 u'conv6_num_filters': 128,
 u'conv7_num_filters': 64,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.1,
 u'fc1_max_col_norm': 0.1,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 512,
 u'fc2_dropout_p': 0.1,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 512,
 u'fc3_dropout_p': 0.1,
 u'fc3_max_col_norm': 0.1,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 1.0,
 u'train_sample_rate': 0.05}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
