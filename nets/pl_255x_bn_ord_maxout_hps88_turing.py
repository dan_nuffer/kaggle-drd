#job_id = 88
hyperparameters = \
{u'batch_size': 32,
 u'conv10_num_filters': 245,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 60,
 u'conv3_num_filters': 97,
 u'conv4_num_filters': 61,
 u'conv5_num_filters': 179,
 u'conv6_num_filters': 235,
 u'conv7_num_filters': 119,
 u'conv8_num_filters': 370,
 u'conv9_num_filters': 358,
 u'fc1_dropout_p': 0.63125,
 u'fc1_max_col_norm': 0.87734375,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3060,
 u'fc2_dropout_p': 0.18125000000000002,
 u'fc2_max_col_norm': 18.60078125,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1212,
 u'fc3_dropout_p': 0.26875000000000004,
 u'fc3_max_col_norm': 17.66796875,
 u'learning_rate': 0.07424453125000001,
 u'learning_rate_jitter': 4.15625,
 u'train_sample_rate': 0.38046874999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
