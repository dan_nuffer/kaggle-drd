#job_id = 279
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 175,
 u'conv1_num_filters': 20,
 u'conv2_num_filters': 6,
 u'conv3_num_filters': 39,
 u'conv4_num_filters': 77,
 u'conv5_num_filters': 227,
 u'conv6_num_filters': 408,
 u'conv7_num_filters': 485,
 u'conv8_num_filters': 640,
 u'conv9_num_filters': 515,
 u'fc1_dropout_p': 0.3555608677008516,
 u'fc1_max_col_norm': 17.735074403632513,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 195,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 11.492081062957977,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3017,
 u'fc3_dropout_p': 0.6978130972503976,
 u'fc3_max_col_norm': 30.0,
 u'learning_rate': 0.01416567061444052,
 u'learning_rate_jitter': 5.4054520597452544,
 u'train_sample_rate': 0.5015507041849869,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
