#job_id = 139
hyperparameters = \
{u'batch_size': 82,
 u'conv10_num_filters': 129,
 u'conv1_num_filters': 41,
 u'conv2_num_filters': 30,
 u'conv3_num_filters': 128,
 u'conv4_num_filters': 43,
 u'conv5_num_filters': 157,
 u'conv6_num_filters': 217,
 u'conv7_num_filters': 182,
 u'conv8_num_filters': 285,
 u'conv9_num_filters': 497,
 u'fc1_dropout_p': 0.40937500000000004,
 u'fc1_max_col_norm': 3.9089843749999997,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2850,
 u'fc2_dropout_p': 0.846875,
 u'fc2_max_col_norm': 14.636328124999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1254,
 u'fc3_dropout_p': 0.709375,
 u'fc3_max_col_norm': 6.241015624999999,
 u'learning_rate': 0.045757421875,
 u'learning_rate_jitter': 3.671875,
 u'train_sample_rate': 0.559765625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
