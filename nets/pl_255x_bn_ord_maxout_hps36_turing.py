#job_id = 36
hyperparameters = \
{u'batch_size': 305,
 u'conv10_num_filters': 218,
 u'conv1_num_filters': 44,
 u'conv2_num_filters': 48,
 u'conv3_num_filters': 67,
 u'conv4_num_filters': 121,
 u'conv5_num_filters': 222,
 u'conv6_num_filters': 166,
 u'conv7_num_filters': 91,
 u'conv8_num_filters': 436,
 u'conv9_num_filters': 436,
 u'fc1_dropout_p': 0.7875,
 u'fc1_max_col_norm': 8.495312499999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 6152,
 u'fc2_dropout_p': 0.3125,
 u'fc2_max_col_norm': 11.604687499999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1832,
 u'fc3_dropout_p': 0.6375,
 u'fc3_max_col_norm': 0.41093749999999996,
 u'learning_rate': 0.0516109375,
 u'learning_rate_jitter': 2.8125,
 u'train_sample_rate': 0.31718749999999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
