hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 59,
                          'per_pixel_gaussian_noise_sigma': 0.14485812525128183,
                          'rotation_range': (0, 284.76303667151103),
                          'shear_range': (-15.89443441007636,
                                          15.89443441007636),
                          'translation_range': (-9.568905844782524,
                                                9.568905844782524),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 43,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-20.674569023545892,
                                           20.674569023545892),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.5366042659271582,
                                          1.8635707233377565)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
