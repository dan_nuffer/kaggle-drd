hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.7531618560243722,
                          'color_shift_sigma': 0.027774136082713355,
                          'contrast_noise_sigma': 0.1687481832904503,
                          'do_flip': False,
                          'num_transforms': 69,
                          'per_pixel_gaussian_noise_sigma': 0.19437828896544507,
                          'rotation_range': (0, 20.600667580049542),
                          'shear_range': (-0.0, 0.0),
                          'translation_range': (-17.711372867950022,
                                                17.711372867950022),
                          'zoom_range': (0.6416293669218169,
                                         1.5585321550935975)},
 'train_data_processing': {'allow_stretch': 1.1544358864259237,
                           'color_shift_sigma': 0.1258341595946461,
                           'contrast_noise_sigma': 0.09564327359807218,
                           'do_flip': True,
                           'num_transforms': 80,
                           'per_pixel_gaussian_noise_sigma': 0.11881371364683475,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-27.84986580589766,
                                           27.84986580589766),
                           'translation_range': (-8.42772377595599,
                                                 8.42772377595599),
                           'zoom_range': (1.0, 1.0)}}


from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
