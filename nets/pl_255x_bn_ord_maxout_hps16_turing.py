#job_id = 16
hyperparameters = \
{u'batch_size': 65,
 u'conv10_num_filters': 344,
 u'conv1_num_filters': 54,
 u'conv2_num_filters': 25,
 u'conv3_num_filters': 116,
 u'conv4_num_filters': 62,
 u'conv5_num_filters': 168,
 u'conv6_num_filters': 216,
 u'conv7_num_filters': 76,
 u'conv8_num_filters': 304,
 u'conv9_num_filters': 336,
 u'fc1_dropout_p': 0.75,
 u'fc1_max_col_norm': 18.75625,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 7712,
 u'fc2_dropout_p': 0.65,
 u'fc2_max_col_norm': 16.26875,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2912,
 u'fc3_dropout_p': 0.55,
 u'fc3_max_col_norm': 16.26875,
 u'learning_rate': 0.09375625,
 u'learning_rate_jitter': 3.25,
 u'train_sample_rate': 0.33125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
