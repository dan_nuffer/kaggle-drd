hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.19941034486644726,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 99,
                          'per_pixel_gaussian_noise_sigma': 0.17844692011792154,
                          'rotation_range': (0, 115.10752694727938),
                          'shear_range': (-24.80033188744253,
                                          24.80033188744253),
                          'translation_range': (-4.067819513827293,
                                                4.067819513827293),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 88,
                           'per_pixel_gaussian_noise_sigma': 0.15223794258245674,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-4.62891041965761,
                                           4.62891041965761),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.6507712414934717,
                                          1.5366382781529717)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
