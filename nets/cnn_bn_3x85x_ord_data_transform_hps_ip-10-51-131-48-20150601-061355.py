hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.4572429159607043,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 7,
                          'per_pixel_gaussian_noise_sigma': 0.15166530902222844,
                          'rotation_range': (0, 202.5192689585199),
                          'shear_range': (-38.83057790190269,
                                          38.83057790190269),
                          'translation_range': (-14.046962966973208,
                                                14.046962966973208),
                          'zoom_range': (0.3699628909674749,
                                         2.702973796601439)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.006659743412949677,
                           'do_flip': True,
                           'num_transforms': 95,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 90.7607333867267),
                           'shear_range': (-21.343291593224635,
                                           21.343291593224635),
                           'translation_range': (-11.59065893252393,
                                                 11.59065893252393),
                           'zoom_range': (0.42441536640284877,
                                          2.3561823608686563)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
