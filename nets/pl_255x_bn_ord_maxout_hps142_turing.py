#job_id = 142
hyperparameters = \
{u'batch_size': 90,
 u'conv10_num_filters': 346,
 u'conv1_num_filters': 63,
 u'conv2_num_filters': 39,
 u'conv3_num_filters': 75,
 u'conv4_num_filters': 61,
 u'conv5_num_filters': 181,
 u'conv6_num_filters': 128,
 u'conv7_num_filters': 170,
 u'conv8_num_filters': 301,
 u'conv9_num_filters': 417,
 u'fc1_dropout_p': 0.659375,
 u'fc1_max_col_norm': 15.102734374999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1730,
 u'fc2_dropout_p': 0.396875,
 u'fc2_max_col_norm': 8.417578124999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 1926,
 u'fc3_dropout_p': 0.359375,
 u'fc3_max_col_norm': 12.459765625,
 u'learning_rate': 0.052001171875,
 u'learning_rate_jitter': 1.921875,
 u'train_sample_rate': 0.728515625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
