hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.006353732263828296,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 209.31765815120417),
                          'shear_range': (-26.796464930153267,
                                          26.796464930153267),
                          'translation_range': (-1.3504130370442442,
                                                1.3504130370442442),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.36679611520007543,
              'momentum': 0.9431408310492895},
 'regularization': {'dropout_p1': 0.17054332028100364,
                    'dropout_p2': 0.4108696158595469,
                    'dropout_p3': 0.5758368121389307,
                    'l1': 0.0,
                    'l2': 0.0,
                    'max_col_norm1': 9.981688598080417,
                    'max_col_norm2': 6.488597170848727,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-11.417420562660203,
                                           11.417420562660203),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.8094619527566761,
                                          1.2353885152902295)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
