from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import BatchNormalizationLayer, batch_norm

import data
from lasagne.regularization import l2, l1
import load
import nn_plankton
import dihedral
import tmp_dnn
import tta
from utils import polynomial_decay
import utils


pre_init_path = "metadata/cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best.pkl"

data_dir = "data-256x256"


hyperparameters = \
{'data_params': {'batch_size': 24,
                 'chunk_size': 4096,
                 'input_side_len': 255,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.0009678169648478438,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 146.7571648807644),
                          'shear_range': (-28.76268856887393,
                                          28.76268856887393),
                          'translation_range': (-5.18930034644836 / 127. * 255.,
                                                5.18930034644836 / 127. * 255.),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.016757507007876708,
              'momentum': 0.8414962130680428},
 'regularization': {'dropout_p1': 0.14637726007519286,
                    'dropout_p2': 0.5889941458290928,
                    'dropout_p3': 0.7552314381211893,
                    'l1': 4.4739397499527467e-07,
                    'l2': 0.0,
                    'max_col_norm1': 3.02428309381065,
                    'max_col_norm2': 9.98036983203762,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-12.380037740463948,
                                           12.380037740463948),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.704438464523333,
                                          1.4195704101374735)}}


patch_size = (255, 255, 3)

augmentation_params = hyperparameters['train_data_processing']

batch_size = hyperparameters['data_params']['batch_size']
chunk_size = hyperparameters['data_params']['chunk_size']

momentum = hyperparameters['learning']['momentum']
# learning_rate = 0.05
# learning_rate_decay = 0.90
# learning_rate_patience = 60
# learning_rate_rolling_mean_window = 200
# learning_rate_improvement = 0.999
# num_chunks_train=200000
num_examples = 90335
num_chunks_train = 6 * num_examples / chunk_size
learning_rate_schedule = utils.initial_learning_rate_exploration(num_chunks_train)

divergence_threshold = np.inf

validate_every = num_chunks_train + 1 # don't need to validate for this, just care about the loss

save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(
    num_transforms = hyperparameters['eval_data_processing']['num_transforms'],
    zoom_range = hyperparameters['eval_data_processing']['zoom_range'],
    rotation_range = hyperparameters['eval_data_processing']['rotation_range'],
    shear_range = hyperparameters['eval_data_processing']['shear_range'],
    translation_range = hyperparameters['eval_data_processing']['translation_range'],
    do_flip=hyperparameters['eval_data_processing']['do_flip'],
    allow_stretch=hyperparameters['eval_data_processing']['allow_stretch'])



data_loader = load.ZmuvRescaledDataLoader(
    estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
    patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression",
    color_space="rgb")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))

    l1a = batch_norm(Conv2DLayer(l0, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    def none_for_zero(x):
        return x if x != 0. else None

    max_col_norm1 = none_for_zero(hyperparameters['regularization']['max_col_norm1'])
    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=hyperparameters['regularization']['dropout_p1']), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu, max_col_norm=max_col_norm1))

    max_col_norm2 = none_for_zero(hyperparameters['regularization']['max_col_norm2'])
    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=hyperparameters['regularization']['dropout_p2']), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu, max_col_norm=max_col_norm2))

    max_col_norm3 = none_for_zero(hyperparameters['regularization']['max_col_norm3'])
    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=hyperparameters['regularization']['dropout_p3']), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0), max_col_norm=max_col_norm3))

    return [l0], l7

def build_objective(l_ins, l_out):
    # reg_lambda = .00003
    # reg_f = reg_lambda * l2(l_out)
    reg_f = hyperparameters['regularization']['l1'] * l1(l_out) + hyperparameters['regularization']['l2'] * l2(l_out)
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy, regularization_function=reg_f)

