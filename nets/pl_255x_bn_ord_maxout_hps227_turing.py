#job_id = 227
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 512,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 98,
 u'conv5_num_filters': 224,
 u'conv6_num_filters': 246,
 u'conv7_num_filters': 241,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 0.1,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2069,
 u'fc2_dropout_p': 0.8997095826016915,
 u'fc2_max_col_norm': 0.1000000000962052,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2735,
 u'fc3_dropout_p': 0.17363704532432495,
 u'fc3_max_col_norm': 13.879942406907693,
 u'learning_rate': 0.1,
 u'learning_rate_jitter': 4.724342883148717,
 u'train_sample_rate': 0.05}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
