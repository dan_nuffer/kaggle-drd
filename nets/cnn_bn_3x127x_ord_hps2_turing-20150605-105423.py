hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.09599827174213194,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 188.01235298042963),
                          'shear_range': (-22.27583246470718,
                                          22.27583246470718),
                          'translation_range': (-7.771823090961812,
                                                7.771823090961812),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.000108570135670471,
              'momentum': 0.9277563568126683},
 'regularization': {'dropout_p1': 0.18131360004240069,
                    'dropout_p2': 0.630309542477636,
                    'dropout_p3': 0.539578267040669,
                    'l1': 0.0,
                    'l2': 5.525816100848846e-06,
                    'max_col_norm1': 2.842122690303823,
                    'max_col_norm2': 0.0,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.03662529004863689,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-17.03513807484051,
                                           17.03513807484051),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.5697212345001623,
                                          1.755244388735725)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
