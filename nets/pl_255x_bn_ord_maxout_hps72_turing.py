#job_id = 72
hyperparameters = \
{u'batch_size': 20,
 u'conv10_num_filters': 473,
 u'conv1_num_filters': 49,
 u'conv2_num_filters': 19,
 u'conv3_num_filters': 66,
 u'conv4_num_filters': 88,
 u'conv5_num_filters': 143,
 u'conv6_num_filters': 255,
 u'conv7_num_filters': 125,
 u'conv8_num_filters': 282,
 u'conv9_num_filters': 462,
 u'fc1_dropout_p': 0.60625,
 u'fc1_max_col_norm': 15.18046875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3844,
 u'fc2_dropout_p': 0.15625,
 u'fc2_max_col_norm': 6.785156249999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 4012,
 u'fc3_dropout_p': 0.64375,
 u'fc3_max_col_norm': 9.583593749999999,
 u'learning_rate': 0.07736640625,
 u'learning_rate_jitter': 3.78125,
 u'train_sample_rate': 0.91484375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
