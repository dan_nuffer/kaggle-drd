#job_id = 256
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 74,
 u'conv2_num_filters': 14,
 u'conv3_num_filters': 48,
 u'conv4_num_filters': 85,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 382,
 u'conv7_num_filters': 300,
 u'conv8_num_filters': 371,
 u'conv9_num_filters': 257,
 u'fc1_dropout_p': 0.8120182588357592,
 u'fc1_max_col_norm': 0.1,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1530,
 u'fc2_dropout_p': 0.2282884272747569,
 u'fc2_max_col_norm': 17.453203354320213,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 581,
 u'fc3_dropout_p': 0.20196072941400448,
 u'fc3_max_col_norm': 3.3514485585009983,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.790480044290607,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
