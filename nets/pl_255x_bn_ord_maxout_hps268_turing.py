#job_id = 268
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 141,
 u'conv1_num_filters': 26,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 45,
 u'conv4_num_filters': 97,
 u'conv5_num_filters': 217,
 u'conv6_num_filters': 258,
 u'conv7_num_filters': 206,
 u'conv8_num_filters': 314,
 u'conv9_num_filters': 411,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 25.0,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 588,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 17.111280547973962,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 3582,
 u'fc3_dropout_p': 0.17598412851777123,
 u'fc3_max_col_norm': 20.0,
 u'learning_rate': 0.0027641232649489537,
 u'learning_rate_jitter': 4.967886823449147,
 u'train_sample_rate': 0.5002016501737814}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
