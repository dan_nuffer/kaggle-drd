#job_id = 133
hyperparameters = \
{u'batch_size': 3,
 u'conv10_num_filters': 466,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 49,
 u'conv3_num_filters': 120,
 u'conv4_num_filters': 55,
 u'conv5_num_filters': 141,
 u'conv6_num_filters': 233,
 u'conv7_num_filters': 206,
 u'conv8_num_filters': 445,
 u'conv9_num_filters': 465,
 u'fc1_dropout_p': 0.709375,
 u'fc1_max_col_norm': 11.371484375,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 610,
 u'fc2_dropout_p': 0.546875,
 u'fc2_max_col_norm': 17.123828125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 806,
 u'fc3_dropout_p': 0.8093750000000001,
 u'fc3_max_col_norm': 18.678515625,
 u'learning_rate': 0.033269921875000004,
 u'learning_rate_jitter': 2.171875,
 u'train_sample_rate': 0.22226562499999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
