#job_id = 259
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 132,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 43,
 u'conv4_num_filters': 76,
 u'conv5_num_filters': 237,
 u'conv6_num_filters': 386,
 u'conv7_num_filters': 300,
 u'conv8_num_filters': 512,
 u'conv9_num_filters': 637,
 u'fc1_dropout_p': 0.299567086439871,
 u'fc1_max_col_norm': 15.926497535256972,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 437,
 u'fc2_dropout_p': 0.3871194127432842,
 u'fc2_max_col_norm': 13.64061823981491,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 615,
 u'fc3_dropout_p': 0.34080827163535843,
 u'fc3_max_col_norm': 9.726065728378074,
 u'learning_rate': 0.0026147157320713698,
 u'learning_rate_jitter': 5.106057116159429,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
