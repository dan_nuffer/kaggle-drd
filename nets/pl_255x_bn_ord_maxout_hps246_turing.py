#job_id = 246
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 184,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 49,
 u'conv3_num_filters': 61,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 128,
 u'conv6_num_filters': 266,
 u'conv7_num_filters': 252,
 u'conv8_num_filters': 408,
 u'conv9_num_filters': 438,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 16.56716257804703,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4398,
 u'fc2_dropout_p': 0.10105188103605246,
 u'fc2_max_col_norm': 20.0,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.1,
 u'fc3_max_col_norm': 3.3633749951451652,
 u'learning_rate': 0.09699035297242904,
 u'learning_rate_jitter': 5.660844585743191,
 u'train_sample_rate': 0.8080165550958637}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
