#job_id = 272
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 24,
 u'conv2_num_filters': 7,
 u'conv3_num_filters': 59,
 u'conv4_num_filters': 52,
 u'conv5_num_filters': 289,
 u'conv6_num_filters': 304,
 u'conv7_num_filters': 278,
 u'conv8_num_filters': 131,
 u'conv9_num_filters': 600,
 u'fc1_dropout_p': 0.5879204592008576,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 3647,
 u'fc3_dropout_p': 0.4522824705964261,
 u'fc3_max_col_norm': 30.0,
 u'learning_rate': 0.027361732575181624,
 u'learning_rate_jitter': 1.0,
 u'train_sample_rate': 0.5,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
