#job_id = 77
hyperparameters = \
{u'batch_size': 44,
 u'conv10_num_filters': 449,
 u'conv1_num_filters': 43,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 111,
 u'conv4_num_filters': 46,
 u'conv5_num_filters': 215,
 u'conv6_num_filters': 247,
 u'conv7_num_filters': 186,
 u'conv8_num_filters': 330,
 u'conv9_num_filters': 510,
 u'fc1_dropout_p': 0.55625,
 u'fc1_max_col_norm': 6.4742187499999995,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2276,
 u'fc2_dropout_p': 0.50625,
 u'fc2_max_col_norm': 13.003906249999998,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1548,
 u'fc3_dropout_p': 0.39375000000000004,
 u'fc3_max_col_norm': 5.852343749999999,
 u'learning_rate': 0.03366015625,
 u'learning_rate_jitter': 3.03125,
 u'train_sample_rate': 0.29609375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
