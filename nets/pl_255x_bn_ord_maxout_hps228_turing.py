#job_id = 228
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 54,
 u'conv2_num_filters': 31,
 u'conv3_num_filters': 66,
 u'conv4_num_filters': 109,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 139,
 u'conv7_num_filters': 250,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 512,
 u'fc1_dropout_p': 0.26197072227629625,
 u'fc1_max_col_norm': 19.207260579287773,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2459,
 u'fc2_dropout_p': 0.13230008255409442,
 u'fc2_max_col_norm': 19.36736818997409,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2883,
 u'fc3_dropout_p': 0.11341976297995522,
 u'fc3_max_col_norm': 1.5046428894700414,
 u'learning_rate': 0.00047568347035597977,
 u'learning_rate_jitter': 4.982846300438913,
 u'train_sample_rate': 0.33828291452009396}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
