hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.1414011861229566,
                          'do_flip': True,
                          'num_transforms': 83,
                          'per_pixel_gaussian_noise_sigma': 0.14920141306636897,
                          'rotation_range': (0, 192.86251759209955),
                          'shear_range': (-26.71360798013859,
                                          26.71360798013859),
                          'translation_range': (-3.404629700357491,
                                                3.404629700357491),
                          'zoom_range': (0.8836713168991921,
                                         1.1316424793655246)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 46,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-12.870857234595388,
                                           12.870857234595388),
                           'translation_range': (-7.6578361273540665,
                                                 7.6578361273540665),
                           'zoom_range': (0.3403905444331063,
                                          2.9378019347318283)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
