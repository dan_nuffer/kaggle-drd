#job_id = 22
hyperparameters = \
{u'batch_size': 993,
 u'conv10_num_filters': 332,
 u'conv1_num_filters': 53,
 u'conv2_num_filters': 32,
 u'conv3_num_filters': 90,
 u'conv4_num_filters': 101,
 u'conv5_num_filters': 140,
 u'conv6_num_filters': 132,
 u'conv7_num_filters': 154,
 u'conv8_num_filters': 392,
 u'conv9_num_filters': 328,
 u'fc1_dropout_p': 0.42500000000000004,
 u'fc1_max_col_norm': 3.209375,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 6032,
 u'fc2_dropout_p': 0.625,
 u'fc2_max_col_norm': 1.965625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 5552,
 u'fc3_dropout_p': 0.475,
 u'fc3_max_col_norm': 19.378125,
 u'learning_rate': 0.05941562500000001,
 u'learning_rate_jitter': 2.375,
 u'train_sample_rate': 0.7531249999999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
