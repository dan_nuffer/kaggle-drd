#job_id = 150
hyperparameters = \
{u'batch_size': 78,
 u'conv10_num_filters': 430,
 u'conv1_num_filters': 38,
 u'conv2_num_filters': 56,
 u'conv3_num_filters': 118,
 u'conv4_num_filters': 71,
 u'conv5_num_filters': 242,
 u'conv6_num_filters': 188,
 u'conv7_num_filters': 91,
 u'conv8_num_filters': 341,
 u'conv9_num_filters': 505,
 u'fc1_dropout_p': 0.334375,
 u'fc1_max_col_norm': 16.968359375,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3634,
 u'fc2_dropout_p': 0.12187500000000001,
 u'fc2_max_col_norm': 17.745703125,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1814,
 u'fc3_dropout_p': 0.884375,
 u'fc3_max_col_norm': 19.300390625,
 u'learning_rate': 0.06761054687500001,
 u'learning_rate_jitter': 3.796875,
 u'train_sample_rate': 0.137890625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
