#job_id = 300
hyperparameters = \
{u'batch_size': 5,
 u'conv10_num_filters': 35,
 u'conv1_num_filters': 18,
 u'conv2_num_filters': 15,
 u'conv3_num_filters': 48,
 u'conv4_num_filters': 50,
 u'conv5_num_filters': 316,
 u'conv6_num_filters': 87,
 u'conv7_num_filters': 296,
 u'conv8_num_filters': 509,
 u'conv9_num_filters': 524,
 u'fc1_dropout_p': 0.1001988127057715,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 125,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 23.605332440584487,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 2258,
 u'fc3_dropout_p': 0.8041705964388027,
 u'fc3_max_col_norm': 29.68085627183851,
 u'learning_rate': 0.0046392055622671985,
 u'learning_rate_jitter': 2.831590868912037,
 u'train_sample_rate': 0.25,
 u'training_time': 33.41413550841101}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
