hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.2152431305782975,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 216.11466313832224),
                          'shear_range': (-34.08853996975129,
                                          34.08853996975129),
                          'translation_range': (-10.676865167954205,
                                                10.676865167954205),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.04046356343946717,
              'momentum': 0.7062711640724253},
 'regularization': {'dropout_p1': 0.7575418951098833,
                    'dropout_p2': 0.2923603126075875,
                    'dropout_p3': 0.5990391988742945,
                    'l1': 4.601660863352516e-06,
                    'l2': 3.355988301300906e-07,
                    'max_col_norm1': 3.4400401056612355,
                    'max_col_norm2': 3.336720002648418,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-24.63185571737374,
                                           24.63185571737374),
                           'translation_range': (-5.2042725236164795,
                                                 5.2042725236164795),
                           'zoom_range': (0.4446168153051838,
                                          2.249127710821289)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
