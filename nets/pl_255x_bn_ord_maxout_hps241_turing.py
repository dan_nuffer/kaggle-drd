#job_id = 241
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 45,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 48,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 382,
 u'conv7_num_filters': 329,
 u'conv8_num_filters': 407,
 u'conv9_num_filters': 494,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 20.0,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1785,
 u'fc2_dropout_p': 0.11482161797419971,
 u'fc2_max_col_norm': 20.0,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 512,
 u'fc3_dropout_p': 0.2210314997143378,
 u'fc3_max_col_norm': 2.3193143259416997,
 u'learning_rate': 0.02882786494737478,
 u'learning_rate_jitter': 5.866321247758246,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
