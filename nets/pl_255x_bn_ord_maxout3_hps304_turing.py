#job_id = 304
hyperparameters = \
{u'batch_size': 7,
 u'conv10_num_filters': 32,
 u'conv1_num_filters': 41,
 u'conv2_num_filters': 56,
 u'conv3_num_filters': 67,
 u'conv4_num_filters': 43,
 u'conv5_num_filters': 287,
 u'conv6_num_filters': 64,
 u'conv7_num_filters': 241,
 u'conv8_num_filters': 361,
 u'conv9_num_filters': 585,
 u'fc1_dropout_p': 0.439791739401732,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 352,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 4.07466802103882,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 611,
 u'fc3_dropout_p': 0.7887978901614633,
 u'fc3_max_col_norm': 21.464105707106057,
 u'learning_rate': 0.00779073505258567,
 u'learning_rate_jitter': 5.8638756173417095,
 u'train_sample_rate': 0.25,
 u'training_time': 36.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
