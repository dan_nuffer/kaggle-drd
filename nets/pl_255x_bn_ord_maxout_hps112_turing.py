#job_id = 112
hyperparameters = \
{u'batch_size': 62,
 u'conv10_num_filters': 239,
 u'conv1_num_filters': 42,
 u'conv2_num_filters': 43,
 u'conv3_num_filters': 118,
 u'conv4_num_filters': 47,
 u'conv5_num_filters': 249,
 u'conv6_num_filters': 129,
 u'conv7_num_filters': 140,
 u'conv8_num_filters': 414,
 u'conv9_num_filters': 298,
 u'fc1_dropout_p': 0.64375,
 u'fc1_max_col_norm': 7.407031249999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2780,
 u'fc2_dropout_p': 0.81875,
 u'fc2_max_col_norm': 1.49921875,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2388,
 u'fc3_dropout_p': 0.55625,
 u'fc3_max_col_norm': 13.62578125,
 u'learning_rate': 0.06956171875,
 u'learning_rate_jitter': 2.34375,
 u'train_sample_rate': 0.8726562499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
