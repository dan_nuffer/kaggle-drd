#job_id = 97
hyperparameters = \
{u'batch_size': 6,
 u'conv10_num_filters': 311,
 u'conv1_num_filters': 48,
 u'conv2_num_filters': 46,
 u'conv3_num_filters': 89,
 u'conv4_num_filters': 90,
 u'conv5_num_filters': 161,
 u'conv6_num_filters': 137,
 u'conv7_num_filters': 177,
 u'conv8_num_filters': 462,
 u'conv9_num_filters': 282,
 u'fc1_dropout_p': 0.59375,
 u'fc1_max_col_norm': 16.11328125,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1212,
 u'fc2_dropout_p': 0.46875,
 u'fc2_max_col_norm': 17.66796875,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1268,
 u'fc3_dropout_p': 0.40625,
 u'fc3_max_col_norm': 12.382031249999999,
 u'learning_rate': 0.01336796875,
 u'learning_rate_jitter': 2.59375,
 u'train_sample_rate': 0.36640624999999993}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
