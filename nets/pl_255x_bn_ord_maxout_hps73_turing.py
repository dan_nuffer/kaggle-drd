#job_id = 73
hyperparameters = \
{u'batch_size': 28,
 u'conv10_num_filters': 305,
 u'conv1_num_filters': 38,
 u'conv2_num_filters': 22,
 u'conv3_num_filters': 119,
 u'conv4_num_filters': 107,
 u'conv5_num_filters': 167,
 u'conv6_num_filters': 167,
 u'conv7_num_filters': 113,
 u'conv8_num_filters': 298,
 u'conv9_num_filters': 414,
 u'fc1_dropout_p': 0.45625000000000004,
 u'fc1_max_col_norm': 3.9867187499999996,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 932,
 u'fc2_dropout_p': 0.60625,
 u'fc2_max_col_norm': 10.51640625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2892,
 u'fc3_dropout_p': 0.29375,
 u'fc3_max_col_norm': 13.314843749999998,
 u'learning_rate': 0.02117265625,
 u'learning_rate_jitter': 1.53125,
 u'train_sample_rate': 0.63359375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
