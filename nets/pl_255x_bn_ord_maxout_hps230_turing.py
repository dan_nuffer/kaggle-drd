#job_id = 230
hyperparameters = \
{u'batch_size': 12,
 u'conv10_num_filters': 351,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 43,
 u'conv3_num_filters': 67,
 u'conv4_num_filters': 107,
 u'conv5_num_filters': 162,
 u'conv6_num_filters': 211,
 u'conv7_num_filters': 210,
 u'conv8_num_filters': 370,
 u'conv9_num_filters': 512,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 19.935368967761693,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 4096,
 u'fc2_dropout_p': 0.12431476194926824,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2550,
 u'fc3_dropout_p': 0.3394373078556139,
 u'fc3_max_col_norm': 0.5376992825632501,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 4.842519459789756,
 u'train_sample_rate': 0.33368650690617624}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
