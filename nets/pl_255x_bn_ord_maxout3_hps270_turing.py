#job_id = 270
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 115,
 u'conv1_num_filters': 23,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 67,
 u'conv4_num_filters': 143,
 u'conv5_num_filters': 313,
 u'conv6_num_filters': 84,
 u'conv7_num_filters': 328,
 u'conv8_num_filters': 506,
 u'conv9_num_filters': 720,
 u'fc1_dropout_p': 0.9331409259762478,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.2042271976525245,
 u'fc2_max_col_norm': 10.916616191813898,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 408,
 u'fc3_dropout_p': 0.31847201646201473,
 u'fc3_max_col_norm': 24.335209728401416,
 u'learning_rate': 0.0036137580976085054,
 u'learning_rate_jitter': 5.263748928876314,
 u'train_sample_rate': 0.5,
 u'training_time': 24.148907160687745}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
