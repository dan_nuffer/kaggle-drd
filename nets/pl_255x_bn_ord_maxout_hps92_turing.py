#job_id = 92
hyperparameters = \
{u'batch_size': 56,
 u'conv10_num_filters': 173,
 u'conv1_num_filters': 39,
 u'conv2_num_filters': 45,
 u'conv3_num_filters': 101,
 u'conv4_num_filters': 67,
 u'conv5_num_filters': 187,
 u'conv6_num_filters': 147,
 u'conv7_num_filters': 83,
 u'conv8_num_filters': 258,
 u'conv9_num_filters': 502,
 u'fc1_dropout_p': 0.68125,
 u'fc1_max_col_norm': 14.558593749999998,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 596,
 u'fc2_dropout_p': 0.43125,
 u'fc2_max_col_norm': 19.84453125,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 988,
 u'fc3_dropout_p': 0.41875000000000007,
 u'fc3_max_col_norm': 18.91171875,
 u'learning_rate': 0.055513281250000004,
 u'learning_rate_jitter': 3.40625,
 u'train_sample_rate': 0.77421875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
