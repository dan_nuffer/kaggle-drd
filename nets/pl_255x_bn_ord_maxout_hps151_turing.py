#job_id = 151
hyperparameters = \
{u'batch_size': 110,
 u'conv10_num_filters': 141,
 u'conv1_num_filters': 46,
 u'conv2_num_filters': 44,
 u'conv3_num_filters': 69,
 u'conv4_num_filters': 46,
 u'conv5_num_filters': 210,
 u'conv6_num_filters': 156,
 u'conv7_num_filters': 140,
 u'conv8_num_filters': 277,
 u'conv9_num_filters': 313,
 u'fc1_dropout_p': 0.534375,
 u'fc1_max_col_norm': 11.993359374999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2738,
 u'fc2_dropout_p': 0.721875,
 u'fc2_max_col_norm': 2.820703125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 918,
 u'fc3_dropout_p': 0.28437500000000004,
 u'fc3_max_col_norm': 4.375390625,
 u'learning_rate': 0.042635546875000006,
 u'learning_rate_jitter': 2.796875,
 u'train_sample_rate': 0.812890625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
