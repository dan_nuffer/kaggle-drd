#job_id = 80
hyperparameters = \
{u'batch_size': 12,
 u'conv10_num_filters': 161,
 u'conv1_num_filters': 34,
 u'conv2_num_filters': 28,
 u'conv3_num_filters': 94,
 u'conv4_num_filters': 70,
 u'conv5_num_filters': 247,
 u'conv6_num_filters': 215,
 u'conv7_num_filters': 234,
 u'conv8_num_filters': 266,
 u'conv9_num_filters': 318,
 u'fc1_dropout_p': 0.35625000000000007,
 u'fc1_max_col_norm': 1.49921875,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1380,
 u'fc2_dropout_p': 0.30625,
 u'fc2_max_col_norm': 8.02890625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 652,
 u'fc3_dropout_p': 0.59375,
 u'fc3_max_col_norm': 10.827343749999999,
 u'learning_rate': 0.05863515625000001,
 u'learning_rate_jitter': 2.03125,
 u'train_sample_rate': 0.52109375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
