#job_id = 161
hyperparameters = \
{u'batch_size': 39,
 u'conv10_num_filters': 165,
 u'conv1_num_filters': 44,
 u'conv2_num_filters': 23,
 u'conv3_num_filters': 81,
 u'conv4_num_filters': 125,
 u'conv5_num_filters': 185,
 u'conv6_num_filters': 132,
 u'conv7_num_filters': 248,
 u'conv8_num_filters': 421,
 u'conv9_num_filters': 489,
 u'fc1_dropout_p': 0.784375,
 u'fc1_max_col_norm': 18.212109375,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3858,
 u'fc2_dropout_p': 0.8718750000000001,
 u'fc2_max_col_norm': 9.039453125,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3382,
 u'fc3_dropout_p': 0.234375,
 u'fc3_max_col_norm': 8.106640624999999,
 u'learning_rate': 0.011416796875,
 u'learning_rate_jitter': 2.546875,
 u'train_sample_rate': 0.081640625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
