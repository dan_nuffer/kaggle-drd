#job_id = 95
hyperparameters = \
{u'batch_size': 72,
 u'conv10_num_filters': 221,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 27,
 u'conv3_num_filters': 92,
 u'conv4_num_filters': 55,
 u'conv5_num_filters': 139,
 u'conv6_num_filters': 131,
 u'conv7_num_filters': 155,
 u'conv8_num_filters': 482,
 u'conv9_num_filters': 278,
 u'fc1_dropout_p': 0.78125,
 u'fc1_max_col_norm': 7.096093749999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3732,
 u'fc2_dropout_p': 0.7312500000000001,
 u'fc2_max_col_norm': 7.407031249999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1436,
 u'fc3_dropout_p': 0.51875,
 u'fc3_max_col_norm': 11.449218749999998,
 u'learning_rate': 0.043025781250000006,
 u'learning_rate_jitter': 3.90625,
 u'train_sample_rate': 0.66171875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
