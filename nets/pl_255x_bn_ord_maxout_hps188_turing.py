#job_id = 188
hyperparameters = \
{u'batch_size': 29,
 u'conv10_num_filters': 448,
 u'conv1_num_filters': 54,
 u'conv2_num_filters': 57,
 u'conv3_num_filters': 78,
 u'conv4_num_filters': 121,
 u'conv5_num_filters': 159,
 u'conv6_num_filters': 138,
 u'conv7_num_filters': 137,
 u'conv8_num_filters': 257,
 u'conv9_num_filters': 429,
 u'fc1_dropout_p': 0.8718750000000001,
 u'fc1_max_col_norm': 6.707421874999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2346,
 u'fc2_dropout_p': 0.8093750000000001,
 u'fc2_max_col_norm': 16.812890625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 750,
 u'fc3_dropout_p': 0.671875,
 u'fc3_max_col_norm': 11.526953124999999,
 u'learning_rate': 0.07853710937500001,
 u'learning_rate_jitter': 4.609375,
 u'train_sample_rate': 0.236328125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
