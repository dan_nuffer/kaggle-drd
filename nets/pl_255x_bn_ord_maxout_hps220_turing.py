#job_id = 220
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 147,
 u'conv1_num_filters': 36,
 u'conv2_num_filters': 54,
 u'conv3_num_filters': 80,
 u'conv4_num_filters': 35,
 u'conv5_num_filters': 198,
 u'conv6_num_filters': 255,
 u'conv7_num_filters': 197,
 u'conv8_num_filters': 430,
 u'conv9_num_filters': 440,
 u'fc1_dropout_p': 0.12419877356115296,
 u'fc1_max_col_norm': 13.674292947157426,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4096,
 u'fc2_dropout_p': 0.3790650516608045,
 u'fc2_max_col_norm': 16.62493168353359,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1434,
 u'fc3_dropout_p': 0.8854408097683762,
 u'fc3_max_col_norm': 0.29753478646690695,
 u'learning_rate': 0.05144858818252911,
 u'learning_rate_jitter': 4.99980529373275,
 u'train_sample_rate': 0.5845529379352609}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
