#job_id = 5
hyperparameters = \
{u'batch_size': 385,
 u'conv10_num_filters': 368,
 u'conv1_num_filters': 44,
 u'conv2_num_filters': 22,
 u'conv3_num_filters': 88,
 u'conv4_num_filters': 116,
 u'conv5_num_filters': 240,
 u'conv6_num_filters': 208,
 u'conv7_num_filters': 232,
 u'conv8_num_filters': 352,
 u'conv9_num_filters': 352,
 u'fc1_dropout_p': 0.8,
 u'fc1_max_col_norm': 7.562499999999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3392,
 u'fc2_dropout_p': 0.2,
 u'fc2_max_col_norm': 2.5875,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 7232,
 u'fc3_dropout_p': 0.4,
 u'fc3_max_col_norm': 17.5125,
 u'learning_rate': 0.037562500000000006,
 u'learning_rate_jitter': 3.5,
 u'train_sample_rate': 0.8374999999999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
