#job_id = 81
hyperparameters = \
{u'batch_size': 16,
 u'conv10_num_filters': 197,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 42,
 u'conv3_num_filters': 72,
 u'conv4_num_filters': 122,
 u'conv5_num_filters': 227,
 u'conv6_num_filters': 155,
 u'conv7_num_filters': 240,
 u'conv8_num_filters': 338,
 u'conv9_num_filters': 454,
 u'fc1_dropout_p': 0.53125,
 u'fc1_max_col_norm': 3.36484375,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2612,
 u'fc2_dropout_p': 0.88125,
 u'fc2_max_col_norm': 1.18828125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2556,
 u'fc3_dropout_p': 0.56875,
 u'fc3_max_col_norm': 15.18046875,
 u'learning_rate': 0.011807031249999999,
 u'learning_rate_jitter': 3.65625,
 u'train_sample_rate': 0.49296874999999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
