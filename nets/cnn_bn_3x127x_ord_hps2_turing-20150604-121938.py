hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.14176478293106654,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 83.80036503217731),
                          'shear_range': (-26.889591498403277,
                                          26.889591498403277),
                          'translation_range': (-14.919797127455576,
                                                14.919797127455576),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.0007118316951587322,
              'momentum': 0.6282670830291053},
 'regularization': {'dropout_p1': 0.4757602981281389,
                    'dropout_p2': 0.6495711890791345,
                    'dropout_p3': 0.2999948874864592,
                    'l1': 0.000143732747690102,
                    'l2': 1.733194549361614e-05,
                    'max_col_norm1': 5.6235068265247214,
                    'max_col_norm2': 0.0,
                    'max_col_norm3': 0.5767194319278677},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.19293831216610124,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-27.550887096194987,
                                           27.550887096194987),
                           'translation_range': (-16.194982136522327,
                                                 16.194982136522327),
                           'zoom_range': (0.4892957176245554,
                                          2.0437538363401666)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
