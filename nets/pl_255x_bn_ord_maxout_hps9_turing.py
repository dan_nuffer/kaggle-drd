#job_id = 9
hyperparameters = \
{u'batch_size': 193,
 u'conv10_num_filters': 488,
 u'conv1_num_filters': 50,
 u'conv2_num_filters': 31,
 u'conv3_num_filters': 92,
 u'conv4_num_filters': 122,
 u'conv5_num_filters': 248,
 u'conv6_num_filters': 168,
 u'conv7_num_filters': 196,
 u'conv8_num_filters': 272,
 u'conv9_num_filters': 496,
 u'fc1_dropout_p': 0.85,
 u'fc1_max_col_norm': 16.26875,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 6752,
 u'fc2_dropout_p': 0.35,
 u'fc2_max_col_norm': 3.83125,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 7712,
 u'fc3_dropout_p': 0.25,
 u'fc3_max_col_norm': 18.75625,
 u'learning_rate': 0.03131875,
 u'learning_rate_jitter': 4.75,
 u'train_sample_rate': 0.4437499999999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
