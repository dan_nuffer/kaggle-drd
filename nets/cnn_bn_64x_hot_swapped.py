import numpy as np
import theano.tensor as T
import data

from lasagne.layers import DenseLayer
import lasagne as nn
from batch_normalization_layer import batch_norm
from load import ZmuvRescaledDataLoader
import nn_plankton
import tta
import utils

hyperparameters = None
data_dir = None
input_side_len = None
patch_size = None
augmentation_params = None
batch_size = None
chunk_size = None
momentum = None
num_examples = None
learning_rate_method = None
# Stop training if the loss hasn't improved within convergence_patience_epochs of moving_average(loss, convergence_window_epochs). Requires num_examples.
convergence_patience_epochs = None
convergence_window_epochs = None
num_chunks_train = None
divergence_threshold = None
validate_every = None
save_every = None
augmentation_transforms_test = None
data_loader = None

validate_train = False

hyperparameters = \
{'data_params': {'batch_size': 256,
                 'chunk_size': 256 * 16,
                 'input_side_len': 63},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 0),
                           'shear_range': (-12.380037740463948,
                                           12.380037740463948),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.704438464523333,
                                          1.4195704101374735)},
 'learning': {'learning_rate': 0.016757507007876708,
              'momentum': 0.8414962130680428},
 'regularization': {'dropout_p1': 0.5,
                    'dropout_p2': 0.5,
                    'l1': 0.0,
                    'l2': 0.0,
                    'max_col_norm1': 0.0,
                    'max_col_norm2': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-12.380037740463948,
                                           12.380037740463948),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.704438464523333,
                                          1.4195704101374735)}}


def set_hyperparameters(hyperparameters_arg):
    global hyperparameters
    hyperparameters = hyperparameters_arg
    global data_dir
    data_dir = "data-64x64"
    global input_side_len
    input_side_len = hyperparameters['data_params']['input_side_len']
    global patch_size
    patch_size = (input_side_len, input_side_len, 3)
    global augmentation_params
    augmentation_params = hyperparameters['train_data_processing']
    global batch_size
    batch_size = hyperparameters['data_params']['batch_size']
    global chunk_size
    chunk_size = hyperparameters['data_params']['chunk_size']
    global momentum
    momentum = hyperparameters['learning']['momentum']
    global num_examples
    num_examples = 88702
    global learning_rate_method
    learning_rate_method = "hot_swapped"
    global convergence_patience_epochs
    convergence_patience_epochs = 3
    global convergence_window_epochs
    convergence_window_epochs = 10
    global num_chunks_train
    num_chunks_train = 10000
    global divergence_threshold
    divergence_threshold = 26.0
    global validate_every
    validate_every = 1 * num_examples // chunk_size # about once per 1 epochs
    global save_every
    save_every = 1
    global augmentation_transforms_test
    augmentation_transforms_test = tta.build_quasirandom_transforms(
        num_transforms = hyperparameters['eval_data_processing']['num_transforms'],
        zoom_range = hyperparameters['eval_data_processing']['zoom_range'],
        rotation_range = hyperparameters['eval_data_processing']['rotation_range'],
        shear_range = hyperparameters['eval_data_processing']['shear_range'],
        translation_range = hyperparameters['eval_data_processing']['translation_range'],
        do_flip=hyperparameters['eval_data_processing']['do_flip'],
        allow_stretch=hyperparameters['eval_data_processing']['allow_stretch'])
    global data_loader

    def estimate_scale(img):
        return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]

    data_loader = ZmuvRescaledDataLoader(data_dir=data_dir, estimate_scale=estimate_scale,
                                         num_chunks_train=num_chunks_train,
                                         patch_size=patch_size, chunk_size=chunk_size,
                                         augmentation_params=augmentation_params,
                                         augmentation_transforms_test=augmentation_transforms_test,
                                         predict_mode='ordinal_regression',
                                         num_classes=data.num_classes)


set_hyperparameters(hyperparameters)


def build_model():
    Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
    MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l1a = batch_norm(Conv2DLayer(l0, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l4f = nn.layers.flatten(l2)

    def none_for_zero(x):
        return x if x != 0. else None

    max_col_norm2 = none_for_zero(hyperparameters['regularization']['max_col_norm1'])
    l6 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=hyperparameters['regularization']['dropout_p1']), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu, max_col_norm=max_col_norm2))

    max_col_norm3 = none_for_zero(hyperparameters['regularization']['max_col_norm2'])
    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=hyperparameters['regularization']['dropout_p2']), num_units=data_loader.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0), max_col_norm=max_col_norm3))

    return [l0], l7

def build_objective(l_ins, l_out):
    reg_f = hyperparameters['regularization']['l1'] * nn.regularization.l1(l_out) + \
            hyperparameters['regularization']['l2'] * nn.regularization.l2(l_out)
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy, regularization_function=reg_f)
