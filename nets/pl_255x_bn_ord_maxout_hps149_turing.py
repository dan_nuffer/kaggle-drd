#job_id = 149
hyperparameters = \
{u'batch_size': 15,
 u'conv10_num_filters': 237,
 u'conv1_num_filters': 54,
 u'conv2_num_filters': 32,
 u'conv3_num_filters': 85,
 u'conv4_num_filters': 119,
 u'conv5_num_filters': 177,
 u'conv6_num_filters': 253,
 u'conv7_num_filters': 188,
 u'conv8_num_filters': 469,
 u'conv9_num_filters': 377,
 u'fc1_dropout_p': 0.734375,
 u'fc1_max_col_norm': 7.018359374999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1842,
 u'fc2_dropout_p': 0.521875,
 u'fc2_max_col_norm': 7.795703124999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3606,
 u'fc3_dropout_p': 0.484375,
 u'fc3_max_col_norm': 9.350390625,
 u'learning_rate': 0.017660546875,
 u'learning_rate_jitter': 1.796875,
 u'train_sample_rate': 0.587890625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
