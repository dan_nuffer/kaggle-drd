#job_id = 42
hyperparameters = \
{u'batch_size': 753,
 u'conv10_num_filters': 338,
 u'conv1_num_filters': 38,
 u'conv2_num_filters': 27,
 u'conv3_num_filters': 79,
 u'conv4_num_filters': 115,
 u'conv5_num_filters': 198,
 u'conv6_num_filters': 238,
 u'conv7_num_filters': 127,
 u'conv8_num_filters': 356,
 u'conv9_num_filters': 260,
 u'fc1_dropout_p': 0.4375,
 u'fc1_max_col_norm': 9.7390625,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 5672,
 u'fc2_dropout_p': 0.36250000000000004,
 u'fc2_max_col_norm': 17.8234375,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 1352,
 u'fc3_dropout_p': 0.7875,
 u'fc3_max_col_norm': 14.092187499999998,
 u'learning_rate': 0.0578546875,
 u'learning_rate_jitter': 4.0625,
 u'train_sample_rate': 0.48593749999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
