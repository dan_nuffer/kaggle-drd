#job_id = 209
hyperparameters = \
{u'batch_size': 36,
 u'conv10_num_filters': 379,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 46,
 u'conv3_num_filters': 110,
 u'conv4_num_filters': 59,
 u'conv5_num_filters': 243,
 u'conv6_num_filters': 200,
 u'conv7_num_filters': 75,
 u'conv8_num_filters': 455,
 u'conv9_num_filters': 383,
 u'fc1_dropout_p': 0.503125,
 u'fc1_max_col_norm': 4.064453125,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2430,
 u'fc2_dropout_p': 0.690625,
 u'fc2_max_col_norm': 13.548046874999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 722,
 u'fc3_dropout_p': 0.240625,
 u'fc3_max_col_norm': 9.194921874999999,
 u'learning_rate': 0.094926953125,
 u'learning_rate_jitter': 2.390625,
 u'train_sample_rate': 0.18710937499999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
