#job_id = 286
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 164,
 u'conv1_num_filters': 41,
 u'conv2_num_filters': 28,
 u'conv3_num_filters': 48,
 u'conv4_num_filters': 160,
 u'conv5_num_filters': 258,
 u'conv6_num_filters': 88,
 u'conv7_num_filters': 288,
 u'conv8_num_filters': 293,
 u'conv9_num_filters': 720,
 u'fc1_dropout_p': 0.7660149540069137,
 u'fc1_max_col_norm': 29.352990365584986,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 25.0,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1679,
 u'fc3_dropout_p': 0.3920623783577546,
 u'fc3_max_col_norm': 8.883483511441625,
 u'learning_rate': 0.1,
 u'learning_rate_jitter': 4.755958231118715,
 u'train_sample_rate': 0.5,
 u'training_time': 36.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
