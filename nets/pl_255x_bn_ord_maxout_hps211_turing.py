#job_id = 211
hyperparameters = \
{u'batch_size': 115,
 u'conv10_num_filters': 234,
 u'conv1_num_filters': 52,
 u'conv2_num_filters': 40,
 u'conv3_num_filters': 102,
 u'conv4_num_filters': 71,
 u'conv5_num_filters': 227,
 u'conv6_num_filters': 248,
 u'conv7_num_filters': 99,
 u'conv8_num_filters': 359,
 u'conv9_num_filters': 351,
 u'fc1_dropout_p': 0.203125,
 u'fc1_max_col_norm': 11.526953124999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1086,
 u'fc2_dropout_p': 0.790625,
 u'fc2_max_col_norm': 16.035546874999998,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1170,
 u'fc3_dropout_p': 0.140625,
 u'fc3_max_col_norm': 16.657421875,
 u'learning_rate': 0.082439453125,
 u'learning_rate_jitter': 3.890625,
 u'train_sample_rate': 0.5246093749999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
