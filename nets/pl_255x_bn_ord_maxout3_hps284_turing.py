#job_id = 284
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 30,
 u'conv3_num_filters': 84,
 u'conv4_num_filters': 34,
 u'conv5_num_filters': 266,
 u'conv6_num_filters': 288,
 u'conv7_num_filters': 281,
 u'conv8_num_filters': 478,
 u'conv9_num_filters': 417,
 u'fc1_dropout_p': 0.6324320570548352,
 u'fc1_max_col_norm': 27.601006400580065,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.18733710952708665,
 u'fc2_max_col_norm': 14.589267400700505,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 664,
 u'fc3_dropout_p': 0.31950714547220804,
 u'fc3_max_col_norm': 15.863302297587504,
 u'learning_rate': 0.04224853112547277,
 u'learning_rate_jitter': 4.960927517910174,
 u'train_sample_rate': 0.5,
 u'training_time': 33.70288090738019}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
