#job_id = 301
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 53,
 u'conv1_num_filters': 65,
 u'conv2_num_filters': 32,
 u'conv3_num_filters': 32,
 u'conv4_num_filters': 125,
 u'conv5_num_filters': 263,
 u'conv6_num_filters': 158,
 u'conv7_num_filters': 259,
 u'conv8_num_filters': 360,
 u'conv9_num_filters': 613,
 u'fc1_dropout_p': 0.1450637215874821,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 5120,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 8.749151086502508,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 584,
 u'fc3_dropout_p': 0.49434705960977865,
 u'fc3_max_col_norm': 28.527955874958003,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.881849759375705,
 u'train_sample_rate': 0.25,
 u'training_time': 24.020558694473248}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
