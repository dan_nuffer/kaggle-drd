#job_id = 299
hyperparameters = \
{u'batch_size': 2,
 u'conv10_num_filters': 93,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 44,
 u'conv3_num_filters': 60,
 u'conv4_num_filters': 129,
 u'conv5_num_filters': 177,
 u'conv6_num_filters': 193,
 u'conv7_num_filters': 450,
 u'conv8_num_filters': 451,
 u'conv9_num_filters': 482,
 u'fc1_dropout_p': 0.6362572980792832,
 u'fc1_max_col_norm': 17.998962525932264,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 673,
 u'fc2_dropout_p': 0.05101733300408136,
 u'fc2_max_col_norm': 20.84422819275474,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.3312640379789036,
 u'fc3_max_col_norm': 24.8692312781252,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 6.0,
 u'train_sample_rate': 0.32345717872854884,
 u'training_time': 25.3987301457192}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
