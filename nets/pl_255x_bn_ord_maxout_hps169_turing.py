#job_id = 169
hyperparameters = \
{u'batch_size': 57,
 u'conv10_num_filters': 412,
 u'conv1_num_filters': 53,
 u'conv2_num_filters': 34,
 u'conv3_num_filters': 92,
 u'conv4_num_filters': 75,
 u'conv5_num_filters': 131,
 u'conv6_num_filters': 190,
 u'conv7_num_filters': 106,
 u'conv8_num_filters': 297,
 u'conv9_num_filters': 453,
 u'fc1_dropout_p': 0.296875,
 u'fc1_max_col_norm': 17.279296875,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2682,
 u'fc2_dropout_p': 0.234375,
 u'fc2_max_col_norm': 13.703515624999998,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2654,
 u'fc3_dropout_p': 0.846875,
 u'fc3_max_col_norm': 15.880078124999999,
 u'learning_rate': 0.044196484375000004,
 u'learning_rate_jitter': 2.484375,
 u'train_sample_rate': 0.37695312499999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
