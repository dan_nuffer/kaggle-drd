#job_id = 38
hyperparameters = \
{u'batch_size': 945,
 u'conv10_num_filters': 458,
 u'conv1_num_filters': 57,
 u'conv2_num_filters': 30,
 u'conv3_num_filters': 75,
 u'conv4_num_filters': 109,
 u'conv5_num_filters': 206,
 u'conv6_num_filters': 150,
 u'conv7_num_filters': 67,
 u'conv8_num_filters': 276,
 u'conv9_num_filters': 404,
 u'fc1_dropout_p': 0.48750000000000004,
 u'fc1_max_col_norm': 15.9578125,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1352,
 u'fc2_dropout_p': 0.21250000000000002,
 u'fc2_max_col_norm': 19.0671875,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 872,
 u'fc3_dropout_p': 0.5375,
 u'fc3_max_col_norm': 12.8484375,
 u'learning_rate': 0.06409843750000001,
 u'learning_rate_jitter': 3.3125,
 u'train_sample_rate': 0.8796875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
