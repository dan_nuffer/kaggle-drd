#job_id = 253
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 425,
 u'conv1_num_filters': 69,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 56,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 133,
 u'conv6_num_filters': 312,
 u'conv7_num_filters': 319,
 u'conv8_num_filters': 512,
 u'conv9_num_filters': 371,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 15.033145523799922,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 4614,
 u'fc2_dropout_p': 0.10965334404743764,
 u'fc2_max_col_norm': 2.1526021619451896,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 3574,
 u'fc3_dropout_p': 0.8044384847120181,
 u'fc3_max_col_norm': 4.937351668734236,
 u'learning_rate': 0.03842112513334798,
 u'learning_rate_jitter': 5.14131871576692,
 u'train_sample_rate': 0.010927877989650775}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
