#job_id = 223
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 52,
 u'conv2_num_filters': 39,
 u'conv3_num_filters': 77,
 u'conv4_num_filters': 106,
 u'conv5_num_filters': 251,
 u'conv6_num_filters': 216,
 u'conv7_num_filters': 226,
 u'conv8_num_filters': 312,
 u'conv9_num_filters': 495,
 u'fc1_dropout_p': 0.7944577192797156,
 u'fc1_max_col_norm': 14.941602425131586,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3805,
 u'fc2_dropout_p': 0.13160345377630067,
 u'fc2_max_col_norm': 11.171608001370378,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2575,
 u'fc3_dropout_p': 0.4593317621633479,
 u'fc3_max_col_norm': 1.774765775368832,
 u'learning_rate': 0.016773638108670188,
 u'learning_rate_jitter': 4.975835309424861,
 u'train_sample_rate': 0.9499877740383897}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
