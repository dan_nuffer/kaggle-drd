from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import BatchNormalizationLayer, batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import dihedral
import tmp_dnn
import tta
from utils import polynomial_decay
import utils

pre_init_path = "metadata/cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best.pkl"

data_dir = "data-256x256"
patch_size = (255, 255, 3)
augmentation_params = {
    'zoom_range': (1 / 1.6, 1.6),
    'rotation_range': (0, 360),
    'shear_range': (-20, 20),
    'translation_range': (-10, 10),
    'do_flip': True,
    'allow_stretch': 1.3,
}

batch_size = 24
chunk_size = 2048

momentum = 0.9

num_examples = 90335 + 10535 + 53575  # train (upsampled) + val (pseudo) + test (pseudo)
# these two values are from examining the loss curve from training the initial network
learning_rate_schedule = utils.make_full_triangle2_schedule(num_examples, chunk_size, 2.0e-3, 6.0e-3)
num_chunks_train = len(learning_rate_schedule)


divergence_threshold=26.0

validate_every = 100

save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 360),
    'shear_range': (-10, 10),
    'translation_range': (-8, 8),
    'do_flip': True,
    'allow_stretch': 1.2,
})


test_pred_file = "predictions/test--cnn_batchnormdense_3channel_255patch_ordinal_l2--cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best--avg-probs.npy"
valid_pred_file = "predictions/valid--cnn_batchnormdense_3channel_255patch_ordinal_l2--cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best--avg-probs.npy"

data_loader = load.PseudolabelingZmuvRescaledDataLoader(
    test_pred_file=test_pred_file,
    train_sample_weight=1. / 2., # TODO: make this somthing else? like 1/3? 2/3? hyperoptimize it?
    valid_pred_file=valid_pred_file,
    data_dir=data_dir,
    estimate_scale=estimate_scale,
    num_chunks_train=num_chunks_train,
    patch_size=patch_size,
    chunk_size=chunk_size,
    augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression",
    color_space="rgb")

Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))

    l1a = batch_norm(Conv2DLayer(l0, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=0.5), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0)))

    return [l0], l7

def build_objective(l_ins, l_out):
    reg_lambda = .00003
    reg_f = reg_lambda * l2(l_out)
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy, regularization_function=reg_f)

