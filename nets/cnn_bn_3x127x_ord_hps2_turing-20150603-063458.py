hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.14559400889874477,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 312.48638239097465),
                          'shear_range': (-31.56043916984264,
                                          31.56043916984264),
                          'translation_range': (-2.3385490558790245,
                                                2.3385490558790245),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.30689935417603353,
              'momentum': 0.9871858308595279},
 'regularization': {'dropout_p1': 0.5062722303062078,
                    'dropout_p2': 0.13588621504473722,
                    'dropout_p3': 0.38964542858119444,
                    'l1': 2.99659074010232e-05,
                    'l2': 0.0,
                    'max_col_norm1': 0.639358885369539,
                    'max_col_norm2': 0.8964077169392337,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-5.699143983947303,
                                           5.699143983947303),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.6399643237017821,
                                          1.5625871051930567)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
