#job_id = 8
hyperparameters = \
{u'batch_size': 129,
 u'conv10_num_filters': 272,
 u'conv1_num_filters': 36,
 u'conv2_num_filters': 34,
 u'conv3_num_filters': 104,
 u'conv4_num_filters': 92,
 u'conv5_num_filters': 208,
 u'conv6_num_filters': 240,
 u'conv7_num_filters': 184,
 u'conv8_num_filters': 288,
 u'conv9_num_filters': 416,
 u'fc1_dropout_p': 0.2,
 u'fc1_max_col_norm': 2.5875,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1472,
 u'fc2_dropout_p': 0.8,
 u'fc2_max_col_norm': 17.5125,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 5312,
 u'fc3_dropout_p': 0.6,
 u'fc3_max_col_norm': 2.5875,
 u'learning_rate': 0.0625375,
 u'learning_rate_jitter': 2.5,
 u'train_sample_rate': 0.16249999999999998}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
