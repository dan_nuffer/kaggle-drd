hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.7538753319536478,
                          'color_shift_sigma': 0.08483904705487,
                          'contrast_noise_sigma': 0.04446452201803597,
                          'do_flip': True,
                          'num_transforms': 65,
                          'per_pixel_gaussian_noise_sigma': 0.07628788813885652,
                          'rotation_range': (0, 41.85900040024712),
                          'shear_range': (-15.086320350145712,
                                          15.086320350145712),
                          'translation_range': (-3.7429798210014398,
                                                3.7429798210014398),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.17579425400393286,
                           'do_flip': True,
                           'num_transforms': 21,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-2.352860941402013,
                                           2.352860941402013),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (1.0, 1.0)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
