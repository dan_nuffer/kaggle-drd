hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.11370599070731251,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 107.89000166153404),
                          'shear_range': (-30.812004758465545,
                                          30.812004758465545),
                          'translation_range': (-9.914323622400378,
                                                9.914323622400378),
                          'zoom_range': (0.532623634298293,
                                         1.877498360202235)},
 'learning': {'learning_rate': 1.8234703333119522,
              'momentum': 0.7815059610128139},
 'regularization': {'dropout_p1': 0.6047505548788566,
                    'dropout_p2': 0.28901458414344816,
                    'dropout_p3': 0.873425832723255,
                    'l1': 0.0003832071421285099,
                    'l2': 0.0,
                    'max_col_norm1': 9.251209995381114,
                    'max_col_norm2': 7.702505117322814,
                    'max_col_norm3': 7.061255218081893},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-20.755378195447488,
                                           20.755378195447488),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.654403707969943,
                                          1.528108700823453)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
