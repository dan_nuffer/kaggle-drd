#job_id = 257
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 23,
 u'conv3_num_filters': 74,
 u'conv4_num_filters': 70,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 382,
 u'conv7_num_filters': 378,
 u'conv8_num_filters': 196,
 u'conv9_num_filters': 388,
 u'fc1_dropout_p': 0.6551553969945144,
 u'fc1_max_col_norm': 16.27323801756279,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 512,
 u'fc2_dropout_p': 0.19579833689857234,
 u'fc2_max_col_norm': 19.966575694546083,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 512,
 u'fc3_dropout_p': 0.2538332949049389,
 u'fc3_max_col_norm': 16.70667596174553,
 u'learning_rate': 0.013756776772079158,
 u'learning_rate_jitter': 5.530961555025204,
 u'train_sample_rate': 0.01094061500137864}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
