#job_id = 238
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 222,
 u'conv1_num_filters': 71,
 u'conv2_num_filters': 25,
 u'conv3_num_filters': 66,
 u'conv4_num_filters': 83,
 u'conv5_num_filters': 215,
 u'conv6_num_filters': 164,
 u'conv7_num_filters': 237,
 u'conv8_num_filters': 198,
 u'conv9_num_filters': 510,
 u'fc1_dropout_p': 0.8803451929058221,
 u'fc1_max_col_norm': 0.7944552534692233,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 5120,
 u'fc2_dropout_p': 0.1,
 u'fc2_max_col_norm': 11.58134555208748,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 2278,
 u'fc3_dropout_p': 0.833918191480735,
 u'fc3_max_col_norm': 13.904706248649267,
 u'learning_rate': 0.014771992904603036,
 u'learning_rate_jitter': 5.8041720541435575,
 u'train_sample_rate': 0.5325558140726987}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
