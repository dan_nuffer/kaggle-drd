#job_id = 75
hyperparameters = \
{u'batch_size': 124,
 u'conv10_num_filters': 209,
 u'conv1_num_filters': 63,
 u'conv2_num_filters': 59,
 u'conv3_num_filters': 103,
 u'conv4_num_filters': 34,
 u'conv5_num_filters': 199,
 u'conv6_num_filters': 199,
 u'conv7_num_filters': 162,
 u'conv8_num_filters': 490,
 u'conv9_num_filters': 478,
 u'fc1_dropout_p': 0.25625,
 u'fc1_max_col_norm': 18.91171875,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3620,
 u'fc2_dropout_p': 0.80625,
 u'fc2_max_col_norm': 15.491406249999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 1996,
 u'fc3_dropout_p': 0.49375,
 u'fc3_max_col_norm': 18.28984375,
 u'learning_rate': 0.04614765625,
 u'learning_rate_jitter': 2.53125,
 u'train_sample_rate': 0.8585937499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
