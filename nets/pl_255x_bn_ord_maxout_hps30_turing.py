#job_id = 30
hyperparameters = \
{u'batch_size': 801,
 u'conv10_num_filters': 308,
 u'conv1_num_filters': 39,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 70,
 u'conv4_num_filters': 59,
 u'conv5_num_filters': 244,
 u'conv6_num_filters': 172,
 u'conv7_num_filters': 214,
 u'conv8_num_filters': 408,
 u'conv9_num_filters': 440,
 u'fc1_dropout_p': 0.5750000000000001,
 u'fc1_max_col_norm': 19.378125,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3632,
 u'fc2_dropout_p': 0.875,
 u'fc2_max_col_norm': 3.209375,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3152,
 u'fc3_dropout_p': 0.325,
 u'fc3_max_col_norm': 0.7218749999999999,
 u'learning_rate': 0.078146875,
 u'learning_rate_jitter': 3.625,
 u'train_sample_rate': 0.696875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
