#job_id = 204
hyperparameters = \
{u'batch_size': 68,
 u'conv10_num_filters': 376,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 115,
 u'conv4_num_filters': 90,
 u'conv5_num_filters': 167,
 u'conv6_num_filters': 227,
 u'conv7_num_filters': 125,
 u'conv8_num_filters': 401,
 u'conv9_num_filters': 477,
 u'fc1_dropout_p': 0.521875,
 u'fc1_max_col_norm': 0.488671875,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3466,
 u'fc2_dropout_p': 0.259375,
 u'fc2_max_col_norm': 8.106640624999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 1870,
 u'fc3_dropout_p': 0.12187500000000001,
 u'fc3_max_col_norm': 17.745703125,
 u'learning_rate': 0.009855859375,
 u'learning_rate_jitter': 3.359375,
 u'train_sample_rate': 0.742578125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
