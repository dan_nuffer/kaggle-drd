#job_id = 249
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 273,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 43,
 u'conv3_num_filters': 59,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 210,
 u'conv6_num_filters': 204,
 u'conv7_num_filters': 292,
 u'conv8_num_filters': 327,
 u'conv9_num_filters': 414,
 u'fc1_dropout_p': 0.785592559647207,
 u'fc1_max_col_norm': 16.104901298078342,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 5120,
 u'fc2_dropout_p': 0.8998506829206452,
 u'fc2_max_col_norm': 19.476608582325174,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2593,
 u'fc3_dropout_p': 0.1252731638619044,
 u'fc3_max_col_norm': 5.807731335282074,
 u'learning_rate': 0.038907669526295104,
 u'learning_rate_jitter': 5.962107536499365,
 u'train_sample_rate': 0.6661616377892832}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
