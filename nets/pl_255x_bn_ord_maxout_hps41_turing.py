#job_id = 41
hyperparameters = \
{u'batch_size': 241,
 u'conv10_num_filters': 146,
 u'conv1_num_filters': 55,
 u'conv2_num_filters': 51,
 u'conv3_num_filters': 111,
 u'conv4_num_filters': 66,
 u'conv5_num_filters': 134,
 u'conv6_num_filters': 174,
 u'conv7_num_filters': 223,
 u'conv8_num_filters': 484,
 u'conv9_num_filters': 388,
 u'fc1_dropout_p': 0.8375,
 u'fc1_max_col_norm': 19.6890625,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1832,
 u'fc2_dropout_p': 0.7625000000000001,
 u'fc2_max_col_norm': 7.873437499999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 5192,
 u'fc3_dropout_p': 0.38750000000000007,
 u'fc3_max_col_norm': 4.1421874999999995,
 u'learning_rate': 0.0079046875,
 u'learning_rate_jitter': 2.0625,
 u'train_sample_rate': 0.9359375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
