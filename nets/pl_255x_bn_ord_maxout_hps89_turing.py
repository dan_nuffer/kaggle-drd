#job_id = 89
hyperparameters = \
{u'batch_size': 24,
 u'conv10_num_filters': 461,
 u'conv1_num_filters': 48,
 u'conv2_num_filters': 57,
 u'conv3_num_filters': 84,
 u'conv4_num_filters': 43,
 u'conv5_num_filters': 155,
 u'conv6_num_filters': 179,
 u'conv7_num_filters': 131,
 u'conv8_num_filters': 322,
 u'conv9_num_filters': 310,
 u'fc1_dropout_p': 0.48125000000000007,
 u'fc1_max_col_norm': 19.53359375,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1492,
 u'fc2_dropout_p': 0.63125,
 u'fc2_max_col_norm': 4.9195312499999995,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 1884,
 u'fc3_dropout_p': 0.61875,
 u'fc3_max_col_norm': 3.9867187499999996,
 u'learning_rate': 0.03053828125,
 u'learning_rate_jitter': 2.40625,
 u'train_sample_rate': 0.09921875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
