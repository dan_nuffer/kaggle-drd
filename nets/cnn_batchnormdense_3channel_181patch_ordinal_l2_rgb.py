from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import BatchNormalizationLayer, batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import dihedral
import tmp_dnn
import tta
from utils import polynomial_decay

data_dir = "data-256x256"
patch_size = (181, 181, 3)
augmentation_params = {
    'zoom_range': (1 / 1.6, 1.6),
    'rotation_range': (0, 360),
    'shear_range': (-20, 20),
    'translation_range': (-10, 10),
    'do_flip': True,
    'allow_stretch': 1.3,
}

batch_size = 60
chunk_size = 2048

momentum = 0.9
learning_rate = 0.05
learning_rate_decay = 0.90
learning_rate_patience = 20
learning_rate_rolling_mean_window = 60
learning_rate_improvement = 0.999
num_chunks_train=40000
divergence_threshold=26.0

validate_every = 50

save_every = 1


def estimate_scale(img):
    #print "img.shape:", str(img.shape), "patch_size:", str(patch_size), "np.maximum(img.shape[0], img.shape[1]) / patch_size[0]:", np.maximum(img.shape[0], img.shape[1]) / float(patch_size[0])
    # Note that this actually has a bug and always returns 1 - but that's how the net was trained so I'm leaving it.
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    #return np.maximum(img.shape[0], img.shape[1]) / float(patch_size[0])
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 360),
    'shear_range': (-10, 10),
    'translation_range': (-8, 8),
    'do_flip': True,
    'allow_stretch': 1.2,
})



data_loader = load.ZmuvRescaledDataLoader(
    estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
    patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression",
    color_space="rgb")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))

    l1a = batch_norm(Conv2DLayer(l0, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=0.5), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0)))

    return [l0], l7

def build_objective(l_ins, l_out):
    reg_lambda = .0001
    reg_f = reg_lambda * l2(l_out)
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy, regularization_function=reg_f)

