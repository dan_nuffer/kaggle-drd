#job_id = 302
hyperparameters = \
{u'batch_size': 8,
 u'conv10_num_filters': 59,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 21,
 u'conv3_num_filters': 128,
 u'conv4_num_filters': 41,
 u'conv5_num_filters': 315,
 u'conv6_num_filters': 64,
 u'conv7_num_filters': 275,
 u'conv8_num_filters': 336,
 u'conv9_num_filters': 420,
 u'fc1_dropout_p': 0.5288656073914583,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 125,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 11.84562390891752,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 2834,
 u'fc3_dropout_p': 0.2532151464678154,
 u'fc3_max_col_norm': 20.64889235998394,
 u'learning_rate': 0.007895547787462435,
 u'learning_rate_jitter': 3.9567368349785035,
 u'train_sample_rate': 0.25,
 u'training_time': 35.9997071318426}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
