#job_id = 53
hyperparameters = \
{u'batch_size': 59,
 u'conv10_num_filters': 494,
 u'conv1_num_filters': 33,
 u'conv2_num_filters': 25,
 u'conv3_num_filters': 73,
 u'conv4_num_filters': 88,
 u'conv5_num_filters': 178,
 u'conv6_num_filters': 194,
 u'conv7_num_filters': 181,
 u'conv8_num_filters': 508,
 u'conv9_num_filters': 444,
 u'fc1_dropout_p': 0.8625,
 u'fc1_max_col_norm': 10.360937499999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3256,
 u'fc2_dropout_p': 0.6375,
 u'fc2_max_col_norm': 15.9578125,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1464,
 u'fc3_dropout_p': 0.5625,
 u'fc3_max_col_norm': 14.714062499999999,
 u'learning_rate': 0.0360015625,
 u'learning_rate_jitter': 2.6875,
 u'train_sample_rate': 0.7390625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
