#job_id = 152
hyperparameters = \
{u'batch_size': 47,
 u'conv10_num_filters': 334,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 20,
 u'conv3_num_filters': 101,
 u'conv4_num_filters': 95,
 u'conv5_num_filters': 145,
 u'conv6_num_filters': 221,
 u'conv7_num_filters': 236,
 u'conv8_num_filters': 405,
 u'conv9_num_filters': 441,
 u'fc1_dropout_p': 0.13437500000000002,
 u'fc1_max_col_norm': 2.0433593749999996,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 946,
 u'fc2_dropout_p': 0.321875,
 u'fc2_max_col_norm': 12.770703124999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2710,
 u'fc3_dropout_p': 0.684375,
 u'fc3_max_col_norm': 14.325390624999999,
 u'learning_rate': 0.09258554687500001,
 u'learning_rate_jitter': 4.796875,
 u'train_sample_rate': 0.36289062499999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
