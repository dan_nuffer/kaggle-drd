#job_id = 163
hyperparameters = \
{u'batch_size': 70,
 u'conv10_num_filters': 261,
 u'conv1_num_filters': 52,
 u'conv2_num_filters': 59,
 u'conv3_num_filters': 65,
 u'conv4_num_filters': 52,
 u'conv5_num_filters': 218,
 u'conv6_num_filters': 229,
 u'conv7_num_filters': 103,
 u'conv8_num_filters': 357,
 u'conv9_num_filters': 425,
 u'fc1_dropout_p': 0.584375,
 u'fc1_max_col_norm': 3.287109375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1170,
 u'fc2_dropout_p': 0.671875,
 u'fc2_max_col_norm': 4.064453125,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 694,
 u'fc3_dropout_p': 0.43437500000000007,
 u'fc3_max_col_norm': 3.1316406249999997,
 u'learning_rate': 0.03639179687500001,
 u'learning_rate_jitter': 1.546875,
 u'train_sample_rate': 0.30664062499999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
