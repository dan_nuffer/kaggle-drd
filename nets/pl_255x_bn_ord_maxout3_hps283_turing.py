#job_id = 283
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 71,
 u'conv2_num_filters': 27,
 u'conv3_num_filters': 84,
 u'conv4_num_filters': 57,
 u'conv5_num_filters': 248,
 u'conv6_num_filters': 64,
 u'conv7_num_filters': 286,
 u'conv8_num_filters': 239,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.4242998310746696,
 u'fc1_max_col_norm': 28.431756256309303,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 20.535526668223202,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 256,
 u'fc3_dropout_p': 0.46587343939112835,
 u'fc3_max_col_norm': 2.5710574268488595,
 u'learning_rate': 0.01682800921694443,
 u'learning_rate_jitter': 4.829316465375314,
 u'train_sample_rate': 0.5,
 u'training_time': 24.039690577924052}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
