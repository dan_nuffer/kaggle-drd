#job_id = 43
hyperparameters = \
{u'batch_size': 1009,
 u'conv10_num_filters': 242,
 u'conv1_num_filters': 46,
 u'conv2_num_filters': 39,
 u'conv3_num_filters': 127,
 u'conv4_num_filters': 91,
 u'conv5_num_filters': 230,
 u'conv6_num_filters': 206,
 u'conv7_num_filters': 79,
 u'conv8_num_filters': 292,
 u'conv9_num_filters': 452,
 u'fc1_dropout_p': 0.6375,
 u'fc1_max_col_norm': 4.7640625,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 7592,
 u'fc2_dropout_p': 0.5625,
 u'fc2_max_col_norm': 2.8984375,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3272,
 u'fc3_dropout_p': 0.1875,
 u'fc3_max_col_norm': 9.117187499999998,
 u'learning_rate': 0.032879687500000004,
 u'learning_rate_jitter': 1.0625,
 u'train_sample_rate': 0.7109375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
