#job_id = 210
hyperparameters = \
{u'batch_size': 52,
 u'conv10_num_filters': 427,
 u'conv1_num_filters': 36,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 69,
 u'conv4_num_filters': 120,
 u'conv5_num_filters': 162,
 u'conv6_num_filters': 183,
 u'conv7_num_filters': 195,
 u'conv8_num_filters': 487,
 u'conv9_num_filters': 479,
 u'fc1_dropout_p': 0.603125,
 u'fc1_max_col_norm': 1.576953125,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2878,
 u'fc2_dropout_p': 0.390625,
 u'fc2_max_col_norm': 6.0855468749999995,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2962,
 u'fc3_dropout_p': 0.540625,
 u'fc3_max_col_norm': 6.707421874999999,
 u'learning_rate': 0.032489453125000005,
 u'learning_rate_jitter': 1.890625,
 u'train_sample_rate': 0.074609375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
