hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 69,
                          'per_pixel_gaussian_noise_sigma': 0.1206621658132784,
                          'rotation_range': (0, 147.7738274439704),
                          'shear_range': (-22.06696197363353,
                                          22.06696197363353),
                          'translation_range': (-6.92874855931553,
                                                6.92874855931553),
                          'zoom_range': (0.9919981517670803,
                                         1.008066394295862)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 38,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-0.0, 0.0),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.6627071660676904,
                                          1.508962104535109)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
