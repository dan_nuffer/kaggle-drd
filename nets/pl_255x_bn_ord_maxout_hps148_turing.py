#job_id = 148
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 297,
 u'conv1_num_filters': 43,
 u'conv2_num_filters': 46,
 u'conv3_num_filters': 67,
 u'conv4_num_filters': 74,
 u'conv5_num_filters': 165,
 u'conv6_num_filters': 176,
 u'conv7_num_filters': 194,
 u'conv8_num_filters': 397,
 u'conv9_num_filters': 385,
 u'fc1_dropout_p': 0.15937500000000002,
 u'fc1_max_col_norm': 7.6402343749999995,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3970,
 u'fc2_dropout_p': 0.296875,
 u'fc2_max_col_norm': 0.9550781249999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1478,
 u'fc3_dropout_p': 0.459375,
 u'fc3_max_col_norm': 4.997265624999999,
 u'learning_rate': 0.064488671875,
 u'learning_rate_jitter': 4.421875,
 u'train_sample_rate': 0.391015625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
