#job_id = 69
hyperparameters = \
{u'batch_size': 52,
 u'conv10_num_filters': 185,
 u'conv1_num_filters': 57,
 u'conv2_num_filters': 31,
 u'conv3_num_filters': 115,
 u'conv4_num_filters': 113,
 u'conv5_num_filters': 175,
 u'conv6_num_filters': 223,
 u'conv7_num_filters': 77,
 u'conv8_num_filters': 346,
 u'conv9_num_filters': 270,
 u'fc1_dropout_p': 0.40625,
 u'fc1_max_col_norm': 10.20546875,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2948,
 u'fc2_dropout_p': 0.75625,
 u'fc2_max_col_norm': 11.76015625,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3116,
 u'fc3_dropout_p': 0.44375,
 u'fc3_max_col_norm': 14.558593749999998,
 u'learning_rate': 0.00244140625,
 u'learning_rate_jitter': 2.78125,
 u'train_sample_rate': 0.23984374999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
