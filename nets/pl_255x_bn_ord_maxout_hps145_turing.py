#job_id = 145
hyperparameters = \
{u'batch_size': 43,
 u'conv10_num_filters': 394,
 u'conv1_num_filters': 34,
 u'conv2_num_filters': 58,
 u'conv3_num_filters': 116,
 u'conv4_num_filters': 49,
 u'conv5_num_filters': 133,
 u'conv6_num_filters': 144,
 u'conv7_num_filters': 242,
 u'conv8_num_filters': 461,
 u'conv9_num_filters': 321,
 u'fc1_dropout_p': 0.759375,
 u'fc1_max_col_norm': 2.665234375,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3074,
 u'fc2_dropout_p': 0.896875,
 u'fc2_max_col_norm': 15.880078124999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 582,
 u'fc3_dropout_p': 0.659375,
 u'fc3_max_col_norm': 19.922265625,
 u'learning_rate': 0.039513671875,
 u'learning_rate_jitter': 1.421875,
 u'train_sample_rate': 0.616015625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
