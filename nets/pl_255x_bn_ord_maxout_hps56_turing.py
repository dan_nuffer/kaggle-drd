#job_id = 56
hyperparameters = \
{u'batch_size': 27,
 u'conv10_num_filters': 206,
 u'conv1_num_filters': 41,
 u'conv2_num_filters': 38,
 u'conv3_num_filters': 121,
 u'conv4_num_filters': 112,
 u'conv5_num_filters': 146,
 u'conv6_num_filters': 226,
 u'conv7_num_filters': 229,
 u'conv8_num_filters': 444,
 u'conv9_num_filters': 380,
 u'fc1_dropout_p': 0.2625,
 u'fc1_max_col_norm': 15.335937499999998,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2360,
 u'fc2_dropout_p': 0.4375,
 u'fc2_max_col_norm': 1.0328125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 568,
 u'fc3_dropout_p': 0.36250000000000004,
 u'fc3_max_col_norm': 9.7390625,
 u'learning_rate': 0.060976562500000005,
 u'learning_rate_jitter': 3.6875,
 u'train_sample_rate': 0.0640625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
