#job_id = 13
hyperparameters = \
{u'batch_size': 321,
 u'conv10_num_filters': 248,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 37,
 u'conv3_num_filters': 68,
 u'conv4_num_filters': 38,
 u'conv5_num_filters': 136,
 u'conv6_num_filters': 248,
 u'conv7_num_filters': 124,
 u'conv8_num_filters': 368,
 u'conv9_num_filters': 400,
 u'fc1_dropout_p': 0.15000000000000002,
 u'fc1_max_col_norm': 13.781249999999998,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 5792,
 u'fc2_dropout_p': 0.45000000000000007,
 u'fc2_max_col_norm': 1.34375,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 992,
 u'fc3_dropout_p': 0.35,
 u'fc3_max_col_norm': 1.34375,
 u'learning_rate': 0.01883125,
 u'learning_rate_jitter': 2.25,
 u'train_sample_rate': 0.55625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
