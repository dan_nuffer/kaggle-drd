#job_id = 162
hyperparameters = \
{u'batch_size': 102,
 u'conv10_num_filters': 358,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 47,
 u'conv3_num_filters': 114,
 u'conv4_num_filters': 77,
 u'conv5_num_filters': 250,
 u'conv6_num_filters': 197,
 u'conv7_num_filters': 152,
 u'conv8_num_filters': 293,
 u'conv9_num_filters': 361,
 u'fc1_dropout_p': 0.384375,
 u'fc1_max_col_norm': 8.262109375,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2066,
 u'fc2_dropout_p': 0.47187500000000004,
 u'fc2_max_col_norm': 18.989453125,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1590,
 u'fc3_dropout_p': 0.634375,
 u'fc3_max_col_norm': 18.056640625,
 u'learning_rate': 0.061366796875000004,
 u'learning_rate_jitter': 4.546875,
 u'train_sample_rate': 0.531640625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
