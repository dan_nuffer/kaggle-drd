#job_id = 291
hyperparameters = \
{u'batch_size': 8,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 20,
 u'conv3_num_filters': 128,
 u'conv4_num_filters': 85,
 u'conv5_num_filters': 302,
 u'conv6_num_filters': 386,
 u'conv7_num_filters': 218,
 u'conv8_num_filters': 128,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.4618551786334363,
 u'fc1_max_col_norm': 29.446914216437253,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.3886442153251074,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.4053400499396208,
 u'fc3_max_col_norm': 24.324666589069484,
 u'learning_rate': 0.019539178964514754,
 u'learning_rate_jitter': 6.0,
 u'train_sample_rate': 0.6035760930131718,
 u'training_time': 35.91285209868775}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
