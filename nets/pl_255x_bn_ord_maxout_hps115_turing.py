#job_id = 115
hyperparameters = \
{u'batch_size': 78,
 u'conv10_num_filters': 287,
 u'conv1_num_filters': 54,
 u'conv2_num_filters': 24,
 u'conv3_num_filters': 77,
 u'conv4_num_filters': 60,
 u'conv5_num_filters': 201,
 u'conv6_num_filters': 145,
 u'conv7_num_filters': 68,
 u'conv8_num_filters': 382,
 u'conv9_num_filters': 458,
 u'fc1_dropout_p': 0.74375,
 u'fc1_max_col_norm': 14.869531249999998,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1436,
 u'fc2_dropout_p': 0.31875,
 u'fc2_max_col_norm': 13.936718749999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3732,
 u'fc3_dropout_p': 0.45625000000000004,
 u'fc3_max_col_norm': 16.11328125,
 u'learning_rate': 0.032099218750000005,
 u'learning_rate_jitter': 2.84375,
 u'train_sample_rate': 0.53515625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
