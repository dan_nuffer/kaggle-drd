#job_id = 3
hyperparameters = \
{u'batch_size': 769,
 u'conv10_num_filters': 224,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 52,
 u'conv3_num_filters': 80,
 u'conv4_num_filters': 104,
 u'conv5_num_filters': 224,
 u'conv6_num_filters': 224,
 u'conv7_num_filters': 208,
 u'conv8_num_filters': 448,
 u'conv9_num_filters': 320,
 u'fc1_dropout_p': 0.30000000000000004,
 u'fc1_max_col_norm': 15.024999999999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 6272,
 u'fc2_dropout_p': 0.30000000000000004,
 u'fc2_max_col_norm': 5.074999999999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 6272,
 u'fc3_dropout_p': 0.30000000000000004,
 u'fc3_max_col_norm': 5.074999999999999,
 u'learning_rate': 0.025075,
 u'learning_rate_jitter': 2.0,
 u'train_sample_rate': 0.27499999999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
