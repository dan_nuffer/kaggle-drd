#job_id = 185
hyperparameters = \
{u'batch_size': 61,
 u'conv10_num_filters': 159,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 45,
 u'conv3_num_filters': 127,
 u'conv4_num_filters': 96,
 u'conv5_num_filters': 191,
 u'conv6_num_filters': 170,
 u'conv7_num_filters': 88,
 u'conv8_num_filters': 321,
 u'conv9_num_filters': 365,
 u'fc1_dropout_p': 0.271875,
 u'fc1_max_col_norm': 1.732421875,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3242,
 u'fc2_dropout_p': 0.209375,
 u'fc2_max_col_norm': 1.887890625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1646,
 u'fc3_dropout_p': 0.47187500000000004,
 u'fc3_max_col_norm': 6.551953124999999,
 u'learning_rate': 0.003612109375,
 u'learning_rate_jitter': 1.609375,
 u'train_sample_rate': 0.9113281249999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
