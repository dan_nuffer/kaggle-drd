#job_id = 104
hyperparameters = \
{u'batch_size': 22,
 u'conv10_num_filters': 167,
 u'conv1_num_filters': 52,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 114,
 u'conv4_num_filters': 53,
 u'conv5_num_filters': 241,
 u'conv6_num_filters': 249,
 u'conv7_num_filters': 104,
 u'conv8_num_filters': 494,
 u'conv9_num_filters': 442,
 u'fc1_dropout_p': 0.69375,
 u'fc1_max_col_norm': 18.60078125,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 764,
 u'fc2_dropout_p': 0.56875,
 u'fc2_max_col_norm': 0.25546875,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2612,
 u'fc3_dropout_p': 0.70625,
 u'fc3_max_col_norm': 14.869531249999998,
 u'learning_rate': 0.05083046875,
 u'learning_rate_jitter': 1.09375,
 u'train_sample_rate': 0.4789062499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
