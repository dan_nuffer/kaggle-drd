#job_id = 269
hyperparameters = \
{u'batch_size': 8,
 u'conv10_num_filters': 81,
 u'conv1_num_filters': 26,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 88,
 u'conv4_num_filters': 111,
 u'conv5_num_filters': 128,
 u'conv6_num_filters': 131,
 u'conv7_num_filters': 298,
 u'conv8_num_filters': 512,
 u'conv9_num_filters': 471,
 u'fc1_dropout_p': 0.7569964459953837,
 u'fc1_max_col_norm': 24.230197010770457,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 256,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 10.648643561747788,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 401,
 u'fc3_dropout_p': 0.3001794722732085,
 u'fc3_max_col_norm': 20.0,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.455451085341292,
 u'train_sample_rate': 0.5043446921862265}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
