#job_id = 45
hyperparameters = \
{u'batch_size': 47,
 u'conv10_num_filters': 386,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 45,
 u'conv3_num_filters': 119,
 u'conv4_num_filters': 103,
 u'conv5_num_filters': 246,
 u'conv6_num_filters': 254,
 u'conv7_num_filters': 103,
 u'conv8_num_filters': 388,
 u'conv9_num_filters': 484,
 u'fc1_dropout_p': 0.1375,
 u'fc1_max_col_norm': 12.226562499999998,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1576,
 u'fc2_dropout_p': 0.8625,
 u'fc2_max_col_norm': 5.385937499999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2248,
 u'fc3_dropout_p': 0.2875,
 u'fc3_max_col_norm': 16.5796875,
 u'learning_rate': 0.0453671875,
 u'learning_rate_jitter': 4.5625,
 u'train_sample_rate': 0.1484375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
