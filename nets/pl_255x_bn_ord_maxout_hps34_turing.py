#job_id = 34
hyperparameters = \
{u'batch_size': 561,
 u'conv10_num_filters': 314,
 u'conv1_num_filters': 53,
 u'conv2_num_filters': 36,
 u'conv3_num_filters': 83,
 u'conv4_num_filters': 48,
 u'conv5_num_filters': 190,
 u'conv6_num_filters': 198,
 u'conv7_num_filters': 235,
 u'conv8_num_filters': 372,
 u'conv9_num_filters': 500,
 u'fc1_dropout_p': 0.5875,
 u'fc1_max_col_norm': 13.470312499999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 4232,
 u'fc2_dropout_p': 0.1125,
 u'fc2_max_col_norm': 16.5796875,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 7592,
 u'fc3_dropout_p': 0.8375,
 u'fc3_max_col_norm': 5.385937499999999,
 u'learning_rate': 0.0765859375,
 u'learning_rate_jitter': 1.8125,
 u'train_sample_rate': 0.0921875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
