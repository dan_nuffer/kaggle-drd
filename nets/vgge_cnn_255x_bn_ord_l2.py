from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import BatchNormalizationLayer, batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import dihedral
import tmp_dnn
import tta
from utils import polynomial_decay
import utils

data_dir = "data-362x362"
patch_size = (255, 255, 3)
augmentation_params = {
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-12.39, 12.38),
    'translation_range': (-20 * patch_size[0] / 127., 20 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
}

batch_size = 8
chunk_size = batch_size * 50

momentum = 0.9

num_examples = 90335
learning_rate_schedule = utils.make_full_triangle2_schedule(num_examples, chunk_size,
                                                            init_min_lr=2.514e-3, init_max_lr=6.738e-3, iter_scale=1.)
num_chunks_train = len(learning_rate_schedule)

divergence_threshold = 20.0

validate_every = 1 * num_examples // chunk_size # about once per 1 epoch

save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-10, 10),
    'translation_range': (-16 * patch_size[0] / 127., 16 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
})



data_loader = load.ZmuvRescaledDataLoader(
    estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
    patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression",
    color_space="rgb")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))

    l1a = batch_norm(Conv2DLayer(l0, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l1 = MaxPool2DLayer(l1b, pool_size=(2, 2), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l2 = MaxPool2DLayer(l2b, pool_size=(2, 2), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l3d = batch_norm(Conv2DLayer(l3c, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l3 = MaxPool2DLayer(l3d, pool_size=(2, 2), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l4d = batch_norm(Conv2DLayer(l4c, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l4 = MaxPool2DLayer(l4d, pool_size=(2, 2), stride=(2, 2))

    l5a = batch_norm(Conv2DLayer(l4, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l5b = batch_norm(Conv2DLayer(l5a, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l5c = batch_norm(Conv2DLayer(l5b, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l5d = batch_norm(Conv2DLayer(l5c, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn.nonlinearities.rectify))
    l5 = MaxPool2DLayer(l5d, pool_size=(2, 2), stride=(2, 2))
    l4f = nn.layers.flatten(l5)

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=0.5), num_units=4096, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn.nonlinearities.rectify))

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=0.5), num_units=4096, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn.nonlinearities.rectify))

    l7 = batch_norm(DenseLayer(l6, num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0)))

    return [l0], l7

def build_objective(l_ins, l_out):
    reg_lambda = 5e-4
    reg_f = reg_lambda * l2(l_out)
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy, regularization_function=reg_f)

