hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 78,
                          'per_pixel_gaussian_noise_sigma': 0.13074433906796165,
                          'rotation_range': (0, 31.467016370571372),
                          'shear_range': (-25.610223417501338,
                                          25.610223417501338),
                          'translation_range': (-1.237444140337081,
                                                1.237444140337081),
                          'zoom_range': (0.8688274988080592,
                                         1.1509764612329787)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 66,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-0.0, 0.0),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.40259379667768047,
                                          2.483893215077547)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
