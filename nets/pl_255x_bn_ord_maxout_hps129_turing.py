#job_id = 129
hyperparameters = \
{u'batch_size': 34,
 u'conv10_num_filters': 323,
 u'conv1_num_filters': 39,
 u'conv2_num_filters': 20,
 u'conv3_num_filters': 128,
 u'conv4_num_filters': 63,
 u'conv5_num_filters': 149,
 u'conv6_num_filters': 229,
 u'conv7_num_filters': 231,
 u'conv8_num_filters': 470,
 u'conv9_num_filters': 290,
 u'fc1_dropout_p': 0.51875,
 u'fc1_max_col_norm': 14.247656249999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3564,
 u'fc2_dropout_p': 0.14375000000000002,
 u'fc2_max_col_norm': 5.852343749999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3844,
 u'fc3_dropout_p': 0.63125,
 u'fc3_max_col_norm': 1.81015625,
 u'learning_rate': 0.04146484375,
 u'learning_rate_jitter': 2.21875,
 u'train_sample_rate': 0.39453124999999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
