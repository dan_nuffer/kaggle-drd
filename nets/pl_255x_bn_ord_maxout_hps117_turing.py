#job_id = 117
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 443,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 35,
 u'conv3_num_filters': 124,
 u'conv4_num_filters': 57,
 u'conv5_num_filters': 157,
 u'conv6_num_filters': 157,
 u'conv7_num_filters': 171,
 u'conv8_num_filters': 422,
 u'conv9_num_filters': 434,
 u'fc1_dropout_p': 0.56875,
 u'fc1_max_col_norm': 0.56640625,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1996,
 u'fc2_dropout_p': 0.49375,
 u'fc2_max_col_norm': 7.096093749999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 4068,
 u'fc3_dropout_p': 0.88125,
 u'fc3_max_col_norm': 0.56640625,
 u'learning_rate': 0.03522109375,
 u'learning_rate_jitter': 1.46875,
 u'train_sample_rate': 0.90078125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
