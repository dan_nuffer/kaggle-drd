hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.353112625357393,
                          'color_shift_sigma': 0.03011701868984811,
                          'contrast_noise_sigma': 0.18625198792256312,
                          'do_flip': False,
                          'num_transforms': 71,
                          'per_pixel_gaussian_noise_sigma': 0.1089861460336719,
                          'rotation_range': (0, 86.41627657830328),
                          'shear_range': (-0.0, 0.0),
                          'translation_range': (-12.041980370395137,
                                                12.041980370395137),
                          'zoom_range': (0.4953882309101508,
                                         2.0186188076425484)},
 'train_data_processing': {'allow_stretch': 1.8610869280603959,
                           'color_shift_sigma': 0.05521150034367797,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 24,
                           'per_pixel_gaussian_noise_sigma': 0.033711196869363476,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-26.17186060404639,
                                           26.17186060404639),
                           'translation_range': (-17.954249692158086,
                                                 17.954249692158086),
                           'zoom_range': (1.0, 1.0)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
