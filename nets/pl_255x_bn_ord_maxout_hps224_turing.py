#job_id = 224
hyperparameters = \
{u'batch_size': 6,
 u'conv10_num_filters': 410,
 u'conv1_num_filters': 47,
 u'conv2_num_filters': 31,
 u'conv3_num_filters': 75,
 u'conv4_num_filters': 115,
 u'conv5_num_filters': 236,
 u'conv6_num_filters': 252,
 u'conv7_num_filters': 210,
 u'conv8_num_filters': 416,
 u'conv9_num_filters': 462,
 u'fc1_dropout_p': 0.4849110775904706,
 u'fc1_max_col_norm': 19.08713185210621,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 4088,
 u'fc2_dropout_p': 0.10000018837901327,
 u'fc2_max_col_norm': 2.8132237409120138,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3262,
 u'fc3_dropout_p': 0.17738974191700324,
 u'fc3_max_col_norm': 1.948090905445158,
 u'learning_rate': 0.09670600659698005,
 u'learning_rate_jitter': 5.0,
 u'train_sample_rate': 0.6201915100751797}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
