#job_id = 141
hyperparameters = \
{u'batch_size': 27,
 u'conv10_num_filters': 153,
 u'conv1_num_filters': 47,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 107,
 u'conv4_num_filters': 110,
 u'conv5_num_filters': 246,
 u'conv6_num_filters': 193,
 u'conv7_num_filters': 73,
 u'conv8_num_filters': 429,
 u'conv9_num_filters': 289,
 u'fc1_dropout_p': 0.259375,
 u'fc1_max_col_norm': 5.152734375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3522,
 u'fc2_dropout_p': 0.796875,
 u'fc2_max_col_norm': 18.367578125,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3718,
 u'fc3_dropout_p': 0.759375,
 u'fc3_max_col_norm': 2.509765625,
 u'learning_rate': 0.002051171875,
 u'learning_rate_jitter': 3.921875,
 u'train_sample_rate': 0.278515625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
