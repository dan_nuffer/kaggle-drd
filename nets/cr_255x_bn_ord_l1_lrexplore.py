import numpy as np
import theano.tensor as T

import dihedral_fast
from lasagne.layers import DenseLayer
import lasagne as nn
from batch_normalization_layer import batch_norm
import data
import load
import nn_plankton
import dihedral
import tta
import utils

data_dir = "data-362x362"
patch_size = (255, 255, 3)
augmentation_params = {
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-12.39, 12.38),
    'translation_range': (-20 * patch_size[0] / 127., 20 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
}

batch_size = 7
chunk_size = batch_size * 50

momentum = 0.88

num_examples = 90335
num_chunks_train = 5 * num_examples / chunk_size
learning_rate_schedule = utils.initial_learning_rate_exploration(num_chunks_train)

divergence_threshold = 26.0

validate_every = num_chunks_train + 1

save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-10, 10),
    'translation_range': (-8 * patch_size[0] / 127., 8 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
})



data_loader = load.ZmuvRescaledDataLoader(
    estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
    patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression",
    color_space="rgb")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0c = dihedral.CyclicSliceLayer(l0)

    l1a = batch_norm(Conv2DLayer(l0c, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))
    l1r = dihedral_fast.CyclicConvRollLayer(l1)

    l2a = batch_norm(Conv2DLayer(l1r, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))
    l2r = dihedral_fast.CyclicConvRollLayer(l2)

    l3a = batch_norm(Conv2DLayer(l2r, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))
    l3r = dihedral_fast.CyclicConvRollLayer(l3)

    l4a = batch_norm(Conv2DLayer(l3r, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4r = dihedral_fast.CyclicConvRollLayer(l4)
    l4f = nn.layers.flatten(l4r)

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=0.25), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu, max_col_norm=4.847806))
    l5r = dihedral_fast.CyclicRollLayer(l5)

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5r, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu, max_col_norm=5.124759))
    l6m = dihedral.CyclicPoolLayer(l6, pool_function=nn_plankton.rms)

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6m, p=0.5), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0), max_col_norm=None))

    return [l0], l7

def build_objective(l_ins, l_out):
    reg_lambda = 4.4739397499527467e-07 / 10.  # / 10. because this network has roughly 10x as many parameters as the one where this value was tested - a 127x127 image
    reg_f = reg_lambda * nn.regularization.l1(l_out)
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy, regularization_function=reg_f)

