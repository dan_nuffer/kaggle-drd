#job_id = 200
hyperparameters = \
{u'batch_size': 116,
 u'conv10_num_filters': 135,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 23,
 u'conv3_num_filters': 106,
 u'conv4_num_filters': 78,
 u'conv5_num_filters': 216,
 u'conv6_num_filters': 178,
 u'conv7_num_filters': 197,
 u'conv8_num_filters': 497,
 u'conv9_num_filters': 445,
 u'fc1_dropout_p': 0.421875,
 u'fc1_max_col_norm': 7.951171874999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3018,
 u'fc2_dropout_p': 0.15937500000000002,
 u'fc2_max_col_norm': 5.619140624999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2318,
 u'fc3_dropout_p': 0.421875,
 u'fc3_max_col_norm': 0.333203125,
 u'learning_rate': 0.04731835937500001,
 u'learning_rate_jitter': 1.859375,
 u'train_sample_rate': 0.18007812499999998}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
