hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.05989082642800747,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 230.45719784546787),
                          'shear_range': (-24.940266495314148,
                                          24.940266495314148),
                          'translation_range': (-11.53363748466439,
                                                11.53363748466439),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.5955801288077058,
              'momentum': 0.640052648701539},
 'regularization': {'dropout_p1': 0.5665786892040452,
                    'dropout_p2': 0.18067207713827638,
                    'dropout_p3': 0.47459965585908065,
                    'l1': 0.0,
                    'l2': 2.4664180598107673e-07,
                    'max_col_norm1': 7.140362404753105,
                    'max_col_norm2': 0.0,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-26.026159299621284,
                                           26.026159299621284),
                           'translation_range': (-10.573267466037812,
                                                 10.573267466037812),
                           'zoom_range': (0.4209739299380537,
                                          2.37544400943581)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
