#job_id = 171
hyperparameters = \
{u'batch_size': 88,
 u'conv10_num_filters': 508,
 u'conv1_num_filters': 44,
 u'conv2_num_filters': 46,
 u'conv3_num_filters': 76,
 u'conv4_num_filters': 99,
 u'conv5_num_filters': 228,
 u'conv6_num_filters': 223,
 u'conv7_num_filters': 251,
 u'conv8_num_filters': 489,
 u'conv9_num_filters': 389,
 u'fc1_dropout_p': 0.49687500000000007,
 u'fc1_max_col_norm': 2.3542968749999997,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1786,
 u'fc2_dropout_p': 0.43437500000000007,
 u'fc2_max_col_norm': 18.678515625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1758,
 u'fc3_dropout_p': 0.646875,
 u'fc3_max_col_norm': 10.905078125,
 u'learning_rate': 0.019221484375,
 u'learning_rate_jitter': 1.484375,
 u'train_sample_rate': 0.151953125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
