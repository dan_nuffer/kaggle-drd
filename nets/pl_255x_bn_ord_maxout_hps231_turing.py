#job_id = 231
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 141,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 26,
 u'conv3_num_filters': 65,
 u'conv4_num_filters': 111,
 u'conv5_num_filters': 242,
 u'conv6_num_filters': 205,
 u'conv7_num_filters': 115,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 509,
 u'fc1_dropout_p': 0.894764913333708,
 u'fc1_max_col_norm': 16.846569405848463,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4096,
 u'fc2_dropout_p': 0.9,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3249,
 u'fc3_dropout_p': 0.5660347014575967,
 u'fc3_max_col_norm': 0.46698397995445895,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 4.8381889397726825,
 u'train_sample_rate': 0.8365969952103783}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
