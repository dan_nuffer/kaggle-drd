hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.2675969871750523,
                          'color_shift_sigma': 0.16970620765978084,
                          'contrast_noise_sigma': 0.07492939153331156,
                          'do_flip': True,
                          'num_transforms': 97,
                          'per_pixel_gaussian_noise_sigma': 0.06468496964810187,
                          'rotation_range': (0, 200.15030373880987),
                          'shear_range': (-27.491961105360883,
                                          27.491961105360883),
                          'translation_range': (-10.74608094325628,
                                                10.74608094325628),
                          'zoom_range': (0.9079008230053686,
                                         1.1014418917363256)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.07099558318710127,
                           'contrast_noise_sigma': 0.16639399310347047,
                           'do_flip': True,
                           'num_transforms': 48,
                           'per_pixel_gaussian_noise_sigma': 0.16723682202666137,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-32.98403155519789,
                                           32.98403155519789),
                           'translation_range': (-5.4705595912088185,
                                                 5.4705595912088185),
                           'zoom_range': (1.0, 1.0)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
