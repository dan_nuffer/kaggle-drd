#job_id = 54
hyperparameters = \
{u'batch_size': 123,
 u'conv10_num_filters': 302,
 u'conv1_num_filters': 50,
 u'conv2_num_filters': 50,
 u'conv3_num_filters': 105,
 u'conv4_num_filters': 39,
 u'conv5_num_filters': 242,
 u'conv6_num_filters': 130,
 u'conv7_num_filters': 85,
 u'conv8_num_filters': 380,
 u'conv9_num_filters': 316,
 u'fc1_dropout_p': 0.4625,
 u'fc1_max_col_norm': 0.41093749999999996,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1464,
 u'fc2_dropout_p': 0.23750000000000002,
 u'fc2_max_col_norm': 6.007812499999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3256,
 u'fc3_dropout_p': 0.1625,
 u'fc3_max_col_norm': 4.7640625,
 u'learning_rate': 0.08595156250000001,
 u'learning_rate_jitter': 4.6875,
 u'train_sample_rate': 0.2890625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
