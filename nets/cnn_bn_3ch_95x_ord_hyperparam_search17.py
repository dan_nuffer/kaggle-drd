from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import BatchNormalizationLayer, batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import dihedral
import tmp_dnn
import tta
from utils import polynomial_decay

# for now, copy and paste from the output of hyperparameter_search.py and remove model item.
hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.4370743841279565,
                          'color_shift_sigma': 0.18603857332590495,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 32,
                          'per_pixel_gaussian_noise_sigma': 0.031575350914027946,
                          'rotation_range': (0, 32.15391975095041),
                          'shear_range': (-33.34105127627621,
                                          33.34105127627621),
                          'translation_range': (-0.0, 0.0),
                          'zoom_range': (0.5447016806328377,
                                         1.8358672931542894)},
 'train_data_processing': {'allow_stretch': 1.165981920291994,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0004388172520205958,
                           'do_flip': True,
                           'num_transforms': 56,
                           'per_pixel_gaussian_noise_sigma': 0.08223677868934985,
                           'rotation_range': (0, 41.25808247022675),
                           'shear_range': (-10.010487518991722,
                                           10.010487518991722),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.3989643393867187,
                                          2.506489681602078)}}



data_dir = "data-128x128"
input_side_len = hyperparameters['data_params']['input_side_len']
patch_size = (input_side_len, input_side_len, 3)
augmentation_params = hyperparameters['train_data_processing']

batch_size = hyperparameters['data_params']['batch_size']
chunk_size = hyperparameters['data_params']['chunk_size']

momentum = 0.9

learning_rate = 0.2
learning_rate_decay = 0.90
learning_rate_patience = 20
learning_rate_rolling_mean_window = 30
learning_rate_improvement = 0.999
num_chunks_train = hyperparameters['data_params']['num_chunks_train']
divergence_threshold = 26.0

validate_every = 30

save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(
    num_transforms = hyperparameters['eval_data_processing']['num_transforms'],
    zoom_range = hyperparameters['eval_data_processing']['zoom_range'],
    rotation_range = hyperparameters['eval_data_processing']['rotation_range'],
    shear_range = hyperparameters['eval_data_processing']['shear_range'],
    translation_range = hyperparameters['eval_data_processing']['translation_range'],
    do_flip=hyperparameters['eval_data_processing']['do_flip'],
    allow_stretch=hyperparameters['eval_data_processing']['allow_stretch'])


data_loader = load.ZmuvRescaledDataLoader(estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
    patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer


def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0g = nn.layers.GaussianNoiseLayer(l0, sigma=hyperparameters['train_data_processing']['per_pixel_gaussian_noise_sigma'])
    l0c = nn_plankton.ContrastNoiseLayer(l0g, sigma=hyperparameters['train_data_processing']['contrast_noise_sigma'],
                                         test_sigma=hyperparameters['eval_data_processing']['contrast_noise_sigma'])
    l0cs = nn_plankton.ColorShiftNoiseLayer(l0c, sigma=hyperparameters['train_data_processing']['color_shift_sigma'],
                                            test_sigma=hyperparameters['eval_data_processing']['color_shift_sigma'])

    l1a = batch_norm(Conv2DLayer(l0cs, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=0.5), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0)))

    return [l0], l7

def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy)
