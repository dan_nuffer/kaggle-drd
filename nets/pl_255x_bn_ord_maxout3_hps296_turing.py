#job_id = 296
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 32,
 u'conv1_num_filters': 21,
 u'conv2_num_filters': 6,
 u'conv3_num_filters': 84,
 u'conv4_num_filters': 110,
 u'conv5_num_filters': 320,
 u'conv6_num_filters': 64,
 u'conv7_num_filters': 512,
 u'conv8_num_filters': 128,
 u'conv9_num_filters': 574,
 u'fc1_dropout_p': 0.8261799926535301,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 32,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 14.311351437142354,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 4800,
 u'fc3_dropout_p': 0.31818298140939627,
 u'fc3_max_col_norm': 13.745763037130772,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 1.0,
 u'train_sample_rate': 0.25,
 u'training_time': 36.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
