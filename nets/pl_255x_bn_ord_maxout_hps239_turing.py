#job_id = 239
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 35,
 u'conv2_num_filters': 19,
 u'conv3_num_filters': 48,
 u'conv4_num_filters': 69,
 u'conv5_num_filters': 156,
 u'conv6_num_filters': 209,
 u'conv7_num_filters': 239,
 u'conv8_num_filters': 302,
 u'conv9_num_filters': 512,
 u'fc1_dropout_p': 0.6128265273995757,
 u'fc1_max_col_norm': 0.1,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 5080,
 u'fc2_dropout_p': 0.33756879579484195,
 u'fc2_max_col_norm': 0.17777574525562428,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2056,
 u'fc3_dropout_p': 0.8998030603747573,
 u'fc3_max_col_norm': 3.334640196395872,
 u'learning_rate': 0.021550084307074426,
 u'learning_rate_jitter': 5.275519814338839,
 u'train_sample_rate': 0.3386443111272716}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
