#job_id = 221
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 175,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 125,
 u'conv5_num_filters': 246,
 u'conv6_num_filters': 237,
 u'conv7_num_filters': 256,
 u'conv8_num_filters': 312,
 u'conv9_num_filters': 332,
 u'fc1_dropout_p': 0.8848659128478543,
 u'fc1_max_col_norm': 18.538208067973777,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4096,
 u'fc2_dropout_p': 0.11075493325133319,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2268,
 u'fc3_dropout_p': 0.3806503292750114,
 u'fc3_max_col_norm': 2.469499346367848,
 u'learning_rate': 0.005620214186478745,
 u'learning_rate_jitter': 4.817596535802692,
 u'train_sample_rate': 0.33620904462239126}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
