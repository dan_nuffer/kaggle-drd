#job_id = 267
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 111,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 52,
 u'conv4_num_filters': 128,
 u'conv5_num_filters': 320,
 u'conv6_num_filters': 148,
 u'conv7_num_filters': 401,
 u'conv8_num_filters': 209,
 u'conv9_num_filters': 640,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 25.0,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 256,
 u'fc2_dropout_p': 0.10897968086722726,
 u'fc2_max_col_norm': 25.0,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 256,
 u'fc3_dropout_p': 0.24094244686355823,
 u'fc3_max_col_norm': 20.0,
 u'learning_rate': 0.019892942754737304,
 u'learning_rate_jitter': 5.207224445144247,
 u'train_sample_rate': 0.5036158687413617}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
