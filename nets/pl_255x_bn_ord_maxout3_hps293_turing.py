#job_id = 293
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 32,
 u'conv1_num_filters': 55,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 119,
 u'conv4_num_filters': 74,
 u'conv5_num_filters': 298,
 u'conv6_num_filters': 64,
 u'conv7_num_filters': 257,
 u'conv8_num_filters': 640,
 u'conv9_num_filters': 363,
 u'fc1_dropout_p': 0.5791406064046114,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 32,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 22.864757089588228,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 4576,
 u'fc3_dropout_p': 0.34481498836067237,
 u'fc3_max_col_norm': 24.442855588154963,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 4.288682622984174,
 u'train_sample_rate': 0.25,
 u'training_time': 30.411213382505117}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
