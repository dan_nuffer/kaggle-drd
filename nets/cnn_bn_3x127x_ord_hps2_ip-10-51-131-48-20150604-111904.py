hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.12843795647018857,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 257.93095673193324),
                          'shear_range': (-25.842889696375266,
                                          25.842889696375266),
                          'translation_range': (-11.25989621150687,
                                                11.25989621150687),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.001839048466843869,
              'momentum': 0.6484578262442405},
 'regularization': {'dropout_p1': 0.45710320682026123,
                    'dropout_p2': 0.23370229646523058,
                    'dropout_p3': 0.42576955904737523,
                    'l1': 0.03463276011792456,
                    'l2': 0.0,
                    'max_col_norm1': 3.4781913619193805,
                    'max_col_norm2': 8.75242923545867,
                    'max_col_norm3': 4.522613225576697},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-19.218172368298298,
                                           19.218172368298298),
                           'translation_range': (-9.522937805340742,
                                                 9.522937805340742),
                           'zoom_range': (0.9988974015853589,
                                          1.0011038154798393)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
