#job_id = 158
hyperparameters = \
{u'batch_size': 86,
 u'conv10_num_filters': 213,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 41,
 u'conv3_num_filters': 105,
 u'conv4_num_filters': 89,
 u'conv5_num_filters': 137,
 u'conv6_num_filters': 148,
 u'conv7_num_filters': 176,
 u'conv8_num_filters': 325,
 u'conv9_num_filters': 265,
 u'fc1_dropout_p': 0.684375,
 u'fc1_max_col_norm': 0.799609375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 722,
 u'fc2_dropout_p': 0.37187500000000007,
 u'fc2_max_col_norm': 16.501953125,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2934,
 u'fc3_dropout_p': 0.734375,
 u'fc3_max_col_norm': 0.6441406249999999,
 u'learning_rate': 0.098829296875,
 u'learning_rate_jitter': 2.046875,
 u'train_sample_rate': 0.41914062499999993}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
