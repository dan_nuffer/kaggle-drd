#job_id = 233
hyperparameters = \
{u'batch_size': 3,
 u'conv10_num_filters': 171,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 26,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 116,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 248,
 u'conv7_num_filters': 198,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 487,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 20.0,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2284,
 u'fc2_dropout_p': 0.10982374489087945,
 u'fc2_max_col_norm': 20.0,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 4096,
 u'fc3_dropout_p': 0.5045534326153237,
 u'fc3_max_col_norm': 0.1,
 u'learning_rate': 0.0019503363011954956,
 u'learning_rate_jitter': 1.4949865373442228,
 u'train_sample_rate': 0.95}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
