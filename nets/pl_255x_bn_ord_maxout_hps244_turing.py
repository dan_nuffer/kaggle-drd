#job_id = 244
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 183,
 u'conv1_num_filters': 76,
 u'conv2_num_filters': 62,
 u'conv3_num_filters': 69,
 u'conv4_num_filters': 97,
 u'conv5_num_filters': 187,
 u'conv6_num_filters': 201,
 u'conv7_num_filters': 237,
 u'conv8_num_filters': 227,
 u'conv9_num_filters': 449,
 u'fc1_dropout_p': 0.7393023719006314,
 u'fc1_max_col_norm': 19.52947198933701,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4397,
 u'fc2_dropout_p': 0.12188541940691516,
 u'fc2_max_col_norm': 5.372027758578792,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 3024,
 u'fc3_dropout_p': 0.643498926917783,
 u'fc3_max_col_norm': 3.6085512648981193,
 u'learning_rate': 0.03384615406674849,
 u'learning_rate_jitter': 5.580675968342314,
 u'train_sample_rate': 0.0679627482032996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
