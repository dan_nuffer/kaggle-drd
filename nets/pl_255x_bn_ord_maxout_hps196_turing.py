#job_id = 196
hyperparameters = \
{u'batch_size': 5,
 u'conv10_num_filters': 183,
 u'conv1_num_filters': 39,
 u'conv2_num_filters': 42,
 u'conv3_num_filters': 82,
 u'conv4_num_filters': 42,
 u'conv5_num_filters': 232,
 u'conv6_num_filters': 162,
 u'conv7_num_filters': 221,
 u'conv8_num_filters': 273,
 u'conv9_num_filters': 349,
 u'fc1_dropout_p': 0.12187500000000001,
 u'fc1_max_col_norm': 10.438671874999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1674,
 u'fc2_dropout_p': 0.659375,
 u'fc2_max_col_norm': 18.056640625,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3662,
 u'fc3_dropout_p': 0.521875,
 u'fc3_max_col_norm': 7.795703124999999,
 u'learning_rate': 0.059805859375000006,
 u'learning_rate_jitter': 1.359375,
 u'train_sample_rate': 0.29257812499999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
