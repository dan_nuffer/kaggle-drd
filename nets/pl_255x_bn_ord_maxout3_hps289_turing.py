#job_id = 289
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 67,
 u'conv2_num_filters': 53,
 u'conv3_num_filters': 49,
 u'conv4_num_filters': 136,
 u'conv5_num_filters': 254,
 u'conv6_num_filters': 318,
 u'conv7_num_filters': 336,
 u'conv8_num_filters': 480,
 u'conv9_num_filters': 493,
 u'fc1_dropout_p': 0.32511914526530306,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05000371878238732,
 u'fc2_max_col_norm': 24.948946933543358,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.3910326586514653,
 u'fc3_max_col_norm': 22.162620676853134,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 3.9720872970303427,
 u'train_sample_rate': 0.5,
 u'training_time': 33.57563057602193}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
