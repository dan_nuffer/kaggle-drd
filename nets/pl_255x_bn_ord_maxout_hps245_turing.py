#job_id = 245
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 76,
 u'conv2_num_filters': 50,
 u'conv3_num_filters': 87,
 u'conv4_num_filters': 99,
 u'conv5_num_filters': 161,
 u'conv6_num_filters': 238,
 u'conv7_num_filters': 380,
 u'conv8_num_filters': 252,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.7384707623379828,
 u'fc1_max_col_norm': 15.454770194458712,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 4520,
 u'fc2_dropout_p': 0.10703414076105218,
 u'fc2_max_col_norm': 12.580127840645696,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 2996,
 u'fc3_dropout_p': 0.3821180948969952,
 u'fc3_max_col_norm': 13.629815529269006,
 u'learning_rate': 0.0928343057024265,
 u'learning_rate_jitter': 5.260079300449323,
 u'train_sample_rate': 0.010174700299404876}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
