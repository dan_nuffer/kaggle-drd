#job_id = 156
hyperparameters = \
{u'batch_size': 31,
 u'conv10_num_filters': 189,
 u'conv1_num_filters': 50,
 u'conv2_num_filters': 26,
 u'conv3_num_filters': 126,
 u'conv4_num_filters': 58,
 u'conv5_num_filters': 226,
 u'conv6_num_filters': 140,
 u'conv7_num_filters': 67,
 u'conv8_num_filters': 501,
 u'conv9_num_filters': 473,
 u'fc1_dropout_p': 0.834375,
 u'fc1_max_col_norm': 9.505859374999998,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2290,
 u'fc2_dropout_p': 0.421875,
 u'fc2_max_col_norm': 10.283203124999998,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2262,
 u'fc3_dropout_p': 0.784375,
 u'fc3_max_col_norm': 6.8628906249999995,
 u'learning_rate': 0.055123046875000005,
 u'learning_rate_jitter': 2.296875,
 u'train_sample_rate': 0.7003906249999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
