#job_id = 58
hyperparameters = \
{u'batch_size': 83,
 u'conv10_num_filters': 182,
 u'conv1_num_filters': 47,
 u'conv2_num_filters': 53,
 u'conv3_num_filters': 109,
 u'conv4_num_filters': 33,
 u'conv5_num_filters': 250,
 u'conv6_num_filters': 250,
 u'conv7_num_filters': 121,
 u'conv8_num_filters': 268,
 u'conv9_num_filters': 428,
 u'fc1_dropout_p': 0.4125,
 u'fc1_max_col_norm': 14.092187499999998,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3928,
 u'fc2_dropout_p': 0.38750000000000007,
 u'fc2_max_col_norm': 7.2515624999999995,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3480,
 u'fc3_dropout_p': 0.3125,
 u'fc3_max_col_norm': 3.5203124999999997,
 u'learning_rate': 0.0921953125,
 u'learning_rate_jitter': 3.9375,
 u'train_sample_rate': 0.7953125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
