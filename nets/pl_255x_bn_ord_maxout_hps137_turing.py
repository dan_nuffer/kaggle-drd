#job_id = 137
hyperparameters = \
{u'batch_size': 51,
 u'conv10_num_filters': 225,
 u'conv1_num_filters': 49,
 u'conv2_num_filters': 42,
 u'conv3_num_filters': 111,
 u'conv4_num_filters': 116,
 u'conv5_num_filters': 254,
 u'conv6_num_filters': 184,
 u'conv7_num_filters': 134,
 u'conv8_num_filters': 477,
 u'conv9_num_filters': 433,
 u'fc1_dropout_p': 0.209375,
 u'fc1_max_col_norm': 18.833984375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1954,
 u'fc2_dropout_p': 0.646875,
 u'fc2_max_col_norm': 19.611328125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3942,
 u'fc3_dropout_p': 0.509375,
 u'fc3_max_col_norm': 1.266015625,
 u'learning_rate': 0.020782421875,
 u'learning_rate_jitter': 4.671875,
 u'train_sample_rate': 0.784765625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
