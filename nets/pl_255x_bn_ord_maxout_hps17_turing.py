#job_id = 17
hyperparameters = \
{u'batch_size': 97,
 u'conv10_num_filters': 380,
 u'conv1_num_filters': 41,
 u'conv2_num_filters': 63,
 u'conv3_num_filters': 98,
 u'conv4_num_filters': 113,
 u'conv5_num_filters': 188,
 u'conv6_num_filters': 148,
 u'conv7_num_filters': 82,
 u'conv8_num_filters': 360,
 u'conv9_num_filters': 424,
 u'fc1_dropout_p': 0.125,
 u'fc1_max_col_norm': 15.646874999999998,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3152,
 u'fc2_dropout_p': 0.125,
 u'fc2_max_col_norm': 14.403125,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 6512,
 u'fc3_dropout_p': 0.5750000000000001,
 u'fc3_max_col_norm': 11.915624999999999,
 u'learning_rate': 0.046928125,
 u'learning_rate_jitter': 2.875,
 u'train_sample_rate': 0.640625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
