#job_id = 170
hyperparameters = \
{u'batch_size': 120,
 u'conv10_num_filters': 219,
 u'conv1_num_filters': 36,
 u'conv2_num_filters': 59,
 u'conv3_num_filters': 125,
 u'conv4_num_filters': 124,
 u'conv5_num_filters': 196,
 u'conv6_num_filters': 255,
 u'conv7_num_filters': 203,
 u'conv8_num_filters': 425,
 u'conv9_num_filters': 325,
 u'fc1_dropout_p': 0.696875,
 u'fc1_max_col_norm': 7.329296874999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 890,
 u'fc2_dropout_p': 0.634375,
 u'fc2_max_col_norm': 3.753515625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 862,
 u'fc3_dropout_p': 0.446875,
 u'fc3_max_col_norm': 5.930078125,
 u'learning_rate': 0.094146484375,
 u'learning_rate_jitter': 4.484375,
 u'train_sample_rate': 0.826953125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
