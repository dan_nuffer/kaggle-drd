#job_id = 44
hyperparameters = \
{u'batch_size': 63,
 u'conv10_num_filters': 434,
 u'conv1_num_filters': 63,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 95,
 u'conv4_num_filters': 42,
 u'conv5_num_filters': 166,
 u'conv6_num_filters': 142,
 u'conv7_num_filters': 175,
 u'conv8_num_filters': 420,
 u'conv9_num_filters': 324,
 u'fc1_dropout_p': 0.23750000000000002,
 u'fc1_max_col_norm': 14.714062499999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2024,
 u'fc2_dropout_p': 0.1625,
 u'fc2_max_col_norm': 12.8484375,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3592,
 u'fc3_dropout_p': 0.5875,
 u'fc3_max_col_norm': 19.0671875,
 u'learning_rate': 0.0828296875,
 u'learning_rate_jitter': 3.0625,
 u'train_sample_rate': 0.2609375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
