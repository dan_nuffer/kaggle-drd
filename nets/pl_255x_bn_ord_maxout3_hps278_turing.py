#job_id = 278
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 6,
 u'conv3_num_filters': 71,
 u'conv4_num_filters': 40,
 u'conv5_num_filters': 232,
 u'conv6_num_filters': 130,
 u'conv7_num_filters': 261,
 u'conv8_num_filters': 334,
 u'conv9_num_filters': 361,
 u'fc1_dropout_p': 0.5564532660393001,
 u'fc1_max_col_norm': 17.548140776026944,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2126,
 u'fc3_dropout_p': 0.6574918313627174,
 u'fc3_max_col_norm': 30.0,
 u'learning_rate': 0.024584128383727043,
 u'learning_rate_jitter': 5.01823146798342,
 u'train_sample_rate': 0.6575154117183232,
 u'training_time': 24.295216327272367}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
