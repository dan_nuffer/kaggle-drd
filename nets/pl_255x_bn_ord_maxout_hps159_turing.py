#job_id = 159
hyperparameters = \
{u'batch_size': 118,
 u'conv10_num_filters': 502,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 53,
 u'conv3_num_filters': 89,
 u'conv4_num_filters': 113,
 u'conv5_num_filters': 169,
 u'conv6_num_filters': 180,
 u'conv7_num_filters': 224,
 u'conv8_num_filters': 261,
 u'conv9_num_filters': 457,
 u'fc1_dropout_p': 0.484375,
 u'fc1_max_col_norm': 5.774609374999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1618,
 u'fc2_dropout_p': 0.571875,
 u'fc2_max_col_norm': 1.576953125,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3830,
 u'fc3_dropout_p': 0.13437500000000002,
 u'fc3_max_col_norm': 15.569140625,
 u'learning_rate': 0.023904296875,
 u'learning_rate_jitter': 3.046875,
 u'train_sample_rate': 0.644140625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
