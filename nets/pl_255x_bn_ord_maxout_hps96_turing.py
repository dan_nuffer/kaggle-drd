#job_id = 96
hyperparameters = \
{u'batch_size': 8,
 u'conv10_num_filters': 413,
 u'conv1_num_filters': 44,
 u'conv2_num_filters': 51,
 u'conv3_num_filters': 125,
 u'conv4_num_filters': 103,
 u'conv5_num_filters': 203,
 u'conv6_num_filters': 195,
 u'conv7_num_filters': 252,
 u'conv8_num_filters': 354,
 u'conv9_num_filters': 406,
 u'fc1_dropout_p': 0.38125,
 u'fc1_max_col_norm': 17.04609375,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1940,
 u'fc2_dropout_p': 0.33125000000000004,
 u'fc2_max_col_norm': 17.35703125,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3228,
 u'fc3_dropout_p': 0.11875000000000001,
 u'fc3_max_col_norm': 1.49921875,
 u'learning_rate': 0.09297578125,
 u'learning_rate_jitter': 1.90625,
 u'train_sample_rate': 0.21171875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
