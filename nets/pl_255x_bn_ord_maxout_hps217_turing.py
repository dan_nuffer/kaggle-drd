#job_id = 217
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 171,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 28,
 u'conv3_num_filters': 66,
 u'conv4_num_filters': 88,
 u'conv5_num_filters': 244,
 u'conv6_num_filters': 239,
 u'conv7_num_filters': 228,
 u'conv8_num_filters': 329,
 u'conv9_num_filters': 422,
 u'fc1_dropout_p': 0.7983722240022474,
 u'fc1_max_col_norm': 14.972725535588783,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4035,
 u'fc2_dropout_p': 0.13858561933839897,
 u'fc2_max_col_norm': 11.221059529448167,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2211,
 u'fc3_dropout_p': 0.5466596800875329,
 u'fc3_max_col_norm': 2.3827000258285502,
 u'learning_rate': 0.006843907490088458,
 u'learning_rate_jitter': 4.996682116412599,
 u'train_sample_rate': 0.7153107103669218}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
