#job_id = 290
hyperparameters = \
{u'batch_size': 5,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 18,
 u'conv2_num_filters': 52,
 u'conv3_num_filters': 38,
 u'conv4_num_filters': 150,
 u'conv5_num_filters': 247,
 u'conv6_num_filters': 300,
 u'conv7_num_filters': 488,
 u'conv8_num_filters': 280,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.1,
 u'fc1_max_col_norm': 28.858426167761895,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.7212535221903382,
 u'fc3_max_col_norm': 4.454483633258381,
 u'learning_rate': 0.01323451180200957,
 u'learning_rate_jitter': 6.0,
 u'train_sample_rate': 0.5,
 u'training_time': 36.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
