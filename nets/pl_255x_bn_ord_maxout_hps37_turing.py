#job_id = 37
hyperparameters = \
{u'batch_size': 433,
 u'conv10_num_filters': 266,
 u'conv1_num_filters': 40,
 u'conv2_num_filters': 55,
 u'conv3_num_filters': 107,
 u'conv4_num_filters': 60,
 u'conv5_num_filters': 142,
 u'conv6_num_filters': 214,
 u'conv7_num_filters': 163,
 u'conv8_num_filters': 404,
 u'conv9_num_filters': 276,
 u'fc1_dropout_p': 0.8875000000000001,
 u'fc1_max_col_norm': 6.007812499999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 5192,
 u'fc2_dropout_p': 0.6125,
 u'fc2_max_col_norm': 9.117187499999998,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 4712,
 u'fc3_dropout_p': 0.1375,
 u'fc3_max_col_norm': 2.8984375,
 u'learning_rate': 0.0141484375,
 u'learning_rate_jitter': 1.3125,
 u'train_sample_rate': 0.42968749999999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
