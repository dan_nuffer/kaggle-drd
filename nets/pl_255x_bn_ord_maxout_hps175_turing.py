#job_id = 175
hyperparameters = \
{u'batch_size': 128,
 u'conv10_num_filters': 388,
 u'conv1_num_filters': 50,
 u'conv2_num_filters': 56,
 u'conv3_num_filters': 72,
 u'conv4_num_filters': 93,
 u'conv5_num_filters': 236,
 u'conv6_num_filters': 166,
 u'conv7_num_filters': 191,
 u'conv8_num_filters': 409,
 u'conv9_num_filters': 277,
 u'fc1_dropout_p': 0.446875,
 u'fc1_max_col_norm': 13.548046874999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3802,
 u'fc2_dropout_p': 0.184375,
 u'fc2_max_col_norm': 19.922265625,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1534,
 u'fc3_dropout_p': 0.896875,
 u'fc3_max_col_norm': 12.148828125,
 u'learning_rate': 0.000490234375,
 u'learning_rate_jitter': 2.234375,
 u'train_sample_rate': 0.545703125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
