#job_id = 64
hyperparameters = \
{u'batch_size': 3,
 u'conv10_num_filters': 422,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 22,
 u'conv3_num_filters': 101,
 u'conv4_num_filters': 45,
 u'conv5_num_filters': 234,
 u'conv6_num_filters': 202,
 u'conv7_num_filters': 145,
 u'conv8_num_filters': 428,
 u'conv9_num_filters': 396,
 u'fc1_dropout_p': 0.7125,
 u'fc1_max_col_norm': 1.6546875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1688,
 u'fc2_dropout_p': 0.2875,
 u'fc2_max_col_norm': 4.7640625,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3928,
 u'fc3_dropout_p': 0.4125,
 u'fc3_max_col_norm': 10.9828125,
 u'learning_rate': 0.0797078125,
 u'learning_rate_jitter': 2.4375,
 u'train_sample_rate': 0.45781249999999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
