#job_id = 237
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 167,
 u'conv1_num_filters': 71,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 72,
 u'conv4_num_filters': 112,
 u'conv5_num_filters': 250,
 u'conv6_num_filters': 209,
 u'conv7_num_filters': 380,
 u'conv8_num_filters': 241,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.828215675308407,
 u'fc1_max_col_norm': 18.389909898452732,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 5073,
 u'fc2_dropout_p': 0.1,
 u'fc2_max_col_norm': 10.172954254373138,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2192,
 u'fc3_dropout_p': 0.3826340785305028,
 u'fc3_max_col_norm': 19.530579515033846,
 u'learning_rate': 0.0650683973805498,
 u'learning_rate_jitter': 5.472568457449747,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
