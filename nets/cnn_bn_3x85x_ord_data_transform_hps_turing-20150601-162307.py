hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 96,
                          'per_pixel_gaussian_noise_sigma': 0.1492185611304934,
                          'rotation_range': (0, 34.13250170958507),
                          'shear_range': (-23.422019255559285,
                                          23.422019255559285),
                          'translation_range': (-1.7030294235578856,
                                                1.7030294235578856),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 60,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-4.278573618801871,
                                           4.278573618801871),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.5122329822697596,
                                          1.9522366474116764)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
