#job_id = 63
hyperparameters = \
{u'batch_size': 67,
 u'conv10_num_filters': 230,
 u'conv1_num_filters': 43,
 u'conv2_num_filters': 47,
 u'conv3_num_filters': 69,
 u'conv4_num_filters': 94,
 u'conv5_num_filters': 170,
 u'conv6_num_filters': 138,
 u'conv7_num_filters': 241,
 u'conv8_num_filters': 300,
 u'conv9_num_filters': 268,
 u'fc1_dropout_p': 0.3125,
 u'fc1_max_col_norm': 11.604687499999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3480,
 u'fc2_dropout_p': 0.6875,
 u'fc2_max_col_norm': 14.714062499999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2136,
 u'fc3_dropout_p': 0.8125,
 u'fc3_max_col_norm': 1.0328125,
 u'learning_rate': 0.0297578125,
 u'learning_rate_jitter': 4.4375,
 u'train_sample_rate': 0.9078124999999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
