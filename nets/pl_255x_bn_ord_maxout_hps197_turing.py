#job_id = 197
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 282,
 u'conv1_num_filters': 40,
 u'conv2_num_filters': 58,
 u'conv3_num_filters': 94,
 u'conv4_num_filters': 35,
 u'conv5_num_filters': 211,
 u'conv6_num_filters': 232,
 u'conv7_num_filters': 123,
 u'conv8_num_filters': 391,
 u'conv9_num_filters': 447,
 u'fc1_dropout_p': 0.303125,
 u'fc1_max_col_norm': 9.039453125,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3326,
 u'fc2_dropout_p': 0.490625,
 u'fc2_max_col_norm': 8.573046875,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1618,
 u'fc3_dropout_p': 0.8406250000000001,
 u'fc3_max_col_norm': 14.169921874999998,
 u'learning_rate': 0.020001953125,
 u'learning_rate_jitter': 3.390625,
 u'train_sample_rate': 0.862109375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
