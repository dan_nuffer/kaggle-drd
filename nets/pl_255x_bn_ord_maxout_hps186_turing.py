#job_id = 186
hyperparameters = \
{u'batch_size': 124,
 u'conv10_num_filters': 352,
 u'conv1_num_filters': 45,
 u'conv2_num_filters': 20,
 u'conv3_num_filters': 94,
 u'conv4_num_filters': 48,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 235,
 u'conv7_num_filters': 185,
 u'conv8_num_filters': 449,
 u'conv9_num_filters': 493,
 u'fc1_dropout_p': 0.671875,
 u'fc1_max_col_norm': 11.682421875,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1450,
 u'fc2_dropout_p': 0.609375,
 u'fc2_max_col_norm': 11.837890624999998,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3438,
 u'fc3_dropout_p': 0.8718750000000001,
 u'fc3_max_col_norm': 16.501953125,
 u'learning_rate': 0.05356210937500001,
 u'learning_rate_jitter': 3.609375,
 u'train_sample_rate': 0.4613281249999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
