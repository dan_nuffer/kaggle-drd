#job_id = 250
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 52,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 59,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 128,
 u'conv6_num_filters': 350,
 u'conv7_num_filters': 382,
 u'conv8_num_filters': 211,
 u'conv9_num_filters': 285,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 14.786157504465073,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 4166,
 u'fc2_dropout_p': 0.1593024807006186,
 u'fc2_max_col_norm': 3.2149888074327584,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 512,
 u'fc3_dropout_p': 0.6830528680541146,
 u'fc3_max_col_norm': 18.671252045279395,
 u'learning_rate': 0.0759180573634114,
 u'learning_rate_jitter': 5.519273193573051,
 u'train_sample_rate': 0.010042658861099413}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
