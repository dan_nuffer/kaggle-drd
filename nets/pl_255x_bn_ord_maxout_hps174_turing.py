#job_id = 174
hyperparameters = \
{u'batch_size': 96,
 u'conv10_num_filters': 291,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 43,
 u'conv3_num_filters': 121,
 u'conv4_num_filters': 118,
 u'conv5_num_filters': 204,
 u'conv6_num_filters': 134,
 u'conv7_num_filters': 239,
 u'conv8_num_filters': 473,
 u'conv9_num_filters': 469,
 u'fc1_dropout_p': 0.646875,
 u'fc1_max_col_norm': 18.523046875,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2906,
 u'fc2_dropout_p': 0.784375,
 u'fc2_max_col_norm': 4.997265624999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 638,
 u'fc3_dropout_p': 0.296875,
 u'fc3_max_col_norm': 7.173828124999999,
 u'learning_rate': 0.075415234375,
 u'learning_rate_jitter': 3.234375,
 u'train_sample_rate': 0.320703125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
