#job_id = 48
hyperparameters = \
{u'batch_size': 15,
 u'conv10_num_filters': 290,
 u'conv1_num_filters': 51,
 u'conv2_num_filters': 58,
 u'conv3_num_filters': 71,
 u'conv4_num_filters': 127,
 u'conv5_num_filters': 214,
 u'conv6_num_filters': 222,
 u'conv7_num_filters': 151,
 u'conv8_num_filters': 452,
 u'conv9_num_filters': 292,
 u'fc1_dropout_p': 0.7375,
 u'fc1_max_col_norm': 17.2015625,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 680,
 u'fc2_dropout_p': 0.2625,
 u'fc2_max_col_norm': 10.360937499999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1352,
 u'fc3_dropout_p': 0.8875000000000001,
 u'fc3_max_col_norm': 1.6546875,
 u'learning_rate': 0.0703421875,
 u'learning_rate_jitter': 1.5625,
 u'train_sample_rate': 0.8234374999999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
