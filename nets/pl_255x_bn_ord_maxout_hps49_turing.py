#job_id = 49
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 254,
 u'conv1_num_filters': 45,
 u'conv2_num_filters': 19,
 u'conv3_num_filters': 81,
 u'conv4_num_filters': 75,
 u'conv5_num_filters': 194,
 u'conv6_num_filters': 146,
 u'conv7_num_filters': 157,
 u'conv8_num_filters': 412,
 u'conv9_num_filters': 476,
 u'fc1_dropout_p': 0.1625,
 u'fc1_max_col_norm': 17.8234375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2808,
 u'fc2_dropout_p': 0.5375,
 u'fc2_max_col_norm': 18.4453125,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2808,
 u'fc3_dropout_p': 0.8625,
 u'fc3_max_col_norm': 7.2515624999999995,
 u'learning_rate': 0.0235140625,
 u'learning_rate_jitter': 4.1875,
 u'train_sample_rate': 0.1765625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
