#job_id = 201
hyperparameters = \
{u'batch_size': 53,
 u'conv10_num_filters': 328,
 u'conv1_num_filters': 43,
 u'conv2_num_filters': 48,
 u'conv3_num_filters': 74,
 u'conv4_num_filters': 127,
 u'conv5_num_filters': 151,
 u'conv6_num_filters': 243,
 u'conv7_num_filters': 100,
 u'conv8_num_filters': 369,
 u'conv9_num_filters': 317,
 u'fc1_dropout_p': 0.821875,
 u'fc1_max_col_norm': 17.901171875,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1226,
 u'fc2_dropout_p': 0.5593750000000001,
 u'fc2_max_col_norm': 15.569140625,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 526,
 u'fc3_dropout_p': 0.821875,
 u'fc3_max_col_norm': 10.283203124999998,
 u'learning_rate': 0.097268359375,
 u'learning_rate_jitter': 3.859375,
 u'train_sample_rate': 0.6300781249999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
