hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.04158461539442526,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 292.1411467619687),
                          'shear_range': (-33.6520449048122,
                                          33.6520449048122),
                          'translation_range': (-3.1271931211267816,
                                                3.1271931211267816),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 4.163775902661193,
              'momentum': 0.7988449157788942},
 'regularization': {'dropout_p1': 0.4033770653681634,
                    'dropout_p2': 0.2858912350652785,
                    'dropout_p3': 0.2708889246696867,
                    'l1': 7.296329644939572e-05,
                    'l2': 0.0,
                    'max_col_norm1': 4.300889933890295,
                    'max_col_norm2': 3.3444088675020764,
                    'max_col_norm3': 3.957899794123155},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-9.575912096040224,
                                           9.575912096040224),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.8094223181283304,
                                          1.2354490080188947)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
