hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0090578552888498,
                          'color_shift_sigma': 0.11132758853039171,
                          'contrast_noise_sigma': 0.17124472407484811,
                          'do_flip': True,
                          'num_transforms': 6,
                          'per_pixel_gaussian_noise_sigma': 0.16092997062925865,
                          'rotation_range': (0, 0.0),
                          'shear_range': (-0.0, 0.0),
                          'translation_range': (-8.12795929665581,
                                                8.12795929665581),
                          'zoom_range': (0.9832625525976029,
                                         1.0170223582279014)},
 'train_data_processing': {'allow_stretch': 1.328382272831758,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0819431762714262,
                           'do_flip': True,
                           'num_transforms': 62,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 3.9446974791390943),
                           'shear_range': (-0.0, 0.0),
                           'translation_range': (-8.617090736468523,
                                                 8.617090736468523),
                           'zoom_range': (1.0, 1.0)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
