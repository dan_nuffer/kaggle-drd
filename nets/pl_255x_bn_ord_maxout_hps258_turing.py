#job_id = 258
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 191,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 29,
 u'conv3_num_filters': 48,
 u'conv4_num_filters': 93,
 u'conv5_num_filters': 220,
 u'conv6_num_filters': 238,
 u'conv7_num_filters': 266,
 u'conv8_num_filters': 377,
 u'conv9_num_filters': 424,
 u'fc1_dropout_p': 0.6416048071662075,
 u'fc1_max_col_norm': 19.92590864385698,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 518,
 u'fc2_dropout_p': 0.1836738942903046,
 u'fc2_max_col_norm': 20.0,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 512,
 u'fc3_dropout_p': 0.4632716363767567,
 u'fc3_max_col_norm': 19.99752701734999,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.409236231793914,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
