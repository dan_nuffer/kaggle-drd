hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 80,
                          'per_pixel_gaussian_noise_sigma': 0.1121448327161158,
                          'rotation_range': (0, 31.167497573505955),
                          'shear_range': (-0.0, 0.0),
                          'translation_range': (-1.2823552250067096,
                                                1.2823552250067096),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 70,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-0.0, 0.0),
                           'translation_range': (-10.552838573951117,
                                                 10.552838573951117),
                           'zoom_range': (0.3801045878516573,
                                          2.630854854059978)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
