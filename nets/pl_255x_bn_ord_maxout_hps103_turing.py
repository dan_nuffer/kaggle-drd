#job_id = 103
hyperparameters = \
{u'batch_size': 86,
 u'conv10_num_filters': 359,
 u'conv1_num_filters': 36,
 u'conv2_num_filters': 40,
 u'conv3_num_filters': 81,
 u'conv4_num_filters': 102,
 u'conv5_num_filters': 177,
 u'conv6_num_filters': 185,
 u'conv7_num_filters': 201,
 u'conv8_num_filters': 366,
 u'conv9_num_filters': 314,
 u'fc1_dropout_p': 0.29375,
 u'fc1_max_col_norm': 8.65078125,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2556,
 u'fc2_dropout_p': 0.16875,
 u'fc2_max_col_norm': 10.20546875,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 820,
 u'fc3_dropout_p': 0.30625,
 u'fc3_max_col_norm': 4.9195312499999995,
 u'learning_rate': 0.0008804687500000001,
 u'learning_rate_jitter': 3.09375,
 u'train_sample_rate': 0.9289062499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
