#job_id = 125
hyperparameters = \
{u'batch_size': 18,
 u'conv10_num_filters': 179,
 u'conv1_num_filters': 43,
 u'conv2_num_filters': 26,
 u'conv3_num_filters': 104,
 u'conv4_num_filters': 99,
 u'conv5_num_filters': 229,
 u'conv6_num_filters': 181,
 u'conv7_num_filters': 110,
 u'conv8_num_filters': 438,
 u'conv9_num_filters': 322,
 u'fc1_dropout_p': 0.41875000000000007,
 u'fc1_max_col_norm': 16.73515625,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3116,
 u'fc2_dropout_p': 0.24375000000000002,
 u'fc2_max_col_norm': 8.339843749999998,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 708,
 u'fc3_dropout_p': 0.7312500000000001,
 u'fc3_max_col_norm': 19.22265625,
 u'learning_rate': 0.00400234375,
 u'learning_rate_jitter': 4.71875,
 u'train_sample_rate': 0.5070312499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
