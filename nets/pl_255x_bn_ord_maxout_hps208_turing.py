#job_id = 208
hyperparameters = \
{u'batch_size': 99,
 u'conv10_num_filters': 186,
 u'conv1_num_filters': 48,
 u'conv2_num_filters': 21,
 u'conv3_num_filters': 77,
 u'conv4_num_filters': 108,
 u'conv5_num_filters': 178,
 u'conv6_num_filters': 135,
 u'conv7_num_filters': 171,
 u'conv8_num_filters': 327,
 u'conv9_num_filters': 511,
 u'fc1_dropout_p': 0.10312500000000001,
 u'fc1_max_col_norm': 14.014453125,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 638,
 u'fc2_dropout_p': 0.290625,
 u'fc2_max_col_norm': 3.5980468749999996,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2514,
 u'fc3_dropout_p': 0.640625,
 u'fc3_max_col_norm': 19.144921875,
 u'learning_rate': 0.044976953125000003,
 u'learning_rate_jitter': 4.390625,
 u'train_sample_rate': 0.637109375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
