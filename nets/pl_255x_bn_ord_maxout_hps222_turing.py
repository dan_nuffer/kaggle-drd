#job_id = 222
hyperparameters = \
{u'batch_size': 8,
 u'conv10_num_filters': 294,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 31,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 92,
 u'conv5_num_filters': 253,
 u'conv6_num_filters': 237,
 u'conv7_num_filters': 218,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 512,
 u'fc1_dropout_p': 0.8998098229489382,
 u'fc1_max_col_norm': 15.62554950929504,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4076,
 u'fc2_dropout_p': 0.11765791214739643,
 u'fc2_max_col_norm': 20.0,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 4096,
 u'fc3_dropout_p': 0.725370676231352,
 u'fc3_max_col_norm': 1.4662103336070562,
 u'learning_rate': 0.0019654486129458797,
 u'learning_rate_jitter': 4.884813719199917,
 u'train_sample_rate': 0.7210306553406375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
