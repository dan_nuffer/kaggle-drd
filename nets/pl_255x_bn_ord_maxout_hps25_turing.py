#job_id = 25
hyperparameters = \
{u'batch_size': 161,
 u'conv10_num_filters': 260,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 48,
 u'conv3_num_filters': 126,
 u'conv4_num_filters': 47,
 u'conv5_num_filters': 196,
 u'conv6_num_filters': 188,
 u'conv7_num_filters': 190,
 u'conv8_num_filters': 376,
 u'conv9_num_filters': 344,
 u'fc1_dropout_p': 0.875,
 u'fc1_max_col_norm': 1.965625,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4592,
 u'fc2_dropout_p': 0.375,
 u'fc2_max_col_norm': 10.671874999999998,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2192,
 u'fc3_dropout_p': 0.625,
 u'fc3_max_col_norm': 8.184375,
 u'learning_rate': 0.015709375,
 u'learning_rate_jitter': 3.125,
 u'train_sample_rate': 0.809375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
