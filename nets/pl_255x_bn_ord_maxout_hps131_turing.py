#job_id = 131
hyperparameters = \
{u'batch_size': 66,
 u'conv10_num_filters': 419,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 56,
 u'conv3_num_filters': 112,
 u'conv4_num_filters': 87,
 u'conv5_num_filters': 245,
 u'conv6_num_filters': 133,
 u'conv7_num_filters': 86,
 u'conv8_num_filters': 278,
 u'conv9_num_filters': 354,
 u'fc1_dropout_p': 0.71875,
 u'fc1_max_col_norm': 9.272656249999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 876,
 u'fc2_dropout_p': 0.34375,
 u'fc2_max_col_norm': 0.87734375,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1156,
 u'fc3_dropout_p': 0.83125,
 u'fc3_max_col_norm': 6.785156249999999,
 u'learning_rate': 0.01648984375,
 u'learning_rate_jitter': 1.21875,
 u'train_sample_rate': 0.16953125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
