#job_id = 83
hyperparameters = \
{u'batch_size': 112,
 u'conv10_num_filters': 293,
 u'conv1_num_filters': 37,
 u'conv2_num_filters': 30,
 u'conv3_num_filters': 88,
 u'conv4_num_filters': 49,
 u'conv5_num_filters': 131,
 u'conv6_num_filters': 251,
 u'conv7_num_filters': 95,
 u'conv8_num_filters': 402,
 u'conv9_num_filters': 390,
 u'fc1_dropout_p': 0.7312500000000001,
 u'fc1_max_col_norm': 18.28984375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1716,
 u'fc2_dropout_p': 0.68125,
 u'fc2_max_col_norm': 6.163281249999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1660,
 u'fc3_dropout_p': 0.76875,
 u'fc3_max_col_norm': 10.20546875,
 u'learning_rate': 0.03678203125000001,
 u'learning_rate_jitter': 4.65625,
 u'train_sample_rate': 0.26796875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
