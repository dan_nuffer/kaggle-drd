#job_id = 240
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 59,
 u'conv4_num_filters': 89,
 u'conv5_num_filters': 211,
 u'conv6_num_filters': 183,
 u'conv7_num_filters': 228,
 u'conv8_num_filters': 244,
 u'conv9_num_filters': 449,
 u'fc1_dropout_p': 0.7217263920932719,
 u'fc1_max_col_norm': 12.303699820774634,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 5006,
 u'fc2_dropout_p': 0.10099942148234138,
 u'fc2_max_col_norm': 13.721070913016817,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 1255,
 u'fc3_dropout_p': 0.6458162305372632,
 u'fc3_max_col_norm': 0.34733143002965006,
 u'learning_rate': 0.003986793385446638,
 u'learning_rate_jitter': 6.0,
 u'train_sample_rate': 0.44508591965175787}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
