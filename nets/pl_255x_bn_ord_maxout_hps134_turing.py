#job_id = 134
hyperparameters = \
{u'batch_size': 66,
 u'conv10_num_filters': 273,
 u'conv1_num_filters': 45,
 u'conv2_num_filters': 24,
 u'conv3_num_filters': 87,
 u'conv4_num_filters': 104,
 u'conv5_num_filters': 206,
 u'conv6_num_filters': 168,
 u'conv7_num_filters': 109,
 u'conv8_num_filters': 317,
 u'conv9_num_filters': 337,
 u'fc1_dropout_p': 0.309375,
 u'fc1_max_col_norm': 1.421484375,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2402,
 u'fc2_dropout_p': 0.146875,
 u'fc2_max_col_norm': 7.173828124999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2598,
 u'fc3_dropout_p': 0.40937500000000004,
 u'fc3_max_col_norm': 8.728515624999998,
 u'learning_rate': 0.083219921875,
 u'learning_rate_jitter': 4.171875,
 u'train_sample_rate': 0.672265625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
