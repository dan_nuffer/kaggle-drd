#job_id = 176
hyperparameters = \
{u'batch_size': 65,
 u'conv10_num_filters': 195,
 u'conv1_num_filters': 34,
 u'conv2_num_filters': 31,
 u'conv3_num_filters': 104,
 u'conv4_num_filters': 45,
 u'conv5_num_filters': 171,
 u'conv6_num_filters': 231,
 u'conv7_num_filters': 94,
 u'conv8_num_filters': 281,
 u'conv9_num_filters': 405,
 u'fc1_dropout_p': 0.846875,
 u'fc1_max_col_norm': 3.5980468749999996,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2010,
 u'fc2_dropout_p': 0.584375,
 u'fc2_max_col_norm': 9.972265624999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3326,
 u'fc3_dropout_p': 0.49687500000000007,
 u'fc3_max_col_norm': 2.198828125,
 u'learning_rate': 0.050440234375000004,
 u'learning_rate_jitter': 4.234375,
 u'train_sample_rate': 0.095703125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
