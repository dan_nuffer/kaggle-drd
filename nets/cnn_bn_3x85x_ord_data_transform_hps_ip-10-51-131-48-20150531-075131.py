hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.2560445575140802,
                          'color_shift_sigma': 0.13066629341600408,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 15,
                          'per_pixel_gaussian_noise_sigma': 0.10221291978902464,
                          'rotation_range': (0, 189.223996852807),
                          'shear_range': (-4.005728063876335,
                                          4.005728063876335),
                          'translation_range': (-0.0, 0.0),
                          'zoom_range': (0.6519308217399373,
                                         1.533905081111369)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 13,
                           'per_pixel_gaussian_noise_sigma': 0.051986940079999225,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-28.069404293355184,
                                           28.069404293355184),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.37671109289130594,
                                          2.654554163310859)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
