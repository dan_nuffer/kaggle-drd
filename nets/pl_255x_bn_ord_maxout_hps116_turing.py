#job_id = 116
hyperparameters = \
{u'batch_size': 14,
 u'conv10_num_filters': 479,
 u'conv1_num_filters': 38,
 u'conv2_num_filters': 49,
 u'conv3_num_filters': 110,
 u'conv4_num_filters': 108,
 u'conv5_num_filters': 137,
 u'conv6_num_filters': 209,
 u'conv7_num_filters': 165,
 u'conv8_num_filters': 510,
 u'conv9_num_filters': 330,
 u'fc1_dropout_p': 0.34375,
 u'fc1_max_col_norm': 4.9195312499999995,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3228,
 u'fc2_dropout_p': 0.71875,
 u'fc2_max_col_norm': 3.9867187499999996,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 1940,
 u'fc3_dropout_p': 0.8562500000000001,
 u'fc3_max_col_norm': 6.163281249999999,
 u'learning_rate': 0.08204921875,
 u'learning_rate_jitter': 4.84375,
 u'train_sample_rate': 0.08515625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
