#job_id = 248
hyperparameters = \
{u'batch_size': 6,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 20,
 u'conv3_num_filters': 61,
 u'conv4_num_filters': 53,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 128,
 u'conv7_num_filters': 320,
 u'conv8_num_filters': 192,
 u'conv9_num_filters': 512,
 u'fc1_dropout_p': 0.782686797026618,
 u'fc1_max_col_norm': 14.365482163344435,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2634,
 u'fc2_dropout_p': 0.12802731698076447,
 u'fc2_max_col_norm': 19.82119365620417,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1856,
 u'fc3_dropout_p': 0.1,
 u'fc3_max_col_norm': 12.389437085273016,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.861797611180711,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
