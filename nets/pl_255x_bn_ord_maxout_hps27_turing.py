#job_id = 27
hyperparameters = \
{u'batch_size': 929,
 u'conv10_num_filters': 164,
 u'conv1_num_filters': 35,
 u'conv2_num_filters': 35,
 u'conv3_num_filters': 110,
 u'conv4_num_filters': 119,
 u'conv5_num_filters': 164,
 u'conv6_num_filters': 220,
 u'conv7_num_filters': 142,
 u'conv8_num_filters': 440,
 u'conv9_num_filters': 280,
 u'fc1_dropout_p': 0.675,
 u'fc1_max_col_norm': 16.890625,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2672,
 u'fc2_dropout_p': 0.17500000000000002,
 u'fc2_max_col_norm': 15.646874999999998,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 7952,
 u'fc3_dropout_p': 0.8250000000000001,
 u'fc3_max_col_norm': 3.209375,
 u'learning_rate': 0.040684375,
 u'learning_rate_jitter': 4.125,
 u'train_sample_rate': 0.584375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
