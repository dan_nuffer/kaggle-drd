#job_id = 114
hyperparameters = \
{u'batch_size': 110,
 u'conv10_num_filters': 383,
 u'conv1_num_filters': 63,
 u'conv2_num_filters': 37,
 u'conv3_num_filters': 126,
 u'conv4_num_filters': 35,
 u'conv5_num_filters': 233,
 u'conv6_num_filters': 177,
 u'conv7_num_filters': 116,
 u'conv8_num_filters': 318,
 u'conv9_num_filters': 266,
 u'fc1_dropout_p': 0.14375000000000002,
 u'fc1_max_col_norm': 19.84453125,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 540,
 u'fc2_dropout_p': 0.51875,
 u'fc2_max_col_norm': 8.96171875,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2836,
 u'fc3_dropout_p': 0.65625,
 u'fc3_max_col_norm': 1.18828125,
 u'learning_rate': 0.05707421875,
 u'learning_rate_jitter': 3.84375,
 u'train_sample_rate': 0.31015624999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
