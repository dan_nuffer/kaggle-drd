#job_id = 184
hyperparameters = \
{u'batch_size': 45,
 u'conv10_num_filters': 303,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 63,
 u'conv3_num_filters': 86,
 u'conv4_num_filters': 36,
 u'conv5_num_filters': 240,
 u'conv6_num_filters': 219,
 u'conv7_num_filters': 161,
 u'conv8_num_filters': 353,
 u'conv9_num_filters': 461,
 u'fc1_dropout_p': 0.171875,
 u'fc1_max_col_norm': 4.219921874999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3690,
 u'fc2_dropout_p': 0.709375,
 u'fc2_max_col_norm': 19.300390625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3886,
 u'fc3_dropout_p': 0.771875,
 u'fc3_max_col_norm': 9.039453125,
 u'learning_rate': 0.066049609375,
 u'learning_rate_jitter': 2.109375,
 u'train_sample_rate': 0.798828125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
