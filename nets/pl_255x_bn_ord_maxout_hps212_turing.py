#job_id = 212
hyperparameters = \
{u'batch_size': 83,
 u'conv10_num_filters': 331,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 28,
 u'conv3_num_filters': 86,
 u'conv4_num_filters': 47,
 u'conv5_num_filters': 195,
 u'conv6_num_filters': 216,
 u'conv7_num_filters': 147,
 u'conv8_num_filters': 295,
 u'conv9_num_filters': 415,
 u'fc1_dropout_p': 0.803125,
 u'fc1_max_col_norm': 16.501953125,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1982,
 u'fc2_dropout_p': 0.19062500000000002,
 u'fc2_max_col_norm': 1.110546875,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2066,
 u'fc3_dropout_p': 0.740625,
 u'fc3_max_col_norm': 1.732421875,
 u'learning_rate': 0.007514453125000001,
 u'learning_rate_jitter': 2.890625,
 u'train_sample_rate': 0.29960937499999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
