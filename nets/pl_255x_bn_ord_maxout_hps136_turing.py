#job_id = 136
hyperparameters = \
{u'batch_size': 35,
 u'conv10_num_filters': 177,
 u'conv1_num_filters': 53,
 u'conv2_num_filters': 61,
 u'conv3_num_filters': 71,
 u'conv4_num_filters': 80,
 u'conv5_num_filters': 173,
 u'conv6_num_filters': 201,
 u'conv7_num_filters': 254,
 u'conv8_num_filters': 509,
 u'conv9_num_filters': 273,
 u'fc1_dropout_p': 0.109375,
 u'fc1_max_col_norm': 16.346484375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1506,
 u'fc2_dropout_p': 0.34687500000000004,
 u'fc2_max_col_norm': 2.198828125,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1702,
 u'fc3_dropout_p': 0.209375,
 u'fc3_max_col_norm': 3.753515625,
 u'learning_rate': 0.058244921875,
 u'learning_rate_jitter': 3.171875,
 u'train_sample_rate': 0.897265625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
