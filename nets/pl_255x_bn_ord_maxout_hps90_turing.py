#job_id = 90
hyperparameters = \
{u'batch_size': 88,
 u'conv10_num_filters': 269,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 33,
 u'conv3_num_filters': 117,
 u'conv4_num_filters': 91,
 u'conv5_num_filters': 219,
 u'conv6_num_filters': 243,
 u'conv7_num_filters': 228,
 u'conv8_num_filters': 450,
 u'conv9_num_filters': 438,
 u'fc1_dropout_p': 0.88125,
 u'fc1_max_col_norm': 9.583593749999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3284,
 u'fc2_dropout_p': 0.23125,
 u'fc2_max_col_norm': 14.869531249999998,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3676,
 u'fc3_dropout_p': 0.21875,
 u'fc3_max_col_norm': 13.936718749999999,
 u'learning_rate': 0.08048828125,
 u'learning_rate_jitter': 4.40625,
 u'train_sample_rate': 0.54921875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
