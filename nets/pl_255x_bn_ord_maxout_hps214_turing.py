#job_id = 214
hyperparameters = \
{u'batch_size': 28,
 u'conv10_num_filters': 355,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 43,
 u'conv3_num_filters': 65,
 u'conv4_num_filters': 126,
 u'conv5_num_filters': 170,
 u'conv6_num_filters': 208,
 u'conv7_num_filters': 256,
 u'conv8_num_filters': 407,
 u'conv9_num_filters': 335,
 u'fc1_dropout_p': 0.653125,
 u'fc1_max_col_norm': 12.770703124999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 862,
 u'fc2_dropout_p': 0.240625,
 u'fc2_max_col_norm': 7.329296874999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3186,
 u'fc3_dropout_p': 0.790625,
 u'fc3_max_col_norm': 5.463671874999999,
 u'learning_rate': 0.038733203125000004,
 u'learning_rate_jitter': 2.640625,
 u'train_sample_rate': 0.580859375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
