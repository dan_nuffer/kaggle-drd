#job_id = 160
hyperparameters = \
{u'batch_size': 55,
 u'conv10_num_filters': 309,
 u'conv1_num_filters': 48,
 u'conv2_num_filters': 29,
 u'conv3_num_filters': 122,
 u'conv4_num_filters': 64,
 u'conv5_num_filters': 234,
 u'conv6_num_filters': 245,
 u'conv7_num_filters': 128,
 u'conv8_num_filters': 389,
 u'conv9_num_filters': 329,
 u'fc1_dropout_p': 0.884375,
 u'fc1_max_col_norm': 15.724609374999998,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3410,
 u'fc2_dropout_p': 0.171875,
 u'fc2_max_col_norm': 11.526953124999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2038,
 u'fc3_dropout_p': 0.534375,
 u'fc3_max_col_norm': 5.619140624999999,
 u'learning_rate': 0.073854296875,
 u'learning_rate_jitter': 1.046875,
 u'train_sample_rate': 0.194140625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
