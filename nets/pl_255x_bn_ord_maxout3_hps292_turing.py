#job_id = 292
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 65,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 123,
 u'conv4_num_filters': 108,
 u'conv5_num_filters': 320,
 u'conv6_num_filters': 301,
 u'conv7_num_filters': 417,
 u'conv8_num_filters': 404,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.4190476406871043,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.2859916988379721,
 u'fc2_max_col_norm': 0.7351968448459076,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 5073,
 u'fc3_dropout_p': 0.28058926781083193,
 u'fc3_max_col_norm': 25.23501334914082,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.980557884244943,
 u'train_sample_rate': 0.5,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
