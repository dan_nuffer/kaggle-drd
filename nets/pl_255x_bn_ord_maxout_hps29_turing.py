#job_id = 29
hyperparameters = \
{u'batch_size': 289,
 u'conv10_num_filters': 500,
 u'conv1_num_filters': 55,
 u'conv2_num_filters': 42,
 u'conv3_num_filters': 102,
 u'conv4_num_filters': 107,
 u'conv5_num_filters': 180,
 u'conv6_num_filters': 236,
 u'conv7_num_filters': 118,
 u'conv8_num_filters': 280,
 u'conv9_num_filters': 312,
 u'fc1_dropout_p': 0.17500000000000002,
 u'fc1_max_col_norm': 9.428125,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 7472,
 u'fc2_dropout_p': 0.475,
 u'fc2_max_col_norm': 13.159374999999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 6992,
 u'fc3_dropout_p': 0.725,
 u'fc3_max_col_norm': 10.671874999999998,
 u'learning_rate': 0.028196875,
 u'learning_rate_jitter': 1.625,
 u'train_sample_rate': 0.24687499999999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
