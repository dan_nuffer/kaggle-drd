#job_id = 157
hyperparameters = \
{u'batch_size': 23,
 u'conv10_num_filters': 406,
 u'conv1_num_filters': 40,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 73,
 u'conv4_num_filters': 40,
 u'conv5_num_filters': 202,
 u'conv6_num_filters': 213,
 u'conv7_num_filters': 79,
 u'conv8_num_filters': 453,
 u'conv9_num_filters': 393,
 u'fc1_dropout_p': 0.28437500000000004,
 u'fc1_max_col_norm': 10.749609374999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2514,
 u'fc2_dropout_p': 0.771875,
 u'fc2_max_col_norm': 6.551953124999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1142,
 u'fc3_dropout_p': 0.334375,
 u'fc3_max_col_norm': 10.594140625,
 u'learning_rate': 0.048879296875000006,
 u'learning_rate_jitter': 4.046875,
 u'train_sample_rate': 0.869140625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
