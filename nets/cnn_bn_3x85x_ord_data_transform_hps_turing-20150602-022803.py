hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 91,
                          'per_pixel_gaussian_noise_sigma': 0.144925447871382,
                          'rotation_range': (0, 1.1620221366842145),
                          'shear_range': (-29.209035494502334,
                                          29.209035494502334),
                          'translation_range': (-3.7355903258144085,
                                                3.7355903258144085),
                          'zoom_range': (0.6733832894690941,
                                         1.4850383364701782)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 58,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-0.0, 0.0),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.39199271168571975,
                                          2.551067839245313)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
