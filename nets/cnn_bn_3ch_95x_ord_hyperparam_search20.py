hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0319587228537253,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.14902283208089717,
                          'do_flip': True,
                          'num_transforms': 74,
                          'per_pixel_gaussian_noise_sigma': 0.10963348047525566,
                          'rotation_range': (0, 0.0),
                          'shear_range': (-22.1188062539313,
                                          22.1188062539313),
                          'translation_range': (-0.0, 0.0),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.127254257515342,
                           'color_shift_sigma': 0.12297544428768765,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 14,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 282.11161322201656),
                           'shear_range': (-0.0, 0.0),
                           'translation_range': (-16.6494282554425,
                                                 16.6494282554425),
                           'zoom_range': (1.0, 1.0)}}


from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
