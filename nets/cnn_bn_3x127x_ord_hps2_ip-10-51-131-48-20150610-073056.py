hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.08509227682192817,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 114.58422458737805),
                          'shear_range': (-25.829415442458583,
                                          25.829415442458583),
                          'translation_range': (-3.859858142801185,
                                                3.859858142801185),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.030570795340125263,
              'momentum': 0.864158000043117},
 'regularization': {'dropout_p1': 0.17332394294380146,
                    'dropout_p2': 0.5218394246753562,
                    'dropout_p3': 0.7430939795000234,
                    'l1': 1.4342370681094502e-06,
                    'l2': 0.0,
                    'max_col_norm1': 5.673374753125775,
                    'max_col_norm2': 2.3444446654872957,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-14.003648630203143,
                                           14.003648630203143),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.5843138300536349,
                                          1.71140908971504)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
