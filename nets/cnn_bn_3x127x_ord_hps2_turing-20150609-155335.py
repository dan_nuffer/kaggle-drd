hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.12621309031506,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 294.10699208213975),
                          'shear_range': (-30.130184554273615,
                                          30.130184554273615),
                          'translation_range': (-2.1292501211122676,
                                                2.1292501211122676),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 2.3297370815832834e-05,
              'momentum': 0.8356668234206007},
 'regularization': {'dropout_p1': 0.3965062592917588,
                    'dropout_p2': 0.38359962669946596,
                    'dropout_p3': 0.5538616274122397,
                    'l1': 0.0023624660774760843,
                    'l2': 0.009743925925313465,
                    'max_col_norm1': 0.0,
                    'max_col_norm2': 0.0,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-3.4760986406732055,
                                           3.4760986406732055),
                           'translation_range': (-11.765841910434473,
                                                 11.765841910434473),
                           'zoom_range': (0.6520722450740911,
                                          1.5335724032946318)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
