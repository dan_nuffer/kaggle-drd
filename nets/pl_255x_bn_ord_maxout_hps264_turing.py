#job_id = 264
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 166,
 u'conv1_num_filters': 43,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 121,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 240,
 u'conv6_num_filters': 379,
 u'conv7_num_filters': 410,
 u'conv8_num_filters': 425,
 u'conv9_num_filters': 640,
 u'fc1_dropout_p': 0.33745430864171305,
 u'fc1_max_col_norm': 25.0,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1358,
 u'fc2_dropout_p': 0.1362989477435731,
 u'fc2_max_col_norm': 8.586722252097209,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1053,
 u'fc3_dropout_p': 0.32667031966254145,
 u'fc3_max_col_norm': 17.875383566042903,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.346636047063626,
 u'train_sample_rate': 0.0108547446105572}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
