#job_id = 47
hyperparameters = \
{u'batch_size': 79,
 u'conv10_num_filters': 482,
 u'conv1_num_filters': 34,
 u'conv2_num_filters': 33,
 u'conv3_num_filters': 103,
 u'conv4_num_filters': 78,
 u'conv5_num_filters': 150,
 u'conv6_num_filters': 158,
 u'conv7_num_filters': 247,
 u'conv8_num_filters': 324,
 u'conv9_num_filters': 420,
 u'fc1_dropout_p': 0.3375,
 u'fc1_max_col_norm': 7.2515624999999995,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2472,
 u'fc2_dropout_p': 0.6625,
 u'fc2_max_col_norm': 0.41093749999999996,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3144,
 u'fc3_dropout_p': 0.48750000000000004,
 u'fc3_max_col_norm': 11.604687499999999,
 u'learning_rate': 0.0203921875,
 u'learning_rate_jitter': 3.5625,
 u'train_sample_rate': 0.3734375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
