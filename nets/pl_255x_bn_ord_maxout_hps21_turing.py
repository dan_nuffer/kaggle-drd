#job_id = 21
hyperparameters = \
{u'batch_size': 481,
 u'conv10_num_filters': 140,
 u'conv1_num_filters': 37,
 u'conv2_num_filters': 57,
 u'conv3_num_filters': 122,
 u'conv4_num_filters': 53,
 u'conv5_num_filters': 204,
 u'conv6_num_filters': 196,
 u'conv7_num_filters': 250,
 u'conv8_num_filters': 264,
 u'conv9_num_filters': 456,
 u'fc1_dropout_p': 0.8250000000000001,
 u'fc1_max_col_norm': 13.159374999999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2192,
 u'fc2_dropout_p': 0.225,
 u'fc2_max_col_norm': 11.915624999999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1712,
 u'fc3_dropout_p': 0.875,
 u'fc3_max_col_norm': 9.428125,
 u'learning_rate': 0.009465625,
 u'learning_rate_jitter': 4.375,
 u'train_sample_rate': 0.303125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
