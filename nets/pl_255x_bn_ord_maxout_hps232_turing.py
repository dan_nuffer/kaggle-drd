#job_id = 232
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 174,
 u'conv1_num_filters': 57,
 u'conv2_num_filters': 18,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 90,
 u'conv5_num_filters': 202,
 u'conv6_num_filters': 236,
 u'conv7_num_filters': 256,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 430,
 u'fc1_dropout_p': 0.8412976373651336,
 u'fc1_max_col_norm': 12.706037342753637,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4096,
 u'fc2_dropout_p': 0.24034876214536066,
 u'fc2_max_col_norm': 0.35554504318680447,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3597,
 u'fc3_dropout_p': 0.46693733451282016,
 u'fc3_max_col_norm': 4.103177666086698,
 u'learning_rate': 0.016778710062879405,
 u'learning_rate_jitter': 4.998417553779065,
 u'train_sample_rate': 0.05}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
