#job_id = 2
hyperparameters = \
{u'batch_size': 513,
 u'conv10_num_filters': 320,
 u'conv1_num_filters': 48,
 u'conv2_num_filters': 40,
 u'conv3_num_filters': 96,
 u'conv4_num_filters': 80,
 u'conv5_num_filters': 192,
 u'conv6_num_filters': 192,
 u'conv7_num_filters': 160,
 u'conv8_num_filters': 384,
 u'conv9_num_filters': 384,
 u'fc1_dropout_p': 0.5,
 u'fc1_max_col_norm': 10.049999999999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4352,
 u'fc2_dropout_p': 0.5,
 u'fc2_max_col_norm': 10.049999999999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 4352,
 u'fc3_dropout_p': 0.5,
 u'fc3_max_col_norm': 10.049999999999999,
 u'learning_rate': 0.050050000000000004,
 u'learning_rate_jitter': 3.0,
 u'train_sample_rate': 0.49999999999999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
