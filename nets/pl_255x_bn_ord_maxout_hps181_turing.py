#job_id = 181
hyperparameters = \
{u'batch_size': 13,
 u'conv10_num_filters': 400,
 u'conv1_num_filters': 49,
 u'conv2_num_filters': 51,
 u'conv3_num_filters': 102,
 u'conv4_num_filters': 60,
 u'conv5_num_filters': 208,
 u'conv6_num_filters': 251,
 u'conv7_num_filters': 209,
 u'conv8_num_filters': 289,
 u'conv9_num_filters': 269,
 u'fc1_dropout_p': 0.771875,
 u'fc1_max_col_norm': 9.194921874999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 2794,
 u'fc2_dropout_p': 0.109375,
 u'fc2_max_col_norm': 4.375390625,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2990,
 u'fc3_dropout_p': 0.171875,
 u'fc3_max_col_norm': 14.014453125,
 u'learning_rate': 0.041074609375,
 u'learning_rate_jitter': 3.109375,
 u'train_sample_rate': 0.123828125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
