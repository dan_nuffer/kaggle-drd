#job_id = 127
hyperparameters = \
{u'batch_size': 114,
 u'conv10_num_filters': 275,
 u'conv1_num_filters': 51,
 u'conv2_num_filters': 63,
 u'conv3_num_filters': 120,
 u'conv4_num_filters': 75,
 u'conv5_num_filters': 133,
 u'conv6_num_filters': 213,
 u'conv7_num_filters': 255,
 u'conv8_num_filters': 374,
 u'conv9_num_filters': 258,
 u'fc1_dropout_p': 0.21875,
 u'fc1_max_col_norm': 1.81015625,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2220,
 u'fc2_dropout_p': 0.44375,
 u'fc2_max_col_norm': 3.36484375,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3396,
 u'fc3_dropout_p': 0.53125,
 u'fc3_max_col_norm': 14.247656249999999,
 u'learning_rate': 0.02897734375,
 u'learning_rate_jitter': 3.71875,
 u'train_sample_rate': 0.73203125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
