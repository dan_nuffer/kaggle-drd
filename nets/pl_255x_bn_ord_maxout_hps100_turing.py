#job_id = 100
hyperparameters = \
{u'batch_size': 38,
 u'conv10_num_filters': 407,
 u'conv1_num_filters': 57,
 u'conv2_num_filters': 58,
 u'conv3_num_filters': 106,
 u'conv4_num_filters': 114,
 u'conv5_num_filters': 129,
 u'conv6_num_filters': 169,
 u'conv7_num_filters': 225,
 u'conv8_num_filters': 398,
 u'conv9_num_filters': 474,
 u'fc1_dropout_p': 0.39375000000000004,
 u'fc1_max_col_norm': 11.138281249999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2108,
 u'fc2_dropout_p': 0.66875,
 u'fc2_max_col_norm': 2.7429687499999997,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2164,
 u'fc3_dropout_p': 0.60625,
 u'fc3_max_col_norm': 7.407031249999999,
 u'learning_rate': 0.08829296875,
 u'learning_rate_jitter': 3.59375,
 u'train_sample_rate': 0.59140625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
