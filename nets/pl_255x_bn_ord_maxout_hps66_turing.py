#job_id = 66
hyperparameters = \
{u'batch_size': 68,
 u'conv10_num_filters': 137,
 u'conv1_num_filters': 36,
 u'conv2_num_filters': 62,
 u'conv3_num_filters': 74,
 u'conv4_num_filters': 100,
 u'conv5_num_filters': 159,
 u'conv6_num_filters': 207,
 u'conv7_num_filters': 149,
 u'conv8_num_filters': 442,
 u'conv9_num_filters': 494,
 u'fc1_dropout_p': 0.10625000000000001,
 u'fc1_max_col_norm': 7.717968749999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1604,
 u'fc2_dropout_p': 0.45625000000000004,
 u'fc2_max_col_norm': 4.297656249999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3564,
 u'fc3_dropout_p': 0.5437500000000001,
 u'fc3_max_col_norm': 17.04609375,
 u'learning_rate': 0.08985390625,
 u'learning_rate_jitter': 2.28125,
 u'train_sample_rate': 0.35234374999999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
