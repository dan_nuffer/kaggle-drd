#job_id = 98
hyperparameters = \
{u'batch_size': 70,
 u'conv10_num_filters': 503,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 21,
 u'conv3_num_filters': 122,
 u'conv4_num_filters': 41,
 u'conv5_num_filters': 225,
 u'conv6_num_filters': 201,
 u'conv7_num_filters': 80,
 u'conv8_num_filters': 334,
 u'conv9_num_filters': 410,
 u'fc1_dropout_p': 0.19375,
 u'fc1_max_col_norm': 6.163281249999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3004,
 u'fc2_dropout_p': 0.86875,
 u'fc2_max_col_norm': 7.717968749999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3060,
 u'fc3_dropout_p': 0.80625,
 u'fc3_max_col_norm': 2.43203125,
 u'learning_rate': 0.06331796875000001,
 u'learning_rate_jitter': 4.59375,
 u'train_sample_rate': 0.81640625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
