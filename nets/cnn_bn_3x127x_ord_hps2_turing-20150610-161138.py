hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.021585249650806763,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 183.81764419785156),
                          'shear_range': (-30.45599089588796,
                                          30.45599089588796),
                          'translation_range': (-7.227991922011992,
                                                7.227991922011992),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.010374298370810789,
              'momentum': 0.7626349300474732},
 'regularization': {'dropout_p1': 0.20355587250623094,
                    'dropout_p2': 0.5742991064233137,
                    'dropout_p3': 0.42218514657011,
                    'l1': 0.0,
                    'l2': 0.0,
                    'max_col_norm1': 6.387202915326899,
                    'max_col_norm2': 4.534610212757531,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-25.491873136259574,
                                           25.491873136259574),
                           'translation_range': (-12.181186082303451,
                                                 12.181186082303451),
                           'zoom_range': (0.8695915600128232,
                                          1.1499651629383958)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
