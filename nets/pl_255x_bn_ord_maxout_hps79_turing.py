#job_id = 79
hyperparameters = \
{u'batch_size': 76,
 u'conv10_num_filters': 353,
 u'conv1_num_filters': 51,
 u'conv2_num_filters': 53,
 u'conv3_num_filters': 127,
 u'conv4_num_filters': 119,
 u'conv5_num_filters': 183,
 u'conv6_num_filters': 151,
 u'conv7_num_filters': 137,
 u'conv8_num_filters': 394,
 u'conv9_num_filters': 446,
 u'fc1_dropout_p': 0.75625,
 u'fc1_max_col_norm': 11.449218749999998,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3172,
 u'fc2_dropout_p': 0.70625,
 u'fc2_max_col_norm': 17.97890625,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2444,
 u'fc3_dropout_p': 0.19375,
 u'fc3_max_col_norm': 0.87734375,
 u'learning_rate': 0.00868515625,
 u'learning_rate_jitter': 4.03125,
 u'train_sample_rate': 0.07109375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
