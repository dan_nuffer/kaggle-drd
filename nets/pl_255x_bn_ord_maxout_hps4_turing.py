#job_id = 4
hyperparameters = \
{u'batch_size': 257,
 u'conv10_num_filters': 416,
 u'conv1_num_filters': 40,
 u'conv2_num_filters': 28,
 u'conv3_num_filters': 112,
 u'conv4_num_filters': 56,
 u'conv5_num_filters': 160,
 u'conv6_num_filters': 160,
 u'conv7_num_filters': 112,
 u'conv8_num_filters': 320,
 u'conv9_num_filters': 448,
 u'fc1_dropout_p': 0.7000000000000001,
 u'fc1_max_col_norm': 5.074999999999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 2432,
 u'fc2_dropout_p': 0.7000000000000001,
 u'fc2_max_col_norm': 15.024999999999999,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2432,
 u'fc3_dropout_p': 0.7000000000000001,
 u'fc3_max_col_norm': 15.024999999999999,
 u'learning_rate': 0.07502500000000001,
 u'learning_rate_jitter': 4.0,
 u'train_sample_rate': 0.725}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
