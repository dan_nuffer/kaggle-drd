#job_id = 15
hyperparameters = \
{u'batch_size': 577,
 u'conv10_num_filters': 152,
 u'conv1_num_filters': 38,
 u'conv2_num_filters': 49,
 u'conv3_num_filters': 84,
 u'conv4_num_filters': 110,
 u'conv5_num_filters': 232,
 u'conv6_num_filters': 152,
 u'conv7_num_filters': 172,
 u'conv8_num_filters': 432,
 u'conv9_num_filters': 464,
 u'fc1_dropout_p': 0.35,
 u'fc1_max_col_norm': 8.806249999999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3872,
 u'fc2_dropout_p': 0.25,
 u'fc2_max_col_norm': 6.31875,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 6752,
 u'fc3_dropout_p': 0.15000000000000002,
 u'fc3_max_col_norm': 6.31875,
 u'learning_rate': 0.043806250000000005,
 u'learning_rate_jitter': 1.25,
 u'train_sample_rate': 0.78125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
