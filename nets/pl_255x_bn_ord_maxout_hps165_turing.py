#job_id = 165
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 171,
 u'conv1_num_filters': 57,
 u'conv2_num_filters': 28,
 u'conv3_num_filters': 68,
 u'conv4_num_filters': 87,
 u'conv5_num_filters': 244,
 u'conv6_num_filters': 239,
 u'conv7_num_filters': 227,
 u'conv8_num_filters': 329,
 u'conv9_num_filters': 421,
 u'fc1_dropout_p': 0.796875,
 u'fc1_max_col_norm': 14.791796875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4026,
 u'fc2_dropout_p': 0.13437500000000002,
 u'fc2_max_col_norm': 11.216015624999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2206,
 u'fc3_dropout_p': 0.546875,
 u'fc3_max_col_norm': 3.442578125,
 u'learning_rate': 0.006733984375000001,
 u'learning_rate_jitter': 4.984375,
 u'train_sample_rate': 0.714453125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
