hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.10762234695296753,
                          'contrast_noise_sigma': 0.14817953148952065,
                          'do_flip': True,
                          'num_transforms': 43,
                          'per_pixel_gaussian_noise_sigma': 0.19821579413600243,
                          'rotation_range': (0, 59.021181142569475),
                          'shear_range': (-36.03224303619426,
                                          36.03224303619426),
                          'translation_range': (-14.738349864334308,
                                                14.738349864334308),
                          'zoom_range': (0.3617680682422512,
                                         2.764201950876352)},
 'train_data_processing': {'allow_stretch': 1.320637652385543,
                           'color_shift_sigma': 0.15095670092026658,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 29,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 328.22085801622836),
                           'shear_range': (-23.651124384376185,
                                           23.651124384376185),
                           'translation_range': (-5.93126617675741,
                                                 5.93126617675741),
                           'zoom_range': (0.8213790916858116,
                                          1.2174646398018045)}}


from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
