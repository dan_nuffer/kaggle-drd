#job_id = 180
hyperparameters = \
{u'batch_size': 17,
 u'conv10_num_filters': 340,
 u'conv1_num_filters': 46,
 u'conv2_num_filters': 37,
 u'conv3_num_filters': 113,
 u'conv4_num_filters': 105,
 u'conv5_num_filters': 220,
 u'conv6_num_filters': 182,
 u'conv7_num_filters': 215,
 u'conv8_num_filters': 377,
 u'conv9_num_filters': 501,
 u'fc1_dropout_p': 0.146875,
 u'fc1_max_col_norm': 6.0855468749999995,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 666,
 u'fc2_dropout_p': 0.684375,
 u'fc2_max_col_norm': 7.484765624999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1086,
 u'fc3_dropout_p': 0.19687500000000002,
 u'fc3_max_col_norm': 19.611328125,
 u'learning_rate': 0.087902734375,
 u'learning_rate_jitter': 2.734375,
 u'train_sample_rate': 0.883203125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
