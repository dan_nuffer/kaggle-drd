#job_id = 271
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 122,
 u'conv1_num_filters': 19,
 u'conv2_num_filters': 26,
 u'conv3_num_filters': 32,
 u'conv4_num_filters': 43,
 u'conv5_num_filters': 219,
 u'conv6_num_filters': 240,
 u'conv7_num_filters': 316,
 u'conv8_num_filters': 602,
 u'conv9_num_filters': 262,
 u'fc1_dropout_p': 0.5745108118558676,
 u'fc1_max_col_norm': 29.894704462369425,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 128,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 15.926112739461693,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.2944996287231135,
 u'fc3_max_col_norm': 24.48637477899347,
 u'learning_rate': 0.005761965164564669,
 u'learning_rate_jitter': 5.087310704434069,
 u'train_sample_rate': 0.5,
 u'training_time': 24.00296873371328}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
