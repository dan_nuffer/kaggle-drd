#job_id = 255
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 129,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 48,
 u'conv4_num_filters': 78,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 382,
 u'conv7_num_filters': 272,
 u'conv8_num_filters': 444,
 u'conv9_num_filters': 493,
 u'fc1_dropout_p': 0.7438666909440443,
 u'fc1_max_col_norm': 19.866243403106775,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 684,
 u'fc2_dropout_p': 0.14783218982804902,
 u'fc2_max_col_norm': 19.844146882585378,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 513,
 u'fc3_dropout_p': 0.39233406654096037,
 u'fc3_max_col_norm': 7.442927971600109,
 u'learning_rate': 0.018491297077272775,
 u'learning_rate_jitter': 5.224269462901756,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
