hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.1957547637193722,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 25.20687016871119),
                          'shear_range': (-27.80290829524159,
                                          27.80290829524159),
                          'translation_range': (-5.1238469578065216,
                                                5.1238469578065216),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.006635199202389738,
              'momentum': 0.8827891062656585},
 'regularization': {'dropout_p1': 0.16088556066658596,
                    'dropout_p2': 0.7232467676593133,
                    'dropout_p3': 0.7963357811319577,
                    'l1': 1.8501405651910942e-07,
                    'l2': 0.0,
                    'max_col_norm1': 0.0,
                    'max_col_norm2': 8.757726681974898,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.010583649532553668,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-10.368630392445779,
                                           10.368630392445779),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.6343561593197758,
                                          1.5764014982881327)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
