#job_id = 178
hyperparameters = \
{u'batch_size': 112,
 u'conv10_num_filters': 436,
 u'conv1_num_filters': 55,
 u'conv2_num_filters': 49,
 u'conv3_num_filters': 96,
 u'conv4_num_filters': 33,
 u'conv5_num_filters': 187,
 u'conv6_num_filters': 215,
 u'conv7_num_filters': 70,
 u'conv8_num_filters': 441,
 u'conv9_num_filters': 437,
 u'fc1_dropout_p': 0.34687500000000004,
 u'fc1_max_col_norm': 11.060546874999998,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 3354,
 u'fc2_dropout_p': 0.884375,
 u'fc2_max_col_norm': 2.509765625,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3774,
 u'fc3_dropout_p': 0.396875,
 u'fc3_max_col_norm': 14.636328124999999,
 u'learning_rate': 0.062927734375,
 u'learning_rate_jitter': 1.734375,
 u'train_sample_rate': 0.658203125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
