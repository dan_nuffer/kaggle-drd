#job_id = 28
hyperparameters = \
{u'batch_size': 417,
 u'conv10_num_filters': 356,
 u'conv1_num_filters': 51,
 u'conv2_num_filters': 60,
 u'conv3_num_filters': 78,
 u'conv4_num_filters': 71,
 u'conv5_num_filters': 228,
 u'conv6_num_filters': 156,
 u'conv7_num_filters': 238,
 u'conv8_num_filters': 312,
 u'conv9_num_filters': 408,
 u'fc1_dropout_p': 0.275,
 u'fc1_max_col_norm': 6.940624999999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 6512,
 u'fc2_dropout_p': 0.5750000000000001,
 u'fc2_max_col_norm': 5.6968749999999995,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 4112,
 u'fc3_dropout_p': 0.42500000000000004,
 u'fc3_max_col_norm': 13.159374999999999,
 u'learning_rate': 0.090634375,
 u'learning_rate_jitter': 2.125,
 u'train_sample_rate': 0.134375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
