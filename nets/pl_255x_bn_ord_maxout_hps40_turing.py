#job_id = 40
hyperparameters = \
{u'batch_size': 177,
 u'conv10_num_filters': 362,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 42,
 u'conv3_num_filters': 91,
 u'conv4_num_filters': 36,
 u'conv5_num_filters': 174,
 u'conv6_num_filters': 246,
 u'conv7_num_filters': 211,
 u'conv8_num_filters': 468,
 u'conv9_num_filters': 468,
 u'fc1_dropout_p': 0.2875,
 u'fc1_max_col_norm': 1.0328125,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 7112,
 u'fc2_dropout_p': 0.4125,
 u'fc2_max_col_norm': 14.092187499999998,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 6632,
 u'fc3_dropout_p': 0.7375,
 u'fc3_max_col_norm': 17.8234375,
 u'learning_rate': 0.0890734375,
 u'learning_rate_jitter': 4.3125,
 u'train_sample_rate': 0.6546875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
