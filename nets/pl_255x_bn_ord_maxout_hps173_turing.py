#job_id = 173
hyperparameters = \
{u'batch_size': 33,
 u'conv10_num_filters': 484,
 u'conv1_num_filters': 42,
 u'conv2_num_filters': 19,
 u'conv3_num_filters': 88,
 u'conv4_num_filters': 69,
 u'conv5_num_filters': 139,
 u'conv6_num_filters': 199,
 u'conv7_num_filters': 143,
 u'conv8_num_filters': 345,
 u'conv9_num_filters': 341,
 u'fc1_dropout_p': 0.246875,
 u'fc1_max_col_norm': 8.573046875,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1114,
 u'fc2_dropout_p': 0.384375,
 u'fc2_max_col_norm': 14.947265624999998,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2430,
 u'fc3_dropout_p': 0.696875,
 u'fc3_max_col_norm': 17.123828125,
 u'learning_rate': 0.025465234375,
 u'learning_rate_jitter': 1.234375,
 u'train_sample_rate': 0.7707031249999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
