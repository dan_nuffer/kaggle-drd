#job_id = 140
hyperparameters = \
{u'batch_size': 19,
 u'conv10_num_filters': 322,
 u'conv1_num_filters': 57,
 u'conv2_num_filters': 55,
 u'conv3_num_filters': 95,
 u'conv4_num_filters': 92,
 u'conv5_num_filters': 222,
 u'conv6_num_filters': 152,
 u'conv7_num_filters': 85,
 u'conv8_num_filters': 413,
 u'conv9_num_filters': 369,
 u'fc1_dropout_p': 0.8093750000000001,
 u'fc1_max_col_norm': 13.858984374999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1058,
 u'fc2_dropout_p': 0.446875,
 u'fc2_max_col_norm': 4.686328124999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3046,
 u'fc3_dropout_p': 0.309375,
 u'fc3_max_col_norm': 16.191015625,
 u'learning_rate': 0.095707421875,
 u'learning_rate_jitter': 1.671875,
 u'train_sample_rate': 0.109765625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
