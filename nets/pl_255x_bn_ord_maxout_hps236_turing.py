#job_id = 236
hyperparameters = \
{u'batch_size': 13,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 67,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 88,
 u'conv4_num_filters': 61,
 u'conv5_num_filters': 192,
 u'conv6_num_filters': 320,
 u'conv7_num_filters': 121,
 u'conv8_num_filters': 307,
 u'conv9_num_filters': 467,
 u'fc1_dropout_p': 0.8813819955261957,
 u'fc1_max_col_norm': 11.90254411534939,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 4620,
 u'fc2_dropout_p': 0.19017894053292891,
 u'fc2_max_col_norm': 4.731627471088063,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 4455,
 u'fc3_dropout_p': 0.5868780527747228,
 u'fc3_max_col_norm': 8.825133767024761,
 u'learning_rate': 0.018460906557145558,
 u'learning_rate_jitter': 6.0,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
