#job_id = 126
hyperparameters = \
{u'batch_size': 82,
 u'conv10_num_filters': 371,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 50,
 u'conv3_num_filters': 71,
 u'conv4_num_filters': 50,
 u'conv5_num_filters': 165,
 u'conv6_num_filters': 245,
 u'conv7_num_filters': 207,
 u'conv8_num_filters': 310,
 u'conv9_num_filters': 450,
 u'fc1_dropout_p': 0.81875,
 u'fc1_max_col_norm': 6.785156249999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1324,
 u'fc2_dropout_p': 0.64375,
 u'fc2_max_col_norm': 18.28984375,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2500,
 u'fc3_dropout_p': 0.33125000000000004,
 u'fc3_max_col_norm': 9.272656249999999,
 u'learning_rate': 0.053952343750000006,
 u'learning_rate_jitter': 2.71875,
 u'train_sample_rate': 0.057031250000000006}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
