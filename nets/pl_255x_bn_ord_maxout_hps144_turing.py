#job_id = 144
hyperparameters = \
{u'batch_size': 59,
 u'conv10_num_filters': 442,
 u'conv1_num_filters': 39,
 u'conv2_num_filters': 52,
 u'conv3_num_filters': 91,
 u'conv4_num_filters': 86,
 u'conv5_num_filters': 214,
 u'conv6_num_filters': 225,
 u'conv7_num_filters': 122,
 u'conv8_num_filters': 493,
 u'conv9_num_filters': 481,
 u'fc1_dropout_p': 0.859375,
 u'fc1_max_col_norm': 0.177734375,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2626,
 u'fc2_dropout_p': 0.19687500000000002,
 u'fc2_max_col_norm': 3.442578125,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2822,
 u'fc3_dropout_p': 0.15937500000000002,
 u'fc3_max_col_norm': 17.434765625,
 u'learning_rate': 0.076976171875,
 u'learning_rate_jitter': 2.921875,
 u'train_sample_rate': 0.503515625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
