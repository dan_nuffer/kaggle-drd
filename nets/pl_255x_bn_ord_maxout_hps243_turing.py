#job_id = 243
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 165,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 79,
 u'conv4_num_filters': 115,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 311,
 u'conv7_num_filters': 312,
 u'conv8_num_filters': 192,
 u'conv9_num_filters': 512,
 u'fc1_dropout_p': 0.8954602789964489,
 u'fc1_max_col_norm': 12.410557039424745,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 4806,
 u'fc2_dropout_p': 0.1010350789062841,
 u'fc2_max_col_norm': 20.0,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 4577,
 u'fc3_dropout_p': 0.8217804953125979,
 u'fc3_max_col_norm': 6.922268500577579,
 u'learning_rate': 0.0027622550073631747,
 u'learning_rate_jitter': 5.712418880945942,
 u'train_sample_rate': 0.22746986622464213}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
