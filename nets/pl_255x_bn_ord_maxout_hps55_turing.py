#job_id = 55
hyperparameters = \
{u'batch_size': 91,
 u'conv10_num_filters': 398,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 62,
 u'conv3_num_filters': 89,
 u'conv4_num_filters': 63,
 u'conv5_num_filters': 210,
 u'conv6_num_filters': 162,
 u'conv7_num_filters': 133,
 u'conv8_num_filters': 316,
 u'conv9_num_filters': 508,
 u'fc1_dropout_p': 0.6625,
 u'fc1_max_col_norm': 5.385937499999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 568,
 u'fc2_dropout_p': 0.8375,
 u'fc2_max_col_norm': 10.9828125,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2360,
 u'fc3_dropout_p': 0.7625000000000001,
 u'fc3_max_col_norm': 19.6890625,
 u'learning_rate': 0.0110265625,
 u'learning_rate_jitter': 1.6875,
 u'train_sample_rate': 0.5140625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
