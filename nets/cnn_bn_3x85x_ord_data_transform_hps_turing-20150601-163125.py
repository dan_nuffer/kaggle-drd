hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.11077418919370541,
                          'do_flip': True,
                          'num_transforms': 99,
                          'per_pixel_gaussian_noise_sigma': 0.19042057536952628,
                          'rotation_range': (0, 109.71511758006324),
                          'shear_range': (-29.167870962662967,
                                          29.167870962662967),
                          'translation_range': (-0.17187677847668148,
                                                0.17187677847668148),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 69,
                           'per_pixel_gaussian_noise_sigma': 0.07504764153852513,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-5.423152703096054,
                                           5.423152703096054),
                           'translation_range': (-17.42240215766713,
                                                 17.42240215766713),
                           'zoom_range': (0.9089084756842292,
                                          1.1002207887292468)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
