from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import tta

data_dir = "unused"
patch_size = (255, 255, 3)
augmentation_params = {
    'zoom_range': (1 / 1.6, 1.6),
    'rotation_range': (0, 360),
    'shear_range': (-30, 30),
    'translation_range': (-10 * patch_size[0] / 127., 10 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.3,
}

batch_size = 14
chunk_size = batch_size * 50
predict_batch_size = 4

num_examples = int(35162 * 3 * .9) # * 3 because of upsampling class imbalance. .9 because of training/validation split
momentum = 0.88
learning_rate = 0.01
learning_rate_decay = 0.50
learning_rate_patience = 6.0 * num_examples // chunk_size
learning_rate_rolling_mean_window = 6.0 * num_examples // chunk_size
learning_rate_improvement = 1.0
learning_rate_jitter = 4.79
num_chunks_train = num_examples // chunk_size * 10000 # stop after 10000 epochs
divergence_threshold = 26.0
failed_training_patience = 25

validate_every = 2 * num_examples // chunk_size # about once per 2 epochs
validate_train = False
save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 360),
    'shear_range': (-10, 10),
    'translation_range': (-8 * patch_size[0] / 127., 8 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.2,
})


data_loader = load.CachingDataLoader(
    estimate_scale=estimate_scale,
    num_chunks_train=num_chunks_train,
    patch_size=patch_size,
    chunk_size=chunk_size,
    augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    test_size=0.1,
    image_yx=(256,256),
    cache_dir="unsup-data-256x256",
    predict_mode="regression",
    random_state=2564 # 256x-4, change this for other nets
)


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer


def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0d = nn.layers.dropout(l0, p=0.1)

    l1a = nn.layers.dropout(batch_norm(Conv2DLayer(l0d, num_filters=40, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l1b = nn.layers.dropout(batch_norm(Conv2DLayer(l1a, num_filters=40, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l1 = nn.layers.dropout(MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2)), p=0.25)

    l2a = nn.layers.dropout(batch_norm(Conv2DLayer(l1, num_filters=80, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l2b = nn.layers.dropout(batch_norm(Conv2DLayer(l2a, num_filters=80, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l2 = nn.layers.dropout(MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2)), p=0.25)

    l3a = nn.layers.dropout(batch_norm(Conv2DLayer(l2, num_filters=160, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l3b = nn.layers.dropout(batch_norm(Conv2DLayer(l3a, num_filters=160, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l3c = nn.layers.dropout(batch_norm(Conv2DLayer(l3b, num_filters=160, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l3 = nn.layers.dropout(MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2)), p=0.25)
    
    l4a = nn.layers.dropout(batch_norm(Conv2DLayer(l3, num_filters=320, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l4b = nn.layers.dropout(batch_norm(Conv2DLayer(l4a, num_filters=320, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l4c = nn.layers.dropout(batch_norm(Conv2DLayer(l4b, num_filters=320, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    l4 = nn.layers.dropout(MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2)), p=0.25)
    #
    # l5a = nn.layers.dropout(batch_norm(Conv2DLayer(l4, num_filters=640, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    # l5b = nn.layers.dropout(batch_norm(Conv2DLayer(l5a, num_filters=640, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    # l5c = nn.layers.dropout(batch_norm(Conv2DLayer(l5b, num_filters=640, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu_01)), p=0.25)
    # l5 = nn.layers.dropout(MaxPool2DLayer(l5c, pool_size=(3, 3), stride=(2, 2)), p=0.25)

    l5f = nn.layers.flatten(l4)

    l6 = DenseLayer(l5f, num_units=1024, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=None, max_col_norm=25.0)
    l6fp = batch_norm(nn.layers.FeaturePoolLayer(l6, pool_size=2))

    l7 = DenseLayer(nn.layers.dropout(l6fp, p=0.5), num_units=1024, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=None, max_col_norm=25.0)
    l7fp = batch_norm(nn.layers.FeaturePoolLayer(l7, pool_size=2))

    l8 = batch_norm(DenseLayer(nn.layers.dropout(l7fp, p=0.5), num_units=1, nonlinearity=None, W=nn_plankton.Orthogonal(1.0), max_col_norm=25.0))

    return [l0], l8, l3


def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.mean_squared_error)


#pre_init_path = "metadata/mse_64x-5-turing-20150804-212732_best.pkl"
pre_init_path = "metadata/mse_128x-3-turing-20150808-160424.pkl"
