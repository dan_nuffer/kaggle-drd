#job_id = 85
hyperparameters = \
{u'batch_size': 64,
 u'conv10_num_filters': 341,
 u'conv1_num_filters': 50,
 u'conv2_num_filters': 48,
 u'conv3_num_filters': 80,
 u'conv4_num_filters': 37,
 u'conv5_num_filters': 147,
 u'conv6_num_filters': 203,
 u'conv7_num_filters': 71,
 u'conv8_num_filters': 306,
 u'conv9_num_filters': 422,
 u'fc1_dropout_p': 0.43125,
 u'fc1_max_col_norm': 5.852343749999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3956,
 u'fc2_dropout_p': 0.78125,
 u'fc2_max_col_norm': 3.67578125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2108,
 u'fc3_dropout_p': 0.86875,
 u'fc3_max_col_norm': 2.7429687499999997,
 u'learning_rate': 0.049269531250000005,
 u'learning_rate_jitter': 1.15625,
 u'train_sample_rate': 0.60546875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
