#job_id = 26
hyperparameters = \
{u'batch_size': 673,
 u'conv10_num_filters': 452,
 u'conv1_num_filters': 43,
 u'conv2_num_filters': 23,
 u'conv3_num_filters': 94,
 u'conv4_num_filters': 95,
 u'conv5_num_filters': 132,
 u'conv6_num_filters': 252,
 u'conv7_num_filters': 94,
 u'conv8_num_filters': 504,
 u'conv9_num_filters': 472,
 u'fc1_dropout_p': 0.475,
 u'fc1_max_col_norm': 11.915624999999999,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 752,
 u'fc2_dropout_p': 0.775,
 u'fc2_max_col_norm': 0.7218749999999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 6032,
 u'fc3_dropout_p': 0.225,
 u'fc3_max_col_norm': 18.134375,
 u'learning_rate': 0.065659375,
 u'learning_rate_jitter': 1.125,
 u'train_sample_rate': 0.35937499999999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
