#job_id = 130
hyperparameters = \
{u'batch_size': 98,
 u'conv10_num_filters': 131,
 u'conv1_num_filters': 55,
 u'conv2_num_filters': 44,
 u'conv3_num_filters': 95,
 u'conv4_num_filters': 111,
 u'conv5_num_filters': 213,
 u'conv6_num_filters': 165,
 u'conv7_num_filters': 134,
 u'conv8_num_filters': 342,
 u'conv9_num_filters': 418,
 u'fc1_dropout_p': 0.11875000000000001,
 u'fc1_max_col_norm': 4.297656249999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 1772,
 u'fc2_dropout_p': 0.5437500000000001,
 u'fc2_max_col_norm': 15.802343749999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2052,
 u'fc3_dropout_p': 0.23125,
 u'fc3_max_col_norm': 11.76015625,
 u'learning_rate': 0.09141484375,
 u'learning_rate_jitter': 4.21875,
 u'train_sample_rate': 0.84453125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
