hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.03015682407250818,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 204.21599736797475),
                          'shear_range': (-26.935384172049762,
                                          26.935384172049762),
                          'translation_range': (-9.828677082762535,
                                                9.828677082762535),
                          'zoom_range': (1.0, 1.0)},
 'learning': {'learning_rate': 0.0005500266703501113,
              'momentum': 0.7888257531709809},
 'regularization': {'dropout_p1': 0.23856828909947497,
                    'dropout_p2': 0.7011511069770595,
                    'dropout_p3': 0.10749055860103013,
                    'l1': 0.0013535447652207834,
                    'l2': 0.0,
                    'max_col_norm1': 5.794737271725581,
                    'max_col_norm2': 9.153718261128914,
                    'max_col_norm3': 4.9568377094399425},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-16.27470706185495,
                                           16.27470706185495),
                           'translation_range': (-7.060307826540182,
                                                 7.060307826540182),
                           'zoom_range': (0.5220484207544401,
                                          1.915531127466771)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
