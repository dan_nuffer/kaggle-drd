#job_id = 61
hyperparameters = \
{u'batch_size': 35,
 u'conv10_num_filters': 134,
 u'conv1_num_filters': 52,
 u'conv2_num_filters': 35,
 u'conv3_num_filters': 85,
 u'conv4_num_filters': 69,
 u'conv5_num_filters': 202,
 u'conv6_num_filters': 234,
 u'conv7_num_filters': 97,
 u'conv8_num_filters': 492,
 u'conv9_num_filters': 332,
 u'fc1_dropout_p': 0.1125,
 u'fc1_max_col_norm': 6.629687499999999,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 792,
 u'fc2_dropout_p': 0.8875000000000001,
 u'fc2_max_col_norm': 19.6890625,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3032,
 u'fc3_dropout_p': 0.6125,
 u'fc3_max_col_norm': 6.007812499999999,
 u'learning_rate': 0.004782812500000001,
 u'learning_rate_jitter': 3.4375,
 u'train_sample_rate': 0.6828124999999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
