#job_id = 57
hyperparameters = \
{u'batch_size': 19,
 u'conv10_num_filters': 374,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 29,
 u'conv3_num_filters': 77,
 u'conv4_num_filters': 82,
 u'conv5_num_filters': 186,
 u'conv6_num_filters': 186,
 u'conv7_num_filters': 217,
 u'conv8_num_filters': 396,
 u'conv9_num_filters': 300,
 u'fc1_dropout_p': 0.8125,
 u'fc1_max_col_norm': 4.1421874999999995,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2136,
 u'fc2_dropout_p': 0.7875,
 u'fc2_max_col_norm': 17.2015625,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1688,
 u'fc3_dropout_p': 0.7125,
 u'fc3_max_col_norm': 13.470312499999999,
 u'learning_rate': 0.04224531250000001,
 u'learning_rate_jitter': 1.9375,
 u'train_sample_rate': 0.34531249999999997}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
