#job_id = 275
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 116,
 u'conv1_num_filters': 50,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 32,
 u'conv4_num_filters': 80,
 u'conv5_num_filters': 234,
 u'conv6_num_filters': 401,
 u'conv7_num_filters': 269,
 u'conv8_num_filters': 140,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.95,
 u'fc1_max_col_norm': 27.684293226672715,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 694,
 u'fc2_dropout_p': 0.1564031704358414,
 u'fc2_max_col_norm': 23.950519984798245,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 1918,
 u'fc3_dropout_p': 0.13641284421468064,
 u'fc3_max_col_norm': 29.945096682474368,
 u'learning_rate': 0.011547914169592303,
 u'learning_rate_jitter': 2.9304307438306862,
 u'train_sample_rate': 0.5,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
