#job_id = 99
hyperparameters = \
{u'batch_size': 102,
 u'conv10_num_filters': 215,
 u'conv1_num_filters': 40,
 u'conv2_num_filters': 33,
 u'conv3_num_filters': 73,
 u'conv4_num_filters': 66,
 u'conv5_num_filters': 193,
 u'conv6_num_filters': 233,
 u'conv7_num_filters': 128,
 u'conv8_num_filters': 270,
 u'conv9_num_filters': 346,
 u'fc1_dropout_p': 0.7937500000000001,
 u'fc1_max_col_norm': 1.18828125,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3900,
 u'fc2_dropout_p': 0.26875000000000004,
 u'fc2_max_col_norm': 12.692968749999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3956,
 u'fc3_dropout_p': 0.20625000000000002,
 u'fc3_max_col_norm': 17.35703125,
 u'learning_rate': 0.038342968750000005,
 u'learning_rate_jitter': 1.59375,
 u'train_sample_rate': 0.14140625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
