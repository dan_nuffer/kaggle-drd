#job_id = 247
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 321,
 u'conv1_num_filters': 44,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 94,
 u'conv4_num_filters': 72,
 u'conv5_num_filters': 172,
 u'conv6_num_filters': 128,
 u'conv7_num_filters': 382,
 u'conv8_num_filters': 326,
 u'conv9_num_filters': 399,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 19.98705919061973,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 1078,
 u'fc2_dropout_p': 0.1,
 u'fc2_max_col_norm': 10.97816966968141,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 701,
 u'fc3_dropout_p': 0.1,
 u'fc3_max_col_norm': 0.1,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.999777164207616,
 u'train_sample_rate': 0.9278039095788507}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
