#job_id = 294
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 32,
 u'conv1_num_filters': 34,
 u'conv2_num_filters': 6,
 u'conv3_num_filters': 85,
 u'conv4_num_filters': 53,
 u'conv5_num_filters': 273,
 u'conv6_num_filters': 289,
 u'conv7_num_filters': 245,
 u'conv8_num_filters': 361,
 u'conv9_num_filters': 411,
 u'fc1_dropout_p': 0.947931495960018,
 u'fc1_max_col_norm': 30.0,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 32,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 15.91101131591006,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 5120,
 u'fc3_dropout_p': 0.35366538544261417,
 u'fc3_max_col_norm': 20.807679160496008,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 2.9540501213605146,
 u'train_sample_rate': 0.25,
 u'training_time': 30.898117364930993}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
