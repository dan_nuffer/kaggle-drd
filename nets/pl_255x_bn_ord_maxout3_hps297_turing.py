#job_id = 297
hyperparameters = \
{u'batch_size': 2,
 u'conv10_num_filters': 41,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 68,
 u'conv4_num_filters': 58,
 u'conv5_num_filters': 289,
 u'conv6_num_filters': 68,
 u'conv7_num_filters': 466,
 u'conv8_num_filters': 185,
 u'conv9_num_filters': 444,
 u'fc1_dropout_p': 0.1,
 u'fc1_max_col_norm': 18.52510061642872,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 666,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 0.2945560893005644,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 5089,
 u'fc3_dropout_p': 0.1,
 u'fc3_max_col_norm': 24.73056933263293,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 6.0,
 u'train_sample_rate': 0.25,
 u'training_time': 25.738373479649905}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
