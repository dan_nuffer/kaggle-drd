#job_id = 120
hyperparameters = \
{u'batch_size': 42,
 u'conv10_num_filters': 155,
 u'conv1_num_filters': 49,
 u'conv2_num_filters': 23,
 u'conv3_num_filters': 75,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 189,
 u'conv6_num_filters': 189,
 u'conv7_num_filters': 219,
 u'conv8_num_filters': 486,
 u'conv9_num_filters': 370,
 u'fc1_dropout_p': 0.36875,
 u'fc1_max_col_norm': 5.54140625,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1100,
 u'fc2_dropout_p': 0.69375,
 u'fc2_max_col_norm': 12.07109375,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3172,
 u'fc3_dropout_p': 0.28125,
 u'fc3_max_col_norm': 15.491406249999999,
 u'learning_rate': 0.060196093750000006,
 u'learning_rate_jitter': 4.46875,
 u'train_sample_rate': 0.22578125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
