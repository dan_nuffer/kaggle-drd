#job_id = 146
hyperparameters = \
{u'batch_size': 106,
 u'conv10_num_filters': 201,
 u'conv1_num_filters': 51,
 u'conv2_num_filters': 33,
 u'conv3_num_filters': 83,
 u'conv4_num_filters': 98,
 u'conv5_num_filters': 198,
 u'conv6_num_filters': 209,
 u'conv7_num_filters': 146,
 u'conv8_num_filters': 333,
 u'conv9_num_filters': 449,
 u'fc1_dropout_p': 0.359375,
 u'fc1_max_col_norm': 12.615234374999998,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1282,
 u'fc2_dropout_p': 0.49687500000000007,
 u'fc2_max_col_norm': 5.930078125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2374,
 u'fc3_dropout_p': 0.259375,
 u'fc3_max_col_norm': 9.972265624999999,
 u'learning_rate': 0.08946367187500001,
 u'learning_rate_jitter': 3.421875,
 u'train_sample_rate': 0.166015625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
