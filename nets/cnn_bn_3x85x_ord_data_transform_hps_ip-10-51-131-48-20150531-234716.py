hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.9166851070439215,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 3,
                          'per_pixel_gaussian_noise_sigma': 0.09525276661652979,
                          'rotation_range': (0, 17.85212677767086),
                          'shear_range': (-39.82927709825826,
                                          39.82927709825826),
                          'translation_range': (-15.698552845597284,
                                                15.698552845597284),
                          'zoom_range': (0.4324534793593424,
                                         2.3123874537475073)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.009373533808280904,
                           'do_flip': True,
                           'num_transforms': 58,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 296.44632011682825),
                           'shear_range': (-13.875629897347753,
                                           13.875629897347753),
                           'translation_range': (-0.04921451505587651,
                                                 0.04921451505587651),
                           'zoom_range': (0.48691000735868606,
                                          2.0537676056909264)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
