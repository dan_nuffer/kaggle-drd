#job_id = 187
hyperparameters = \
{u'batch_size': 92,
 u'conv10_num_filters': 255,
 u'conv1_num_filters': 37,
 u'conv2_num_filters': 33,
 u'conv3_num_filters': 110,
 u'conv4_num_filters': 72,
 u'conv5_num_filters': 224,
 u'conv6_num_filters': 203,
 u'conv7_num_filters': 233,
 u'conv8_num_filters': 385,
 u'conv9_num_filters': 301,
 u'fc1_dropout_p': 0.47187500000000004,
 u'fc1_max_col_norm': 16.657421875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 554,
 u'fc2_dropout_p': 0.40937500000000004,
 u'fc2_max_col_norm': 6.8628906249999995,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2542,
 u'fc3_dropout_p': 0.271875,
 u'fc3_max_col_norm': 1.576953125,
 u'learning_rate': 0.028587109375,
 u'learning_rate_jitter': 2.609375,
 u'train_sample_rate': 0.686328125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
