#job_id = 155
hyperparameters = \
{u'batch_size': 94,
 u'conv10_num_filters': 382,
 u'conv1_num_filters': 33,
 u'conv2_num_filters': 50,
 u'conv3_num_filters': 93,
 u'conv4_num_filters': 107,
 u'conv5_num_filters': 161,
 u'conv6_num_filters': 205,
 u'conv7_num_filters': 164,
 u'conv8_num_filters': 373,
 u'conv9_num_filters': 345,
 u'fc1_dropout_p': 0.43437500000000007,
 u'fc1_max_col_norm': 19.455859375,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 4082,
 u'fc2_dropout_p': 0.821875,
 u'fc2_max_col_norm': 0.333203125,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 4054,
 u'fc3_dropout_p': 0.384375,
 u'fc3_max_col_norm': 16.812890625,
 u'learning_rate': 0.005173046875,
 u'learning_rate_jitter': 4.296875,
 u'train_sample_rate': 0.250390625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
