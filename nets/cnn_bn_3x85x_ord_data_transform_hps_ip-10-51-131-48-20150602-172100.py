hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.10301112408136151,
                          'do_flip': True,
                          'num_transforms': 75,
                          'per_pixel_gaussian_noise_sigma': 0.15244589967545588,
                          'rotation_range': (0, 92.12642919528793),
                          'shear_range': (-26.049063660687548,
                                          26.049063660687548),
                          'translation_range': (-6.172426629123915,
                                                6.172426629123915),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 55,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-25.939837301503673,
                                           25.939837301503673),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.6015638615231143,
                                          1.66233389995881)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
