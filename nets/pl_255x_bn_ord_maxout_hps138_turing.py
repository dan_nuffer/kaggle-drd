#job_id = 138
hyperparameters = \
{u'batch_size': 114,
 u'conv10_num_filters': 418,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 18,
 u'conv3_num_filters': 79,
 u'conv4_num_filters': 67,
 u'conv5_num_filters': 189,
 u'conv6_num_filters': 249,
 u'conv7_num_filters': 230,
 u'conv8_num_filters': 349,
 u'conv9_num_filters': 305,
 u'fc1_dropout_p': 0.609375,
 u'fc1_max_col_norm': 8.883984374999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3746,
 u'fc2_dropout_p': 0.246875,
 u'fc2_max_col_norm': 9.661328124999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 2150,
 u'fc3_dropout_p': 0.109375,
 u'fc3_max_col_norm': 11.216015624999999,
 u'learning_rate': 0.070732421875,
 u'learning_rate_jitter': 2.671875,
 u'train_sample_rate': 0.33476562499999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
