#job_id = 153
hyperparameters = \
{u'batch_size': 63,
 u'conv10_num_filters': 478,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 38,
 u'conv3_num_filters': 77,
 u'conv4_num_filters': 34,
 u'conv5_num_filters': 194,
 u'conv6_num_filters': 172,
 u'conv7_num_filters': 116,
 u'conv8_num_filters': 437,
 u'conv9_num_filters': 281,
 u'fc1_dropout_p': 0.234375,
 u'fc1_max_col_norm': 4.5308593749999995,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1394,
 u'fc2_dropout_p': 0.621875,
 u'fc2_max_col_norm': 5.3082031249999995,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1366,
 u'fc3_dropout_p': 0.184375,
 u'fc3_max_col_norm': 11.837890624999998,
 u'learning_rate': 0.030148046875,
 u'learning_rate_jitter': 3.296875,
 u'train_sample_rate': 0.47539062499999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
