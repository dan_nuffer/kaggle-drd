from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import BatchNormalizationLayer, batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import dihedral
import tmp_dnn
import tta
from utils import polynomial_decay

# for now, copy and paste from the output of hyperparameter_search.py and remove model item.
hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.6672923571571703,
                          'color_shift_sigma': 0.16744279880103755,
                          'contrast_noise_sigma': 0.18457517348923713,
                          'do_flip': False,
                          'num_transforms': 25,
                          'per_pixel_gaussian_noise_sigma': 0.0,
                          'rotation_range': (0, 318.6933681490268),
                          'shear_range': (-2.6807140089544435,
                                          2.6807140089544435),
                          'translation_range': (-0.0, 0.0),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.9211278398605247,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.11424356061674275,
                           'do_flip': True,
                           'num_transforms': 81,
                           'per_pixel_gaussian_noise_sigma': 0.006323759192873535,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-5.057986057733288,
                                           5.057986057733288),
                           'translation_range': (-2.5162283722071477,
                                                 2.5162283722071477),
                           'zoom_range': (0.6161092651858012,
                                          1.62308872225518)}}



data_dir = "data-128x128"
input_side_len = hyperparameters['data_params']['input_side_len']
patch_size = (input_side_len, input_side_len, 3)
augmentation_params = hyperparameters['train_data_processing']

batch_size = hyperparameters['data_params']['batch_size']
chunk_size = hyperparameters['data_params']['chunk_size']

momentum = 0.9

learning_rate = 0.2
learning_rate_decay = 0.90
learning_rate_patience = 20
learning_rate_rolling_mean_window = 30
learning_rate_improvement = 0.999
num_chunks_train = hyperparameters['data_params']['num_chunks_train']
divergence_threshold = 26.0

validate_every = 30

save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(
    num_transforms = hyperparameters['eval_data_processing']['num_transforms'],
    zoom_range = hyperparameters['eval_data_processing']['zoom_range'],
    rotation_range = hyperparameters['eval_data_processing']['rotation_range'],
    shear_range = hyperparameters['eval_data_processing']['shear_range'],
    translation_range = hyperparameters['eval_data_processing']['translation_range'],
    do_flip=hyperparameters['eval_data_processing']['do_flip'],
    allow_stretch=hyperparameters['eval_data_processing']['allow_stretch'])


data_loader = load.ZmuvRescaledDataLoader(estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
    patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer


def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0g = nn.layers.GaussianNoiseLayer(l0, sigma=hyperparameters['train_data_processing']['per_pixel_gaussian_noise_sigma'])
    l0c = nn_plankton.ContrastNoiseLayer(l0g, sigma=hyperparameters['train_data_processing']['contrast_noise_sigma'],
                                         test_sigma=hyperparameters['eval_data_processing']['contrast_noise_sigma'])
    l0cs = nn_plankton.ColorShiftNoiseLayer(l0c, sigma=hyperparameters['train_data_processing']['color_shift_sigma'],
                                            test_sigma=hyperparameters['eval_data_processing']['color_shift_sigma'])

    l1a = batch_norm(Conv2DLayer(l0cs, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=0.5), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0)))

    return [l0], l7

def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy)
