#job_id = 60
hyperparameters = \
{u'batch_size': 51,
 u'conv10_num_filters': 278,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 125,
 u'conv4_num_filters': 106,
 u'conv5_num_filters': 154,
 u'conv6_num_filters': 154,
 u'conv7_num_filters': 169,
 u'conv8_num_filters': 460,
 u'conv9_num_filters': 492,
 u'fc1_dropout_p': 0.21250000000000002,
 u'fc1_max_col_norm': 9.117187499999998,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 1240,
 u'fc2_dropout_p': 0.1875,
 u'fc2_max_col_norm': 2.2765625,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 792,
 u'fc3_dropout_p': 0.1125,
 u'fc3_max_col_norm': 8.495312499999999,
 u'learning_rate': 0.0672203125,
 u'learning_rate_jitter': 4.9375,
 u'train_sample_rate': 0.5703125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
