hyperparameters = \
{'data_params': {'batch_size': 32,
                 'chunk_size': 4096,
                 'input_side_len': 127,
                 'num_chunks_train': 600},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'contrast_noise_sigma': 0.017347522837373494,
                          'do_flip': True,
                          'num_transforms': 90,
                          'rotation_range': (0, 218.15962332813916),
                          'shear_range': (-23.983664608625, 23.983664608625),
                          'translation_range': (-6.026818525875832,
                                                6.026818525875832),
                          'zoom_range': (0.8514588183944862,
                                         1.1744549218312208)},
 'learning': {'learning_rate': 0.007730501587659688,
              'momentum': 0.9840813572358316},
 'regularization': {'dropout_p1': 0.43273999883624487,
                    'dropout_p2': 0.6540923510222514,
                    'dropout_p3': 0.7495587488898587,
                    'l1': 4.209893334259202e-05,
                    'l2': 2.395480397911203e-05,
                    'max_col_norm1': 0.0,
                    'max_col_norm2': 0.0,
                    'max_col_norm3': 0.0},
 'train_data_processing': {'allow_stretch': 1.0,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'rotation_range': (0, 0),
                           'shear_range': (-9.404575383034128,
                                           9.404575383034128),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.8142710813821422,
                                          1.2280922445417095)}}
from . import cnn_bn_3x127x_ord_hps2_tmpl
cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3x127x_ord_hps2_tmpl import *
