#job_id = 123
hyperparameters = \
{u'batch_size': 90,
 u'conv10_num_filters': 203,
 u'conv1_num_filters': 45,
 u'conv2_num_filters': 41,
 u'conv3_num_filters': 116,
 u'conv4_num_filters': 69,
 u'conv5_num_filters': 141,
 u'conv6_num_filters': 173,
 u'conv7_num_filters': 195,
 u'conv8_num_filters': 262,
 u'conv9_num_filters': 402,
 u'fc1_dropout_p': 0.26875000000000004,
 u'fc1_max_col_norm': 13.003906249999998,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3340,
 u'fc2_dropout_p': 0.19375,
 u'fc2_max_col_norm': 4.608593749999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3620,
 u'fc3_dropout_p': 0.78125,
 u'fc3_max_col_norm': 13.003906249999998,
 u'learning_rate': 0.04770859375000001,
 u'learning_rate_jitter': 4.96875,
 u'train_sample_rate': 0.33828125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
