#job_id = 7
hyperparameters = \
{u'batch_size': 641,
 u'conv10_num_filters': 464,
 u'conv1_num_filters': 52,
 u'conv2_num_filters': 58,
 u'conv3_num_filters': 72,
 u'conv4_num_filters': 44,
 u'conv5_num_filters': 144,
 u'conv6_num_filters': 176,
 u'conv7_num_filters': 88,
 u'conv8_num_filters': 416,
 u'conv9_num_filters': 288,
 u'fc1_dropout_p': 0.6,
 u'fc1_max_col_norm': 12.5375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 5312,
 u'fc2_dropout_p': 0.4,
 u'fc2_max_col_norm': 7.562499999999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1472,
 u'fc3_dropout_p': 0.2,
 u'fc3_max_col_norm': 12.5375,
 u'learning_rate': 0.0125875,
 u'learning_rate_jitter': 4.5,
 u'train_sample_rate': 0.6125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
