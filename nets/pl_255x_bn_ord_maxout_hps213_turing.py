#job_id = 213
hyperparameters = \
{u'batch_size': 20,
 u'conv10_num_filters': 138,
 u'conv1_num_filters': 44,
 u'conv2_num_filters': 52,
 u'conv3_num_filters': 118,
 u'conv4_num_filters': 96,
 u'conv5_num_filters': 130,
 u'conv6_num_filters': 151,
 u'conv7_num_filters': 244,
 u'conv8_num_filters': 423,
 u'conv9_num_filters': 287,
 u'fc1_dropout_p': 0.40312500000000007,
 u'fc1_max_col_norm': 6.551953124999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3774,
 u'fc2_dropout_p': 0.5906250000000001,
 u'fc2_max_col_norm': 11.060546874999998,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 3858,
 u'fc3_dropout_p': 0.340625,
 u'fc3_max_col_norm': 11.682421875,
 u'learning_rate': 0.057464453125,
 u'learning_rate_jitter': 4.890625,
 u'train_sample_rate': 0.749609375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
