#job_id = 50
hyperparameters = \
{u'batch_size': 75,
 u'conv10_num_filters': 446,
 u'conv1_num_filters': 62,
 u'conv2_num_filters': 44,
 u'conv3_num_filters': 113,
 u'conv4_num_filters': 124,
 u'conv5_num_filters': 130,
 u'conv6_num_filters': 210,
 u'conv7_num_filters': 253,
 u'conv8_num_filters': 284,
 u'conv9_num_filters': 348,
 u'fc1_dropout_p': 0.5625,
 u'fc1_max_col_norm': 7.873437499999999,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 1016,
 u'fc2_dropout_p': 0.1375,
 u'fc2_max_col_norm': 8.495312499999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1016,
 u'fc3_dropout_p': 0.4625,
 u'fc3_max_col_norm': 17.2015625,
 u'learning_rate': 0.07346406250000001,
 u'learning_rate_jitter': 2.1875,
 u'train_sample_rate': 0.6265625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
