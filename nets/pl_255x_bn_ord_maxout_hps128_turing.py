#job_id = 128
hyperparameters = \
{u'batch_size': 50,
 u'conv10_num_filters': 467,
 u'conv1_num_filters': 35,
 u'conv2_num_filters': 38,
 u'conv3_num_filters': 87,
 u'conv4_num_filters': 123,
 u'conv5_num_filters': 197,
 u'conv6_num_filters': 149,
 u'conv7_num_filters': 158,
 u'conv8_num_filters': 502,
 u'conv9_num_filters': 386,
 u'fc1_dropout_p': 0.61875,
 u'fc1_max_col_norm': 11.76015625,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4012,
 u'fc2_dropout_p': 0.84375,
 u'fc2_max_col_norm': 13.314843749999998,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1604,
 u'fc3_dropout_p': 0.13125,
 u'fc3_max_col_norm': 4.297656249999999,
 u'learning_rate': 0.07892734375,
 u'learning_rate_jitter': 1.71875,
 u'train_sample_rate': 0.28203124999999996}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
