#job_id = 82
hyperparameters = \
{u'batch_size': 80,
 u'conv10_num_filters': 389,
 u'conv1_num_filters': 46,
 u'conv2_num_filters': 17,
 u'conv3_num_filters': 105,
 u'conv4_num_filters': 73,
 u'conv5_num_filters': 163,
 u'conv6_num_filters': 219,
 u'conv7_num_filters': 143,
 u'conv8_num_filters': 466,
 u'conv9_num_filters': 326,
 u'fc1_dropout_p': 0.13125,
 u'fc1_max_col_norm': 13.314843749999998,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 820,
 u'fc2_dropout_p': 0.48125000000000007,
 u'fc2_max_col_norm': 11.138281249999999,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 764,
 u'fc3_dropout_p': 0.16875,
 u'fc3_max_col_norm': 5.230468749999999,
 u'learning_rate': 0.061757031250000004,
 u'learning_rate_jitter': 1.65625,
 u'train_sample_rate': 0.9429687499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
