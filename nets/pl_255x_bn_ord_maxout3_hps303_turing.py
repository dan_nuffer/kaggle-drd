#job_id = 303
hyperparameters = \
{u'batch_size': 8,
 u'conv10_num_filters': 32,
 u'conv1_num_filters': 60,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 114,
 u'conv4_num_filters': 81,
 u'conv5_num_filters': 320,
 u'conv6_num_filters': 138,
 u'conv7_num_filters': 266,
 u'conv8_num_filters': 128,
 u'conv9_num_filters': 550,
 u'fc1_dropout_p': 0.317723220415678,
 u'fc1_max_col_norm': 28.021716574083012,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 43,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 22.76023929038012,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 2407,
 u'fc3_dropout_p': 0.782774815339742,
 u'fc3_max_col_norm': 26.67082740090436,
 u'learning_rate': 0.003258587959120175,
 u'learning_rate_jitter': 5.34597072017553,
 u'train_sample_rate': 0.2535058781979751,
 u'training_time': 24.0}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
