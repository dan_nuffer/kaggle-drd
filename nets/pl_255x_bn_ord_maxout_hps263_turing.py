#job_id = 263
hyperparameters = \
{u'batch_size': 9,
 u'conv10_num_filters': 117,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 46,
 u'conv4_num_filters': 110,
 u'conv5_num_filters': 267,
 u'conv6_num_filters': 128,
 u'conv7_num_filters': 325,
 u'conv8_num_filters': 484,
 u'conv9_num_filters': 632,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 25.0,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 256,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 20.80087718237284,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 2309,
 u'fc3_dropout_p': 0.4299422218548179,
 u'fc3_max_col_norm': 20.0,
 u'learning_rate': 0.01431616811292456,
 u'learning_rate_jitter': 4.794414741732794,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
