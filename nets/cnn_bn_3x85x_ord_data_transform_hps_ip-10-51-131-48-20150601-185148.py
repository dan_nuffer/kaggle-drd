hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 96,
                          'per_pixel_gaussian_noise_sigma': 0.19928875330423657,
                          'rotation_range': (0, 164.67665474168754),
                          'shear_range': (-31.89302978085165,
                                          31.89302978085165),
                          'translation_range': (-5.064344269115659,
                                                5.064344269115659),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.18524045929642013,
                           'do_flip': True,
                           'num_transforms': 48,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-9.379470391007995,
                                           9.379470391007995),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.9564254850565876,
                                          1.045559759358393)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
