hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.1875978276216103,
                          'contrast_noise_sigma': 0.12462947436370998,
                          'do_flip': True,
                          'num_transforms': 84,
                          'per_pixel_gaussian_noise_sigma': 0.19364910305271538,
                          'rotation_range': (0, 25.106298810043853),
                          'shear_range': (-20.57587031832506,
                                          20.57587031832506),
                          'translation_range': (-1.538924067625981,
                                                1.538924067625981),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.16203093988601597,
                           'contrast_noise_sigma': 0.0,
                           'do_flip': True,
                           'num_transforms': 52,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 0.0),
                           'shear_range': (-7.722915040510252,
                                           7.722915040510252),
                           'translation_range': (-0.0, 0.0),
                           'zoom_range': (0.4218164615399551,
                                          2.3706993234669635)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
