#job_id = 251
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 53,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 54,
 u'conv4_num_filters': 35,
 u'conv5_num_filters': 233,
 u'conv6_num_filters': 364,
 u'conv7_num_filters': 236,
 u'conv8_num_filters': 434,
 u'conv9_num_filters': 440,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 17.7445397358087,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4838,
 u'fc2_dropout_p': 0.13563920745497257,
 u'fc2_max_col_norm': 0.6700555612787318,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 2091,
 u'fc3_dropout_p': 0.5401211422726875,
 u'fc3_max_col_norm': 19.926054836012483,
 u'learning_rate': 0.0882221170472015,
 u'learning_rate_jitter': 5.971519405100324,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
