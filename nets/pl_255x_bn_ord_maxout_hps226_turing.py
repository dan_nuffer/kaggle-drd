#job_id = 226
hyperparameters = \
{u'batch_size': 4,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 128,
 u'conv4_num_filters': 99,
 u'conv5_num_filters': 129,
 u'conv6_num_filters': 128,
 u'conv7_num_filters': 256,
 u'conv8_num_filters': 256,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 0.1,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1258,
 u'fc2_dropout_p': 0.14077856534607724,
 u'fc2_max_col_norm': 19.91360784231875,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 536,
 u'fc3_dropout_p': 0.10131189331260636,
 u'fc3_max_col_norm': 19.888246419446073,
 u'learning_rate': 0.001040837962206244,
 u'learning_rate_jitter': 4.921878515357795,
 u'train_sample_rate': 0.05642205849907334}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
