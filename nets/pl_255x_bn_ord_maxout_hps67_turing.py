#job_id = 67
hyperparameters = \
{u'batch_size': 100,
 u'conv10_num_filters': 425,
 u'conv1_num_filters': 45,
 u'conv2_num_filters': 50,
 u'conv3_num_filters': 123,
 u'conv4_num_filters': 125,
 u'conv5_num_filters': 191,
 u'conv6_num_filters': 239,
 u'conv7_num_filters': 101,
 u'conv8_num_filters': 506,
 u'conv9_num_filters': 302,
 u'fc1_dropout_p': 0.70625,
 u'fc1_max_col_norm': 2.7429687499999997,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 708,
 u'fc2_dropout_p': 0.65625,
 u'fc2_max_col_norm': 19.22265625,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2668,
 u'fc3_dropout_p': 0.34375,
 u'fc3_max_col_norm': 2.12109375,
 u'learning_rate': 0.01492890625,
 u'learning_rate_jitter': 3.28125,
 u'train_sample_rate': 0.57734375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
