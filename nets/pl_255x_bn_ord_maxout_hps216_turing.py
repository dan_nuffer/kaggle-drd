#job_id = 216
hyperparameters = \
{u'batch_size': 6,
 u'conv10_num_filters': 156,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 30,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 100,
 u'conv5_num_filters': 251,
 u'conv6_num_filters': 253,
 u'conv7_num_filters': 240,
 u'conv8_num_filters': 365,
 u'conv9_num_filters': 365,
 u'fc1_dropout_p': 0.7545854868945322,
 u'fc1_max_col_norm': 13.106679614780667,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 4053,
 u'fc2_dropout_p': 0.1482804955801177,
 u'fc2_max_col_norm': 11.5692632021476,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1459,
 u'fc3_dropout_p': 0.5860216324521167,
 u'fc3_max_col_norm': 1.2312875951983127,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.0,
 u'train_sample_rate': 0.8924813248079827}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
