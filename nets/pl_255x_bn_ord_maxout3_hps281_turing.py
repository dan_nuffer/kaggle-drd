#job_id = 281
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 152,
 u'conv1_num_filters': 42,
 u'conv2_num_filters': 21,
 u'conv3_num_filters': 32,
 u'conv4_num_filters': 77,
 u'conv5_num_filters': 138,
 u'conv6_num_filters': 417,
 u'conv7_num_filters': 303,
 u'conv8_num_filters': 532,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.95,
 u'fc1_max_col_norm': 18.11480685839711,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 2753,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 16.950870884648033,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 256,
 u'fc3_dropout_p': 0.49318680578334206,
 u'fc3_max_col_norm': 30.0,
 u'learning_rate': 0.015483848707484342,
 u'learning_rate_jitter': 4.811328452556542,
 u'train_sample_rate': 0.5532748914241595,
 u'training_time': 24.151054102242355}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
