from lasagne.layers import DenseLayer
import numpy as np

import theano 
import theano.tensor as T

import lasagne as nn
from batch_normalization_layer import batch_norm

import data
from lasagne.regularization import l2
import load
import nn_plankton
import tta

data_dir = "unused"
patch_size = (63, 63, 3)
augmentation_params = {
    'zoom_range': (1 / 1.6, 1.6),
    'rotation_range': (0, 360),
    'shear_range': (-30 * patch_size[0] / 127., 30 * patch_size[0] / 127.),
    'translation_range': (-10, 10),
    'do_flip': True,
    'allow_stretch': 1.3,
}

batch_size = 256
chunk_size = batch_size * 100
predict_batch_size = 4

num_examples = int(35162 * 3 * .9) # * 3 because of upsampling class imbalance. .9 because of training/validation split
momentum = 0.88
learning_rate = 0.01431
learning_rate_decay = 0.50
learning_rate_patience = 24.0 * num_examples // chunk_size
learning_rate_rolling_mean_window = 24.0 * num_examples // chunk_size
learning_rate_improvement = 1.0
learning_rate_jitter = 4.79
num_chunks_train = num_examples // chunk_size * 10000 # stop after 10000 epochs
divergence_threshold = 26.0
failed_training_patience = 100

validate_every = 10 * num_examples // chunk_size # about once per 10 epochs
validate_train = False
save_every = 1


def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 360),
    'shear_range': (-10, 10),
    'translation_range': (-8 * patch_size[0] / 127., 8 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.2,
})


data_loader = load.CachingDataLoader(
    estimate_scale=estimate_scale,
    num_chunks_train=num_chunks_train,
    patch_size=patch_size,
    chunk_size=chunk_size,
    augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    test_size=0.1,
    image_yx=(64,64),
    cache_dir="unsup-data-64x64",
    predict_mode="regression"
)


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer


def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0d = nn.layers.dropout(l0, p=0.1)

    l1a = nn.layers.dropout(batch_norm(Conv2DLayer(l0d, num_filters=40, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu)), p=0.25)
    l1b = nn.layers.dropout(batch_norm(Conv2DLayer(l1a, num_filters=15, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu)), p=0.25)
    l1 = nn.layers.dropout(MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2)), p=0.25)

    l2a = nn.layers.dropout(batch_norm(Conv2DLayer(l1, num_filters=58, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu)), p=0.25)
    l2b = nn.layers.dropout(batch_norm(Conv2DLayer(l2a, num_filters=138, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu)), p=0.25)
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l2f = nn.layers.flatten(l2)

    l3 = DenseLayer(nn.layers.dropout(l2f, p=0.9), num_units=256, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=None, max_col_norm=25.0)
    l3fp = batch_norm(nn.layers.FeaturePoolLayer(l3, pool_size=4))

    l4 = DenseLayer(nn.layers.dropout(l3fp, p=0.05), num_units=2304, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=None, max_col_norm=20.8)
    l4fp = batch_norm(nn.layers.FeaturePoolLayer(l4, pool_size=6))

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4fp, p=0.43), num_units=1, nonlinearity=None, W=nn_plankton.Orthogonal(1.0), max_col_norm=20.0))

    return [l0], l5, l2


def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.mean_squared_error)

