import numpy as np
import theano.tensor as T

import dihedral_fast
from lasagne.layers import DenseLayer
import lasagne as nn
from batch_normalization_layer import batch_norm
import data
import load
import nn_plankton
import dihedral
import tta
import utils

data_dir = "data-362x362"
patch_size = (361, 361, 3)
augmentation_params = {
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-12.39, 12.38),
    'translation_range': (-20 * patch_size[0] / 127., 20 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
}

batch_size = 4
chunk_size = batch_size * 50

momentum = 0.88

num_examples = 90335 + 10535 + 53575  # train (upsampled) + val (pseudo) + test (pseudo)
train_sample_weight = 1. / 3. # TODO: make this somthing else? like 0.5? 2/3? hyperoptimize it?
# learning_rate_schedule = utils.make_full_triangle2_schedule(num_examples, chunk_size,
#                                                             init_min_lr=3.7e-5, init_max_lr=5.6e-4, iter_scale=1./3.)
# num_chunks_train = len(learning_rate_schedule)

learning_rate = 1e-3
learning_rate_decay = 0.50
learning_rate_patience = 300
learning_rate_rolling_mean_window = 200
learning_rate_improvement = 0.9999
learning_rate_jitter = 2.0

num_chunks_train = num_examples / chunk_size * 100 # stop after 100 epochs
divergence_threshold = 26.0

validate_every = 1 * num_examples // chunk_size # about once per 1 epoch

save_every = 1

test_pred_file = "predictions/test--cnn_batchnormdense_3channel_255patch_ordinal_l2--cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best--avg-probs.npy"
valid_pred_file = "predictions/valid--cnn_batchnormdense_3channel_255patch_ordinal_l2--cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best--avg-probs.npy"

# pick up from the weights found during learning rate exploration
pre_init_path = "metadata/pl_361x_bn_ord_maxout2_large-lrexplore-turing-20150614-134519.pkl"

def estimate_scale(img):
    return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]
    

augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
    'zoom_range': (1 / 1.4, 1.4),
    'rotation_range': (0, 0),
    'shear_range': (-10, 10),
    'translation_range': (-16 * patch_size[0] / 127., 16 * patch_size[0] / 127.),
    'do_flip': True,
    'allow_stretch': 1.0,
})



data_loader = load.PseudolabelingZmuvRescaledDataLoader(
    test_pred_file=test_pred_file,
    train_sample_weight=train_sample_weight,
    valid_pred_file=valid_pred_file,
    data_dir=data_dir,
    estimate_scale=estimate_scale,
    num_chunks_train=num_chunks_train,
    patch_size=patch_size,
    chunk_size=chunk_size,
    augmentation_params=augmentation_params,
    augmentation_transforms_test=augmentation_transforms_test,
    predict_mode="ordinal_regression")


Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))

    l1a = batch_norm(Conv2DLayer(l0, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3d = batch_norm(Conv2DLayer(l3c, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3d, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4d = batch_norm(Conv2DLayer(l4c, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4d, pool_size=(3, 3), stride=(2, 2))

    l5a = batch_norm(Conv2DLayer(l4, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l5b = batch_norm(Conv2DLayer(l5a, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l5c = batch_norm(Conv2DLayer(l5b, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l5d = batch_norm(Conv2DLayer(l5c, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l5 = MaxPool2DLayer(l5d, pool_size=(3, 3), stride=(2, 2))

    l6a = batch_norm(Conv2DLayer(l5, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l6b = batch_norm(Conv2DLayer(l6a, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l6c = batch_norm(Conv2DLayer(l6b, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l6d = batch_norm(Conv2DLayer(l6c, num_filters=512, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l6 = MaxPool2DLayer(l6d, pool_size=(3, 3), stride=(2, 2))
    l6f = nn.layers.flatten(l6)

    l7 = batch_norm(nn.layers.DenseLayer(nn.layers.dropout(l6f, p=0.5), num_units=4096, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=None))
    l7fp = nn.layers.FeaturePoolLayer(l7, pool_size=2)

    l8 = batch_norm(nn.layers.DenseLayer(nn.layers.dropout(l7fp, p=0.5), num_units=4096, W=nn_plankton.Orthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=None))
    l8fp = nn.layers.FeaturePoolLayer(l8, pool_size=2)

    l9 = batch_norm(DenseLayer(nn.layers.dropout(l8fp, p=0.5), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0), max_col_norm=None))

    return [l0], l9

def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy)

