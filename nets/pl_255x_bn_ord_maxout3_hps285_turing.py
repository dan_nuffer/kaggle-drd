#job_id = 285
hyperparameters = \
{u'batch_size': 11,
 u'conv10_num_filters': 64,
 u'conv1_num_filters': 49,
 u'conv2_num_filters': 46,
 u'conv3_num_filters': 128,
 u'conv4_num_filters': 48,
 u'conv5_num_filters': 311,
 u'conv6_num_filters': 271,
 u'conv7_num_filters': 293,
 u'conv8_num_filters': 454,
 u'conv9_num_filters': 256,
 u'fc1_dropout_p': 0.37431706571586865,
 u'fc1_max_col_norm': 29.999829895895513,
 u'fc1_maxout_pool_size': 6,
 u'fc1_num_units': 4788,
 u'fc2_dropout_p': 0.05,
 u'fc2_max_col_norm': 21.286600878633227,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 1093,
 u'fc3_dropout_p': 0.4234742532394479,
 u'fc3_max_col_norm': 23.424318384473647,
 u'learning_rate': 0.029543832298241056,
 u'learning_rate_jitter': 5.589921072853862,
 u'train_sample_rate': 0.5,
 u'training_time': 35.2323594957231}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
