#job_id = 59
hyperparameters = \
{u'batch_size': 115,
 u'conv10_num_filters': 470,
 u'conv1_num_filters': 39,
 u'conv2_num_filters': 41,
 u'conv3_num_filters': 93,
 u'conv4_num_filters': 57,
 u'conv5_num_filters': 218,
 u'conv6_num_filters': 218,
 u'conv7_num_filters': 73,
 u'conv8_num_filters': 332,
 u'conv9_num_filters': 364,
 u'fc1_dropout_p': 0.6125,
 u'fc1_max_col_norm': 19.0671875,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3032,
 u'fc2_dropout_p': 0.5875,
 u'fc2_max_col_norm': 12.226562499999998,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 2584,
 u'fc3_dropout_p': 0.5125000000000001,
 u'fc3_max_col_norm': 18.4453125,
 u'learning_rate': 0.0172703125,
 u'learning_rate_jitter': 2.9375,
 u'train_sample_rate': 0.1203125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
