#job_id = 287
hyperparameters = \
{u'batch_size': 2,
 u'conv10_num_filters': 102,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 23,
 u'conv3_num_filters': 45,
 u'conv4_num_filters': 152,
 u'conv5_num_filters': 281,
 u'conv6_num_filters': 240,
 u'conv7_num_filters': 477,
 u'conv8_num_filters': 302,
 u'conv9_num_filters': 376,
 u'fc1_dropout_p': 0.1,
 u'fc1_max_col_norm': 5.952485816045309,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 3184,
 u'fc2_dropout_p': 0.14645082458606137,
 u'fc2_max_col_norm': 0.1,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 892,
 u'fc3_dropout_p': 0.1,
 u'fc3_max_col_norm': 20.818376075297603,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.236449250189913,
 u'train_sample_rate': 0.9485241092461382,
 u'training_time': 24.00574593078755}
from . import pl_255x_bn_ord_maxout3_hps_tmpl
pl_255x_bn_ord_maxout3_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout3_hps_tmpl import *
