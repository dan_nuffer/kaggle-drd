#job_id = 229
hyperparameters = \
{u'batch_size': 2,
 u'conv10_num_filters': 161,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 51,
 u'conv3_num_filters': 64,
 u'conv4_num_filters': 93,
 u'conv5_num_filters': 248,
 u'conv6_num_filters': 256,
 u'conv7_num_filters': 245,
 u'conv8_num_filters': 265,
 u'conv9_num_filters': 371,
 u'fc1_dropout_p': 0.8355843056328759,
 u'fc1_max_col_norm': 7.600296065224486,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4096,
 u'fc2_dropout_p': 0.1,
 u'fc2_max_col_norm': 3.350055394008609,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 4096,
 u'fc3_dropout_p': 0.23537755463784268,
 u'fc3_max_col_norm': 0.10000181307297083,
 u'learning_rate': 0.000984723406257916,
 u'learning_rate_jitter': 5.0,
 u'train_sample_rate': 0.95}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
