hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.0,
                          'color_shift_sigma': 0.07595737400856253,
                          'contrast_noise_sigma': 0.03672226909514925,
                          'do_flip': True,
                          'num_transforms': 50,
                          'per_pixel_gaussian_noise_sigma': 0.15263203670477204,
                          'rotation_range': (0, 248.3848446175963),
                          'shear_range': (-38.16657902985373,
                                          38.16657902985373),
                          'translation_range': (-14.32151797393454,
                                                14.32151797393454),
                          'zoom_range': (1.0, 1.0)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.1729810158665817,
                           'do_flip': True,
                           'num_transforms': 33,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 263.6161511416008),
                           'shear_range': (-9.341772417715791,
                                           9.341772417715791),
                           'translation_range': (-10.101421369852465,
                                                 10.101421369852465),
                           'zoom_range': (0.8556173539472987,
                                          1.1687467480487714)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
