#job_id = 11
hyperparameters = \
{u'batch_size': 961,
 u'conv10_num_filters': 392,
 u'conv1_num_filters': 42,
 u'conv2_num_filters': 43,
 u'conv3_num_filters': 76,
 u'conv4_num_filters': 50,
 u'conv5_num_filters': 152,
 u'conv6_num_filters': 200,
 u'conv7_num_filters': 148,
 u'conv8_num_filters': 464,
 u'conv9_num_filters': 432,
 u'fc1_dropout_p': 0.65,
 u'fc1_max_col_norm': 1.34375,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 992,
 u'fc2_dropout_p': 0.15000000000000002,
 u'fc2_max_col_norm': 8.806249999999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 1952,
 u'fc3_dropout_p': 0.45000000000000007,
 u'fc3_max_col_norm': 13.781249999999998,
 u'learning_rate': 0.00634375,
 u'learning_rate_jitter': 3.75,
 u'train_sample_rate': 0.21875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
