#job_id = 10
hyperparameters = \
{u'batch_size': 705,
 u'conv10_num_filters': 296,
 u'conv1_num_filters': 34,
 u'conv2_num_filters': 55,
 u'conv3_num_filters': 124,
 u'conv4_num_filters': 74,
 u'conv5_num_filters': 184,
 u'conv6_num_filters': 232,
 u'conv7_num_filters': 100,
 u'conv8_num_filters': 400,
 u'conv9_num_filters': 368,
 u'fc1_dropout_p': 0.45000000000000007,
 u'fc1_max_col_norm': 6.31875,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2912,
 u'fc2_dropout_p': 0.75,
 u'fc2_max_col_norm': 13.781249999999998,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3872,
 u'fc3_dropout_p': 0.65,
 u'fc3_max_col_norm': 8.806249999999999,
 u'learning_rate': 0.08126875,
 u'learning_rate_jitter': 2.75,
 u'train_sample_rate': 0.8937499999999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
