import numpy as np
import theano.tensor as T

from lasagne.layers import DenseLayer
import lasagne as nn
from batch_normalization_layer import batch_norm
from load import UnsupervisedZmuvRescaledDataLoader
import nn_plankton
import tta

hyperparameters = None
data_dir = None
input_side_len = None
patch_size = None
augmentation_params = None
batch_size = None
chunk_size = None
momentum = None
learning_rate = None
learning_rate_decay = None
learning_rate_patience = None
learning_rate_rolling_mean_window = None
learning_rate_improvement = None
num_chunks_train = None
divergence_threshold = None
validate_every = None
save_every = None
augmentation_transforms_test = None
data_loader = None

# these are the best-so-far parameters from my hyperopt search. loss 0.628983 (kappa 0.371017)
hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.6021869210545154,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 5,
                          'per_pixel_gaussian_noise_sigma': 0.08117575510825287,
                          'rotation_range': (0, 316.1925356899096),
                          'shear_range': (-25.49931453720466,
                                          25.49931453720466),
                          'translation_range': (-15.767753814256292,
                                                15.767753814256292),
                          'zoom_range': (0.4260483444349762,
                                         2.3471514748547997)},
 'train_data_processing': {'allow_stretch': 1.1841177362576567,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.03936887660307005,
                           'do_flip': True,
                           'num_transforms': 96,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 155.2270595668627),
                           'shear_range': (-21.18647760436341,
                                           21.18647760436341),
                           'translation_range': (-7.6350678589325565,
                                                 7.6350678589325565),
                           'zoom_range': (0.5339912178136947,
                                          1.8726899743675034)}}


def set_hyperparameters(hyperparameters_arg):
    global hyperparameters
    hyperparameters = hyperparameters_arg
    global data_dir
    data_dir = "unsup-data-257x257"
    global input_side_len
    input_side_len = hyperparameters['data_params']['input_side_len']
    global patch_size
    patch_size = (input_side_len, input_side_len, 3)
    global augmentation_params
    augmentation_params = hyperparameters['train_data_processing']
    global batch_size
    batch_size = hyperparameters['data_params']['batch_size']
    global chunk_size
    chunk_size = hyperparameters['data_params']['chunk_size']
    global momentum
    momentum = 0.9
    global learning_rate
    learning_rate = 0.2
    global learning_rate_decay
    learning_rate_decay = 0.90
    global learning_rate_patience
    learning_rate_patience = 20
    global learning_rate_rolling_mean_window
    learning_rate_rolling_mean_window = 30
    global learning_rate_improvement
    learning_rate_improvement = 0.999
    global num_chunks_train
    num_chunks_train = hyperparameters['data_params']['num_chunks_train']
    global divergence_threshold
    divergence_threshold = 26.0
    global validate_every
    validate_every = 30
    global save_every
    save_every = 1
    global augmentation_transforms_test
    augmentation_transforms_test = tta.build_quasirandom_transforms(
        num_transforms = hyperparameters['eval_data_processing']['num_transforms'],
        zoom_range = hyperparameters['eval_data_processing']['zoom_range'],
        rotation_range = hyperparameters['eval_data_processing']['rotation_range'],
        shear_range = hyperparameters['eval_data_processing']['shear_range'],
        translation_range = hyperparameters['eval_data_processing']['translation_range'],
        do_flip=hyperparameters['eval_data_processing']['do_flip'],
        allow_stretch=hyperparameters['eval_data_processing']['allow_stretch'])
    global data_loader

    def estimate_scale(img):
        return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]

    data_loader = UnsupervisedZmuvRescaledDataLoader(data_dir=data_dir, estimate_scale=estimate_scale, num_chunks_train=num_chunks_train,
        patch_size=patch_size, chunk_size=chunk_size, augmentation_params=augmentation_params,
        augmentation_transforms_test=augmentation_transforms_test)


set_hyperparameters(hyperparameters)


def build_model():
    Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
    MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))
    l0g = nn.layers.GaussianNoiseLayer(l0, sigma=hyperparameters['train_data_processing']['per_pixel_gaussian_noise_sigma'])
    l0c = nn_plankton.ContrastNoiseLayer(l0g, sigma=hyperparameters['train_data_processing']['contrast_noise_sigma'],
                                         test_sigma=hyperparameters['eval_data_processing']['contrast_noise_sigma'])
    l0cs = nn_plankton.ColorShiftNoiseLayer(l0c, sigma=hyperparameters['train_data_processing']['color_shift_sigma'],
                                            test_sigma=hyperparameters['eval_data_processing']['color_shift_sigma'])

    l1a = batch_norm(Conv2DLayer(l0cs, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=16, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=32, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=64, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=256, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=128, filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    l5 = batch_norm(DenseLayer(nn.layers.dropout(l4f, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l6 = batch_norm(DenseLayer(nn.layers.dropout(l5, p=0.5), num_units=256, W=nn_plankton.Orthogonal(1.0), nonlinearity=nn_plankton.leaky_relu))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6, p=0.5), num_units=data_loader.num_classes, nonlinearity=T.nnet.softmax, W=nn_plankton.Orthogonal(1.0)))

    return [l0], l7
