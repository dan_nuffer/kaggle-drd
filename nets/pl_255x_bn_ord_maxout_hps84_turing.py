#job_id = 84
hyperparameters = \
{u'batch_size': 48,
 u'conv10_num_filters': 485,
 u'conv1_num_filters': 54,
 u'conv2_num_filters': 54,
 u'conv3_num_filters': 121,
 u'conv4_num_filters': 97,
 u'conv5_num_filters': 195,
 u'conv6_num_filters': 187,
 u'conv7_num_filters': 192,
 u'conv8_num_filters': 274,
 u'conv9_num_filters': 262,
 u'fc1_dropout_p': 0.33125000000000004,
 u'fc1_max_col_norm': 8.339843749999998,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 3508,
 u'fc2_dropout_p': 0.28125,
 u'fc2_max_col_norm': 16.11328125,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 3452,
 u'fc3_dropout_p': 0.36875,
 u'fc3_max_col_norm': 0.25546875,
 u'learning_rate': 0.08673203125000001,
 u'learning_rate_jitter': 2.65625,
 u'train_sample_rate': 0.7179687499999999}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
