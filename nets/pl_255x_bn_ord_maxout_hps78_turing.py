#job_id = 78
hyperparameters = \
{u'batch_size': 108,
 u'conv10_num_filters': 257,
 u'conv1_num_filters': 59,
 u'conv2_num_filters': 40,
 u'conv3_num_filters': 78,
 u'conv4_num_filters': 94,
 u'conv5_num_filters': 151,
 u'conv6_num_filters': 183,
 u'conv7_num_filters': 89,
 u'conv8_num_filters': 458,
 u'conv9_num_filters': 382,
 u'fc1_dropout_p': 0.15625,
 u'fc1_max_col_norm': 16.42421875,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 4068,
 u'fc2_dropout_p': 0.10625000000000001,
 u'fc2_max_col_norm': 3.05390625,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3340,
 u'fc3_dropout_p': 0.7937500000000001,
 u'fc3_max_col_norm': 15.802343749999999,
 u'learning_rate': 0.08361015625000001,
 u'learning_rate_jitter': 1.03125,
 u'train_sample_rate': 0.74609375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
