import numpy as np
import theano.tensor as T
from lasagne.layers import DenseLayer
import lasagne as nn
from batch_normalization_layer import batch_norm
import data
import load
import nn_plankton
import tta

validate_train = False

hyperparameters = None
data_dir = None
patch_size = None
augmentation_params = None
batch_size = None
chunk_size = None
predict_batch_size = None
momentum = None
num_examples = None
train_sample_weight = None
learning_rate = None
learning_rate_decay = None
learning_rate_patience = None
learning_rate_rolling_mean_window = None
learning_rate_improvement = None
learning_rate_jitter = None
num_chunks_train = None
divergence_threshold = None
validate_every = None
save_every = None
augmentation_transforms_test = None
data_loader = None
test_pred_file = None
valid_pred_file = None
training_time_limit = None

def set_hyperparameters(hyperparameters_arg):
    global hyperparameters
    hyperparameters = hyperparameters_arg
    global data_dir
    data_dir = "data-362x362"
    global patch_size
    patch_size = (255, 255, 3)
    global augmentation_params
    augmentation_params = {
        'zoom_range': (1 / 1.4, 1.4),
        'rotation_range': (0, 0),
        'shear_range': (-12.39, 12.38),
        'translation_range': (-20 * patch_size[0] / 127., 20 * patch_size[0] / 127.),
        'do_flip': True,
        'allow_stretch': 1.0,
    }
    global batch_size
    batch_size = hyperparameters['batch_size']
    global chunk_size
    chunk_size = batch_size * 100
    global predict_batch_size
    predict_batch_size = 4
    global momentum
    momentum = 0.88
    global num_examples
    num_examples = 90335 + 10535 + 53575  # train (upsampled) + val (pseudo) + test (pseudo)
    global train_sample_weight
    train_sample_weight = hyperparameters['train_sample_rate']
    global learning_rate
    learning_rate = hyperparameters['learning_rate']
    global learning_rate_decay
    learning_rate_decay = 0.5
    global learning_rate_patience
    learning_rate_patience = 2.0 * num_examples // chunk_size
    global learning_rate_rolling_mean_window
    learning_rate_rolling_mean_window = num_examples // chunk_size
    global learning_rate_improvement
    learning_rate_improvement = 0.9999
    global learning_rate_jitter
    learning_rate_jitter = hyperparameters['learning_rate_jitter']
    global num_chunks_train
    num_chunks_train = num_examples // chunk_size * 500 # stop after 500 epochs
    global divergence_threshold
    divergence_threshold = 26.0
    global validate_every
    validate_every = 1 * num_examples // chunk_size # about once per 1 epoch
    global save_every
    save_every = 1
    global test_pred_file
    test_pred_file = "predictions/test--weighted_blend-turing-20150722-065659.npy"
    global valid_pred_file
    valid_pred_file = "predictions/valid--weighted_blend-turing-20150722-065659.npy"

    global augmentation_transforms_test
    augmentation_transforms_test = tta.build_quasirandom_transforms(70, **{
        'zoom_range': (1 / 1.4, 1.4),
        'rotation_range': (0, 0),
        'shear_range': (-10, 10),
        'translation_range': (-8 * patch_size[0] / 127., 8 * patch_size[0] / 127.),
        'do_flip': True,
        'allow_stretch': 1.0,
    })
    global data_loader

    def estimate_scale(img):
        return np.maximum(img.shape[0], img.shape[1]) / patch_size[0]

    data_loader = load.PseudolabelingZmuvRescaledDataLoader(
        test_pred_file=test_pred_file,
        train_sample_weight=train_sample_weight,
        valid_pred_file=valid_pred_file,
        data_dir=data_dir,
        estimate_scale=estimate_scale,
        num_chunks_train=num_chunks_train,
        patch_size=patch_size,
        chunk_size=chunk_size,
        augmentation_params=augmentation_params,
        augmentation_transforms_test=augmentation_transforms_test,
        predict_mode="ordinal_regression",
        use_valid_labels=True)

    global training_time_limit
    training_time_limit = 60 * 60 * 24 * 30  # 30 days in seconds



Conv2DLayer = nn.layers.dnn.Conv2DDNNLayer
MaxPool2DLayer = nn.layers.dnn.MaxPool2DDNNLayer

def build_model():
    l0 = nn.layers.InputLayer((batch_size, patch_size[2], patch_size[0], patch_size[1]))

    l1a = batch_norm(Conv2DLayer(l0, num_filters=hyperparameters['conv1_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1b = batch_norm(Conv2DLayer(l1a, num_filters=hyperparameters['conv2_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l1 = MaxPool2DLayer(l1b, pool_size=(3, 3), stride=(2, 2))

    l2a = batch_norm(Conv2DLayer(l1, num_filters=hyperparameters['conv3_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2b = batch_norm(Conv2DLayer(l2a, num_filters=hyperparameters['conv4_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l2 = MaxPool2DLayer(l2b, pool_size=(3, 3), stride=(2, 2))

    l3a = batch_norm(Conv2DLayer(l2, num_filters=hyperparameters['conv5_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3b = batch_norm(Conv2DLayer(l3a, num_filters=hyperparameters['conv6_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3c = batch_norm(Conv2DLayer(l3b, num_filters=hyperparameters['conv7_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l3 = MaxPool2DLayer(l3c, pool_size=(3, 3), stride=(2, 2))

    l4a = batch_norm(Conv2DLayer(l3, num_filters=hyperparameters['conv8_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4b = batch_norm(Conv2DLayer(l4a, num_filters=hyperparameters['conv9_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4c = batch_norm(Conv2DLayer(l4b, num_filters=hyperparameters['conv10_num_filters'], filter_size=(3, 3), border_mode="same", W=nn_plankton.Conv2DOrthogonal(1.0), b=nn.init.Constant(0.1), nonlinearity=nn_plankton.leaky_relu))
    l4 = MaxPool2DLayer(l4c, pool_size=(3, 3), stride=(2, 2))
    l4f = nn.layers.flatten(l4)

    # the num_units has to be a multiple of the maxout_pool_size
    fc1_num_units = hyperparameters['fc1_num_units'] // hyperparameters['fc1_maxout_pool_size'] * hyperparameters['fc1_maxout_pool_size']
    l5 = nn.layers.DenseLayer(nn.layers.dropout(l4f, p=hyperparameters['fc1_dropout_p']),
                                         num_units=fc1_num_units, W=nn_plankton.Orthogonal(1.0),
                                         b=nn.init.Constant(0.1), nonlinearity=None,
                                         max_col_norm=hyperparameters['fc1_max_col_norm'])
    l5fp = batch_norm(nn.layers.FeaturePoolLayer(l5, pool_size=hyperparameters['fc1_maxout_pool_size']))

    fc2_num_units = hyperparameters['fc2_num_units'] // hyperparameters['fc2_maxout_pool_size'] * hyperparameters['fc2_maxout_pool_size']
    l6 = nn.layers.DenseLayer(nn.layers.dropout(l5fp, p=hyperparameters['fc2_dropout_p']),
                                         num_units=fc2_num_units, W=nn_plankton.Orthogonal(1.0),
                                         b=nn.init.Constant(0.1), nonlinearity=None,
                                         max_col_norm=hyperparameters['fc2_max_col_norm'])
    l6fp = batch_norm(nn.layers.FeaturePoolLayer(l6, pool_size=hyperparameters['fc2_maxout_pool_size']))

    l7 = batch_norm(DenseLayer(nn.layers.dropout(l6fp, p=hyperparameters['fc2_dropout_p']), num_units=data.num_classes, nonlinearity=T.nnet.sigmoid, W=nn_plankton.Orthogonal(1.0), max_col_norm=hyperparameters['fc3_max_col_norm']))

    return [l0], l7

def build_objective(l_ins, l_out):
    return nn.objectives.Objective(l_out, loss_function=nn_plankton.sum_binary_crossentropy)

# best hpo result - id 165, # conv filter sizes * 1.5, fc sizes * 1.1
hyperparameters = {
    u'batch_size': 6,
    u'conv10_num_filters': 257,
    u'conv1_num_filters': 86,
    u'conv2_num_filters': 42,
    u'conv3_num_filters': 102,
    u'conv4_num_filters': 131,
    u'conv5_num_filters': 366,
    u'conv6_num_filters': 359,
    u'conv7_num_filters': 341,
    u'conv8_num_filters': 494,
    u'conv9_num_filters': 632,
    u'fc1_dropout_p': 0.796875,
    u'fc1_max_col_norm': 22.1876953125,
    u'fc1_maxout_pool_size': 3,
    u'fc1_num_units': 4429,
    u'fc2_dropout_p': 0.13437500000000002,
    u'fc2_max_col_norm': 16.82402343749999,
    u'fc2_maxout_pool_size': 4,
    u'fc2_num_units': 2427,
    u'fc3_dropout_p': 0.546875,
    u'fc3_max_col_norm': 5.1638671875,
    u'learning_rate': 0.006733984375000001,
    u'learning_rate_jitter': 4.984375,
    u'train_sample_rate': 0.714453125
}

set_hyperparameters(hyperparameters)
