#job_id = 265
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 98,
 u'conv1_num_filters': 40,
 u'conv2_num_filters': 51,
 u'conv3_num_filters': 128,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 228,
 u'conv6_num_filters': 395,
 u'conv7_num_filters': 512,
 u'conv8_num_filters': 219,
 u'conv9_num_filters': 585,
 u'fc1_dropout_p': 0.4343201170404041,
 u'fc1_max_col_norm': 19.31005167770151,
 u'fc1_maxout_pool_size': 7,
 u'fc1_num_units': 256,
 u'fc2_dropout_p': 0.2073061329816096,
 u'fc2_max_col_norm': 24.33070675031824,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 344,
 u'fc3_dropout_p': 0.40029207890556295,
 u'fc3_max_col_norm': 19.79802443274427,
 u'learning_rate': 0.0001,
 u'learning_rate_jitter': 5.08950183780142,
 u'train_sample_rate': 0.5}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
