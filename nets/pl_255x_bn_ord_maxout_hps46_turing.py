#job_id = 46
hyperparameters = \
{u'batch_size': 111,
 u'conv10_num_filters': 194,
 u'conv1_num_filters': 42,
 u'conv2_num_filters': 21,
 u'conv3_num_filters': 87,
 u'conv4_num_filters': 54,
 u'conv5_num_filters': 182,
 u'conv6_num_filters': 190,
 u'conv7_num_filters': 199,
 u'conv8_num_filters': 260,
 u'conv9_num_filters': 356,
 u'fc1_dropout_p': 0.5375,
 u'fc1_max_col_norm': 2.2765625,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3368,
 u'fc2_dropout_p': 0.4625,
 u'fc2_max_col_norm': 15.335937499999998,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 4040,
 u'fc3_dropout_p': 0.6875,
 u'fc3_max_col_norm': 6.629687499999999,
 u'learning_rate': 0.09531718750000001,
 u'learning_rate_jitter': 2.5625,
 u'train_sample_rate': 0.5984375}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
