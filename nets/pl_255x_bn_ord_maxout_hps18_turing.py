#job_id = 18
hyperparameters = \
{u'batch_size': 609,
 u'conv10_num_filters': 188,
 u'conv1_num_filters': 57,
 u'conv2_num_filters': 38,
 u'conv3_num_filters': 66,
 u'conv4_num_filters': 65,
 u'conv5_num_filters': 252,
 u'conv6_num_filters': 212,
 u'conv7_num_filters': 178,
 u'conv8_num_filters': 488,
 u'conv9_num_filters': 296,
 u'fc1_dropout_p': 0.525,
 u'fc1_max_col_norm': 5.6968749999999995,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 6992,
 u'fc2_dropout_p': 0.525,
 u'fc2_max_col_norm': 4.453124999999999,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2672,
 u'fc3_dropout_p': 0.17500000000000002,
 u'fc3_max_col_norm': 1.965625,
 u'learning_rate': 0.09687812500000001,
 u'learning_rate_jitter': 4.875,
 u'train_sample_rate': 0.190625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
