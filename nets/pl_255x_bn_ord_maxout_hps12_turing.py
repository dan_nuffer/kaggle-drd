#job_id = 12
hyperparameters = \
{u'batch_size': 449,
 u'conv10_num_filters': 200,
 u'conv1_num_filters': 58,
 u'conv2_num_filters': 19,
 u'conv3_num_filters': 108,
 u'conv4_num_filters': 98,
 u'conv5_num_filters': 216,
 u'conv6_num_filters': 136,
 u'conv7_num_filters': 244,
 u'conv8_num_filters': 336,
 u'conv9_num_filters': 304,
 u'fc1_dropout_p': 0.25,
 u'fc1_max_col_norm': 11.29375,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 4832,
 u'fc2_dropout_p': 0.55,
 u'fc2_max_col_norm': 18.75625,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 5792,
 u'fc3_dropout_p': 0.85,
 u'fc3_max_col_norm': 3.83125,
 u'learning_rate': 0.056293750000000004,
 u'learning_rate_jitter': 1.75,
 u'train_sample_rate': 0.66875}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
