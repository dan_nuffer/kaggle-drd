#job_id = 262
hyperparameters = \
{u'batch_size': 12,
 u'conv10_num_filters': 66,
 u'conv1_num_filters': 32,
 u'conv2_num_filters': 64,
 u'conv3_num_filters': 38,
 u'conv4_num_filters': 100,
 u'conv5_num_filters': 249,
 u'conv6_num_filters': 366,
 u'conv7_num_filters': 348,
 u'conv8_num_filters': 512,
 u'conv9_num_filters': 431,
 u'fc1_dropout_p': 0.67154274018982,
 u'fc1_max_col_norm': 15.720552069951564,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 386,
 u'fc2_dropout_p': 0.12579588922586224,
 u'fc2_max_col_norm': 6.276074077409297,
 u'fc2_maxout_pool_size': 7,
 u'fc2_num_units': 256,
 u'fc3_dropout_p': 0.36838178646027686,
 u'fc3_max_col_norm': 18.84593498321733,
 u'learning_rate': 0.03640235571366066,
 u'learning_rate_jitter': 5.673059847517871,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
