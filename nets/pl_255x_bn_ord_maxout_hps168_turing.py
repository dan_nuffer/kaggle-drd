#job_id = 168
hyperparameters = \
{u'batch_size': 41,
 u'conv10_num_filters': 460,
 u'conv1_num_filters': 48,
 u'conv2_num_filters': 16,
 u'conv3_num_filters': 117,
 u'conv4_num_filters': 111,
 u'conv5_num_filters': 212,
 u'conv6_num_filters': 207,
 u'conv7_num_filters': 179,
 u'conv8_num_filters': 265,
 u'conv9_num_filters': 357,
 u'fc1_dropout_p': 0.19687500000000002,
 u'fc1_max_col_norm': 19.766796875,
 u'fc1_maxout_pool_size': 4,
 u'fc1_num_units': 3130,
 u'fc2_dropout_p': 0.734375,
 u'fc2_max_col_norm': 6.241015624999999,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 1310,
 u'fc3_dropout_p': 0.34687500000000004,
 u'fc3_max_col_norm': 18.367578125,
 u'learning_rate': 0.08165898437500001,
 u'learning_rate_jitter': 1.984375,
 u'train_sample_rate': 0.48945312499999993}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
