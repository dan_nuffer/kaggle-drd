#job_id = 225
hyperparameters = \
{u'batch_size': 17,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 64,
 u'conv2_num_filters': 22,
 u'conv3_num_filters': 75,
 u'conv4_num_filters': 125,
 u'conv5_num_filters': 256,
 u'conv6_num_filters': 128,
 u'conv7_num_filters': 225,
 u'conv8_num_filters': 374,
 u'conv9_num_filters': 414,
 u'fc1_dropout_p': 0.7760023179131013,
 u'fc1_max_col_norm': 10.670944874197511,
 u'fc1_maxout_pool_size': 2,
 u'fc1_num_units': 4095,
 u'fc2_dropout_p': 0.1,
 u'fc2_max_col_norm': 18.0532706526894,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 982,
 u'fc3_dropout_p': 0.5482518549535811,
 u'fc3_max_col_norm': 19.697376250984412,
 u'learning_rate': 0.031689568230567296,
 u'learning_rate_jitter': 5.0,
 u'train_sample_rate': 0.3550111405848303}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
