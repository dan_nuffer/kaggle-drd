#job_id = 91
hyperparameters = \
{u'batch_size': 120,
 u'conv10_num_filters': 365,
 u'conv1_num_filters': 56,
 u'conv2_num_filters': 20,
 u'conv3_num_filters': 68,
 u'conv4_num_filters': 116,
 u'conv5_num_filters': 251,
 u'conv6_num_filters': 211,
 u'conv7_num_filters': 180,
 u'conv8_num_filters': 386,
 u'conv9_num_filters': 374,
 u'fc1_dropout_p': 0.28125,
 u'fc1_max_col_norm': 4.608593749999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 2388,
 u'fc2_dropout_p': 0.83125,
 u'fc2_max_col_norm': 9.894531249999998,
 u'fc2_maxout_pool_size': 3,
 u'fc2_num_units': 2780,
 u'fc3_dropout_p': 0.81875,
 u'fc3_max_col_norm': 8.96171875,
 u'learning_rate': 0.0055632812500000005,
 u'learning_rate_jitter': 1.40625,
 u'train_sample_rate': 0.32421874999999994}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
