#job_id = 172
hyperparameters = \
{u'batch_size': 25,
 u'conv10_num_filters': 315,
 u'conv1_num_filters': 61,
 u'conv2_num_filters': 22,
 u'conv3_num_filters': 108,
 u'conv4_num_filters': 51,
 u'conv5_num_filters': 163,
 u'conv6_num_filters': 158,
 u'conv7_num_filters': 155,
 u'conv8_num_filters': 361,
 u'conv9_num_filters': 261,
 u'fc1_dropout_p': 0.896875,
 u'fc1_max_col_norm': 12.304296874999999,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3578,
 u'fc2_dropout_p': 0.834375,
 u'fc2_max_col_norm': 8.728515624999998,
 u'fc2_maxout_pool_size': 5,
 u'fc2_num_units': 3550,
 u'fc3_dropout_p': 0.246875,
 u'fc3_max_col_norm': 0.9550781249999999,
 u'learning_rate': 0.06917148437500001,
 u'learning_rate_jitter': 3.484375,
 u'train_sample_rate': 0.601953125}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
