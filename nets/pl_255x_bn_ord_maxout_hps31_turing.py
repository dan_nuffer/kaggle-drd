#job_id = 31
hyperparameters = \
{u'batch_size': 545,
 u'conv10_num_filters': 404,
 u'conv1_num_filters': 47,
 u'conv2_num_filters': 29,
 u'conv3_num_filters': 118,
 u'conv4_num_filters': 35,
 u'conv5_num_filters': 212,
 u'conv6_num_filters': 140,
 u'conv7_num_filters': 166,
 u'conv8_num_filters': 472,
 u'conv9_num_filters': 376,
 u'fc1_dropout_p': 0.375,
 u'fc1_max_col_norm': 14.403125,
 u'fc1_maxout_pool_size': 5,
 u'fc1_num_units': 1712,
 u'fc2_dropout_p': 0.275,
 u'fc2_max_col_norm': 18.134375,
 u'fc2_maxout_pool_size': 4,
 u'fc2_num_units': 1232,
 u'fc3_dropout_p': 0.525,
 u'fc3_max_col_norm': 15.646874999999998,
 u'learning_rate': 0.003221875,
 u'learning_rate_jitter': 2.625,
 u'train_sample_rate': 0.47187499999999993}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
