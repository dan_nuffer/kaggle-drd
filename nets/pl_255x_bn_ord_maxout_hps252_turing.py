#job_id = 252
hyperparameters = \
{u'batch_size': 10,
 u'conv10_num_filters': 128,
 u'conv1_num_filters': 68,
 u'conv2_num_filters': 12,
 u'conv3_num_filters': 48,
 u'conv4_num_filters': 32,
 u'conv5_num_filters': 232,
 u'conv6_num_filters': 382,
 u'conv7_num_filters': 183,
 u'conv8_num_filters': 447,
 u'conv9_num_filters': 305,
 u'fc1_dropout_p': 0.9,
 u'fc1_max_col_norm': 15.13902548012563,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 5117,
 u'fc2_dropout_p': 0.19152939153848647,
 u'fc2_max_col_norm': 0.5699722640860295,
 u'fc2_maxout_pool_size': 6,
 u'fc2_num_units': 5073,
 u'fc3_dropout_p': 0.5797234938547184,
 u'fc3_max_col_norm': 10.044149526680574,
 u'learning_rate': 0.0684309142257321,
 u'learning_rate_jitter': 5.211985880576745,
 u'train_sample_rate': 0.01}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
