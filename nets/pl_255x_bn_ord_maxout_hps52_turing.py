#job_id = 52
hyperparameters = \
{u'batch_size': 43,
 u'conv10_num_filters': 350,
 u'conv1_num_filters': 37,
 u'conv2_num_filters': 32,
 u'conv3_num_filters': 97,
 u'conv4_num_filters': 51,
 u'conv5_num_filters': 226,
 u'conv6_num_filters': 178,
 u'conv7_num_filters': 109,
 u'conv8_num_filters': 476,
 u'conv9_num_filters': 284,
 u'fc1_dropout_p': 0.7625000000000001,
 u'fc1_max_col_norm': 12.8484375,
 u'fc1_maxout_pool_size': 3,
 u'fc1_num_units': 3704,
 u'fc2_dropout_p': 0.3375,
 u'fc2_max_col_norm': 3.5203124999999997,
 u'fc2_maxout_pool_size': 2,
 u'fc2_num_units': 3704,
 u'fc3_dropout_p': 0.2625,
 u'fc3_max_col_norm': 12.226562499999998,
 u'learning_rate': 0.09843906250000001,
 u'learning_rate_jitter': 1.1875,
 u'train_sample_rate': 0.8515625}
from . import pl_255x_bn_ord_maxout_hps_tmpl
pl_255x_bn_ord_maxout_hps_tmpl.set_hyperparameters(hyperparameters)
from pl_255x_bn_ord_maxout_hps_tmpl import *
