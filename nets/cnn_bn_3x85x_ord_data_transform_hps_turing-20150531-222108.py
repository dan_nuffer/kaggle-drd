hyperparameters = \
{'data_params': {'batch_size': 64,
                 'chunk_size': 4096,
                 'input_side_len': 95,
                 'num_chunks_train': 300},
 'eval_data_processing': {'allow_stretch': 1.9950067377941962,
                          'color_shift_sigma': 0.0,
                          'contrast_noise_sigma': 0.0,
                          'do_flip': True,
                          'num_transforms': 8,
                          'per_pixel_gaussian_noise_sigma': 0.10106096759207076,
                          'rotation_range': (0, 58.431936692368495),
                          'shear_range': (-13.656050926866731,
                                          13.656050926866731),
                          'translation_range': (-18.09803372407214,
                                                18.09803372407214),
                          'zoom_range': (0.43597294906983414,
                                         2.2937202918977895)},
 'train_data_processing': {'allow_stretch': 1.0,
                           'color_shift_sigma': 0.0,
                           'contrast_noise_sigma': 0.009024231133682841,
                           'do_flip': True,
                           'num_transforms': 44,
                           'per_pixel_gaussian_noise_sigma': 0.0,
                           'rotation_range': (0, 332.44908568844517),
                           'shear_range': (-2.695251721692749,
                                           2.695251721692749),
                           'translation_range': (-2.1126861079141834,
                                                 2.1126861079141834),
                           'zoom_range': (0.4041677332472985,
                                          2.4742202747494666)}}
from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl
cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)
from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *
