"""
Given a set of validation predictions, this script computes the optimal linear weights on the validation set.
It computes the weighted blend of test predictions, where some models are replaced by their bagged versions.
"""
import glob
import click

import os
import numpy as np
import sys
import skll

import theano
import theano.tensor as T
import scipy

import data
import utils
from clint.textui import progress

if theano.config.device !='cpu' or theano.config.floatX != 'float64':
    raise ValueError("Please set env var THEANO_FLAGS='device=cpu,floatX=float64' when running this program")

def sum_binary_crossentropy(output, target, eps=1e-7):
    """
    Compute the sum of the crossentropy of binary random variables
    output and target are each expectations of binary random
    variables; target may be exactly 0 or 1 but output must
    lie strictly between 0 and 1.
    """
    output = T.clip(output, eps, 1 - eps)
    return -T.sum(target * T.log(output) + (1.0 - target) * T.log(1.0 - output), axis=1)

def predict_ordinal(output, thresh):
    # create a boolean array for numbers over the threshold
    x = output >= thresh
    # forward multiply across the columns, so that earlier falses propagate
    xmul = T.zeros_like(x)
    T.set_subtensor(xmul[:, 0], x[:, 0])
    for col in xrange(1, 5):
        T.set_subtensor(xmul[:, col], x[:, col] * xmul[:, col - 1])
    return T.clip(x.sum(axis=1) - 1, 0, x.shape[1])

@click.command()
@click.option('--optimize-kappa/--no-optimize-kappa', 'optimize_kappa', help='instead of optimizing for loss, optimize for kappa.', default=False)
@click.option('--optimize-error/--no-optimize-error', 'optimize_error', help='optimize for error (1 - accuracy)', default=False)
@click.option('--random-restarts', 'num_random_restarts', help='do this many random restarts', default=100, type=click.INT)
@click.option('--use-basinhopping/--no-use-basinhopping', 'use_basinhopping', help='use basinhopping() instead of fmin_l_bfgs_b()', default=False)
@click.option('--save-predictions/--no-save-predictions', default=True)
@click.option('--bh-niter', help='number of iterations for basinhopping', default=1000000, type=click.INT)
@click.option('--bh-niter-success', help='number of iterations for basinhopping convergence', default=1000, type=click.INT)
@click.option('--bh-t', 'bh_T', help='basinhopping temperature. higher values == more exploration', default=0.9, type=click.FLOAT)
@click.option('--bh-minimizer-method', 'bh_minimizer_method', help='basinhopping minimizer method', default='L-BFGS-B', type=click.STRING)
@click.option('--include-train/--no-include-train', help='Generate training predictions (this will exclude from consideration those models which dont have them', default=True)
@click.option('--include-aug/--no-include-aug', help='Use augmented predictions. This may be prefereable because the upsampled examples will be spread out somewhat in the prediction space.', default=True)
@click.option('--include-noaug/--no-include-noaug', help='Use non-augmented predictions.', default=True)
# TODO: add # random restarts as a parameter
# TODO: apply an ((y-.5)*2)^4 or abs((y-.5)*2) filter to weights before combining, so that strongly confident models can outweigh lesser confidences
# the idea is to flatten out the contribution of non-confident predictions.
def main(optimize_kappa=False, optimize_error=False, num_random_restarts=100, use_basinhopping=False,
         save_predictions=True, bh_niter=1000000,
         bh_niter_success=1000, bh_T=0.9, bh_minimizer_method='L-BFGS-B',
         include_train=True,
         include_aug=True,
         include_noaug=True):
    # I want to find all the configs which have both valid and test predictions. list the files and find the intersection.
    # This script only handles ordinal predictions, so we have to filter for file with _ord in the name
    if include_aug and include_noaug:
        glob_suffix = "*_ord*.npy"
    elif include_aug:
        glob_suffix = "--*_ord*.npy"
    elif include_noaug:
        glob_suffix = "_noaug--*_ord*.npy"
    else:
        raise ValueError("Cannot specify both --no-include-aug and --no-include-noaug")

    pred_train_files = glob.glob('predictions/train%s' % (glob_suffix,))
    pred_valid_files = glob.glob('predictions/valid%s' % (glob_suffix,))
    pred_test_files = glob.glob('predictions/test%s' % (glob_suffix,))
    train_set = set(p.replace('predictions/train', '', 1) for p in pred_train_files)
    valid_set = set(p.replace('predictions/valid', '', 1) for p in pred_valid_files)
    test_set = set(p.replace('predictions/test', '', 1) for p in pred_test_files)
    if include_train:
        predictions_set = train_set & valid_set & test_set
    else:
        predictions_set = valid_set & test_set
    n_models = len(predictions_set)

    if n_models == 0:
        raise ValueError("No suitable models found")

    valid_predictions_paths = ['predictions/valid%s' % (p,) for p in predictions_set]
    test_predictions_paths = ['predictions/test%s' % (p,) for p in predictions_set]
    train_predictions_paths = ['predictions/train%s' % (p,) for p in predictions_set]

    # loading validation predictions
    t_valid = utils.ordinal_targets(data.labels_val).astype(np.float64)

    predictions_list = [np.load(path) for path in valid_predictions_paths]
    predictions_stack = np.array(predictions_list).astype(np.float64)  # num_sources x num_datapoints x 5

    print "Individual prediction errors (sorted worst to best)"
    individual_prediction_errors = [np.mean(utils.sum_binary_crossentropy(p, t_valid)) for p in predictions_list]
    del predictions_list
    for i, loss in sorted(enumerate(individual_prediction_errors), key=lambda tup: -tup[1]):
        print loss, os.path.basename(valid_predictions_paths[i])
    print

    # optimizing weights
    X = theano.shared(predictions_stack)  # source predictions
    t = theano.shared(t_valid)  # targets
    W = T.vector('W')

    s = T.nnet.softmax(W).reshape((W.shape[0], 1, 1))
    weighted_avg_predictions = T.sum(X * s, axis=0)  # T.tensordot(X, s, [[0], [0]])
    loss = sum_binary_crossentropy(weighted_avg_predictions, t).mean()
    grad = T.grad(loss, W)

    #print ("compiling Thenao function f")
    loss_f = theano.function([W], loss)
    #print ("compiling Thenao function g")
    loss_g = theano.function([W], grad)

    # These are used when optimizing for kappa
    cat_outputs_exp = predict_ordinal(weighted_avg_predictions, 0.5) #W_and_thresh[n_models:])
    cat_outputs_f = theano.function([W], cat_outputs_exp)
    labels_val = theano.shared(data.labels_val)
    error_exp = T.mean(T.neq(labels_val, cat_outputs_exp))
    error_f = theano.function([W], error_exp)
    error_g = theano.function([W], T.grad(error_exp, W))

    def kappa_f(x):
        cat_outputs = cat_outputs_f(x)
        return 1.0 - skll.kappa(data.labels_val, cat_outputs, 'quadratic')

    if optimize_kappa:
        f = kappa_f
        g = None
        optim_desc = '1 - kappa'
    elif optimize_error:
        f = error_f
        g = error_g
        optim_desc = 'error'
    else:
        f = loss_f
        g = loss_g
        optim_desc = 'loss'

    if use_basinhopping:
        print ("optimizing weights using basinhopping")
        label = "basinhopping run "
    else:
        print ("optimizing weights using l-bfgs-b")
        label = "l-bfgs-b run "

    fmin_results = []
    for restart in progress.bar(xrange(num_random_restarts), label=label):
        w_init = np.random.random((n_models,)) / n_models
        if use_basinhopping:
            minimizer_kwargs = {'method' : bh_minimizer_method}
            result = scipy.optimize.basinhopping(f, w_init, niter=bh_niter, niter_success=bh_niter_success, disp=True,
                                                 T=bh_T, minimizer_kwargs=minimizer_kwargs)
            out = result.x
            loss = result.fun
        else:
            if g is not None:
                out, loss, _ = scipy.optimize.fmin_l_bfgs_b(f, w_init, fprime=g, pgtol=1e-09, epsilon=1e-08, maxfun=10000)
            else:
                out, loss, _ = scipy.optimize.fmin_l_bfgs_b(f, w_init, approx_grad=True, pgtol=1e-09, epsilon=1e-08, maxfun=10000)
        fmin_results.append((out, loss))

    out, loss = min(fmin_results, key=lambda tup: tup[1])

    weights = np.exp(out[:n_models])
    weights /= weights.sum()
    weights_over_threshold = weights > 1e-5
    weights = weights[weights_over_threshold]
    train_predictions_paths = [p for idx, p in enumerate(train_predictions_paths) if weights_over_threshold[idx]]
    valid_predictions_paths = [p for idx, p in enumerate(valid_predictions_paths) if weights_over_threshold[idx]]
    test_predictions_paths = [p for idx, p in enumerate(test_predictions_paths) if weights_over_threshold[idx]]

    print '10 Most Optimal weights (sorted least to most)'
    for i, weight in sorted(enumerate(weights), key=lambda tup: tup[1])[-10:]:
        print weights[i], os.path.basename(valid_predictions_paths[i])
    print

    if optimize_kappa:
        print ('ensemble valid kappa: %f' % (1. - loss,))
    else:
        print ('ensemble valid %s: %f' % (optim_desc, loss))

    exp_id = utils.generate_expid('weighted_blend')
    def gen_predictions(predictions_paths, subset):
        print 'Generating %s set predictions' % (subset,)
        pred_list = [np.load(path) for path in predictions_paths]
        pred_stack = np.array(pred_list)  # num_sources x num_datapoints x 121
        del pred_list

        target_path = 'predictions/%s--%s.npy' % (subset, exp_id)
        weighted_predictions = np.sum(pred_stack * weights.reshape((weights.shape[0], 1, 1)), axis=0)
        np.save(target_path, weighted_predictions)
        print ' stored %s predictions in %s' % (subset, target_path)

    if save_predictions:
        gen_predictions(test_predictions_paths, 'test')
        gen_predictions(valid_predictions_paths, 'valid')
        if include_train:
            gen_predictions(train_predictions_paths, 'train')

if __name__ == '__main__':
    sys.exit(main())
