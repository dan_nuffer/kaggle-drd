import logging
from pprint import pprint
import click
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from hyperopt.mongoexp import MongoTrials
from hyperopt.pyll import scope, as_apply


def data_space(name):
    return as_apply({
        # 'input_side_len' : input_side_len_space(name),
        'input_side_len' : 95, # hard-code it for faster training - grow as necessary
        # 'batch_size' : batch_size_space(name)
        'batch_size' : 64,
        'chunk_size' : 4096,
        'num_chunks_train' : 300,
    })


def data_processing_space(name):
    # use log(1e-15) here instead of log(0) to avoid numerical -inf problems
    num_eval_transforms = scope.int(hp.uniform(name + '|num_eval_transforms', 3, 100))
    zoom = hp.uniform(name + '|zoom', 0., 2.) * hp.pchoice(name + '|zoom_enable', [(.75, 1), (.25, 0)]) + 1.
    rotation_max = hp.uniform(name + '|rotation_max', 0, 360) * hp.pchoice(name + '|rotation_max_enable', [(.75, 1), (.25, 0)])
    shear = hp.uniform(name + '|shear', 0., 40.) * hp.pchoice(name + '|shear_enable', [(.75, 1), (.25, 0)])
    translation = hp.uniform(name + '|translation', 0., 20.) * hp.pchoice(name + '|translation_enable', [(.75, 1), (.25, 0)])
    do_flip = hp.pchoice(name + '|do_flip', [(.9, True), (.1, False)])
    stretch = hp.uniform(name + '|stretch', 0., 1.) * hp.pchoice(name + '|stretch_enable', [(.75, 1), (.25, 0)]) + 1.
    color_shift = hp.uniform(name + '|colorshift_sigma', 0., 0.2) * hp.pchoice(name + '|colorshift_enable', [(.75, 1), (.25, 0)])
    contrast_noise = hp.uniform(name + '|contrast_noise_sigma', 0., 0.2) * hp.pchoice(name + '|contrast_noise_enable', [(.75, 1), (.25, 0)])
    per_pixel_gaussian_noise = hp.uniform(name + '|per_pixel_gaussian_noise', 0., 0.2) * hp.pchoice(name + '|per_pixel_gaussian_noise_enable', [(.75, 1), (.25, 0)])

    return as_apply({
        'num_transforms' : num_eval_transforms,
        'zoom_range' : (1 / zoom, zoom),
        'rotation_range' : (0, rotation_max),
        'shear_range' : (-shear, shear),
        'translation_range' : (-translation, translation),
        'do_flip' : do_flip,
        'allow_stretch' : stretch,
        'color_shift_sigma' : color_shift,
        'contrast_noise_sigma' : contrast_noise,
        'per_pixel_gaussian_noise_sigma' : per_pixel_gaussian_noise,
    })


def objective(argd):
    from pprint import pprint
    print "-" * 80
    print "Evaluting hyperparameters:"
    pprint(argd)
    kappa = float(raw_input("Enter kappa: "))
    yn = 'n'
    while yn != 'y':
        yn = raw_input("You entered '" + str(kappa) + "' is that correct (y/n)?")

    # here is where we'll train the network and return the best validation score
    return {
        'loss': 1. - kappa,
        'status': STATUS_OK,
    }


@click.command()
@click.option('--count', default=1, help='Number evaluations.')
def main(count):
    logging.basicConfig(level=logging.DEBUG)

    mongotrials = MongoTrials('mongo://localhost:27017/drd_db/jobs', exp_key='data_transform')
    trials = Trials()

    data_params = data_space('data')
    train_data_processing = data_processing_space("train_data")
    eval_data_processing = data_processing_space("eval_data")

    space = as_apply({
                'train_data_processing': train_data_processing,
                'eval_data_processing': eval_data_processing,
                'data_params': data_params,
            })
    best = fmin(fn=objective,
                space=space,
                algo=tpe.suggest,
                max_evals=count,
                trials=trials)

    print "Best result from fmin():"
    pprint(best)
    print
    print "Best trial hyperparameters:"
    print trials.best_trial
    print
    print "All trials:"
    for i, trial in enumerate(trials):
        print "Trial %d:" % (i)
        pprint(trial)

    print
    print "inserting trials into mongodb"
    mongotrials.insert_trial_docs(trials.trials)


if __name__ == '__main__':
    import sys
    sys.exit(main())
