
In the hyperopt toplevel directory, you can type at the shell:

$ shovel util.list_dbs localhost 27017

And it will print something. You can call other methods in util.py using shovel's syntax.
See shovel's documentation for how to pass arguments:
https://github.com/seomoz/shovel

