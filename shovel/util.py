import copy
try:
    import dill as cPickle
except ImportError:
    import cPickle
import pymongo as pm
from shovel import task
import hyperopt
from hyperopt.mongoexp import MongoTrials
from pprint import pprint

@task
def show_vars(host, port, dbname, key, colorize=-1, columns=5):
    """
    Show loss vs. time scatterplots for one experiment or all experiments in a
    database.
    """
    conn = pm.Connection(host=host, port=int(port))
    K = [k for k  in conn[dbname]['jobs'].distinct('exp_key')]
    for k in K:
        print k
    if key is None:
        raise NotImplementedError()
    else:
        docs = list(
                conn[dbname]['jobs'].find(
                    {'exp_key': key},
                    {
                        'tid': 1,
                        'state': 1,
                        'result.loss':1,
                        'result.status':1,
                        'spec':1,
                        'misc.cmd': 1,
                        'misc.tid':1,
                        'misc.idxs':1,
                        'misc.vals': 1,
                    }))
    doc0 = docs[0]
    cmd = doc0['misc']['cmd']
    print 'cmd:', cmd
    if cmd[0] == 'bandit_json evaluate':
        bandit = hyperopt.utils.json_call(cmd[1])
    elif cmd[0] == 'domain_attachment':
        trials = MongoTrials('mongo://%s:%s/%s/jobs' % (
            host, port, dbname))
        blob = trials.attachments[cmd[1]]
        domain = cPickle.loads(blob)
        bandit = domain
    else:
        raise NotImplementedError('loading bandit from cmd', cmd)

    trials = hyperopt.trials_from_docs(docs, validate=False)
    hyperopt.plotting.main_plot_vars(trials, bandit=bandit,
            colorize_best=int(colorize),
            columns=int(columns)
            )


@task
def summarize_top(host, port, dbname, key, percentage=0.1):
    """
    Show loss vs. time scatterplots for one experiment or all experiments in a
    database.
    """
    conn = pm.Connection(host=host, port=int(port))
    if key is None:
        raise NotImplementedError()
    else:
        docs = list(
                conn[dbname]['jobs'].find(
                    {'exp_key': key},
                    {
                        'tid': 1,
                        'state': 1,
                        'result.loss':1,
                        'result.status':1,
                        'spec':1,
                        'misc.cmd': 1,
                        'misc.tid':1,
                        'misc.idxs':1,
                        'misc.vals': 1,
                    }))

    trials = hyperopt.trials_from_docs(docs, validate=False)
    docs = [(d['result']['loss'], d)
            for d in trials
            if d['state'] == hyperopt.JOB_STATE_DONE and d['result']['status'] == 'ok']
    docs.sort()
    if len(docs) < 1:
        print 'no successful trials!'

    top_trials = [trial for loss, trial in docs[:int(len(docs) * percentage)]]
    if len(top_trials) < 1:
        print 'not enough trials (%d) to analyze %.2f percentage' % (len(docs), percentage)

    #top_trials = hyperopt.trials_from_docs([doc for loss, doc in top_docs])
    #return
    #top_trials = hyperopt.Trials()
    #top_trials.insert_trial_docs([doc for loss, doc in top_docs])
    top_miscs = [tt['misc'] for tt in top_trials]
    top_vals = hyperopt.base.miscs_to_idxs_vals(top_miscs)[1]
    top_results = [tt['result'] for tt in top_trials]
    top_losses = [r.get('loss') for r in top_results]

    #pprint(top_docs)
    import pandas as pd

    import matplotlib
    import matplotlib.style
    matplotlib.style.use('ggplot')
    import matplotlib.pyplot as plt
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 250)

    df=pd.DataFrame(trials.vals)
    df = prepare_vars(df)
    df['losses'] = trials.losses()
    print (df.describe(include='all'))
    df = remove_unplottable_vars(df)
    from pandas.tools.plotting import scatter_matrix
    # the data is so sparse, that kde is misleading. hist is better.
    scatter_matrix(df, alpha=1.0, diagonal='hist') # figsize=(6, 6), diagonal='kde' (or 'hist')

    plt.show()

    #plt.figure()
    #df.plot(kind='kde')
    #df.iloc[:,0].plot(kind='kde')
    #df.hist()
    #plt.show()

    top_df = pd.DataFrame(top_vals)

    top_df = prepare_vars(top_df)
    top_df['losses'] = top_losses

    print (top_df.describe(include='all'))

    top_df = remove_unplottable_vars(top_df)

    #top_df.hist()
    from pandas.tools.plotting import scatter_matrix
    # the data is so sparse, that kde is misleading. hist is better.
    scatter_matrix(top_df, alpha=1.0, diagonal='hist') # figsize=(6, 6), diagonal='kde' (or 'hist')

    plt.show()


def remove_unplottable_vars(top_df):
    # eliminate the columns that aren't good to plot.
    bad_columns = [cn for cn in top_df.columns if top_df[cn].max() - top_df[cn].min() == 0 or cn.endswith('_enable')]
    # print bad_columns
    top_df = top_df.drop(bad_columns, axis=1)
    return top_df


def prepare_vars(df):
    # The eval variables were unused, so just eliminate them from the get go.
    eval_columns = [cn for cn in df.columns if cn.startswith("eval_data|")]
    df = df.drop(eval_columns, axis=1)
    import numpy as np

    df['l1'] *= df['l1_enable']
    df['l2'] *= df['l2_enable']
    df['l1_and_l2'] = df['l1'] + df['l2']
    df['max_col_norm1'] *= df['max_col_norm1_enable']
    df['max_col_norm2'] *= df['max_col_norm2_enable']
    df['max_col_norm3'] *= df['max_col_norm3_enable']
    df['learning_rate'] = np.log(df['learning_rate'])
    df['train_data|translation'] *= df['train_data|translation_enable']
    df['train_data|contrast_noise_sigma'] *= df['train_data|contrast_noise_enable']
    df['train_data|zoom'] += 1.
    return df


@task
def show_runtime(host, port, dbname, key):
    """
    Show runtime vs. trial_id
    """
    conn = pm.Connection(host=host, port=int(port))
    K = [k for k  in conn[dbname]['jobs'].distinct('exp_key')]
    print 'Experiments in database', dbname
    for k in K:
        print '* ', k
    docs = list(
            conn[dbname]['jobs'].find(
                {'exp_key': key},
                {
                    'tid': 1,
                    'state': 1,
                    'book_time': 1,
                    'refresh_time': 1,
                    #'result.loss':1,
                    #'result.status':1,
                    #'spec':1,
                    #'misc.cmd': 1,
                    #'misc.tid':1,
                    #'misc.idxs':1,
                    #'misc.vals': 1,
                }))
    import matplotlib.pyplot as plt
    for state in hyperopt.JOB_STATES:
        x = [d['tid'] for d in docs if d['state'] == state]
        if state != hyperopt.JOB_STATE_NEW:
            y = [(d['refresh_time'] - d['book_time']).total_seconds() / 60.0
                 for d in docs if d['state'] == state]
        else:
            y = [0] * len(x)

        if x:
            plt.scatter(x, y,
                    c=['g', 'b', 'k', 'r'][state],
                    label=['NEW', 'RUNNING', "DONE", "ERROR"][state])
    plt.ylabel('runtime (minutes)')
    plt.xlabel('trial identifier')
    plt.legend(loc='upper left')
    plt.show()


@task
def list_dbs(host, port, dbname=None):
    """
    List the databases and experiments being hosted by a mongo server
    """
    conn = pm.Connection(host=host, port=int(port))
    if dbname is None:
        dbnames = conn.database_names()
    else:
        dbnames = [dbname]

    for dbname in dbnames:
        K = [k for k  in conn[dbname]['jobs'].distinct('exp_key')]
        print ''
        print 'Database:', dbname
        for k in K:
            print ' ', k


@task
def transfer_trials(host, port, fromdb, todb):
    """
    Insert all of the documents in `fromdb` into `todb`.
    """
    from_trials = MongoTrials('mongo://%s:%s/%s/jobs' % (host, port, fromdb))
    to_trials = MongoTrials('mongo://%s:%s/%s/jobs' % (host, port, todb))
    from_docs = [copy.deepcopy(doc) for doc in from_trials]
    for doc in from_docs:
        del doc['_id']
    to_trials.insert_trial_docs(from_docs)


@task
def list_all(host, port, dbname, key=None, spec=0, fields=None):
    conn = pm.Connection(host=host, port=int(port))
    if key is None:
        query = {}
    else:
        query = {'exp_key': key}
    if fields is None:
        cursor = conn[dbname]['jobs'].find(query)
    else:
        fields = fields.split(',')
        cursor = conn[dbname]['jobs'].find(query, fields)

    for doc in cursor:
        print repr(doc)


@task
def list_attachments(host, port, dbname, key=None, tid=None):
    if tid is not None:
        tid = int(tid)
        conn = pm.Connection(host=host, port=int(port))
        query = {'tid': tid}
        for doc in conn[dbname]['jobs'].find(query, ['result', '_attachments']):
            for name, ptr in doc['_attachments']:
                print name, ptr
    elif key is not None:
        raise NotImplementedError()
    else:
        raise NotImplementedError()



@task
def list_errors(host, port, dbname, key=None, spec=0):
    conn = pm.Connection(host=host, port=int(port))
    if key is None:
        query = {}
    else:
        query = {'exp_key': key}
    query['state'] = hyperopt.JOB_STATE_ERROR
    retrieve = {'tid': 1, 'state': 1, 'result.status':1, 'misc.cmd': 1,
                'book_time': 1, 'error': 1, 'owner': 1}
    if int(spec):
        retrieve['spec'] = 1
    for doc in conn[dbname]['jobs'].find(query, retrieve):
        print doc['_id'], doc['tid'], doc['book_time'], doc['owner'], doc['error']
        if int(spec):
            print doc['spec']


@task
def list_failures(host, port, dbname, key=None, spec=0):
    conn = pm.Connection(host=host, port=int(port))
    if key is None:
        query = {}
    else:
        query = {'exp_key': key}
    query['state'] = hyperopt.JOB_STATE_DONE
    query['result.status'] = hyperopt.STATUS_FAIL
    retrieve = {'tid': 1,
            'result.failure':1,
            'misc.cmd': 1, 'book_time': 1, 'owner': 1}
    if int(spec):
        retrieve['spec'] = 1
    for doc in conn[dbname]['jobs'].find(query, retrieve):
        print doc['_id'], doc['tid'], doc['book_time'], doc['owner'],
        #print doc['result']['failure']
        pprint(doc['result'])
        if int(spec):
            print doc['spec']


@task
def list_best(host, port, dbname, key=None):
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    mongo_trials = MongoTrials(db, exp_key=key)
    docs = [(d['result']['loss'], d)
            for d in mongo_trials.trials
            if d['state'] == hyperopt.JOB_STATE_DONE and d['result']['status'] == 'ok']
    docs.sort()
    for loss, doc in reversed(docs):
        print loss, doc['owner'], doc['result']
    print 'total oks:', len(docs)
    print 'total trials:', len(mongo_trials.trials)


@task
def print_best_parameters(host, port, dbname, key=None):
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    mongo_trials = MongoTrials(db, exp_key=key)
    docs = [(d['result']['loss'], d)
            for d in mongo_trials.trials
            if d['state'] == hyperopt.JOB_STATE_DONE and d['result']['status'] == 'ok']
    docs.sort()
    if len(docs) < 1:
        print 'no successful trials!'
    else:
        for loss, doc in docs[:1]:
            print 'loss: %f' % (loss,)
            pprint(doc.to_dict())


@task
def delete_one_trial(host, port, dbname, trial_id):
    y, n = 'y', 'n'
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    mongo_trials = MongoTrials(db)
    trials = mongo_trials.handle.jobs.find({'tid': trial_id})
    print ("Found trial(s):")
    pprint(trials)
    print ('Are you sure you want to delete this trials from %s? (y/n)'
            % (n_trials, db))
    if input() != y:
        print 'Aborting'
        return
    for trial in trials:
        mongo_trials.delete_all(cond={'tid': trial['tid']})



@task
def delete_trials(host, port, dbname, key=None):
    y, n = 'y', 'n'
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    if key is None:
        mongo_trials = MongoTrials(db)
        n_trials = mongo_trials.handle.jobs.find({}).count()
        print ('Are you sure you want to delete ALL %i trials from %s? (y/n)'
                % (n_trials, db))
        if input() != y:
            print 'Aborting'
            return
        mongo_trials.delete_all()
    else:
        mongo_trials = MongoTrials(db, exp_key=key)
        n_trials = mongo_trials.handle.jobs.find({'exp_key': key}).count()
        print 'Confirm: delete %i trials matching %s? (y/n)' % (
            n_trials, key)
        if input() != y:
            print 'Aborting'
            return
        mongo_trials.delete_all()


@task
def delete_evaltimeout_trials(host, port, dbname, key=None):
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    if key is None:
        mongo_trials = MongoTrials(db)
        for t in mongo_trials.trials:
            try:
                if 'EvalTimeout' in str(t['result']['failure']):
                    #del_tid = t['tid']
                    pass
                else:
                    continue
            except KeyError:
                continue
            print t['tid'], t['result']['failure']
            mongo_trials.delete_all(cond={'tid': t['tid']})
    else:
        raise NotImplementedError()


@task
def delete_running(host, port, dbname, key=None):
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    if key is None:
        mongo_trials = MongoTrials(db)
    else:
        mongo_trials = MongoTrials(db, exp_key=key)
    mongo_trials.delete_all(cond={'state': hyperopt.JOB_STATE_RUNNING})


@task
def delete_errors(host, port, dbname, key=None):
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    if key is None:
        mongo_trials = MongoTrials(db)
    else:
        mongo_trials = MongoTrials(db, exp_key=key)
    if key is None:
        cond = {}
    else:
        cond = {'exp_key': key}
    cond['state'] = hyperopt.JOB_STATE_ERROR
    n_trials = mongo_trials.handle.jobs.find(cond).count()
    print 'Confirm: delete %i error trials matching %s? (y/n)' % (
        n_trials, key)
    if raw_input() != 'y':
        print 'Aborting'
        return
    mongo_trials.delete_all(cond=cond)


@task
def delete_failures(host, port, dbname, key=None):
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    if key is None:
        mongo_trials = MongoTrials(db)
    else:
        mongo_trials = MongoTrials(db, exp_key=key)
    #mongo_trials.delete_all(cond={'state': hyperopt.JOB_STATE_ERROR})
    if key is None:
        cond = {}
    else:
        cond = {'exp_key': key}
    cond['state'] = hyperopt.JOB_STATE_DONE
    cond['result.status'] = hyperopt.STATUS_FAIL
    n_trials = mongo_trials.handle.jobs.find(cond).count()
    print 'Confirm: delete %i failed trials matching %s? (y/n)' % (
        n_trials, key)
    if raw_input() != 'y':
        print 'Aborting'
        return
    mongo_trials.delete_all(cond=cond)


@task
def snapshot(host, port, dbname, ofilename=None, plevel=-1):
    """
    Save the trials of a mongo database to a pickle file.
    """
    print 'fetching trials'
    db = 'mongo://%s:%s/%s/jobs' % (host, port, dbname)
    from_trials = MongoTrials(db)
    to_trials = hyperopt.base.trials_from_docs(
            from_trials.trials,
            # -- effing slow, but required for un-pickling??
            validate=True)
    if ofilename is None:
        ofilename = dbname+'.snapshot.pkl'
    print 'saving to', ofilename
    ofile = open(ofilename, 'w')
    cPickle.dump(to_trials, ofile, int(plevel))


