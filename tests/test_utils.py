import pytest

import numpy as np
from numpy.testing import assert_equal
import utils


def test_confusion_matrix_two_agree():
    pred = np.array([0, 1])
    actual = np.array([0, 1])
    assert_equal(utils.conf_matrix(pred, actual, 2), [[1, 0], [0, 1]])

def test_confusion_matrix_two_agree_two_disagree():
    pred = np.array([0, 1, 0, 1])
    actual = np.array([0, 1, 1, 0])
    assert_equal(utils.conf_matrix(pred, actual, 2), [[1, 1], [1, 1]])

def test_confusion_matrix_four():
    pred   = np.array([0, 1, 1, 0, 0, 0, 1, 1, 1, 1])
    actual = np.array([0, 1, 1, 1, 1, 1, 0, 0, 0, 0])
    assert_equal(utils.conf_matrix(pred, actual, 2), [[1, 4], [3, 2]])

def test_ordinal_targets():
    labels=np.array([0, 1, 2, 4, 3])
    assert_equal(utils.ordinal_targets(labels), np.array([[1, 0, 0, 0, 0], [1, 1, 0, 0, 0], [1, 1, 1, 0, 0], [1, 1, 1, 1, 1], [1, 1, 1, 1, 0]]))

def test_predict_ordinal():
    labels=np.array([0, 1, 2, 4, 3])
    assert_equal(labels, utils.predict_ordinal(np.array([[1, 0, 0, 0, 0], [1, 1, 0, 0, 0], [1, 1, 1, 0, 0], [1, 1, 1, 1, 1], [1, 1, 1, 1, 0]]), 0.5))
