import glob
import os
import sys

import numpy as np
import pandas as pd

import data
import utils

if len(sys.argv) != 2:
    sys.exit("Usage: create_submissions.py <predictions_path>")

predictions_path = sys.argv[1]
filename = os.path.splitext(os.path.basename(predictions_path))[0]
target_path = "submissions/%s.csv" % filename

print "Load predictions"
predictions = np.load(predictions_path)
level_predictions = utils.predict_ordinal(predictions, thresh=0.5)

print "Generate CSV"

test_images = pd.read_csv('data/test_images.csv')
df = pd.DataFrame(level_predictions, columns=["level"], index=test_images['image'])
df.index.name = 'image'
df.to_csv(target_path)

print "Compress with gzip"
os.system("gzip %s" % target_path)

print "  stored in %s.gz" % target_path
