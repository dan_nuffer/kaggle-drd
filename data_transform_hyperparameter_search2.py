import logging
from pprint import pprint

import click
from hyperopt import fmin, tpe, hp, Trials
from hyperopt.mongoexp import MongoTrials
from hyperopt.pyll import scope, as_apply
import numpy as np

# This script is to narrow down the space of data processing, by removing the options which never were useful
# and narrowing the range of others. I'm also adding some additional hyperparameters for regularization and hidden
# unit sizes.

def data_space(name):
    batch_size = 32
    chunk_size = 4096
    return as_apply({
        # 'input_side_len' : input_side_len_space(name),
        'input_side_len' : 127, # hard-code it for faster training - grow as necessary
        # 'batch_size' : batch_size_space(name)
        'batch_size' : batch_size,
        'chunk_size' : chunk_size,
        'num_chunks_train' : 300 * (64 * 4096) // (batch_size * chunk_size), # scale this by the amount of examples we're doing
    })


def eval_data_processing_space(name):
    zoom = hp.uniform(name + '|zoom', 0., 1.5) * hp.pchoice(name + '|zoom_enable', [(.5, 0), (.5, 1)]) + 1.
    rotation_max = hp.uniform(name + '|rotation_max', 12, 360)
    shear = hp.uniform(name + '|shear', 20., 35.)
    translation = hp.uniform(name + '|translation', 1., 15.)
    contrast_noise = hp.uniform(name + '|contrast_noise_sigma', 0., 0.3)

    return as_apply({
        'num_transforms' : 90,
        'zoom_range' : (1 / zoom, zoom),
        'rotation_range' : (0, rotation_max),
        'shear_range' : (-shear, shear),
        'translation_range' : (-translation, translation),
        'do_flip' : True,
        'allow_stretch' : 1.,
        'contrast_noise_sigma' : contrast_noise,
    })


def train_data_processing_space(name):
    zoom = hp.uniform(name + '|zoom', 0., 1.5) + 1.
    shear = hp.uniform(name + '|shear', 3., 28.)
    translation = hp.uniform(name + '|translation', 5., 20.) * hp.pchoice(name + '|translation_enable', [(.5, 0), (.5, 1)])
    contrast_noise = hp.uniform(name + '|contrast_noise_sigma', 0., 0.2) * hp.pchoice(name + '|contrast_noise_enable', [(.9, 0), (.1, 1)])

    return as_apply({
        'zoom_range' : (1 / zoom, zoom),
        'rotation_range' : (0, 0),
        'shear_range' : (-shear, shear),
        'translation_range' : (-translation, translation),
        'do_flip' : True,
        'allow_stretch' : 1.,
        'contrast_noise_sigma' : contrast_noise,
    })


def learning_parameters_space():
    return as_apply({
        'momentum' : hp.uniform('momentum', 0.5, 0.99),
        'learning_rate' : hp.loguniform('learning_rate', np.log(1e-5), np.log(10)),
    })


def regularization_space():
    return as_apply({
        'l1' : hp.loguniform('l1', np.log(1e-7), np.log(1e-1)) * hp.choice('l1_enable', [0, 1]),
        'l2' : hp.loguniform('l2', np.log(1e-7), np.log(1e-1)) * hp.choice('l2_enable', [0, 1]),
        'dropout_p1' : hp.uniform('dropout_p1', 0.1, 0.9),
        'dropout_p2' : hp.uniform('dropout_p2', 0.1, 0.9),
        'dropout_p3' : hp.uniform('dropout_p3', 0.1, 0.9),
        'max_col_norm1' : hp.uniform('max_col_norm1', 0.1, 10.0) * hp.choice('max_col_norm1_enable', [0, 1]),
        'max_col_norm2' : hp.uniform('max_col_norm2', 0.1, 10.0) * hp.choice('max_col_norm2_enable', [0, 1]),
        'max_col_norm3' : hp.uniform('max_col_norm3', 0.1, 10.0) * hp.choice('max_col_norm3_enable', [0, 1]),
    })


def objective(argd):
    from pprint import pprint, pformat
    from hyperopt import STATUS_OK, STATUS_FAIL
    import platform
    import time

    print "-" * 80
    print "Evaluting hyperparameters:"
    pprint(argd)

    netfile = "nets/cnn_bn_3x127x_ord_hps2_%s-%s.py" % (platform.node(), time.strftime("%Y%m%d-%H%M%S", time.localtime()))
    print "Writing network to %s" % (netfile)
    with open(netfile, "w") as f:
        f.write('hyperparameters = \\\n')
        f.write(pformat(argd))
        f.write('\n')
        f.write('from . import cnn_bn_3x127x_ord_hps2_tmpl\n')
        f.write('cnn_bn_3x127x_ord_hps2_tmpl.set_hyperparameters(hyperparameters)\n')
        f.write('from cnn_bn_3x127x_ord_hps2_tmpl import *\n')

    resultfile = netfile + '-result.pkl'
    command = 'python train_convnet.py %s --store-result-in=%s' % (netfile, resultfile)

    print "Running training command: %s" % (command,)
    # run command
    import os
    if os.system(command) != 0:
        return {
            'loss': 2.0,
            'status': STATUS_FAIL
        }

    # load results
    print "Loading results from: %s" % (resultfile,)
    import cPickle
    with open(resultfile, 'r') as f:
        results = cPickle.load(f)

    kappa = results['best_valid_kappa']

    print "Got best valid kappa: %f" % (kappa,)

    # if the training ended early, call it a failure.
    if results['chunks_since_start'] != results['num_chunks_train']:
        return {
            'loss': 1. - kappa,
            'status': STATUS_FAIL
        }

    # return the best validation kappa, converted to a loss
    return {
        'loss': 1. - kappa,
        'status': STATUS_OK,
    }


@click.command()
@click.option('--count', default=1, help='Number evaluations.')
@click.option('--mongo', default=False, help='Use mongodb. ')
def main(count, mongo):
    logging.basicConfig(level=logging.DEBUG)

    if mongo:
        trials = MongoTrials('mongo://localhost:27017/drd_db/jobs', exp_key='data_transform2')
    else:
        trials = Trials()

    data_params = data_space('data')
    train_data_processing = train_data_processing_space("train_data")
    eval_data_processing = eval_data_processing_space("eval_data")

    space = as_apply({
                'train_data_processing': train_data_processing,
                'eval_data_processing': eval_data_processing,
                'data_params': data_params,
                'learning' : learning_parameters_space(),
                'regularization' : regularization_space(),
            })
    best = fmin(fn=objective,
                space=space,
                algo=tpe.suggest,
                max_evals=count,
                trials=trials)

    print "Best result from fmin():"
    pprint(best)
    print
    print "Best trial hyperparameters:"
    print trials.best_trial
    print
    print "All trials:"
    for i, trial in enumerate(trials):
        print "Trial %d:" % (i)
        pprint(trial)



if __name__ == '__main__':
    import sys
    sys.exit(main())
