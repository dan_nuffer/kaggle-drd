import click
import warnings
import clint
import numpy as np
import time
import os
import sys
import importlib
import cPickle as pickle
from datetime import datetime, timedelta
import string
from itertools import izip
import skll
import theano
import theano.tensor as T
import lasagne as nn
import matplotlib
import nan_guard
from clint.textui import progress, indent, puts, colored
import pandas as pd
from plot import save_training_plot
matplotlib.use('agg') # prevents matplot lib failures due to inability to connect to an X server (e.g. in screen)
import utils
import nn_plankton

warnings.filterwarnings('ignore', '.*get_all_layers.*')
warnings.filterwarnings('ignore', '.*GlorotUniform.*')


@click.command()
@click.option('--config-name', help='Configuration to train, can be classname or filename.')
@click.option('--resume-path', help='metadata save from a previous run. Use to resume interrupted training.',
              default=None, type=click.Path(exists=True, file_okay=True, dir_okay=False, readable=True))
@click.option('--store-result-in', 'result_path', help='save results in the given file', default=None,
              type=click.Path(exists=False, file_okay=True, dir_okay=False, readable=False, writable=True))
@click.option('--validate-every', 'validate_every', help='override the nets validate_every setting.', default=None,
              type=click.INT)
def main(config_name, resume_path, result_path, validate_every):
    if len(sys.argv) < 2:
        sys.exit("Usage: train_convnet.py <configuration_name> [<resume path>]")

    if config_name.startswith('nets/'):
        config_name = config_name[5:-3]
    config = importlib.import_module("nets.%s" % config_name)

    expid = utils.generate_expid(config_name)
    metadata_target_path = os.path.join(os.getcwd(), "metadata/%s.pkl" % expid)
    best_metadata_target_path = os.path.join(os.getcwd(), "metadata/%s_best.pkl" % expid)

    print
    print "Experiment ID: %s" % expid
    print

    print "Build model"
    model = config.build_model()
    if len(model) == 4:
        l_ins, l_out, l_resume, l_exclude = model
    elif len(model) == 3:
        l_ins, l_out, l_resume = model
        l_exclude = l_ins[0]
    else:
        l_ins, l_out = model
        l_resume = l_out
        l_exclude = l_ins[0]

    all_layers = nn.layers.get_all_layers(l_out)
    num_params = nn.layers.count_params(l_out)
    print "  number of parameters: {:,}".format(num_params)
    print "  layer output shapes:"
    for layer in all_layers:
        name = string.ljust(layer.__class__.__name__, 32)
        print "    %s %s" % (name, layer.get_output_shape(),)

    if hasattr(config, 'build_objective'):
        print "Using config build_objective"
        obj = config.build_objective(l_ins, l_out)
    else:
        print "Using loss_function=nn_plankton.log_losses"
        obj = nn.objectives.Objective(l_out, loss_function=nn_plankton.log_losses)

    forward_pass_train_updates = {}
    train_loss = obj.get_loss(updates=forward_pass_train_updates)
    output = l_out.get_output(deterministic=True)
    compute_loss_expr = obj.get_loss(deterministic=True)

    all_params = nn.layers.get_all_params(l_out)
    all_excluded_params = nn.layers.get_all_params(l_exclude)
    all_resume_state_vars = nn.layers.get_state_vars(l_resume)
    all_params = list(set(all_params) - set(all_excluded_params))

    input_ndims = [len(l_in.get_output_shape()) for l_in in l_ins]
    xs_shared = [nn.utils.shared_empty(dim=ndim) for ndim in input_ndims]
    y_shared = nn.utils.shared_empty(dim=2)

    if getattr(config, 'learning_rate_method', '') == 'schedule' or hasattr(config, 'learning_rate_schedule'):
        print "Using config learning_rate_schedule"
        learning_rate_method = 'schedule'
        learning_rate_schedule = config.learning_rate_schedule
        learning_rate = theano.shared(np.float32(learning_rate_schedule[0]))
    elif getattr(config, 'learning_rate_method', '') == 'hot_swapped':
        learning_rate_method = 'hot_swapped'
        convergence_patience_epochs = getattr(config, 'convergence_patience_epochs', 3)
        convergence_patience_chunks = convergence_patience_epochs * config.num_examples // config.chunk_size
        convergence_window_epochs = getattr(config, 'convergence_window_epochs', 10)
        convergence_window_chunks = convergence_window_epochs * config.num_examples // config.chunk_size
        learning_rate_schedule = None
        learning_rate = theano.shared(np.float32(0)) # the value doesn't matter, it will be changed
        print "Using hot swapped learning rate method. patience=%d/%d, window=%d/%d" % \
              (convergence_patience_epochs, convergence_patience_chunks, convergence_window_epochs, convergence_window_chunks)
        # This is setup to have 5 items in each of 5 orders of magnitude. That seemed like a good enough space to
        # make a difference between rates. We don't want to make the bins too small or else the searching will take
        # too long. The 5x5 is just my guess. TODO Test it and see what works best empirically. (Great more hyperparameters!)
        # Because of the way the line search works, order from large to small. TODO Test if that's best, or the reverse, or random?
        # The paper says "The line search ... _decreases_ through the other available learning rates..."
        hs_lr_alphas = nn.utils.floatX(np.exp(np.linspace(np.log(1.0), np.log(1e-5), 5 * 4 + 1)))
        hs_rewards = np.zeros_like(hs_lr_alphas)
        hs_counts = np.zeros_like(hs_lr_alphas)

        # TODO: what is a good value here?
        time_horizon = 100
        gamma = 1 - 1 / (4 * np.sqrt(time_horizon))  # formula from paper
        # TODO: and here?
        explore_const = 0.5  # \xi from DUCB paper
        def get_ducb_suggested_index(rewards, counts):
            rewards *= gamma
            counts *= gamma
            means = rewards / counts  # counts shouldn't ever be zero because initial_alpha_index ensures that each
                                      # index is returned once at the beginning of training
            n = np.sum(counts)
            conf_intervals = np.sqrt(explore_const * np.log(n) / counts)
            ucbs = means + conf_intervals
            puts("ucbs: %s" % (str(ucbs),))
            ucbs_max = ucbs.argmax()
            puts("get_ducb_suggested_index() returning %d" % (ucbs_max,))
            return ucbs_max


        def initial_alpha_index(t):
            if t < len(hs_lr_alphas):
                puts("initial_alpha_index: t is small, returning t: %d" % (t,))
                return t
            return get_ducb_suggested_index(hs_rewards, hs_counts)

        def grant_reward(index, start_loss, current_loss):
            puts("grant_reward(index=%d, start_loss=%f, current_loss=%f), reward: %f" % (index, start_loss, current_loss, np.log(start_loss) - np.log(current_loss)))
            hs_rewards[index] += np.log(start_loss) - np.log(current_loss)
            hs_counts[index] += 1

    else:
        # leave learning_rate_schedule = None to do it dynamically
        learning_rate_method = 'dynamic'
        config_learning_rate = config.learning_rate if hasattr(config, 'learning_rate') else 0.01
        learning_rate_decay = config.learning_rate_decay if hasattr(config, 'learning_rate_decay') else 0.5
        learning_rate_patience = config.learning_rate_patience if hasattr(config, 'learning_rate_patience') else 5
        learning_rate_rolling_mean_window = config.learning_rate_rolling_mean_window if hasattr(config,
                                                                                                'learning_rate_rolling_mean_window') else 10
        learning_rate_improvement = config.learning_rate_improvement if hasattr(config,
                                                                                'learning_rate_improvement') else 0.99

        print "Using initial learning rate: %f" % (config_learning_rate,)
        print "Using learning rate decay: %f" % (learning_rate_decay,)
        print "Using learning rate patience: %d" % (learning_rate_patience,)
        print "Using learning rate rolling mean window: %d" % (learning_rate_rolling_mean_window,)
        print "Using learning rate improvement: %f" % (learning_rate_improvement,)

        learning_rate_schedule = None
        learning_rate = theano.shared(np.float32(config_learning_rate))

    if hasattr(config, 'validate_train'):
        validate_train = config.validate_train
    else:
        validate_train = True

    if validate_every is not None:
        config.validate_every = validate_every

    if hasattr(config, 'train_max_seconds'):
        train_max_seconds = config.train_max_seconds
    else:
        train_max_seconds = 9223372036854775807 # portable maxint - a really long time

    divergence_threshold = config.divergence_threshold if hasattr(config, 'divergence_threshold') else 10.0

    idx = T.lscalar('idx')

    givens = {
        obj.target_var: y_shared[idx * config.batch_size:(idx + 1) * config.batch_size],
    }
    for l_in, x_shared in zip(l_ins, xs_shared):
        givens[l_in.input_var] = x_shared[idx * config.batch_size:(idx + 1) * config.batch_size]

    if hasattr(config, 'build_updates'):
        print "Using config build_updates()"
        updates = config.build_updates(train_loss, all_params, learning_rate)
    else:
        print "Using nesterov momentum"
        updates = nn.updates.nesterov_momentum(train_loss, all_params, learning_rate, config.momentum)

    if hasattr(config, 'censor_updates'):
        print "Using config censor_updates()"
        updates = config.censor_updates(updates, l_out)

    updates.update(forward_pass_train_updates)
    print "Compiling theano function iter_train"
    iter_train = theano.function([idx], train_loss, givens=givens,
                                 updates=updates)  # , mode=nan_guard.NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=True))
    print "Compiling theano function compute_output"
    compute_output = theano.function([idx], output, givens=givens, on_unused_input="ignore")

    print "Compiling theano function compute_loss"
    compute_loss = theano.function([idx], compute_loss_expr, givens=givens, no_default_updates=True)

    best_valid_kappa = -1.0
    best_e = 0

    if hasattr(config, 'resume_path') or resume_path is not None:
        print "Loading model parameters for resuming from ", resume_path if resume_path is not None else config.resume_path
        if hasattr(config, 'pre_init_path'):
            print "lresume=lout"
            l_resume = l_out

        if hasattr(config, 'resume_path'):
            resume_metadata = np.load(config.resume_path)
        else:
            resume_metadata = np.load(resume_path)

        nn.layers.set_state_values(l_resume, resume_metadata['param_values'][:len(all_resume_state_vars)])

        start_chunk_idx = resume_metadata['chunks_since_start'] + 1
        chunks_train_idcs = range(start_chunk_idx, config.num_chunks_train)

        # set lr to the correct value
        if learning_rate_schedule is not None:
            current_lr = np.float32(utils.current_learning_rate(learning_rate_schedule, start_chunk_idx))
        else:
            current_lr = resume_metadata['learning_rates'][-1]

        print "  setting learning rate to %.7f" % current_lr
        learning_rate.set_value(current_lr)
        losses_train = resume_metadata['losses_train']
        losses_eval_valid = resume_metadata['losses_eval_valid']
        losses_eval_train = resume_metadata['losses_eval_train']
        accuracies_eval_valid = resume_metadata[
            'accuracies_eval_valid'] if 'accuracies_eval_valid' in resume_metadata else [0.] * start_chunk_idx
        accuracies_eval_train = resume_metadata[
            'accuracies_eval_train'] if 'accuracies_eval_train' in resume_metadata else [0.] * start_chunk_idx
        kappas_eval_valid = resume_metadata['kappas_eval_valid'] if 'kappas_eval_valid' in resume_metadata else [
                                                                                                                    0.] * start_chunk_idx
        if 'kappas_eval_valid' in resume_metadata and len(kappas_eval_valid) > 0:
            best_valid_kappa = max(kappas_eval_valid)
            best_e = np.argmax(kappas_eval_valid)

        kappas_eval_train = resume_metadata['kappas_eval_train'] if 'kappas_eval_train' in resume_metadata else [
                                                                                                                    0.] * start_chunk_idx
        chunk_durations = resume_metadata['chunk_durations'] if 'chunk_durations' in resume_metadata else [
                                                                                                              0.] * start_chunk_idx
        chunk_times = resume_metadata['chunk_times'] if 'chunk_times' in resume_metadata else [0.] * start_chunk_idx
        learning_rates = resume_metadata['learning_rates'] if 'learning_rates' in resume_metadata else [
                                                                                                           0.] * start_chunk_idx

    elif hasattr(config, 'pre_init_path'):
        print "Load model parameters for initializing first x layers"
        resume_metadata = np.load(config.pre_init_path)
        nn.layers.set_state_values(l_resume, resume_metadata['param_values'][:len(all_resume_state_vars)])

        chunks_train_idcs = range(config.num_chunks_train)
        losses_train = []
        losses_eval_valid = []
        losses_eval_train = []
        accuracies_eval_valid = []
        accuracies_eval_train = []
        kappas_eval_valid = []
        kappas_eval_train = []
        chunk_durations = []
        chunk_times = []
        learning_rates = []
    else:
        chunks_train_idcs = range(config.num_chunks_train)
        losses_train = []
        losses_eval_valid = []
        losses_eval_train = []
        accuracies_eval_valid = []
        accuracies_eval_train = []
        kappas_eval_valid = []
        kappas_eval_train = []
        chunk_durations = []
        chunk_times = []
        learning_rates = []

    valid_chunk_idxs = range(config.validate_every, config.num_chunks_train + config.validate_every,
                             config.validate_every)

    print "Load data"
    config.data_loader.load_train(config.data_dir)

    if hasattr(config, 'resume_path'):
        config.data_loader.set_params(resume_metadata['data_loader_params'])
    else:
        config.data_loader.estimate_params()  # important! this takes care of zmuv parameter estimation etc.

    if hasattr(config, 'create_train_gen'):
        create_train_gen = config.create_train_gen
    else:
        create_train_gen = lambda: config.data_loader.create_random_gen(config.data_loader.images_train,
                                                                        config.data_loader.labels_train)

    if hasattr(config, 'create_eval_valid_gen'):
        create_eval_valid_gen = config.create_eval_valid_gen
    else:
        create_eval_valid_gen = lambda: config.data_loader.create_fixed_gen(config.data_loader.images_valid,
                                                                            augment=False)

    if hasattr(config, 'create_eval_train_gen'):
        create_eval_train_gen = config.create_eval_train_gen
    else:
        create_eval_train_gen = lambda: config.data_loader.create_fixed_gen(config.data_loader.images_train,
                                                                            augment=False)

    print "Train model"
    start_time = time.time()

    num_batches_chunk = config.chunk_size // config.batch_size

    def indented_label(label):
        return ''.join(clint.textui.core.INDENT_STRINGS) + label

    for e, (xs_chunk, y_chunk) in izip(chunks_train_idcs, create_train_gen()):
        chunk_start_time = time.time()
        puts("Chunk %d/%d, learning rate %.7f" % (e + 1, config.num_chunks_train, learning_rate.get_value()))

        with indent(2):
            if learning_rate_method == 'schedule':
                if e in learning_rate_schedule:
                    lr = np.float32(learning_rate_schedule[e])
                    learning_rate.set_value(lr)
            elif learning_rate_method == 'dynamic':
                # do dynamic learning rate algorithm
                if len(learning_rates) >= (learning_rate_patience + learning_rate_rolling_mean_window):
                    # find the index where the current learning rate was first used
                    lr_a = np.array(learning_rates)
                    current_lr_began_idx = (lr_a == lr_a[-1]).argmax()
                    if e - current_lr_began_idx >= learning_rate_patience:
                        rm = pd.rolling_mean(np.array(losses_train), learning_rate_rolling_mean_window)
                        if rm[-1] > rm[-learning_rate_patience + 1] * learning_rate_improvement:
                            puts("loss didn't improve enough. setting learning rate to " + str(
                                learning_rate.get_value() * learning_rate_decay))
                            learning_rate.set_value(
                                np.array(learning_rate.get_value() * learning_rate_decay, dtype=theano.config.floatX))

                            if learning_rate.get_value() < 1e-7:
                                puts("learning rate is too small. Stopping training.")
                                break
                        else:
                            puts("loss improved, not changing learning rate")

            elif learning_rate_method == 'hot_swapped':
                # this will be handled inside the batch training loop
                pass

            for x_shared, x_chunk in zip(xs_shared, xs_chunk):
                # network+theano conv needs data in batch-channel-0-1 tensor dimensions arrangement
                # our data loads images using scikit image, which has the image in 0-1-channel, so we get x_chunk in
                # batch-0-1-channel dimension arrangement. transpose is to put it in the right order.
                x_shared.set_value(x_chunk.transpose(0, 3, 1, 2))
            y_shared.set_value(y_chunk)


            losses = []
            for b in progress.bar(xrange(num_batches_chunk), label=indented_label("training ")):
                if learning_rate_method == 'hot_swapped':
                    puts("begin hot_swapped learning for t: %d" % (e * num_batches_chunk + b,))
                    start_index = initial_alpha_index(e * num_batches_chunk + b)
                    puts("got start_index: %d" % (start_index,))
                    # BacktrackingLineSearchWithRewards(f, g, B, theta_arrow, alpha_arrow, startIndex)
                    start_loss = compute_loss(b)
                    puts("computed start_loss: %f" % (start_loss,))
                    current_loss = start_loss
                    best_loss = np.inf  # This will be set inside the loop the first time through after it is calculated
                    best_learning_rate = hs_lr_alphas[start_index]
                    have_found_better_than_start = False
                    #start_parameters = nn.layers.get_state_values(l_out)
                    start_update_parameters = [u.get_value() for u in updates.keys()]

                    for index in xrange(start_index, len(hs_lr_alphas)):
                        prev_loss = current_loss
                        alpha = hs_lr_alphas[index]
                        puts("setting learning_rate to alpha: %f" % (alpha,))
                        learning_rate.set_value(alpha)

                        # unless it's the first time through the loop, we need to reset the parameters
                        if index != start_index:
                            #nn.layers.set_state_values(l_out, start_parameters)
                            for p, v in zip(updates.keys(), start_update_parameters):
                                p.set_value(v)


                        train_loss = iter_train(b)
                        #puts("train_loss: %f" % (train_loss,))

                        current_loss = compute_loss(b)
                        puts("current_loss: %f" % (current_loss,))
                        grant_reward(index, start_loss, current_loss)

                        # save the initial alpha index parameters as the best
                        if current_loss < best_loss or index == start_index:
                            puts("current < best. have new best_loss")
                            best_loss = current_loss
                            # save these so we don't have to recompute it after the loop ends
                            #best_parameters = nn.layers.get_state_values(l_out)
                            best_update_parameters = [u.get_value() for u in updates.keys()]
                            best_learning_rate = learning_rate.get_value()
                            best_index = index
                            if index != start_index:
                                have_found_better_than_start = True

                        if have_found_better_than_start and current_loss > prev_loss:
                            puts("already found a better, and current > prev. Stopping line search")
                            break

                    #nn.layers.set_state_values(l_out, best_parameters)
                    for p, v in zip(updates.keys(), best_update_parameters):
                        p.set_value(v)
                    learning_rate.set_value(best_learning_rate)
                    puts("Using best_learning_rate: %f (# %d)" % (best_learning_rate, best_index))
                    loss = best_loss

                else:
                    loss = iter_train(b)
                    if np.isnan(loss):
                        raise RuntimeError("NaN DETECTED during training.")


                losses.append(loss)

            learning_rates.append(learning_rate.get_value())

            mean_train_loss = np.mean(losses)
            if len(losses_train) > 0 and mean_train_loss >= min(losses_train):
                puts("mean training loss:\t" + colored.red("%.6f" % mean_train_loss))
            else:
                puts("mean training loss:\t" + colored.green("%.6f" % mean_train_loss))
            losses_train.append(mean_train_loss)

            if mean_train_loss > divergence_threshold or np.isposinf(mean_train_loss):
                raise RuntimeError("loss is over divergence_threshold(%f) or infinite. Stopping training" % (divergence_threshold,))

            now = time.time()
            time_since_start = now - start_time
            chunk_times.append(time_since_start)
            chunk_duration = now - chunk_start_time
            chunk_durations.append(chunk_duration)
            est_time_left = time_since_start * (
                float(config.num_chunks_train - (e + 1)) / float(e + 1 - chunks_train_idcs[0]))
            eta = datetime.now() + timedelta(seconds=est_time_left)
            eta_str = eta.strftime("%c")

            if ((e + 1) % config.validate_every) == 0 or time_since_start >= train_max_seconds:
                puts()
                with indent(-2):
                    puts("Validating")

                if validate_train:
                    subsets = ["train", "valid"]
                    gens = [create_eval_train_gen, create_eval_valid_gen]
                    label_sets = [config.data_loader.labels_train, config.data_loader.labels_valid]
                    losses_eval = [losses_eval_train, losses_eval_valid]
                    accuracies_eval = [accuracies_eval_train, accuracies_eval_valid]
                    kappas_eval = [kappas_eval_train, kappas_eval_valid]
                else:
                    subsets = ["valid"]
                    gens = [create_eval_valid_gen]
                    label_sets = [config.data_loader.labels_valid]
                    losses_eval = [losses_eval_valid]
                    accuracies_eval = [accuracies_eval_valid]
                    kappas_eval = [kappas_eval_valid]

                for subset, create_gen, labels, losses, accuracies, kappas in zip(subsets, gens, label_sets,
                                                                                  losses_eval, accuracies_eval,
                                                                                  kappas_eval):
                    # puts("%s set" % subset)

                    def compute_losses(ys, labels):
                        if config.data_loader.labels_are_ordinal_regression:
                            return utils.sum_binary_crossentropy(ys, labels)
                        else:
                            return utils.log_losses(ys, labels)

                    def compute_categories(ys):
                        if config.data_loader.labels_are_ordinal_regression:
                            return utils.predict_ordinal(ys, thresh=0.5)
                        else:
                            return ys.argmax(axis=1)

                    chunk_losses = []
                    cat_outputs = []

                    with progress.Bar(label=indented_label("%s set " % subset), expected_size=len(labels)) as bar:
                        label_idx_begin = 0
                        for xs_chunk_eval, chunk_length_eval in create_gen():
                            num_batches_chunk_eval = int(np.ceil(chunk_length_eval / float(config.batch_size)))

                            for x_shared, x_chunk_eval in zip(xs_shared, xs_chunk_eval):
                                x_shared.set_value(x_chunk_eval.transpose(0, 3, 1, 2))

                            outputs_chunk = []
                            for b in xrange(num_batches_chunk_eval):
                                out = compute_output(b)
                                outputs_chunk.append(out)
                                bar.show(sum([len(output) for output in cat_outputs]) + sum(
                                    [len(output_chunk) for output_chunk in outputs_chunk]))

                            outputs_chunk = np.vstack(outputs_chunk)
                            outputs_chunk = outputs_chunk[:chunk_length_eval]  # truncate to the right length
                            chunk_losses.append(compute_losses(outputs_chunk, labels[label_idx_begin:label_idx_begin + chunk_length_eval]))
                            cat_outputs.append(compute_categories(outputs_chunk))
                            label_idx_begin += chunk_length_eval

                    chunk_losses = np.concatenate(chunk_losses)
                    cat_outputs = np.concatenate(cat_outputs)

                    #puts()
                    #puts("Outputs:")
                    #pd.options.display.chop_threshold = 9e-7
                    #pd.options.display.width = 160
                    #puts(pd.DataFrame(outputs).describe().transpose().to_string())

                    loss = np.mean(chunk_losses)
                    if np.isnan(loss):
                        raise RuntimeError("NaN DETECTED during validation.")

                    if config.data_loader.labels_are_ordinal_regression:
                        cat_labels = utils.predict_ordinal(labels, thresh=0.5)
                    else:
                        cat_labels = labels if config.data_loader.labels_are_categories else labels.argmax(axis=1)
                    num_classes = getattr(config.data_loader, 'num_classes', np.max(cat_labels) + 1)

                    if len(cat_labels) < 10:
                        puts()
                        puts('Confusion Matrix:')
                        puts('         predicted')
                        puts('actual')
                        with indent(7):
                            puts(str(utils.conf_matrix(cat_outputs, cat_labels, num_classes)))

                    acc = utils.accuracy(cat_outputs, cat_labels)
                    accuracies.append(acc)

                    # computing kappa is O(n^2), so if there's over 1000 classes, just don't bother
                    if num_classes <= 1000:
                        kappa = skll.kappa(cat_labels, cat_outputs, 'quadratic')
                    else:
                        kappa = acc # just to avoid messing with all the best kappa tracking stuff we'll call them the same

                    kappas.append(kappa)

                    with indent(10):
                        puts("loss:\t\t%.6f" % loss)
                        puts("acc:\t\t%.2f%%" % (acc * 100))
                        if kappa < best_valid_kappa:
                            puts("kappa:\t\t" + colored.red("%.6f" % (kappa,)))
                        else:
                            puts("kappa:\t\t" + colored.green("%.6f" % (kappa,)))
                        puts("best val kappa:\t%.6f" % (best_valid_kappa,))
                        puts("best val chunk:\t%d" % (best_e + 1))
                        puts()

                    if subset == "valid" and kappa > best_valid_kappa:
                        puts()
                        puts("Saving best metadata, parameters. Prev best kappa: %.6f, New best kappa: %.6f" % (
                            best_valid_kappa, kappa))

                        best_valid_kappa = kappa
                        best_e = e
                        utils.save_training_state(best_metadata_target_path, config_name, expid, e, losses_train,
                                                  losses_eval_valid, losses_eval_train, time_since_start, l_out, config,
                                                  chunk_durations, chunk_times, accuracies_eval_valid,
                                                  accuracies_eval_train,
                                                  kappas_eval_valid, kappas_eval_train, learning_rates,
                                                  valid_chunk_idxs)

                        puts("saved best to %s" % (best_metadata_target_path,))
                        puts()

                    losses.append(loss)
                    del cat_outputs

            if ((e + 1) % config.save_every) == 0:
                utils.save_training_state(metadata_target_path, config_name, expid, e, losses_train, losses_eval_valid,
                                          losses_eval_train, time_since_start, l_out, config, chunk_durations,
                                          chunk_times,
                                          accuracies_eval_valid, accuracies_eval_train, kappas_eval_valid,
                                          kappas_eval_train,
                                          learning_rates, valid_chunk_idxs)
                puts("saved to %s" % (metadata_target_path,))

            puts("%s since start (%.2f s). Estimated %s to go (ETA: %s)" % (
                utils.hms(time_since_start), chunk_duration, utils.hms(est_time_left), eta_str))
            plot_save_path = "plots/%s.html" % (expid,)
            save_training_plot(plot_save_path, losses_train, learning_rates, losses_eval_valid, losses_eval_train,
                               kappas_eval_valid, kappas_eval_train, valid_chunk_idxs, expid,
                               accuracies_eval_train=accuracies_eval_train, accuracies_eval_valid=accuracies_eval_valid,
                               plot_accuracy=False, plot_kappa=True, configuration=config_name)
            puts("Saved plot to %s" % (plot_save_path,))
            puts()

            if time_since_start >= train_max_seconds:
                break

    puts()
    puts("Best kappa %.6f found on chunk %d" % (best_valid_kappa, best_e + 1))

    if result_path is not None:
        puts("Saving pickled loss results to %s" % (result_path,))
        hyperparameters = {}
        if hasattr(config, 'hyperparameters'):
            hyperparameters = config.hyperparameters
        with open(result_path, 'w') as f:
            pickle.dump({
                'configuration': config_name,
                'experiment_id': expid,
                'chunks_since_start': e,
                'losses_train': losses_train,
                'losses_eval_valid': losses_eval_valid,
                'losses_eval_train': losses_eval_train,
                'accuracies_eval_valid': accuracies_eval_valid,
                'accuracies_eval_train': accuracies_eval_train,
                'kappas_eval_valid': kappas_eval_valid,
                'kappas_eval_train': kappas_eval_train,
                'time_since_start': time_since_start,
                'chunk_durations': chunk_durations,
                'chunk_times': chunk_times,
                'learning_rates': learning_rates,
                'valid_chunk_idxs': valid_chunk_idxs,
                'best_valid_kappa': best_valid_kappa,
                'best_e': best_e,
                'best_metadata_target_path': best_metadata_target_path,
                'metadata_target_path': metadata_target_path,
                'hyperparameters': hyperparameters,
                'plot_save_path': plot_save_path,
                'num_chunks_train': config.num_chunks_train,
            }, f, pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    sys.exit(main())
