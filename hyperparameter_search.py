import click
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
import hyperopt
from hyperopt.pyll import scope, as_apply
import pickle
import time
from pprint import pprint
import lasagne
from lasagne.layers import InputLayer
import numpy as np


@scope.define
def lasagne_InputLayer(*args, **kwargs):
    return InputLayer(*args, **kwargs)

def input_side_len_space(name):
    return hp.quniform(name + ".input_side_len", 96, 360, 12) + 1


def batch_size_space(name):
    return hp.quniform(name + '.batch_size', 8, 128, 8)

def _svc_gamma(name):
    # -- making these non-conditional variables
    #    probably helps the GP algorithm generalize
    gammanz = hp.choice(name + '.gammanz', [0, 1])
    gamma = hp.lognormal(name + '.gamma', np.log(0.01), 2.5)
    return gammanz * gamma


def data_space(name):
    return as_apply({
        # 'input_side_len' : input_side_len_space(name),
        'input_side_len' : 95, # hard-code it for faster training - grow as necessary
        # 'batch_size' : batch_size_space(name)
        'batch_size' : 64,
        'chunk_size' : 4096,

    })


def data_processing_space(name):
    # use log(1e-15) here instead of log(0) to avoid numerical -inf problems
    num_eval_transforms = scope.int(hp.uniform(name + '.num_eval_transforms', 3, 100))
    zoom = hp.uniform(name + '.zoom', 0., 2.) * hp.pchoice(name + '.zoom.enable', [(.75, 1), (.25, 0)]) + 1.
    rotation_max = hp.uniform(name + '.rotation_max', 0, 360) * hp.pchoice(name + '.rotation_max.enable', [(.75, 1), (.25, 0)])
    shear = hp.uniform(name + '.shear', 0., 40.) * hp.pchoice(name + '.shear.enable', [(.75, 1), (.25, 0)])
    translation = hp.uniform(name + '.translation', 0., 20.) * hp.pchoice(name + '.translation.enable', [(.75, 1), (.25, 0)])
    do_flip = hp.pchoice(name + '.do_flip', [(.9, True), (.1, False)])
    stretch = hp.uniform(name + '.stretch', 0., 1.) * hp.pchoice(name + '.stretch.enable', [(.75, 1), (.25, 0)]) + 1.
    color_shift = hp.uniform(name + '.colorshift_sigma', 0., 0.2) * hp.pchoice(name + '.colorshift_enable', [(.75, 1), (.25, 0)])
    contrast_noise = hp.uniform(name + '.contrast_noise_sigma', 0., 0.2) * hp.pchoice(name + '.contrast_noise_enable', [(.75, 1), (.25, 0)])
    per_pixel_gaussian_noise = hp.uniform(name + '.per_pixel_gaussian_noise', 0., 0.2) * hp.pchoice(name + '.per_pixel_gaussian_noise_enable', [(.75, 1), (.25, 0)])

    return as_apply({
        'num_transforms' : num_eval_transforms,
        'zoom_range' : (1 / zoom, zoom),
        'rotation_range' : (0, rotation_max),
        'shear_range' : (-shear, shear),
        'translation_range' : (-translation, translation),
        'do_flip' : do_flip,
        'allow_stretch' : stretch,
        'color_shift_sigma' : color_shift,
        'contrast_noise_sigma' : contrast_noise,
        'per_pixel_gaussian_noise_sigma' : per_pixel_gaussian_noise,
    })


def deep_net_space(name, _data_space):
    side_len = scope.int(_data_space['input_side_len'])
    batch_size = scope.int(_data_space['batch_size'])
    l0 = scope.lasagne_InputLayer((batch_size, 3, side_len, side_len))
    return l0



def objective(argd):
    print
    pprint(argd)
    # here is where we'll train the network and return the best validation score
    return {
        'loss': 2,
        'status': STATUS_OK,
        # -- store other results like this
        'x': 2,
        #'eval_time': time.time(),
        #'other_stuff': {'type': None, 'value': [0, 1, 2]},
        # -- attachments are handled differently - for large values
        #'attachments':
        #    {'time_module': pickle.dumps(time.time)}
    }


@click.command()
@click.option('--count', default=1, help='Number evaluations.')
def main(count):
    trials = Trials()
    # space = hp.choice('a',
    # [
    #     ('case 1', 1 + hp.lognormal('c1', 0, 1)),
    #     ('case 2', hp.uniform('c2', -10, 10))
    # ])
    data_params = data_space('data')
    train_data_processing = data_processing_space("train_data")
    eval_data_processing = data_processing_space("eval_data")

    space = as_apply({
                'model': deep_net_space('net', data_params),
                'train_data_processing': train_data_processing,
                'eval_data_processing': eval_data_processing,
                'data_params': data_params,
                'training': None
            })
    best = fmin(fn=objective,
                space=space,
                algo=tpe.suggest,
                max_evals=count,
                trials=trials)

    print "Best:", best
    #print trials.trials
    #print trials.results
    #print trials.losses()
    #print trials.statuses()
    # loading an attachment
    # msg = trials.trial_attachments(trials.trials[5])['time_module']
    # time_module = pickle.loads(msg)


if __name__ == '__main__':
    import sys
    sys.exit(main())