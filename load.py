import glob
import numpy as np
import itertools
import os.path
import pandas as pd
import sklearn.cross_validation

import data
import buffering
import utils


DEFAULT_VALIDATION_SPLIT_PATH = "validation_split_v1.pkl"

class PredictionsWithFeaturesDataLoader(object):
    params = [] # attributes that need to be stored after training and loaded at test time.

    def __init__(self, **kwargs):
        self.augmentation_transforms_test = [data.tform_identity] # default to no test-time augmentation
        self.color_space = "rgb"  # default
        self.__dict__.update(kwargs)
        self.labels_are_categories = False
        self.labels_are_ordinal_regression = True if hasattr(self, 'predict_mode') and self.predict_mode == "ordinal_regression" else False

    def estimate_params(self):
        pass

    def load_train(self, data_dir):
        labels = utils.one_hot(np.load("data/labels_train_us.npy"), num_classes=121).astype(np.float32)
        split = np.load(DEFAULT_VALIDATION_SPLIT_PATH)

        split = np.load(DEFAULT_VALIDATION_SPLIT_PATH)
        indices_train = split['indices_train']
        indices_valid = split['indices_valid']
        features = np.load("data/features_train.pkl").item()

        if "aaronmoments" in self.features:
            print "aaronmoments"
            def normalize(x):
                return x
                # return (x - x.mean(axis=0,keepdims=True))/x.std(axis=0,keepdims=True)
            image_shapes = np.asarray([img.shape for img in data.load(data_dir, 'train_us')]).astype(np.float32)
            moments = np.load("data/image_moment_stats_v1_train.pkl")
            centroid_distance = np.abs(moments["centroids"][:, [1, 0]] - image_shapes / 2)
            angles = moments["angles"][:, None]
            minor_axes = moments["minor_axes"][:, None]
            major_axes = moments["major_axes"][:, None]
            centroid_distance = normalize(centroid_distance)
            angles = normalize(angles)
            minor_axes = normalize(minor_axes)
            major_axes = normalize(major_axes)
            features["aaronmoments"] = np.concatenate([centroid_distance,angles,minor_axes,major_axes], 1).astype(np.float32)

        info = np.concatenate([features[feat] for feat in self.features], 1).astype(np.float32)

        print info.shape

        self.info_train = info[indices_train]
        self.info_valid = info[indices_valid]

        self.y_train = np.load(self.train_pred_file).astype(np.float32)
        self.y_valid = np.load(self.valid_pred_file).astype(np.float32)
        self.labels_train = labels[indices_train]
        self.labels_valid = labels[indices_valid]

    def load_test(self, data_dir):
        self.y_test = np.load(self.test_pred_file).astype(np.float32)
        self.images_test = data.load(data_dir, 'test')
        features = np.load("data/features_test.pkl").item()

        if "aaronmoments" in self.features:
            print "aaronmoments"
            def normalize(x):
                return x
                # return (x - x.mean(axis=0,keepdims=True))/x.std(axis=0,keepdims=True)
            image_shapes = np.asarray([img.shape for img in self.images_test]).astype(np.float32)
            moments = np.load("data/image_moment_stats_v1_test.pkl")
            centroid_distance = np.abs(moments["centroids"][:, [1, 0]] - image_shapes / 2)
            angles = moments["angles"][:, None]
            minor_axes = moments["minor_axes"][:, None]
            major_axes = moments["major_axes"][:, None]
            centroid_distance = normalize(centroid_distance)
            angles = normalize(angles)
            minor_axes = normalize(minor_axes)
            major_axes = normalize(major_axes)
            features["aaronmoments"] = np.concatenate([centroid_distance,angles,minor_axes,major_axes], 1).astype(np.float32)

        self.info_test = np.concatenate([features[feat] for feat in self.features], 1).astype(np.float32)


    def create_random_gen(self):
        def random_gen():
            for i in range(self.num_chunks_train):
                indices = np.random.randint(self.y_train.shape[0], size=self.chunk_size)
                yield [self.y_train[indices], self.info_train[indices]], self.labels_train[indices]

        return buffering.buffered_gen_threaded(random_gen())

    def create_fixed_gen(self, subset):
        if subset == "train":
            y = self.y_train
            image_shapes = self.info_train
        elif subset == "valid":
            y = self.y_valid
            image_shapes = self.info_valid
        elif subset == "test":
            y = self.y_test
            image_shapes = self.info_test
        else:
            raise Exception

        num_batches = int(np.ceil(float(y.shape[0]) / self.chunk_size))
        def fixed_gen():
            for i in range(num_batches):
                if i == num_batches - 1:
                    chunk_x1 = np.zeros((self.chunk_size, y.shape[1]), dtype=np.float32)
                    chunk_x2 = np.zeros((self.chunk_size, image_shapes.shape[1]), dtype=np.float32)
                    chunk_length = y.shape[0] - (num_batches - 1) * self.chunk_size
                    chunk_x1[:chunk_length] = y[i * self.chunk_size:]
                    chunk_x2[:chunk_length] = image_shapes[i * self.chunk_size:]
                else:
                    chunk_x1 = y[i * self.chunk_size: (i + 1) * self.chunk_size]
                    chunk_x2 = image_shapes[i * self.chunk_size: (i + 1) * self.chunk_size]
                    chunk_length = self.chunk_size
                yield [chunk_x1, chunk_x2], chunk_length

        return buffering.buffered_gen_threaded(fixed_gen())

    def get_params(self):
        return { pname: getattr(self, pname, None) for pname in self.params }
        
    def set_params(self, p):
        self.__dict__.update(p)


class PredictionsWithMomentsDataLoader(object):
    params = [] # attributes that need to be stored after training and loaded at test time.

    def __init__(self, **kwargs):
        self.augmentation_transforms_test = [data.tform_identity] # default to no test-time augmentation
        self.color_space = "rgb"  # default
        self.__dict__.update(kwargs)
        self.labels_are_categories = False
        self.labels_are_ordinal_regression = True if hasattr(self, 'predict_mode') and self.predict_mode == "ordinal_regression" else False

    def estimate_params(self):
        pass

    def load_train(self, data_dir):
        labels = utils.one_hot(np.load("data/labels_train_us.npy")).astype(np.float32)
        split = np.load(DEFAULT_VALIDATION_SPLIT_PATH)

        split = np.load(DEFAULT_VALIDATION_SPLIT_PATH)
        indices_train = split['indices_train']
        indices_valid = split['indices_valid']

        image_shapes = np.asarray([img.shape for img in data.load(data_dir, 'train_us')]).astype(np.float32)
        moments = np.load("data/image_moment_stats_v1_train.pkl")

        centroid_distance = np.abs(moments["centroids"][:, [1, 0]] - image_shapes / 2)
        info = np.concatenate((centroid_distance, image_shapes, moments["angles"][:, None], moments["minor_axes"][:, None], moments["major_axes"][:, None]), 1).astype(np.float32)

        self.info_train = info[indices_train]
        self.info_valid = info[indices_valid]

        self.y_train = np.load(self.train_pred_file).astype(np.float32)
        self.y_valid = np.load(self.valid_pred_file).astype(np.float32)
        self.labels_train = labels[indices_train]
        self.labels_valid = labels[indices_valid]

    def load_test(self, data_dir):
        self.y_test = np.load(self.test_pred_file).astype(np.float32)
        self.images_test = data.load(data_dir, 'test')
        image_shapes_test = np.asarray([img.shape for img in self.images_test]).astype(np.float32)
        moments_test = np.load("data/image_moment_stats_v1_test.pkl")
        centroid_distance = np.abs(moments_test["centroids"][:, [1, 0]] - image_shapes_test / 2)
        self.info_test = np.concatenate((centroid_distance, image_shapes_test, moments_test["angles"][:, None], moments_test["minor_axes"][:, None], moments_test["major_axes"][:, None]), 1).astype(np.float32)
        # self.info_test = np.concatenate((image_shapes_test, moments_test["centroids"], moments_test["minor_axes"][:, None], moments_test["major_axes"][:, None]), 1).astype(np.float32)


    def create_random_gen(self):
        def random_gen():
            for i in range(self.num_chunks_train):
                indices = np.random.randint(self.y_train.shape[0], size=self.chunk_size)
                yield [self.y_train[indices], self.info_train[indices]], self.labels_train[indices]

        return buffering.buffered_gen_threaded(random_gen())

    def create_fixed_gen(self, subset):
        if subset == "train":
            y = self.y_train
            image_shapes = self.info_train
        elif subset == "valid":
            y = self.y_valid
            image_shapes = self.info_valid
        elif subset == "test":
            y = self.y_test
            image_shapes = self.info_test
        else:
            raise Exception

        num_batches = int(np.ceil(float(y.shape[0]) / self.chunk_size))
        def fixed_gen():
            for i in range(num_batches):
                if i == num_batches - 1:
                    chunk_x1 = np.zeros((self.chunk_size, y.shape[1]), dtype=np.float32)
                    chunk_x2 = np.zeros((self.chunk_size, image_shapes.shape[1]), dtype=np.float32)
                    chunk_length = y.shape[0] - (num_batches - 1) * self.chunk_size
                    chunk_x1[:chunk_length] = y[i * self.chunk_size:]
                    chunk_x2[:chunk_length] = image_shapes[i * self.chunk_size:]
                else:
                    chunk_x1 = y[i * self.chunk_size: (i + 1) * self.chunk_size]
                    chunk_x2 = image_shapes[i * self.chunk_size: (i + 1) * self.chunk_size]
                    chunk_length = self.chunk_size
                yield [chunk_x1, chunk_x2], chunk_length

        return buffering.buffered_gen_threaded(fixed_gen())

    def get_params(self):
        return { pname: getattr(self, pname, None) for pname in self.params }
        
    def set_params(self, p):
        self.__dict__.update(p)


class PredictionsWithSizeDataLoader(object):
    params = [] # attributes that need to be stored after training and loaded at test time.

    def __init__(self, **kwargs):
        self.augmentation_transforms_test = [data.tform_identity] # default to no test-time augmentation
        self.color_space = "rgb"  # default
        self.__dict__.update(kwargs)
        self.labels_are_categories = False
        self.labels_are_ordinal_regression = True if hasattr(self, 'predict_mode') and self.predict_mode == "ordinal_regression" else False

    def estimate_params(self):
        pass

    def load_train(self, data_dir):
        labels = utils.one_hot(np.load("data/labels_train_us.npy")).astype(np.float32)
        split = np.load(DEFAULT_VALIDATION_SPLIT_PATH)

        split = np.load(DEFAULT_VALIDATION_SPLIT_PATH)
        indices_train = split['indices_train']
        indices_valid = split['indices_valid']

        image_shapes = np.asarray([img.shape for img in data.load(data_dir, 'train_us')]).astype(np.float32)
        self.image_shapes_train = image_shapes[indices_train]
        self.image_shapes_valid = image_shapes[indices_valid]

        self.y_train = np.load(self.train_pred_file).astype(np.float32)
        self.y_valid = np.load(self.valid_pred_file).astype(np.float32)
        self.labels_train = labels[indices_train]
        self.labels_valid = labels[indices_valid]

    def load_test(self, data_dir):
        self.y_test = np.load(self.test_pred_file).astype(np.float32)
        self.images_test = data.load(data_dir, 'test')
        self.image_shapes_test = np.asarray([img.shape for img in self.images_test]).astype(np.float32)

    def create_random_gen(self):
        def random_gen():
            for i in range(self.num_chunks_train):
                indices = np.random.randint(self.y_train.shape[0], size=self.chunk_size)
                yield [self.y_train[indices], self.image_shapes_train[indices]], self.labels_train[indices]

        return buffering.buffered_gen_threaded(random_gen())

    def create_fixed_gen(self, subset):
        if subset == "train":
            y = self.y_train
            image_shapes = self.image_shapes_train
        elif subset == "valid":
            y = self.y_valid
            image_shapes = self.image_shapes_valid
        elif subset == "test":
            y = self.y_test
            image_shapes = self.image_shapes_test
        else:
            raise Exception

        num_batches = int(np.ceil(float(y.shape[0]) / self.chunk_size))
        def fixed_gen():
            for i in range(num_batches):
                if i == num_batches - 1:
                    chunk_x1 = np.zeros((self.chunk_size, y.shape[1]), dtype=np.float32)
                    chunk_x2 = np.zeros((self.chunk_size, image_shapes.shape[1]), dtype=np.float32)
                    chunk_length = y.shape[0] - (num_batches - 1) * self.chunk_size
                    chunk_x1[:chunk_length] = y[i * self.chunk_size:]
                    chunk_x2[:chunk_length] = image_shapes[i * self.chunk_size:]
                else:
                    chunk_x1 = y[i * self.chunk_size: (i + 1) * self.chunk_size]
                    chunk_x2 = image_shapes[i * self.chunk_size: (i + 1) * self.chunk_size]
                    chunk_length = self.chunk_size
                yield [chunk_x1, chunk_x2], chunk_length

        return buffering.buffered_gen_threaded(fixed_gen())

    def get_params(self):
        return { pname: getattr(self, pname, None) for pname in self.params }
        
    def set_params(self, p):
        self.__dict__.update(p)



class DataLoader(object):
    params = [] # attributes that need to be stored after training and loaded at test time.

    def __init__(self, **kwargs):
        self.augmentation_transforms_test = [data.tform_identity] # default to no test-time augmentation
        self.color_space = "rgb"  # default
        self.__dict__.update(kwargs)
        self.labels_are_categories = False
        self.labels_are_ordinal_regression = True if hasattr(self, 'predict_mode') and self.predict_mode == "ordinal_regression" else False

        if hasattr(self, 'predict_mode') and self.predict_mode == "ordinal_regression":
            print "Using ordinal regression targets"
            self.labels_train = utils.ordinal_targets(np.load("data/labels_train_us.npy")).astype(np.float32)
            self.labels_valid = utils.ordinal_targets(np.load("data/labels_val.npy")).astype(np.float32)
        else: # default to one-hot
            print "Using one-hot targets"
            self.labels_train = utils.one_hot(np.load("data/labels_train_us.npy")).astype(np.float32)
            self.labels_valid = utils.one_hot(np.load("data/labels_val.npy")).astype(np.float32)

    def estimate_params(self):
        pass

    def load_train(self, data_dir):
        self.images_train = data.load(data_dir, 'train_us')
        self.images_valid = data.load(data_dir, 'val')

    def load_test(self, data_dir):
        self.images_test = data.load(data_dir, 'test')

    def get_params(self):
        return { pname: getattr(self, pname, None) for pname in self.params }
        
    def set_params(self, p):
        self.__dict__.update(p)


class RescaledDataLoader(DataLoader):
    def create_random_gen(self, images, labels):
        gen = data.rescaled_patches_gen_augmented(images, labels, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=self.chunk_size, num_chunks=self.num_chunks_train, augmentation_params=self.augmentation_params, color_space=self.color_space)

        def random_gen():
            for chunk_x, chunk_y, chunk_shape in gen:
                yield [chunk_x], chunk_y

        return buffering.buffered_gen_threaded(random_gen())

    def create_fixed_gen(self, images, augment=False):
        augmentation_transforms = self.augmentation_transforms_test if augment else None
        gen = data.rescaled_patches_gen_fixed(images, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=self.chunk_size, augmentation_transforms=augmentation_transforms, color_space=self.color_space)
        
        def fixed_gen():
            for chunk_x, chunk_shape, chunk_length in gen:
                yield [chunk_x], chunk_length

        return buffering.buffered_gen_threaded(fixed_gen())


class ZmuvRescaledDataLoader(DataLoader):
    params = ['zmuv_mean', 'zmuv_std'] # params that need to be stored after training and loaded at test time.

    def estimate_params(self):
        self.estimate_zmuv_params() # zero mean unit variance

    def estimate_zmuv_params(self):
        gen = data.rescaled_patches_gen_augmented(self.images_train, self.labels_train, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=max([4000, self.chunk_size]), num_chunks=1, augmentation_params=self.augmentation_params, color_space=self.color_space)
        chunk_x, _, _ = gen.next()
        self.zmuv_mean = chunk_x.mean()
        self.zmuv_std = chunk_x.std()

    def create_random_gen(self, images, labels):
        gen = data.rescaled_patches_gen_augmented(images, labels, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=self.chunk_size, num_chunks=self.num_chunks_train, augmentation_params=self.augmentation_params, color_space=self.color_space)

        def random_gen():
            for chunk_x, chunk_y, chunk_shape in gen:
                chunk_x -= self.zmuv_mean
                chunk_x /= self.zmuv_std
                yield [chunk_x], chunk_y

        return buffering.buffered_gen_threaded(random_gen())

    def create_fixed_gen(self, images, augment=False):
        augmentation_transforms = self.augmentation_transforms_test if augment else None
        gen = data.rescaled_patches_gen_fixed(images, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=self.chunk_size, augmentation_transforms=augmentation_transforms, color_space=self.color_space)
        
        def fixed_gen():
            for chunk_x, chunk_shape, chunk_length in gen:
                chunk_x -= self.zmuv_mean
                chunk_x /= self.zmuv_std 
                yield [chunk_x], chunk_length

        return buffering.buffered_gen_threaded(fixed_gen())


class ZmuvMultiscaleDataLoader(DataLoader):
    params = ['zmuv_means', 'zmuv_stds'] # params that need to be stored after training and loaded at test time.

    def estimate_params(self):
        self.estimate_zmuv_params() # zero mean unit variance

    def estimate_zmuv_params(self):
        gen = data.multiscale_patches_gen_augmented(self.images_train, self.labels_train, self.scale_factors, patch_shapes=self.patch_sizes,
            chunk_size=max([4000, self.chunk_size]), num_chunks=1, augmentation_params=self.augmentation_params)
        chunks_x, _, _ = gen.next()
        self.zmuv_means = [chunk_x.mean() for chunk_x in chunks_x]
        self.zmuv_stds = [chunk_x.std() for chunk_x in chunks_x]

    def create_random_gen(self, images, labels):
        gen = data.multiscale_patches_gen_augmented(images, labels, self.scale_factors, patch_shapes=self.patch_sizes,
            chunk_size=self.chunk_size, num_chunks=self.num_chunks_train, augmentation_params=self.augmentation_params, color_space=self.color_space)

        def random_gen():
            for chunks_x, chunk_y, chunk_shape in gen:
                for k in xrange(len(chunks_x)):
                    chunks_x[k] -= self.zmuv_means[k]
                    chunks_x[k] /= self.zmuv_stds[k]
                    chunks_x[k] = chunks_x[k][:, None, :, :]

                yield chunks_x, chunk_y

        return buffering.buffered_gen_threaded(random_gen())

    def create_fixed_gen(self, images, augment=False):
        augmentation_transforms = self.augmentation_transforms_test if augment else None
        gen = data.multiscale_patches_gen_fixed(images, self.scale_factors, patch_shapes=self.patch_sizes,
            chunk_size=self.chunk_size, augmentation_transforms=augmentation_transforms, color_space=self.color_space)
        
        def fixed_gen():
            for chunks_x, chunk_shape, chunk_length in gen:
                for k in xrange(len(chunks_x)):
                    chunks_x[k] -= self.zmuv_means[k]
                    chunks_x[k] /= self.zmuv_stds[k]
                    chunks_x[k] = chunks_x[k][:, None, :, :]

                yield chunks_x, chunk_length

        return buffering.buffered_gen_threaded(fixed_gen())


class ShardedResampledPseudolabelingZmuvMultiscaleDataLoader(ZmuvMultiscaleDataLoader):

    def load_train(self, data_dir):
        train_images = data.load(data_dir, 'train_us')
        train_labels = utils.one_hot(np.load("data/labels_train_us.npy")).astype(np.float32)

        if ("valid_pred_file" in self.__dict__):
            valid_pseudo_labels = np.load(self.valid_pred_file).astype(np.float32)
        else:
            print "No valid_pred_file set. Only using test-set for pseudolabeling!!"

        shuffle = np.load("test_shuffle_seed0.npy")
        if not ("shard" in self.__dict__):
            raise ValueError("Missing argument: shard: (should be value in {0, 1, 2})")
        if not self.shard in [0, 1, 2]:
            raise ValueError("Wrong argument: shard: (should be value in {0, 1, 2})")
        N = len(shuffle)
        if self.shard == 0:
            train_shard = shuffle[N/3:]
        if self.shard == 1:
            train_shard = np.concatenate((shuffle[:N/3], shuffle[2*N/3:]))
        if self.shard == 2:
            train_shard = shuffle[:2*N/3]

        test_images = data.load(data_dir, 'test')[train_shard]
        test_pseudo_labels = np.load(self.test_pred_file)[train_shard].astype(np.float32)
        print test_pseudo_labels.shape

        if not hasattr(self, 'validation_split_path'):
            self.validation_split_path = DEFAULT_VALIDATION_SPLIT_PATH
        split = np.load(self.validation_split_path)
        indices_train = split['indices_train']
        indices_valid = split['indices_valid']

        self.images_train = train_images[indices_train]
        self.labels_train = train_labels[indices_train]
        if ("valid_pred_file" in self.__dict__):
            self.images_pseudo = np.concatenate((train_images[indices_valid], test_images), 0)
            self.labels_pseudo = np.concatenate((valid_pseudo_labels, test_pseudo_labels), 0)
        else:
            self.images_pseudo = test_images
            self.labels_pseudo = test_pseudo_labels

        self.images_valid = train_images[indices_valid]
        self.labels_valid = train_labels[indices_valid]

    def create_random_gen(self, *args):
        # we ignore the args
        train_chunk_size = int(round(self.chunk_size * self.train_sample_weight))
        pseudo_chunk_size = self.chunk_size - train_chunk_size

        train_gen = data.multiscale_patches_gen_augmented(self.images_train, self.labels_train, self.scale_factors, patch_shapes=self.patch_sizes,
            chunk_size=train_chunk_size, num_chunks=self.num_chunks_train, augmentation_params=self.augmentation_params, color_space=self.color_space)

        pseudo_gen = data.multiscale_patches_gen_augmented(self.images_pseudo, self.labels_pseudo, self.scale_factors, patch_shapes=self.patch_sizes,
            chunk_size=pseudo_chunk_size, num_chunks=self.num_chunks_train, augmentation_params=self.augmentation_params, color_space=self.color_space)

        def random_gen():
            indices = np.arange(self.chunk_size)
            for a, b in itertools.izip(train_gen, pseudo_gen):
                (chunk_x1, chunk_y1, chunk_shape), (chunk_x2, chunk_y2, _) = a, b
                np.random.shuffle(indices)

                chunk_y = np.concatenate((chunk_y1, chunk_y2), 0)[indices]
                chunks_x = []
                for k in xrange(len(chunk_x1)):
                    chunks_x += [np.concatenate((chunk_x1[k], chunk_x2[k]), 0)[indices]]
                    chunks_x[k] -= self.zmuv_means[k]
                    chunks_x[k] /= self.zmuv_stds[k]
                yield chunks_x, chunk_y

        return buffering.buffered_gen_threaded(random_gen())


class UnsupervisedDataLoader(object):
    params = [] # attributes that need to be stored after training and loaded at test time.

    def __init__(self, **kwargs):
        self.augmentation_transforms_test = [data.tform_identity] # default to no test-time augmentation
        self.color_space = "rgb"  # default
        self.__dict__.update(kwargs)
        cat_labels = np.arange(0., self.num_images)
        if self.num_images < self.num_classes:
            self.num_classes = self.num_images
        cat_labels = np.mod(cat_labels, self.num_classes)
        cat_labels = cat_labels[:self.num_images]

        self.labels_train = cat_labels
        self.labels_valid = self.labels_train
        self.labels_are_categories = True
        self.labels_are_ordinal_regression = False

    def estimate_params(self):
        pass

    def load_train(self, data_dir_arg):
        data_dir = self.data_dir if hasattr(self, 'data_dir') else data_dir_arg
        self.images_train = np.load('%s/images.npy' % (data_dir,))[:self.num_images]
        self.images_valid = self.images_train

    def load_test(self, data_dir):
        self.images_test = np.empty(0, dtype='object')

    def get_params(self):
        return { pname: getattr(self, pname, None) for pname in self.params }

    def set_params(self, p):
        self.__dict__.update(p)


class UnsupervisedZmuvRescaledDataLoader(UnsupervisedDataLoader):
    params = ['zmuv_mean', 'zmuv_std'] # params that need to be stored after training and loaded at test time.

    def estimate_params(self):
        self.estimate_zmuv_params() # zero mean unit variance

    def estimate_zmuv_params(self):
        gen = data.rescaled_patches_gen_augmented(self.images_train, self.labels_train, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=max([4000, self.chunk_size]), num_chunks=1, augmentation_params=self.augmentation_params, color_space=self.color_space)
        chunk_x, _, _ = gen.next()
        self.zmuv_mean = chunk_x.mean()
        self.zmuv_std = np.maximum(1e-7, chunk_x.std())

    def create_random_gen(self, images, labels):
        gen = data.rescaled_patches_gen_augmented(images, labels, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=self.chunk_size, num_chunks=self.num_chunks_train, augmentation_params=self.augmentation_params, color_space=self.color_space)

        def random_gen():
            for chunk_x, chunk_y, chunk_shape in gen:
                chunk_x -= self.zmuv_mean
                chunk_x /= self.zmuv_std
                #yield [chunk_x], chunk_y
                yield [chunk_x], utils.one_hot(chunk_y, self.num_classes)

        return buffering.buffered_gen_threaded(random_gen())

    def create_fixed_gen(self, images, augment=False):
        augmentation_transforms = self.augmentation_transforms_test if augment else None
        gen = data.rescaled_patches_gen_fixed(images, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=self.chunk_size, augmentation_transforms=augmentation_transforms, color_space=self.color_space)

        def fixed_gen():
            for chunk_x, chunk_shape, chunk_length in gen:
                chunk_x -= self.zmuv_mean
                chunk_x /= self.zmuv_std
                yield [chunk_x], chunk_length

        return buffering.buffered_gen_threaded(fixed_gen())


class PseudolabelingZmuvRescaledDataLoader(ZmuvRescaledDataLoader):
    def __init__(self, **kwargs):
        super(PseudolabelingZmuvRescaledDataLoader, self).__init__(**kwargs)
        if not hasattr(self, 'use_valid_labels'):
            self.use_valid_labels = False

    def load_train(self, data_dir):
        super(PseudolabelingZmuvRescaledDataLoader, self).load_train(data_dir)
        self.images_test = data.load(data_dir, 'test')

        if self.use_valid_labels:
            valid_pseudo_labels = self.labels_valid
        else:
            valid_pseudo_labels = np.load(self.valid_pred_file).astype(np.float32)
        test_pseudo_labels = np.load(self.test_pred_file).astype(np.float32)

        prng = np.random.RandomState(9328749)
        pseudo_shuffle = np.arange(len(self.images_test) + len(self.images_valid))
        prng.shuffle(pseudo_shuffle)

        self.images_pseudo = np.concatenate((self.images_valid, self.images_test), 0)[pseudo_shuffle]
        self.labels_pseudo = np.concatenate((valid_pseudo_labels, test_pseudo_labels), 0)[pseudo_shuffle]

    def create_random_gen(self, *args):
        # we ignore the args
        train_chunk_size = int(round(self.chunk_size * self.train_sample_weight))
        pseudo_chunk_size = self.chunk_size - train_chunk_size

        train_gen = data.rescaled_patches_gen_augmented(self.images_train, self.labels_train, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=train_chunk_size, num_chunks=self.num_chunks_train, augmentation_params=self.augmentation_params, color_space=self.color_space)

        pseudo_gen = data.rescaled_patches_gen_augmented(self.images_pseudo, self.labels_pseudo, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=pseudo_chunk_size, num_chunks=self.num_chunks_train, augmentation_params=self.augmentation_params, color_space=self.color_space)

        def random_gen():
            for a, b in itertools.izip(train_gen, pseudo_gen):
                (chunk_x1, chunk_y1, chunk_shape), (chunk_x2, chunk_y2, _) = a, b

                chunk_y = np.concatenate((chunk_y1, chunk_y2), 0)
                chunk_x = np.concatenate((chunk_x1, chunk_x2), 0)
                chunk_x -= self.zmuv_mean
                chunk_x /= self.zmuv_std
                yield [chunk_x], chunk_y

        return buffering.buffered_gen_threaded(random_gen())


class LazyImageLoader:
    def __init__(self, filenames, cache_dir, image_yx):
        self.filenames = filenames
        # Assume the filenames aren't unique (possibly due to upsampling), the in-memory cache will use the filename as the key
        self.loaded = {}
        self.cache_dir = cache_dir
        self.image_yx = image_yx


    def __len__(self):
        return len(self.filenames)


    def __getitem__(self, index):
        if isinstance(index, tuple):
            raise ValueError("Can't handle tuple indexing yet. Implement it.")
        elif isinstance(index, slice):
            raise ValueError("Can't handle slice indexing yet. Implement it.")
        elif isinstance(index, np.ndarray):
            return np.array([self.__getitem__(subitem) for subitem in index])
        else:
            filename = self.filenames[index]
            if filename not in self.loaded:
                self.loaded[filename] = self.load_image(filename)

            return self.loaded[filename]


    def load_image(self, filename):
        cache_filename = os.path.join(self.cache_dir, os.path.splitext(os.path.basename(filename))[0] + '.npy')
        if not os.path.isfile(cache_filename):
            print "Didn't find file %s, creating it" % (cache_filename,)
            return self.create_cache_of_file(filename, cache_filename)

        return np.load(cache_filename, mmap_mode='r')


    def create_cache_of_file(self, filename, cache_filename):
        return data.load_trim_resize_and_save_as_npy(filename, self.image_yx, cache_filename)[1]


class CachingDataLoader:
    params = ['zmuv_mean', 'zmuv_std'] # params that need to be stored after training and loaded at test time.

    def __init__(self, **kwargs):
        self.augmentation_transforms_test = [data.tform_identity] # default to no test-time augmentation
        self.color_space = "rgb"  # default
        self.image_yx = (362, 362)
        self.random_state = 84088 # just some random number, used to randomly sample the cross-validation set.
        self.test_size = 0.1 # % of the training data to use for validation
        self.cache_dir = 'unsup-data-%dx%d' % (self.image_yx[0], self.image_yx[1])
        self.upsample = True
        self.num_classes = 5
        self.predict_mode = "ordinal_regression"

        self.__dict__.update(kwargs)

        if self.predict_mode == "ordinal_regression":
            self.labels_are_categories = False
            self.labels_are_ordinal_regression = True
            self.labels_are_regression = False
        elif self.predict_mode == 'regression':
            self.labels_are_categories = False
            self.labels_are_ordinal_regression = False
            self.labels_are_regression = True
        elif self.predict_mode == 'classification':
            self.labels_are_categories = True
            self.labels_are_ordinal_regression = False
            self.labels_are_regression = False

        os.system('mkdir -p "%s"' % (self.cache_dir,))

    def estimate_params(self):
        pass

    def make_images_and_labels(self, patient_ids, train_and_val_labels):
        def make_left_right(ids):
            return [ s + '_left' for s in ids] + [s + '_right' for s in ids]

        # map the split back to images
        train_images = make_left_right(patient_ids)
        train_images_and_labels = train_and_val_labels.loc[train_images]
        train_filenames = ['data/train/%s.jpeg' % (s,) for s in train_images_and_labels['image']]
        images = LazyImageLoader(train_filenames, self.cache_dir, self.image_yx)
        original_labels = train_images_and_labels['level']
        if self.labels_are_ordinal_regression:
            labels = utils.ordinal_targets(original_labels).astype(np.float32)
        elif self.labels_are_categories:
            labels = utils.one_hot(original_labels, self.num_classes).astype(np.float32)
        else:
            labels = original_labels.astype(np.float32).reshape((len(original_labels), 1))

        return images, labels, original_labels


    def load_train(self, data_dir):
        # read trainLabels.csv
        train_and_val_labels = pd.read_csv('data/trainLabels.csv')
        train_and_val_labels.index = train_and_val_labels['image']
        # extract the patient ids - should be 1/2 the #
        # the values are all x_left, followed by x_right, so just use every other row and get the number out of the
        # image column.
        even_rows = train_and_val_labels[::2]
        image_col = even_rows['image']
        self.full_train_patient_ids = [s.split('_')[0] for s in image_col]

        self.images_full_train, self.labels_full_train, self.original_labels_full_train = \
            self.make_images_and_labels(self.full_train_patient_ids, train_and_val_labels)

        # create a validation split on the patient ids
        self.train_patient_ids, self.val_patient_ids = sklearn.cross_validation.train_test_split(self.full_train_patient_ids,
                                                                                       test_size=self.test_size,
                                                                                       random_state=self.random_state)

        self.images_train, self.labels_train, self.original_labels_train = self.make_images_and_labels(self.train_patient_ids, train_and_val_labels)
        self.images_valid, self.labels_valid, self.original_labels_valid = self.make_images_and_labels(self.val_patient_ids, train_and_val_labels)

        self.label_indices_train = np.array([np.nonzero(self.original_labels_train == label)[0] for label in xrange(self.num_classes)])


    def load_test(self, data_dir):
        test_filenames = glob.glob('data/test/*.jpeg')
        test_filenames.sort()
        self.images_test = LazyImageLoader(test_filenames, self.cache_dir, self.image_yx)

    def get_params(self):
        return { pname: getattr(self, pname, None) for pname in self.params }

    def set_params(self, p):
        self.__dict__.update(p)

    def estimate_params(self):
        self.estimate_zmuv_params() # zero mean unit variance

    def estimate_zmuv_params(self):
        gen = data.rescaled_patches_gen_augmented(self.images_train, self.labels_train, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=max([4000, self.chunk_size]), num_chunks=1, augmentation_params=self.augmentation_params, color_space=self.color_space)
        chunk_x, _, _ = gen.next()
        self.zmuv_mean = chunk_x.mean()
        self.zmuv_std = chunk_x.std()

    def create_random_gen(self, images, labels):
        # create a generator for each class. Scale the chunk size so it's roughly 1/N. We'll handle extra by truncating
        # in the generator that combines the classes
        if self.upsample:
            gens = [data.rescaled_patches_gen_augmented(self.images_train[self.label_indices_train[label]],
                                                        self.labels_train[self.label_indices_train[label]],
                                                        self.estimate_scale,
                                                        patch_shape=self.patch_size,
                                                        chunk_size=self.chunk_size // self.num_classes + 1,
                                                        num_chunks=self.num_chunks_train,
                                                        augmentation_params=self.augmentation_params,
                                                        color_space=self.color_space)
                    for label in xrange(self.num_classes)]
        else:
            gens = [data.rescaled_patches_gen_augmented(self.images_train,
                                                        self.labels_train,
                                                        self.estimate_scale,
                                                        patch_shape=self.patch_size,
                                                        chunk_size=self.chunk_size // self.num_classes + 1,
                                                        num_chunks=self.num_chunks_train,
                                                        augmentation_params=self.augmentation_params,
                                                        color_space=self.color_space)]

        def random_gen():
            for x in itertools.izip(*gens):
                # x is a tuple of dim num_classes, and each element has a tuple with (chunk_x, chunk_y, chunk_shape)
                # create chunk_x by combining the first element of each x and chunk_y by combining the second element
                chunk_x = np.vstack(tuple(gen_out[0] for gen_out in x))
                chunk_y = np.vstack(tuple(gen_out[1] for gen_out in x))
                # shuffle them so that the classes are mixed together, this also limits the chunk size to self.chunk_size
                indices = np.random.permutation(len(chunk_x))[:self.chunk_size]
                chunk_x = chunk_x[indices]
                chunk_y = chunk_y[indices]

                chunk_x -= self.zmuv_mean
                chunk_x /= self.zmuv_std
                yield [chunk_x], chunk_y

        return buffering.buffered_gen_threaded(random_gen())

    def create_fixed_gen(self, images, augment=False):
        augmentation_transforms = self.augmentation_transforms_test if augment else None
        gen = data.rescaled_patches_gen_fixed(images, self.estimate_scale, patch_shape=self.patch_size,
            chunk_size=self.chunk_size, augmentation_transforms=augmentation_transforms, color_space=self.color_space)

        def fixed_gen():
            for chunk_x, chunk_shape, chunk_length in gen:
                chunk_x -= self.zmuv_mean
                chunk_x /= self.zmuv_std
                yield [chunk_x], chunk_length

        return buffering.buffered_gen_threaded(fixed_gen())

