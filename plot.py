import sys
import bokeh
from bokeh.embed import file_html

from bokeh.plotting import *
from bokeh.models import *
from bokeh.resources import INLINE
import numpy as np
import jinja2
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import HtmlFormatter

def template_with_code(filename, code):
    return jinja2.Template("""
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{{ title if title else "Bokeh Plot" }}</title>
        {{ plot_resources|indent(8) }}

        {{ plot_script|indent(8) }}
        <style>
            %s
        </style
    </head>
    <body>
        {{ plot_div|indent(8) }}
        <h2>%s</h2>
        %s
    </body>
</html>
""" % (HtmlFormatter().get_style_defs('.highlight'), filename, highlight(code, PythonLexer(), HtmlFormatter())))


def save_training_plot(save_path, losses_train, learning_rates, losses_eval_valid, losses_eval_train, kappas_eval_valid,
                       kappas_eval_train, valid_chunk_idxs, experiment_id, plot_kappa=True, plot_accuracy=False,
                       accuracies_eval_train=None, accuracies_eval_valid=None, configuration=None, **kwargs):
    chunk_idxs = range(1, len(losses_train) + 1)
    # filter out the ones that haven't been reached yet.
    valid_chunk_idxs = [x for x in valid_chunk_idxs if x <= len(losses_train) + 1]

    tools = "pan,box_zoom,resize,wheel_zoom,save,hover,crosshair,reset"

    if len(losses_train) > 0 and len(losses_eval_valid) > 0 and len(losses_eval_train) > 0:
        y_range = Range1d(0., max(
            [float(np.max(losses_train)), float(np.max(losses_eval_valid)), float(np.max(losses_eval_train))]) * 1.05)
    elif len(losses_train) > 0:
        y_range = Range1d(0., float(np.max(losses_train)))
    else:
        y_range = None

    loss_figure = figure(title='Loss ' + experiment_id, plot_width=1500, plot_height=300, y_axis_location='right',
                         y_range=y_range, tools=tools)
    loss_figure.line(x=chunk_idxs, y=losses_train, color='#1f77b4', legend='chunk mean train')
    # loss_figure.circle(x=chunk_idxs, y=losses_train, color='#1f77b4', size=3)
    loss_figure.line(x=valid_chunk_idxs, y=losses_eval_valid, color='#ff7f0e', legend='valid')
    loss_figure.cross(x=valid_chunk_idxs, y=losses_eval_valid, color='#ff7f0e', size=5)
    loss_figure.line(x=valid_chunk_idxs, y=losses_eval_train, color='#2ca02c', legend='train')
    loss_figure.cross(x=valid_chunk_idxs, y=losses_eval_train, color='#2ca02c', size=5)
    loss_figure.legend.orientation = 'top_right'
    loss_figure.select(dict(type=HoverTool)).tooltips = [('chunk', '@x'), ('loss', '@y')]

    if plot_kappa:
        kappa_figure = figure(title='Kappa', x_range=loss_figure.x_range, plot_width=1500, plot_height=300,
                              y_axis_location='right', y_range=[-.1, 1], tools=tools)
        kappa_figure.line(x=valid_chunk_idxs, y=kappas_eval_train, color='#2ca02c', legend='train')
        kappa_figure.cross(x=valid_chunk_idxs, y=kappas_eval_train, color='#2ca02c', size=5)
        kappa_figure.line(x=valid_chunk_idxs, y=kappas_eval_valid, color='#ff7f0e', legend='valid')
        kappa_figure.cross(x=valid_chunk_idxs, y=kappas_eval_valid, color='#ff7f0e', size=5)
        kappa_figure.legend.orientation = 'top_left'
        kappa_figure.select(dict(type=HoverTool)).tooltips = [('chunk', '@x'), ('kappa', '@y')]
        if len(kappas_eval_valid) > 0:
            which_is_max = np.argmax(kappas_eval_valid)
            maxval = kappas_eval_valid[which_is_max]
            if which_is_max < len(valid_chunk_idxs):
              kappa_figure.title = 'Kappa max: %.6f @ %d' % (maxval, valid_chunk_idxs[which_is_max])

    if plot_accuracy:
        accuracy_figure = figure(title='accuracy', x_range=loss_figure.x_range, plot_width=1500, plot_height=300,
                              y_axis_location='right', y_range=[-.1, 1], tools=tools)
        accuracy_figure.line(x=valid_chunk_idxs, y=accuracies_eval_train, color='#2ca02c', legend='train')
        accuracy_figure.cross(x=valid_chunk_idxs, y=accuracies_eval_train, color='#2ca02c', size=5)
        accuracy_figure.line(x=valid_chunk_idxs, y=accuracies_eval_valid, color='#ff7f0e', legend='valid')
        accuracy_figure.cross(x=valid_chunk_idxs, y=accuracies_eval_valid, color='#ff7f0e', size=5)
        accuracy_figure.legend.orientation = 'top_left'
        accuracy_figure.select(dict(type=HoverTool)).tooltips = [('chunk', '@x'), ('accuracy', '@y')]
        if len(accuracies_eval_valid) > 0:
            which_is_max = np.argmax(accuracies_eval_valid)
            maxval = accuracies_eval_valid[which_is_max]
            if which_is_max < len(valid_chunk_idxs):
              accuracy_figure.title = 'accuracy max: %.6f @ %d' % (maxval, valid_chunk_idxs[which_is_max])

    learning_figure = figure(title='Learning Rate', x_range=loss_figure.x_range, plot_width=1500, plot_height=300,
                             y_axis_location='right', tools=tools) # , y_axis_type='log' - seems to be a bug where the line doesn't draw in some situations
    learning_figure.line(x=range(1, len(learning_rates) + 1), y=learning_rates)
    learning_figure.circle(x=range(1, len(learning_rates) + 1), y=learning_rates, size=3)
    learning_figure.select(dict(type=HoverTool)).tooltips = [('chunk', '@x'), ('rate', '@y')]

    plots_grid = [[loss_figure]]
    if plot_kappa:
        plots_grid.append([kappa_figure])
        
    if plot_accuracy:
        plots_grid.append([accuracy_figure])
        
    plots_grid.append([learning_figure])    
    p = gridplot(plots_grid)

    if configuration is not None:
        configuration_filename = 'nets/%s.py' % (configuration,)
        try:
            code = open(configuration_filename).read()
        except:
            code = "failed to open %s" % (configuration_filename,)
    else:
        code = ""
        configuration_filename = ""

    plot_html = file_html(p, INLINE, title=experiment_id, template=template_with_code(configuration_filename, code))
    with open(save_path, 'w') as f:
        f.write(plot_html)

    return p


if __name__ == '__main__':
    metadata_path = sys.argv[1]
    metadata = np.load(metadata_path)
    p = save_training_plot('training_plot.html', **metadata)
    import os
    os.system('xdg-open %s' % ('training_plot.html',))
