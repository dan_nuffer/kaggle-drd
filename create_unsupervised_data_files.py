import glob
import multiprocessing
import sys
from multiprocessing import Pool
import time
import gc

import warnings
import click
import os
import numpy as np
import os.path
from clint.textui import progress

from data import image_name, load_trim_resize_and_save_as_npy

warnings.filterwarnings('ignore', 'Possible precision loss when converting from float64 to uint8')


def load_trim_resize_and_save_as_npy_for_pool_map(args):
    return load_trim_resize_and_save_as_npy(*args)[0]


@click.command()
@click.option('--image-count', default=-1, help='Number of images. Defaults to all.')
@click.option('--num-classes', default=17000, help='Number of classes.')
@click.option('--image-edge-pixels', default=256)
@click.option('--output-dir', default="")
@click.option('--workers', default=0, help='number of worker threads. Defaults to #cores/2 on python3')
def main(image_count, num_classes, image_edge_pixels, output_dir, workers):
    size = image_edge_pixels
    output_shape = (size, size)

    if len(output_dir) > 0:
        save_dir = output_dir
    else:
        save_dir = "unsup-data-%dx%d" % (size, size)

    os.system('mkdir -p %s' % (save_dir,))

    # do the training images

    paths_train = glob.glob('data/train/*.jpeg')
    paths_test = glob.glob("data/test/*.jpeg")
    paths = paths_train + paths_test
    paths.sort()
    if image_count > 0:
        paths = paths[:image_count]

    if workers > 0:
        num_workers = workers
    else:
        num_workers = int(multiprocessing.cpu_count() / 2) # half because of hyperthreading and/or other work

    print("Trimming and resizing %d images using %d worker threads" % (len(paths), num_workers))
    pool = Pool(processes=num_workers, maxtasksperchild=100)  # really threads because imported multiprocessing.dummy.Pool

    t1 = time.time()
    image_names = []
    processed_image_paths = ["%s/%s.npy" % (save_dir, image_name(path)) for path in paths]
    for name in progress.bar(pool.imap_unordered(load_trim_resize_and_save_as_npy_for_pool_map,
                                      zip(paths, [output_shape] * len(paths),
                                          processed_image_paths)), label='trimming and resizing images ', expected_size=len(paths)):
        image_names.append(name)

    pool.close()
    pool.join()
    del pool

    t2 = time.time()
    time_elapsed = t2 - t1
    print("Time to trim and resize %d images: %g seconds" % (len(paths), time_elapsed))
    print("%f images/sec" % (len(paths) / time_elapsed))
    print("%f seconds/image" % (time_elapsed / len(paths)))

    # load all the processed images
    images = np.empty(len(paths), dtype=np.object)
    for idx, processed_image_path in progress.bar(enumerate(processed_image_paths), label='loading ', expected_size=len(processed_image_paths)):
        images[idx] = np.load(processed_image_path)

    t3 = time.time()
    time_elapsed = t3 - t2
    print("Time to load %d images: %g seconds" % (len(paths), time_elapsed))
    print("%f images/sec" % (len(paths) / time_elapsed))
    print("%f seconds/image" % (time_elapsed / len(paths)))

    # for unsupervised training, each image has a unique label, so ordering doesn't matter, as long as each image
    # has a unique label. If num_classes < len(paths), we'll just cycle and re-use labels. Too many labels causes memory problems because
    # the training code is written assuming a small # (100s) of classes and is O(n^2) with n being a class. The default of num_classes
    # is about a high as my machine can deal with.
    labels = np.mod(np.arange(len(paths)), num_classes)

    print("Saving %d train labels to %s/labels.npy" % (len(labels), save_dir))
    np.save("%s/labels.npy" % (save_dir,), labels)

    save_path = "%s/images.npy" % (save_dir,)
    print("Saving %d train images to %s" % (len(images), save_path))
    gc.collect() # np.save() makes copies and uses lots of ram. Make sure there's as much free as possible
    np.save(save_path, images)

    print("Done")

if __name__ == '__main__':
    sys.exit(main())
