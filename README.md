# kaggle-drd
Solution to the Kaggle Diabetic Retinopathy competiton on Kaggle. The best submission placed 13th.

The code is based on the first place solution for the National Data Science Bowl competition on Kaggle (plankton classification) http://benanne.github.io/2015/03/17/plankton.html.
