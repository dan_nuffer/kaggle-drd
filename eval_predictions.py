import sys
import numpy as np
import utils
import skll
import glob


if len(sys.argv) != 2:
    #sys.exit("Usage: eval_predictions.py <validation_predictions_path>")
    prediction_files = glob.glob('predictions/valid--*.npy') + glob.glob('predictions/valid_noaug--*.npy')
else:
    prediction_files = [sys.argv[1]]


cat_labels = np.load("data/labels_val.npy")
labels = utils.ordinal_targets(cat_labels)

def predict_file(path):
    outputs = np.load(path)
    loss = np.mean(utils.sum_binary_crossentropy(outputs, labels))
    cat_outputs = utils.predict_ordinal(outputs, thresh=0.5)
    acc = utils.accuracy(cat_outputs, cat_labels)
    kappa = skll.kappa(cat_labels, cat_outputs, 'quadratic')
    return {
        'loss' : loss,
        'acc' : acc,
        'kappa' : kappa,
        'path' : path
    }

predictions = [predict_file(path) for path in prediction_files]

predictions.sort(key=lambda p: p['kappa'])
predictions.sort(key=lambda p: -p['loss'])

for prediction in predictions:
    print prediction['path']
    print "loss:\t\t%.5f" % (prediction['loss'],)
    #print "Accuracy:\t%.2f%%" % (prediction['acc'] * 100,)
    print "Kappa:\t\t%.5f" % (prediction['kappa'],)
