#! /usr/bin/python
import copy
import glob
import click
from hpsklearn.estimator import NonFiniteFeature, min_n_iters, tipping_pt_ratio, best_loss_cutoff_n_iters, \
    timeout_buffer
import os
import scipy
import sys

import numpy as np
from clint.textui import puts
from sklearn import cross_validation
import skll
import hyperopt.tpe
import hpsklearn
import hpsklearn.demo_support
from hyperopt import hp, partial
from multiprocessing import Process, Pipe
import time
import pandas as pd

import load
import skhyperopt
import utils
from sklearn import preprocessing
from sklearn import linear_model




"""
Plan for creating a dataset with the ordinal values for one eye together with the values for the opposite eye:
 Desired output: first 5 columns of the eye to predict, second 5 columns of the other eye.
 1. concatenate train, val, test predictions and name
 2. make an "other eye" index by using the name
 4. Append other eye columns
 5. split train, val, test
"""


@click.command()
@click.option('--train', 'train_predictions_path', type=click.STRING)
@click.option('--valid', 'valid_predictions_path', type=click.STRING)
@click.option('--test', 'test_predictions_path', type=click.STRING)
@click.option('--max-hyperopt-evals', default=100, type=click.INT)
@click.option('--trial-timeout', help='seconds to wait for a trial to finish', default=60 * 10, type=click.INT)
@click.option('--random-seed', default=123, type=click.INT)
@click.option('--output-train/--no-output-train', default=False)
@click.option('--train-best/--no-train-best', default=False)
def main(train_predictions_path, valid_predictions_path, test_predictions_path, max_hyperopt_evals, trial_timeout, random_seed, output_train, train_best):
    data_loader = load.DataLoader(predict_mode="ordinal_regression")

    train_X = np.load(train_predictions_path)
    train_Y = utils.predict_ordinal(data_loader.labels_train, thresh=0.5)
    train_labels_train_us = pd.read_csv('data/trainLabels_train_US.csv')
    train_labels_val = pd.read_csv('data/trainLabels_val.csv')
    test_images = pd.read_csv('data/test_images.csv')

    valid_X = np.load(valid_predictions_path)
    valid_Y = utils.predict_ordinal(data_loader.labels_valid, thresh=0.5)

    test_X = np.load(test_predictions_path)

    data_X = np.vstack((train_X, valid_X, test_X))
    assert(data_X.shape[0] == (train_labels_train_us.shape[0] + train_labels_val.shape[0] + test_images.shape[0]))
    image_names = train_labels_train_us['image'].tolist() + train_labels_val['image'].tolist() + test_images['image'].tolist()

    image_idxs = {}
    for row_idx, image_name in enumerate(image_names):
        image_idxs[image_name] = row_idx

    def make_other_image(name):
        if name.endswith('left'):
            return name.split('_')[0] + '_right'
        else:
            return name.split('_')[0] + '_left'

    other_eye_idxs = [image_idxs[make_other_image(image_name)] for image_name in image_names]
    data_X = np.hstack((data_X, data_X[other_eye_idxs]))

    both_train_X = data_X[0:len(train_X)]
    both_valid_X = data_X[len(train_X):len(train_X)+len(valid_X)]
    both_test_X = data_X[len(train_X)+len(valid_X):len(train_X)+len(valid_X)+len(test_X)]

    if train_best:
      estimator = skhyperopt.cv_estimator()
      estimator._best_preprocs=[preprocessing.MinMaxScaler(copy=True, feature_range=(-1.0, 1.0))]
      estimator._best_classif=linear_model.SGDClassifier(alpha=2.30623115012e-06, class_weight=None, epsilon=0.1,
              eta0=0.00243361228807, fit_intercept=True, l1_ratio=0.475857430151,
              learning_rate='invscaling', loss='hinge', n_iter=5, n_jobs=1,
              penalty='l1', power_t=0.73921893387, random_state=None,
              shuffle=False, verbose=False, warm_start=False)
      estimator._best_iters = 5
      estimator.retrain_best_model_on_full_data(both_train_X, train_Y)
    else:
      estimator = skhyperopt.cv_estimator(
          preprocessing=skhyperopt.preprocessing('pp'),
          classifier=hpsklearn.components.any_classifier('clf'),
          algo=hyperopt.tpe.suggest,
          trial_timeout=trial_timeout, # seconds
          max_evals=max_hyperopt_evals,
          verbose=1,
          seed=random_seed,
          )

      fit_iterator = estimator.fit_iter_val(both_train_X, train_Y, both_valid_X, valid_Y)
      fit_iterator.next()
      while len(estimator.trials.trials) < estimator.max_evals:
          fit_iterator.send(1) # -- try one more model
          last_loss = estimator.trials.losses()[-1]
          print "Iteration %d loss: %s, best: %f" % (len(estimator.trials.trials), str(last_loss), estimator._best_loss)
          try:
              print "best model: %s" % (estimator.best_model(),)
          except AttributeError:
              print "no best model yet"

      estimator.retrain_best_model_on_full_data(both_train_X, train_Y)

    print 'Best preprocessing pipeline:'
    best_model = estimator.best_model()
    for pp in best_model['preprocs']:
        print pp
    print
    print 'Best classifier:\n', best_model['classifier']

    def class_error(predicted, expected):
        return sum( int(predicted[i]) != expected[i] for i in range(len(expected))) / float(len(expected))

    def print_prediction_stats(data_set, cat_outputs, cat_labels):
        puts()
        puts("Results for %s" % (data_set,))
        puts("error: %f" % (class_error(cat_outputs, cat_labels)))
        puts("kappa: %f" % (skll.kappa(cat_outputs, cat_labels, 'quadratic')))
        puts("Confusion Matrix:")
        puts(str(utils.conf_matrix(cat_outputs, cat_labels, 5)))

    print_prediction_stats("train model", estimator.predict(both_train_X), train_Y)
    print_prediction_stats("train thresh", utils.predict_ordinal(train_X, thresh=0.5), train_Y)
    print_prediction_stats("valid model", estimator.predict(both_valid_X), valid_Y)
    print_prediction_stats("valid thresh", utils.predict_ordinal(valid_X, thresh=0.5), valid_Y)


    # Can't do this without upsampling the training+val data or coming up with a way to weight the samples. The validation
    # set is unbalanced and so causes the retrained classifier to malfunction.
    # use the best parameters, retrain on the whole training set and predict the actual test set.
    #puts("Retrained on full training data")
    #estimator.retrain_best_model_on_full_data(np.vstack((both_train_X, both_valid_X)), np.concatenate((train_Y, valid_Y)))
    #print_prediction_stats("full model", estimator.predict(np.vstack((both_train_X, both_valid_X))), np.concatenate((train_Y, valid_Y)))

    print "Generate CSV"

    test_predictions = estimator.predict(both_test_X)

    df = pd.DataFrame(test_predictions, columns=["level"], index=test_images["image"])
    df.index.name = 'image'
    filename = os.path.splitext(os.path.basename(test_predictions_path))[0] + '-skhpo_2eye-' + utils.timestamp()
    target_path = "submissions/%s.csv" % filename
    df.to_csv(target_path)

    print "Compress with gzip"
    os.system("gzip %s" % target_path)

    print "  stored in %s.gz" % target_path

    if output_train:
      train_predictions = estimator.predict(both_train_X)
      valid_predictions = estimator.predict(both_valid_X)
      df = pd.concat([pd.DataFrame(train_predictions, columns=["level"], index=train_labels_train_us["image"]),
                      pd.DataFrame(valid_predictions, columns=['level'], index=train_labels_val['image'])])
      df.index.name = 'image'
      df['index'] = df.index
      df.drop_duplicates(subset='index', take_last=True, inplace=True)
      del df['index']
      df = df.sort()

      filename = os.path.splitext(os.path.basename(test_predictions_path))[0] + '-skhpo_2eye-' + utils.timestamp() + '--train'
      target_path = "submissions/%s.csv" % filename
      df.to_csv(target_path)
      os.system("gzip %s" % target_path)
      print "  stored in %s.gz" % target_path

    return 0

if __name__ == '__main__':
    sys.exit(main())


"""
This is the best model found with 1000 iterations with the following command line:
OMP_NUM_THREADS=1 python skhyperopt_predict.py predictions/train_noaug--cnn_batchnormdense_3channel_255patch_ordinal_l2--cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best--avg-probs.npy predictions/valid_noaug--cnn_batchnormdense_3channel_255patch_ordinal_l2--cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best--avg-probs.npy
Which is the best single prediction, as of 7/15. The output improved the prediction on valid by ~.017 over using 0.5 threshold
The best was found on iteration 75. So in hind-sight running 1000 iterations was overkill. Next time I'll run 100 or so, and 50 if
in a hurry.

{'classifier': SGDClassifier(alpha=0.00432923706076, class_weight=None, epsilon=0.1,
       eta0=0.000559970368666, fit_intercept=True, l1_ratio=0.432812887847,
       learning_rate='invscaling', loss='log', n_iter=5, n_jobs=1,
       penalty='l1', power_t=0.918350690955, random_state=None,
       shuffle=False, verbose=False, warm_start=False), 'preprocs': (StandardScaler(copy=True, with_mean=True, with_std=True),)}

Results for train model
error: 0.198749
kappa: 0.932367
Confusion Matrix:
[[16615  1237   197     8    10]
 [ 9591  7930   546     0     0]
 [ 2737  2968 11834   509    19]
 [    0     0    82 17935    50]
 [    0     0     0     0 18067]]

Results for train thresh
error: 0.186152
kappa: 0.939111
Confusion Matrix:
[[15875  1888   290     8     6]
 [ 8063  9582   422     0     0]
 [ 1955  3565 12333   211     3]
 [    0     0   405 17662     0]
 [    0     0     0     0 18067]]
Transforming X of shape (10535, 5)
Predicting X of shape (10535, 5)

Results for valid model
error: 0.224680
kappa: 0.755576
Confusion Matrix:
[[6929  631  153   14   16]
 [ 498  172   60    1    1]
 [ 331  295  792  133   36]
 [  10   10   74  143   24]
 [   8    5   44   23  132]]

Results for valid thresh
error: 0.251258
kappa: 0.738451
Confusion Matrix:
[[6566  937  220    9   11]
 [ 438  230   64    0    0]
 [ 271  345  857   90   24]
 [   7   14  110  114   16]
 [   9    5   56   21  121]]

"""



"""
Plan for creating a dataset with the ordinal values for one eye together with the values for the opposite eye:
 Desired output: first 5 columns of the eye to predict, second 5 columns of the other eye.
 1. concatenate train, val, test predictions and name
 2. make an "other eye" index by using the name
 4. Append other eye columns
 5. split train, val, test
"""

"""
Best with two eye with 100 iters:
MinMaxScaler(copy=True, feature_range=(-1.0, 1.0))

Best classifier:
SGDClassifier(alpha=5.56789027046e-06, class_weight=None, epsilon=0.1,
       eta0=2.92606360107e-05, fit_intercept=False,
       l1_ratio=0.249580783843, learning_rate='invscaling', loss='huber',
       n_iter=5, n_jobs=1, penalty='l1', power_t=0.612065142872,
       random_state=None, shuffle=False, verbose=False, warm_start=False)
Got 0.797 kappa

best: 0.203225
best model: {'classifier': SGDClassifier(alpha=2.30623115012e-06, class_weight=None, epsilon=0.1,
       eta0=0.00243361228807, fit_intercept=True, l1_ratio=0.475857430151,
       learning_rate='invscaling', loss='hinge', n_iter=5, n_jobs=1,
       penalty='l1', power_t=0.73921893387, random_state=None,
       shuffle=False, verbose=False, warm_start=False), 'preprocs': (MinMaxScaler(copy=True, feature_range=(-1.0, 1.0)),)}

"""

"""
300 iters:

best model: {'classifier': SGDClassifier(alpha=2.30623115012e-06, class_weight=None, epsilon=0.1,
       eta0=0.00243361228807, fit_intercept=True, l1_ratio=0.475857430151,
       learning_rate='invscaling', loss='hinge', n_iter=5, n_jobs=1,
       penalty='l1', power_t=0.73921893387, random_state=None,
       shuffle=False, verbose=False, warm_start=False), 'preprocs': (MinMaxScaler(copy=True, feature_range=(-1.0, 1.0)),)}
Best preprocessing pipeline:
MinMaxScaler(copy=True, feature_range=(-1.0, 1.0))

Best classifier:
SGDClassifier(alpha=2.30623115012e-06, class_weight=None, epsilon=0.1,
       eta0=0.00243361228807, fit_intercept=True, l1_ratio=0.475857430151,
       learning_rate='invscaling', loss='hinge', n_iter=5, n_jobs=1,
       penalty='l1', power_t=0.73921893387, random_state=None,
       shuffle=False, verbose=False, warm_start=False)
"""
