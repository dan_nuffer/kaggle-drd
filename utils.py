import time
import platform
import numpy as np 
import gzip
import cPickle as pickle


def hms(seconds):
    seconds = np.floor(seconds)
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)

    return "%02d:%02d:%02d" % (hours, minutes, seconds)


def timestamp():
    return time.strftime("%Y%m%d-%H%M%S", time.localtime())


def hostname():
    return platform.node()


def generate_expid(arch_name):
    return "%s-%s-%s" % (arch_name, hostname(), timestamp())

def one_hot(vec, num_classes=None):
    vec = np.array(vec, dtype=np.int32)
    if num_classes is None:
        num_classes = int(np.max(vec)) + 1

    ret = np.zeros((vec.shape[0], num_classes), dtype=np.float32)
    ret[np.arange(vec.shape[0]), vec] = 1
    return ret


def ordinal_targets(vec, m=None):
    vec = np.array(vec, dtype=np.int32)
    if m is None:
        m = int(np.max(vec)) + 1

    t = one_hot(vec, m)
    for i in xrange(1, m):
        t += one_hot(np.clip(vec - i, 0, m), m)

    return np.clip(t, 0., 1.)


def log_losses(y, t, eps=1e-7):
    if t.ndim == 1:
        # make y into a vector whose elements contain the targets
        y = y[np.arange(y.shape[0]), t.astype(np.int32)]
        y = np.clip(y, eps, 1 - eps)
        return -np.log(y)
    else:
        y = np.clip(y, eps, 1 - eps)
        return -np.sum(t * np.log(y), axis=1)


def log_loss(y, targets, eps=1e-7):
    """
    cross entropy loss, summed over classes, mean over batches
    targets can be a 2d (e.g. one-hot) or 1d (category)
    """
    losses = log_losses(y, targets, eps)
    return np.mean(losses)


def sum_binary_crossentropy(output, target, eps=1e-7):
    """
    Compute the sum of the crossentropy of binary random variables
    output and target are each expectations of binary random
    variables; target may be exactly 0 or 1 but output must
    lie strictly between 0 and 1.
    """
    output = np.clip(output, eps, 1 - eps)
    return -np.sum(target * np.log(output) + (1.0 - target) * np.log(1.0 - output), axis=1)


def predict_ordinal(output, thresh):
    # create a boolean array for numbers over the threshold
    x = output >= thresh
    # forward multiply across the columns, so that earlier falses propagate
    for col in xrange(1, x.shape[1]):
        x[:, col] *= x[:, col - 1]
    return np.clip(x.sum(axis=1) - 1, 0, x.shape[1])


def predict_regression_level(outputs, max_val):
    return np.round(np.clip(outputs, 0, max_val))

def accuracy(y, t):
    if t.ndim == 2:
        t = np.argmax(t, axis=1)

    if y.ndim == 2:
        y = np.argmax(y, axis=1)

    return np.mean(y == t)


def softmax(x): 
    m = np.max(x, axis=1, keepdims=True)
    e = np.exp(x - m)
    return e / np.sum(e, axis=1, keepdims=True)


def entropy(x):
    h = -x * np.log(x)
    h[np.invert(np.isfinite(h))] = 0
    return h.sum(1)


def conf_matrix(p, t, num_classes):
    if p.ndim == 1:
        p = one_hot(p, num_classes)
    if t.ndim == 1:
        t = one_hot(t, num_classes)
    return np.dot(p.T, t).T.astype(np.int32)


def accuracy_topn(y, t, n=5):
    if t.ndim == 2:
        t = np.argmax(t, axis=1)
    
    predictions = np.argsort(y, axis=1)[:, -n:]    
    
    accs = np.any(predictions == t[:, None], axis=1)

    return np.mean(accs)


def current_learning_rate(schedule, idx):
    s = schedule.keys()
    s.sort()
    current_lr = schedule[0]
    for i in s:
        if idx >= i:
            current_lr = schedule[i]

    return current_lr


def load_gz(path): # load a .npy.gz file
    if path.endswith(".gz"):
        f = gzip.open(path, 'rb')
        return np.load(f)
    else:
        return np.load(path)


def log_loss_std(y, t, eps=1e-7):
    """
    cross entropy loss, summed over classes, mean over batches
    """
    losses = log_losses(y, t, eps)
    return np.std(losses)


def polynomial_decay(num_chunks_train, initial_learning_rate, final_learning_rate):
    return dict(((x,
                  (final_learning_rate * initial_learning_rate * (
                  num_chunks_train) - final_learning_rate * initial_learning_rate) /
                  ((initial_learning_rate - final_learning_rate) * (x + 1.) + final_learning_rate * (
                  num_chunks_train) - initial_learning_rate))
                 for x in range(num_chunks_train)))


def exponential_decay(num_chunks_train, initial_learning_rate, final_learning_rate):
    shrink_factor = (final_learning_rate / initial_learning_rate) ** (-1. / (num_chunks_train - 1.))
    return dict(((x, initial_learning_rate * shrink_factor ** -x) for x in range(num_chunks_train)))


def linear_decay(num_chunks_train, initial_learning_rate, final_learning_rate):
    return dict(((x, (final_learning_rate - initial_learning_rate) / (num_chunks_train - 1.) * x + initial_learning_rate) for x in range(num_chunks_train)))


def sigmoid_decay(num_chunks_train, initial_learning_rate, final_learning_rate, begin_drop, end_drop):
    """
    sigmoid decay
    from https://github.com/BVLC/caffe/blob/master/src/caffe/solver.cpp
    sigmoid: the effective learning rate follows a sigmod decay
             return base_lr * ( 1/(1 + exp(-gamma * (iter - stepsize))))
    note that stepsize is the iteration where the sigmoid output is at the midpoint of the output range

    begin_drop and end_drop define what percent through the training the sigmoid should begin and end the large descending
    move. Before begin_drop and after end_drop, the training rate will be close to initial_learning_rate and final_learning_rate respectively
    """
    return dict(((x, (initial_learning_rate - final_learning_rate) / # The sigmoid height is the numerator
                     (1. + np.exp(8. / ((end_drop - begin_drop) * num_chunks_train) * # 8 is the "width" of the non-flat part of the sigmoid. We're scaling the drop so it is between begin_drop and end_drop
                                  (x - (num_chunks_train * (begin_drop + end_drop) / 2.)) # This takes the iteration and shifts it so the middle of the sigmoid lines up with the mean of begin_drop and end_drop
                                  )
                      )
                  + final_learning_rate)) # finally raise the entire thing so that it ends at final_learning_rate
                for x in range(num_chunks_train))


def initial_learning_rate_exploration(num_chunks_train, initial_learning_rate = 1e-7, final_learning_rate = 10.):
    """
    To do an exploration of what are good learning rates to start with, use this learning rate schedule with
    num_chunks_train = 6 * num_examples / examples_per_chunk
    """
    return exponential_decay(num_chunks_train, initial_learning_rate, final_learning_rate)


def triangle_learning_rate(itr, step_size, base_lr=0.001, max_lr=0.005):
    cycle = itr // (2 * step_size)
    x = float(itr - (2 * cycle + 1) * step_size) / step_size
    return base_lr + (max_lr - base_lr) * max([0., (1 - abs(x))  / (cycle + 1.)])


def triangle2_learning_rate(itr, step_size, base_lr=0.001, max_lr=0.005):
    cycle = itr // (2 * step_size)
    x = float(itr - (2 * cycle + 1) * step_size) / step_size
    return base_lr + (max_lr - base_lr) * max([0., (1 - abs(x))  / (2 ** cycle)])


def short_triangle_schedule(num_examples, chunk_size, init_min_lr, init_max_lr, iter_scale = 1.):
    epochs_per_step = 2 # 2-8 is the range in the paper
    num_cycles = 3 # 3-4 is the range in the paper
    num_steps = num_cycles * 2
    num_epochs = num_steps * epochs_per_step
    # a chunk and an iter are the same
    iters_per_epoch = float(num_examples) / chunk_size
    iters = int(iters_per_epoch * num_epochs * iter_scale)
    step_size = int(iters // num_steps)
    return dict((itr, triangle_learning_rate(itr, step_size, init_min_lr, init_max_lr)) for itr in range(iters))


def short_triangle2_schedule(num_examples, chunk_size, init_min_lr, init_max_lr, iter_scale = 1.):
    epochs_per_step = 2 # 2-8 is the range in the paper
    num_cycles = 3 # 3-4 is the range in the paper
    num_steps = num_cycles * 2
    num_epochs = num_steps * epochs_per_step
    # a chunk and an iter are the same
    iters_per_epoch = float(num_examples) / chunk_size
    iters = int(iters_per_epoch * num_epochs * iter_scale)
    step_size = int(iters // num_steps)
    return dict((itr, triangle2_learning_rate(itr, step_size, init_min_lr, init_max_lr)) for itr in range(iters))


def long_triangle_schedule(num_examples, chunk_size, init_min_lr, init_max_lr, iter_scale = 1.):
    epochs_per_step = 8 # 2-8 is the range in the paper
    num_cycles = 4 # 3-4 is the range in the paper
    num_steps = num_cycles * 2
    num_epochs = num_steps * epochs_per_step
    # a chunk and an iter are the same
    iters_per_epoch = float(num_examples) / chunk_size
    iters = int(iters_per_epoch * num_epochs * iter_scale)
    step_size = int(iters // num_steps)
    return dict((itr, triangle_learning_rate(itr, step_size, init_min_lr, init_max_lr)) for itr in range(iters))


def long_triangle2_schedule(num_examples, chunk_size, init_min_lr, init_max_lr, iter_scale = 1.):
    epochs_per_step = 8 # 2-8 is the range in the paper
    num_cycles = 4 # 3-4 is the range in the paper
    num_steps = num_cycles * 2
    num_epochs = num_steps * epochs_per_step
    # a chunk and an iter are the same
    iters_per_epoch = float(num_examples) / chunk_size
    iters = int(iters_per_epoch * num_epochs * iter_scale)
    step_size = int(iters // num_steps)
    return dict((itr, triangle2_learning_rate(itr, step_size, init_min_lr, init_max_lr)) for itr in range(iters))


def make_full_triangle_schedule(num_examples, chunk_size, init_min_lr, init_max_lr, iter_scale=1.0):
    return dict(enumerate(long_triangle_schedule(num_examples, chunk_size, init_min_lr, init_max_lr, iter_scale = 1. * iter_scale).values() + \
        long_triangle_schedule(num_examples, chunk_size, init_min_lr * 0.1, init_min_lr * 0.5, iter_scale = 0.5 * iter_scale).values() + \
        long_triangle_schedule(num_examples, chunk_size, init_min_lr * 0.01, init_min_lr * 0.05, iter_scale = 0.25 * iter_scale).values()))


def make_full_triangle2_schedule(num_examples, chunk_size, init_min_lr, init_max_lr, iter_scale=1.0):
    return dict(enumerate(long_triangle2_schedule(num_examples, chunk_size, init_min_lr, init_max_lr, iter_scale = 1. * iter_scale).values() + \
        long_triangle2_schedule(num_examples, chunk_size, init_min_lr * 0.1, init_min_lr * 0.5, iter_scale = 0.5 * iter_scale).values() + \
        long_triangle2_schedule(num_examples, chunk_size, init_min_lr * 0.01, init_min_lr * 0.05, iter_scale = 0.25 * iter_scale).values()))


def make_explore_triangle2_schedule(num_examples, chunk_size):
    learning_rates = [short_triangle2_schedule(num_examples, chunk_size, begin, end, 1./15.).values()
                      for begin, end in np.exp(zip(range(-12, 2), range(-11, 3)))]
    learning_rates = np.concatenate(learning_rates)

    return dict(enumerate(learning_rates))


def save_training_state(metadata_target_path, config_name, expid, e, losses_train, losses_eval_valid, losses_eval_train,
                        time_since_start, l_out, config, chunk_durations, chunk_times, accuracies_eval_valid,
                        accuracies_eval_train, kappas_eval_valid, kappas_eval_train, learning_rates, valid_chunk_idxs,
                        **kwargs):
    import lasagne as nn
    with open(metadata_target_path, 'w') as f:
        dump_dict = {'configuration': config_name,
                     'experiment_id': expid,
                     'chunks_since_start': e,
                     'losses_train': losses_train,
                     'losses_eval_valid': losses_eval_valid,
                     'losses_eval_train': losses_eval_train,
                     'accuracies_eval_valid': accuracies_eval_valid,
                     'accuracies_eval_train': accuracies_eval_train,
                     'kappas_eval_valid': kappas_eval_valid,
                     'kappas_eval_train': kappas_eval_train,
                     'time_since_start': time_since_start,
                     'param_values': nn.layers.get_state_values(l_out),
                     'data_loader_params': config.data_loader.get_params(),
                     'chunk_durations': chunk_durations,
                     'chunk_times': chunk_times,
                     'learning_rates': learning_rates,
                     'valid_chunk_idxs': valid_chunk_idxs, }
        dump_dict.update(kwargs)
        pickle.dump(dump_dict, f, pickle.HIGHEST_PROTOCOL)

