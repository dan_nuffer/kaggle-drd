import copy
from multiprocessing import Pipe, Process
import time
import hpsklearn
from hpsklearn.estimator import NonFiniteFeature, min_n_iters, tipping_pt_ratio, best_loss_cutoff_n_iters, \
    timeout_buffer
from hyperopt import hp, partial
import hyperopt
import numpy as np
import scipy
from sklearn import cross_validation
import skll
from hyperopt.pyll import scope, as_apply

__author__ = 'dan'

# This is modified to be a max of 10 because that's the # of dimensions, and n_components can't be larger, an exception
# will be thrown if so.
def pca(name, n_components=None, whiten=None, copy=True):
    rval = scope.sklearn_PCA(
        # -- qloguniform is missing a "scale" parameter so we
        #    lower the "high" parameter and multiply by 4 out front
        n_components=scope.min(4 * scope.int(
            hp.qloguniform(
                name + '.n_components',
                low=np.log(0.51),
                high=np.log(10.5),
                q=1.0)), 10) if n_components is None else n_components,
        whiten=hpsklearn.hp_bool(
            name + '.whiten',
            ) if whiten is None else whiten,
        copy=copy,
        )
    return rval


def preprocessing(name):
    """Generic pre-processing appropriate for a wide variety of data
    """
    return hp.choice('%s' % name, [
        [pca(name + '.pca')],
        [hpsklearn.standard_scaler(name + '.standard_scaler')],
        [hpsklearn.min_max_scaler(name + '.min_max_scaler')],
        [hpsklearn.normalizer(name + '.normalizer')],
        [],
        # -- not putting in one-hot because it can make vectors huge
        #[one_hot_encoder(name + '.one_hot_encoder')],
    ])


def _cost_fn(argd, X, y, cv_folds, info, timeout, _conn, best_loss=None):
    ss = cross_validation.ShuffleSplit(len(X), n_iter=cv_folds, test_size=1.0 / cv_folds, random_state=0)
    results = []
    for train_index, test_index in ss:
        rtype, rval = _cost_fn_fold(argd, X[train_index], y[train_index], X[test_index], y[test_index], info, timeout, best_loss)
        if rtype == 'raise' or (rtype == 'return' and rval['status'] == hyperopt.STATUS_FAIL):
            _conn.send((rtype, rval))
            return
        results.append((rtype, rval))

    # find the worst result
    rtype, rval = max(results, key=lambda x: x[1]['loss'])

    # -- return the result to calling process
    _conn.send((rtype, rval))


def _cost_fn_fold(argd, Xfit, yfit, Xval, yval, info, timeout, best_loss=None, _conn=None):
    try:
        t_start = time.time()
        if 'classifier' in argd:
            classifier = argd['classifier']
            preprocessings = argd['preprocessing']
        else:
            classifier = argd['model']['classifier']
            preprocessings = argd['model']['preprocessing']
        untrained_classifier = copy.deepcopy( classifier )
        # -- N.B. modify argd['preprocessing'] in-place
        for pp_algo in preprocessings:
            info('Fitting', pp_algo, 'to X of shape', Xfit.shape)
            pp_algo.fit(Xfit)
            info('Transforming fit and Xval', Xfit.shape, Xval.shape)
            Xfit = pp_algo.transform(Xfit)
            Xval = pp_algo.transform(Xval)

            # np.isfinite() does not work on sparse matrices
            if not (scipy.sparse.issparse(Xfit) or scipy.sparse.issparse(Xval)):
              if not (
                  np.all(np.isfinite(Xfit))
                  and np.all(np.isfinite(Xval))):
                  # -- jump to NonFiniteFeature handler below
                  raise NonFiniteFeature(pp_algo)

        info('Training classifier', classifier,
             'on X of dimension', Xfit.shape)


        def should_stop(scores):
          #TODO: possibly extend min_n_iters based on how close the current
          #      score is to the best score, up to some larger threshold
          if len(scores) < min_n_iters:
            return False
          tipping_pt = int(tipping_pt_ratio * len(scores))
          early_scores = scores[:tipping_pt]
          late_scores = scores[tipping_pt:]
          if max(early_scores) >= max(late_scores):
            return True
          #TODO: make this less confusing and possibly more accurate
          if len(scores) > best_loss_cutoff_n_iters and \
                 max(scores) < 1 - best_loss and \
                 3 * ( max(late_scores) - max(early_scores) ) < \
                 1 - best_loss - max(late_scores):
            info("stopping early due to best_loss cutoff criterion")
            return True
          return False

        n_iters = 0 # Keep track of the number of training iterations
        best_classifier = None
        if hasattr( classifier, "partial_fit" ):
          if timeout is not None:
            timeout_tolerance = timeout * timeout_buffer
          rng = np.random.RandomState(6665)
          train_idxs = rng.permutation(Xfit.shape[0])
          validation_scores = []

          while timeout is not None and \
                time.time() - t_start < timeout - timeout_tolerance:
            n_iters += 1
            rng.shuffle(train_idxs)
            classifier.partial_fit(Xfit[train_idxs], yfit[train_idxs],
                                   classes=np.unique( yfit ))
            validation_scores.append(classifier.score(Xval, yval))
            if max(validation_scores) == validation_scores[-1]:
              best_classifier = copy.deepcopy(classifier)

            if should_stop(validation_scores):
              break
            #info('VSCORE', validation_scores[-1])
          classifier = best_classifier
        else:
          classifier.fit( Xfit, yfit )

        if classifier is None:
            t_done = time.time()
            rval = {
                'status': hyperopt.STATUS_FAIL,
                'failure': 'Not enough time to train anything',
                'duration': t_done - t_start,
                }
            rtype = 'return'
        else:
            info('Scoring on Xval of shape', Xval.shape)
            #loss = 1.0 - classifier.score(Xval,yval)
            loss = 1.0 - skll.kappa(classifier.predict(Xval), yval, 'quadratic')
            # -- squared standard error of mean
            lossvar = (loss * (1 - loss)) / max(1, len(yval) - 1)
            info('OK trial with kappa %.1f +- %.1f' % (
                100 * (1.0 - loss),
                100 * np.sqrt(lossvar)))
            t_done = time.time()
            rval = {
                'loss': loss,
                'loss_variance': lossvar,
                'classifier': untrained_classifier,
                'preprocs': preprocessings,
                'status': hyperopt.STATUS_OK,
                'duration': t_done - t_start,
                'iterations': n_iters,
                }
            rtype = 'return'

    except (NonFiniteFeature,), exc:
        print 'Failing trial due to NaN in', str(exc)
        t_done = time.time()
        rval = {
            'status': hyperopt.STATUS_FAIL,
            'failure': str(exc),
            'duration': t_done - t_start,
            }
        rtype = 'return'

    except (ValueError,), exc:
        t_done = time.time()
        rval = {
            'status': hyperopt.STATUS_FAIL,
            'failure': str(exc),
            'duration': t_done - t_start,
            }
        rtype = 'return'

    except (AttributeError,), exc:
        print 'Failing due to k_means_ weirdness'
        if "'NoneType' object has no attribute 'copy'" in str(exc):
            # -- sklearn/cluster/k_means_.py line 270 raises this sometimes
            t_done = time.time()
            rval = {
                'status': hyperopt.STATUS_FAIL,
                'failure': str(exc),
                'duration': t_done - t_start,
                }
            rtype = 'return'
        else:
            rval = exc
            rtype = 'raise'

    except Exception, exc:
        rval = exc
        rtype = 'raise'

    if _conn is not None:
        _conn.send((rtype, rval))

    return (rtype, rval)


class cv_estimator(hpsklearn.HyperoptEstimator):
    def fit_iter(self, X, y, cv_folds=5, weights=None, increment=None):
        """Generator of Trials after ever-increasing numbers of evaluations
        """
        assert weights is None
        increment = self.fit_increment if increment is None else increment

        # len does not work on sparse matrices, so using shape[0] instead
        # shape[0] does not work on lists, so using len() for those
        if scipy.sparse.issparse(X):
          data_length = X.shape[0]
        else:
          data_length = len(X)
        if type(X) is list:
          X = np.array(X)
        if type(y) is list:
          y = np.array(y)

        # ss = cross_validation.ShuffleSplit(5, n_iter=3, test_size=0.25, random_state=0)
        # p = np.random.RandomState(123).permutation( data_length )
        # n_fit = int(.8 * data_length)
        # Xfit = X[p[:n_fit]]
        # yfit = y[p[:n_fit]]
        # Xval = X[p[n_fit:]]
        # yval = y[p[n_fit:]]
        self.trials = hyperopt.Trials()
        self._best_loss = float('inf')
        fn=partial(_cost_fn,
                X=X, y=y,
                cv_folds=cv_folds,
                info=self.info,
                timeout=self.trial_timeout)
        self._best_loss = float('inf')

        def fn_with_timeout(*args, **kwargs):
            conn1, conn2 = Pipe()
            kwargs['_conn'] = conn2
            th = Process(target=partial(fn, best_loss=self._best_loss),
                         args=args, kwargs=kwargs)
            th.start()
            if conn1.poll(self.trial_timeout):
                fn_rval = conn1.recv()
                th.join()
            else:
                self.info('TERMINATING DUE TO TIMEOUT')
                th.terminate()
                th.join()
                fn_rval = 'return', {
                    'status': hyperopt.STATUS_FAIL,
                    'failure': 'TimeOut'
                }

            assert fn_rval[0] in ('raise', 'return')
            if fn_rval[0] == 'raise':
                raise fn_rval[1]

            # -- remove potentially large objects from the rval
            #    so that the Trials() object below stays small
            #    We can recompute them if necessary, and it's usually
            #    not necessary at all.
            if fn_rval[1]['status'] == hyperopt.STATUS_OK:
                fn_loss = float(fn_rval[1].get('loss'))
                fn_preprocs = fn_rval[1].pop('preprocs')
                fn_classif = fn_rval[1].pop('classifier')
                fn_iters = fn_rval[1].pop('iterations')
                if fn_loss < self._best_loss:
                    self._best_preprocs = fn_preprocs
                    self._best_classif = fn_classif
                    self._best_loss = fn_loss
                    self._best_iters = fn_iters
            return fn_rval[1]

        while True:
            new_increment = yield self.trials
            if new_increment is not None:
                increment = new_increment
            hyperopt.fmin(fn_with_timeout,
                          space=self.space,
                          algo=self.algo,
                          trials=self.trials,
                          max_evals=len(self.trials.trials) + increment,
                          rstate=self.rstate,
                          # -- let exceptions crash the program,
                          #    so we notice them.
                          catch_eval_exceptions=False,
                          return_argmin=False, # -- in case no success so far
                         )

    def fit_iter_val(self, Xfit, yfit, Xval, yval, weights=None, increment=None):
        """Generator of Trials after ever-increasing numbers of evaluations
        """
        assert weights is None
        increment = self.fit_increment if increment is None else increment

        # len does not work on sparse matrices, so using shape[0] instead
        # shape[0] does not work on lists, so using len() for those
        if type(Xfit) is list:
          X = np.array(Xfit)
        if type(yfit) is list:
          y = np.array(yfit)

        self.trials = hyperopt.Trials()
        self._best_loss = float('inf')
        fn=partial(_cost_fn_fold,
                Xfit=Xfit, yfit=yfit,
                Xval=Xval, yval=yval,
                info=self.info,
                timeout=self.trial_timeout)
        self._best_loss = float('inf')

        def fn_with_timeout(*args, **kwargs):
            conn1, conn2 = Pipe()
            kwargs['_conn'] = conn2
            th = Process(target=partial(fn, best_loss=self._best_loss),
                         args=args, kwargs=kwargs)
            th.start()
            if conn1.poll(self.trial_timeout):
                fn_rval = conn1.recv()
                th.join()
            else:
                self.info('TERMINATING DUE TO TIMEOUT')
                th.terminate()
                th.join()
                fn_rval = 'return', {
                    'status': hyperopt.STATUS_FAIL,
                    'failure': 'TimeOut'
                }

            assert fn_rval[0] in ('raise', 'return')
            if fn_rval[0] == 'raise':
                raise fn_rval[1]

            # -- remove potentially large objects from the rval
            #    so that the Trials() object below stays small
            #    We can recompute them if necessary, and it's usually
            #    not necessary at all.
            if fn_rval[1]['status'] == hyperopt.STATUS_OK:
                fn_loss = float(fn_rval[1].get('loss'))
                fn_preprocs = fn_rval[1].pop('preprocs')
                fn_classif = fn_rval[1].pop('classifier')
                fn_iters = fn_rval[1].pop('iterations')
                if fn_loss < self._best_loss:
                    self._best_preprocs = fn_preprocs
                    self._best_classif = fn_classif
                    self._best_loss = fn_loss
                    self._best_iters = fn_iters
            return fn_rval[1]

        while True:
            new_increment = yield self.trials
            if new_increment is not None:
                increment = new_increment
            hyperopt.fmin(fn_with_timeout,
                          space=self.space,
                          algo=self.algo,
                          trials=self.trials,
                          max_evals=len(self.trials.trials) + increment,
                          rstate=self.rstate,
                          # -- let exceptions crash the program,
                          #    so we notice them.
                          catch_eval_exceptions=False,
                          return_argmin=False, # -- in case no success so far
                         )