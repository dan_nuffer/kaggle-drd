import glob

import numpy as np
import skll
import theano
import theano.tensor as T

import data

if theano.config.device !='cpu' or theano.config.floatX != 'float64':
    raise ValueError("Please set env var THEANO_FLAGS='device=cpu,floatX=float64' when running this program")

def predict_ordinal(output, thresh):
    # create a boolean array for numbers over the threshold
    x = output >= thresh
    # forward multiply across the columns, so that earlier falses propagate
    for col in xrange(1, x.shape[1]):
        x[:, col] *= x[:, col - 1]
    return np.clip(x.sum(axis=1) - 1, 0, x.shape[1])


def ensemble(model_w, thresh_w):
    # I want to find all the configs which have both valid and test predictions. list the files and find the intersection.
    # This script only handles ordinal predictions, so we have to filter for file with _ord in the name
    pred_valid_files = glob.glob('predictions/valid--*_ord*.npy')
    pred_test_files = glob.glob('predictions/test--*_ord*.npy')
    valid_set = set(p.replace('predictions/valid--', '', 1) for p in pred_valid_files)
    test_set = set(p.replace('predictions/test--', '', 1) for p in pred_test_files)
    predictions_set = valid_set & test_set

    valid_predictions_paths = ['predictions/valid--%s' % (p,) for p in predictions_set]

    # loading validation predictions
    predictions_list = [np.load(path) for path in valid_predictions_paths]
    predictions_stack = np.array(predictions_list).astype(np.float64)  # num_sources x num_datapoints x 5
    # optimizing weights
    X = theano.shared(predictions_stack)  # source predictions
    W = T.vector('W')

    s = T.nnet.softmax(W).reshape((W.shape[0], 1, 1))
    weighted_avg_predictions = T.sum(X * s, axis=0)  # T.tensordot(X, s, [[0], [0]])

    # These are used when optimizing for kappa
    weighted_avg_predictions_f = theano.function([W], weighted_avg_predictions)

    outputs = weighted_avg_predictions_f(model_w)
    cat_outputs = predict_ordinal(outputs, thresh=thresh_w)
    result = 1.0 - skll.kappa(data.labels_val, cat_outputs, 'quadratic')

    print 'Result = %f' % result
    return result

# Write a function like this called 'main'
def main(job_id, params):
    print 'job #%d' % job_id
    print params
    return ensemble(params['weights'], params['ordinal_thresholds'])
