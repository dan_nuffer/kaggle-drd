import logging
from pprint import pprint
import numpy as np
import click
from hyperopt import fmin, tpe, hp, Trials
from hyperopt.mongoexp import MongoTrials
from hyperopt.pyll import scope, as_apply

def input_side_len_space():
    # odd numbers (bc of convolutional layers)
    return hp.quniform('input_side_len', 96, 360, 12) + 1


def batch_size_space(name):
    # multiples of 8 because I'm superstitious (no evidence) that it helps out memory caching
    return hp.quniform(name + '|batch_size', 8, 1024, 8)


def chunk_size_space(name):
    return hp.quniform(name + '|chunk_size', 1024, 65536, 1024)

def data_space(name, input_side_len):
    batch_size = batch_size_space(name)
    return as_apply({
        'input_side_len' : input_side_len,
        'batch_size' : batch_size,
        #'chunk_size' : 4096,
        #'chunk_size' : chunk_size_space(name),
        # for simplicity, just go for 262144 examples / batch size in order to keep things consistent. This doesn't matter
        # much except for how often the results are saved, but it means the same # of examples are used to train.
        'chunk_size' : scope.int(262144 / batch_size),
        'num_chunks_train' : 300,
    })


def eval_data_processing_space(name, input_side_len):
    zoom = hp.uniform(name + '|zoom', 0., 1.5) * hp.pchoice(name + '|zoom_enable', [(.5, 0), (.5, 1)]) + 1.
    rotation_max = hp.uniform(name + '|rotation_max', 12, 360)
    shear = hp.uniform(name + '|shear', 20., 35.)
    # translation is in pixels scaled for 128x images. Scale it to whatever size we're using
    translation = hp.uniform(name + '|translation', 1., 15.) * input_side_len / 128.
    contrast_noise = hp.uniform(name + '|contrast_noise_sigma', 0., 0.3)

    return as_apply({
        'num_transforms' : 90,
        'zoom_range' : (1 / zoom, zoom),
        'rotation_range' : (0, rotation_max),
        'shear_range' : (-shear, shear),
        'translation_range' : (-translation, translation),
        'do_flip' : True,
        'allow_stretch' : 1.,
        'contrast_noise_sigma' : contrast_noise,
    })


def train_data_processing_space(name, input_side_len):
    zoom = hp.uniform(name + '|zoom', 0., 1.5) + 1.
    shear = hp.uniform(name + '|shear', 3., 28.)
    # translation is in pixels scaled for 128x images. Scale it to whatever size we're using
    translation = hp.uniform(name + '|translation', 5., 20.) * hp.pchoice(name + '|translation_enable', [(.5, 0), (.5, 1)]) * input_side_len / 128.
    contrast_noise = hp.uniform(name + '|contrast_noise_sigma', 0., 0.2) * hp.pchoice(name + '|contrast_noise_enable', [(.9, 0), (.1, 1)])

    return as_apply({
        'zoom_range' : (1 / zoom, zoom),
        'rotation_range' : (0, 0),
        'shear_range' : (-shear, shear),
        'translation_range' : (-translation, translation),
        'do_flip' : True,
        'allow_stretch' : 1.,
        'contrast_noise_sigma' : contrast_noise,
    })


def learning_parameters_space():
    return as_apply({
        'momentum' : hp.uniform('momentum', 0.5, 0.99),
        'learning_rate' : hp.loguniform('learning_rate', np.log(1e-5), np.log(10)),
    })


def regularization_space():
    return as_apply({
        'l1' : hp.loguniform('l1', np.log(1e-7), np.log(1e-1)) * hp.choice('l1_enable', [0, 1]),
        'l2' : hp.loguniform('l2', np.log(1e-7), np.log(1e-1)) * hp.choice('l2_enable', [0, 1]),
        # TODO: Test it out and get it to work - 'adversarial' : adversarial_regularization_space(),
        'fc1_dropout' : dropout_space('fc1_dropout'),
        'fc2_dropout' : dropout_space('fc2_dropout'),
        'fc3_dropout' : dropout_space('fc3_dropout'),
        'max_col_norm1' : hp.uniform('max_col_norm1', 0.1, 10.0) * hp.choice('max_col_norm1_enable', [0, 1]),
        'max_col_norm2' : hp.uniform('max_col_norm2', 0.1, 10.0) * hp.choice('max_col_norm2_enable', [0, 1]),
        'max_col_norm3' : hp.uniform('max_col_norm3', 0.1, 10.0) * hp.choice('max_col_norm3_enable', [0, 1]),
    })



# TODO: Get the code to implement AdversarialRegObjective working before using this.
def adversarial_regularization_space():
    return hp.choice('adversarial_regularization_enable', [True, False])


# I'm having a hard time deciding if I want to make a choice and have it apply to all layers, for instance the
# non-linearity function or initialization strategy or batch normalization, or if I want to have every layer be
# random. I think making each layer be random could find a more apt architecture, but it presents a drastically
# larger search space, so maybe one won't actually be found even if it is possible. The alternative is to be
# homogenous, and that should be easier to search, and works pretty well, so I guess I'll start with that, and
# then try out the random per-layer search after (or in parallel) and see how well it does.
def dropout_space(name):
    return hp.choice(name + '|dropout', [
        ('none', None),
        ('regular', as_apply({
            'p': scope.max(0., hp.normal(name + '|dropout_p', 0.5, 1./8.)),
            'rescale': hp.choice(name + '|dropout_rescale', [True, False])
        })),
        ('gaussian', scope.max(1e-7, hp.normal(name + '|gaussian_dropout_sigma', 1.0, 1./8.))),
        ('tied', as_apply({
            'p': scope.max(0., hp.normal(name + '|tied_dropout_p', 0.5, 1./8.)),
            'rescale': hp.choice(name + '|tied_dropout_rescale', [True, False])
        })),
        ('custom_rescale', as_apply({
            'p': scope.max(0., hp.normal(name + '|tied_dropout_p', 0.5, 1./8.)),
            'rescale': hp.uniform(name + '|tied_dropout_rescale', 0.5, 2.0)
        })),
    ])

def objective(argd):
    from pprint import pprint, pformat
    from hyperopt import STATUS_OK, STATUS_FAIL
    import platform
    import time

    print "-" * 80
    print "Evaluting hyperparameters:"
    pprint(argd)

    netfile = "nets/cnn_bn_3x85x_ord_data_transform_hps_%s-%s.py" % (platform.node(), time.strftime("%Y%m%d-%H%M%S", time.localtime()))
    print "Writing network to %s" % (netfile)
    with open(netfile, "w") as f:
        f.write('hyperparameters = \\\n')
        f.write(pformat(argd))
        f.write('\n')
        f.write('from . import cnn_bn_3ch_95x_ord_hyperparam_tmpl\n')
        f.write('cnn_bn_3ch_95x_ord_hyperparam_tmpl.set_hyperparameters(hyperparameters)\n')
        f.write('from cnn_bn_3ch_95x_ord_hyperparam_tmpl import *\n')

    resultfile = netfile + '-result.pkl'
    command = 'python train_convnet.py %s --store-result-in=%s' % (netfile, resultfile)

    print "Running training command: %s" % (command,)
    # run command
    import os
    if os.system(command) != 0:
        return {
            'status': STATUS_FAIL,
            'loss': 2.0,
            'netfile': netfile,
            'resultfile': resultfile,
            'hyperparameters': argd,
        }

    # load results
    print "Loading results from: %s" % (resultfile,)
    import cPickle
    with open(resultfile, 'r') as f:
        results = cPickle.load(f)

    kappa = results['best_valid_kappa']

    print "Got best valid kappa: %f" % (kappa,)

    # if the training ended early, call it a failure.
    if results['chunks_since_start'] != results['num_chunks_train']:
        return {
            'status': STATUS_FAIL,
            'loss': 1. - kappa,
            'netfile': netfile,
            'resultfile': resultfile,
            'hyperparameters': argd,
        }

    # return the best validation kappa, converted to a loss
    return {
        'status': STATUS_OK,
        'loss': 1. - kappa,
        'netfile': netfile,
        'resultfile': resultfile,
        'hyperparameters': argd,
    }


@click.command()
@click.option('--count', default=1, help='Number evaluations.')
@click.option('--mongo', default=False, help='Use mongodb. ')
def main(count, mongo):
    logging.basicConfig(level=logging.DEBUG)

    if mongo:
        trials = MongoTrials('mongo://localhost:27017/drd_db/jobs', exp_key='data_transform')
    else:
        trials = Trials()

    input_side_len = input_side_len_space()
    data_params = data_space('data', input_side_len)
    train_data_processing = train_data_processing_space('train', input_side_len)
    eval_data_processing = eval_data_processing_space('eval', input_side_len)

    space = as_apply({
                'train_data_processing': train_data_processing,
                'eval_data_processing': eval_data_processing,
                'data_params': data_params,
            })
    best = fmin(fn=objective,
                space=space,
                algo=tpe.suggest,
                max_evals=count,
                trials=trials)

    print "Best result from fmin():"
    pprint(best)
    print
    print "Best trial hyperparameters:"
    print trials.best_trial
    print
    print "All trials:"
    for i, trial in enumerate(trials):
        print "Trial %d:" % (i)
        pprint(trial)



if __name__ == '__main__':
    import sys
    sys.exit(main())
