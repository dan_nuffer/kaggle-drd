import os
import os.path
import lockfile
import glob
import numpy as np
import itertools
import time
from processify import processify


def main():
    # These are too slow, and wouldn't finish for years.
    blacklist = [
        #'metadata/pl_361x_bn_ord_maxout2_large-turing-20150616-214135_best.pkl',
        #'metadata/pl_361x_bn_ord_maxout2_large-turing-20150615-224626_best.pkl',
        #'metadata/convroll4_batchnormdense_3channel_181patch_ordinal-turing-20150522-224945_best.pkl',
        #'metadata/pl_255x_bn_ord_maxout_hps117_turing-turing-20150623-230826_best.pkl',
        #'metadata/vgge_cnn_255x_bn_ord_l2-turing-20150616-215631_best.pkl'
        ]
    while True:
        subsets1 = ['valid_noaug', 'test_noaug', 'train_noaug']
        subsets2 = ['valid', 'test', 'train']
        #subsets2 = []
        best_metadatas = best_metadata_ordered_by_kappa_desc()[:50]
        non_best_metadatas = non_best_metadata_ordered_by_kappa_desc()[:50]
        for metadata_best_file, subset in itertools.chain(itertools.product(best_metadatas, subsets1),
                                                          itertools.product(non_best_metadatas, subsets1),
                                                          itertools.product(best_metadatas, subsets2),
                                                          itertools.product(non_best_metadatas, subsets2)):
            if os.path.isfile("stop_predict_all_convnets_trigger"):
                print "stopping because stop_predict_all_convnets_trigger file exists"
                return

            if metadata_best_file in blacklist:
                print "skipping %s because it is blacklisted" % (metadata_best_file,)
                continue

            prediction_filename = prediction_filename_for_metadata(metadata_best_file, subset)
            metadata_mtime = os.path.getmtime(metadata_best_file)
            if os.path.isfile(prediction_filename) and os.path.getmtime(prediction_filename) > metadata_mtime:
                print "skipping %s because it already exists and is newer than %s" % (prediction_filename, metadata_best_file)
                continue

            if metadata_mtime > time.time() - 15 * 60:
                print "skipping %s because %s was created less than 15 minutes ago." % (prediction_filename, metadata_best_file)
                continue

            lock = lockfile.LockFile("/run/user/%d/predict_all_convnets-%s-%s.lock" % (os.getuid(), os.path.basename(prediction_filename), subset), timeout=1)
            try:
                print ('Attempting to lock %s' % ("/run/user/%d/predict_all_convnets-%s-%s.lock" % (os.getuid(), os.path.basename(prediction_filename), subset)))
                with lock:
                    predict_convnet_cmd = 'python predict_convnet.py _ %s %s' % (metadata_best_file, subset)
                    print ("Running %s" % (predict_convnet_cmd,))
                    exit_status = os.system(predict_convnet_cmd)
                    if exit_status != 0:
                        print ('%s failed with exit status %d' % (predict_convnet_cmd, exit_status))

                    if os.path.getmtime(metadata_best_file) != metadata_mtime:
                        # The metadata was updated while prediction was running. This means the current predictions are out of date and need to
                        # be regenerated
                        os.unlink(prediction_filename)

                    break  # only want to done one metadata before re-reading them off the filesystem so that this program doesn't need to restart whenever new metadata files are added

            except lockfile.LockTimeout:
                # another process is working on this one, just skip it
                print ('Lock is already locked. Moving on.')
                continue


# These functions that load the metadata need to run in their own process so that there aren't multiple uses of the
# GPU in parallel
@processify
def prediction_filename_for_metadata(metadata_best_file, subset):
    metadata = np.load(metadata_best_file)
    config_name = metadata['configuration']
    filename = os.path.splitext(os.path.basename(metadata_best_file))[0]
    avg_method = "avg-probs"
    prediction_filename = "predictions/%s--%s--%s--%s.npy" % (subset, config_name, filename, avg_method)
    return prediction_filename


@processify
def kappa_from_metadata(metadata_filename):
    try:
        md = np.load(metadata_filename)
        if 'kappas_eval_valid' in md:
            if len(md['kappas_eval_valid']) > 0:
                return md['kappas_eval_valid'][-1]
    except:
        pass
    # kappa -1.0 is the worst.
    return -1.0


def best_metadata_ordered_by_kappa_desc():
    metadata_best_files = glob.glob('metadata/*_best.pkl')
    files_and_kappas = sorted([(filename, kappa_from_metadata(filename)) for filename in metadata_best_files], key=lambda tup: -tup[1])
    good_kappa_threshold = 0.5  # there's no point is using anything less than this value
    return [filename for filename, kappa in files_and_kappas if kappa > good_kappa_threshold]


def non_best_metadata_ordered_by_kappa_desc():
    metadata_files = [fn for fn in glob.glob('metadata/*.pkl') if not fn.endswith('_best.pkl')]
    files_and_kappas = sorted([(filename, kappa_from_metadata(filename)) for filename in metadata_files], key=lambda tup: -tup[1])
    good_kappa_threshold = 0.5  # there's no point is using anything less than this value
    return [filename for filename, kappa in files_and_kappas if kappa > good_kappa_threshold]


if __name__ == '__main__':
    main()
