import glob
import sys
import pandas as pd
import numpy as np

size = int(sys.argv[1])
output_shape = (size, size)

save_dir = "data-%dx%d" % (size, size)

# do the training images
def load(path):
    print 'Loading training labels from %s' % (path)
    return pd.read_csv(path)

train_labels_all_us = load('data/trainLabels_all_US.csv')
train_labels_train_us = load('data/trainLabels_train_US.csv')
train_labels_val = load('data/trainLabels_val.csv')

paths_train = glob.glob('data/train/*.jpeg')
paths_test = glob.glob("data/test/*.jpeg")

images_all_us = np.load('%s/images_all_us.npy' % (save_dir))
assert images_all_us.size == 129050
assert images_all_us[0].shape == (size, size, 3)
assert images_all_us[0].dtype == 'uint8'
del images_all_us

images_test = np.load('%s/images_test.npy' % (save_dir))
assert images_test.size == 53576
assert images_test[0].shape == (size, size, 3)
assert images_test[0].dtype == 'uint8'
del images_test

images_train_us = np.load('%s/images_train_us.npy' % (save_dir))
assert images_train_us.size == 90335
assert images_train_us[0].shape == (size, size, 3)
assert images_train_us[0].dtype == 'uint8'
del images_train_us

images_val = np.load('%s/images_val.npy' % (save_dir))
assert images_val.size == 10535
assert images_val[0].shape == (size, size, 3)
assert images_val[0].dtype == 'uint8'
del images_val

labels_all_us = np.load('%s/labels_all_us.npy' % (save_dir))
assert labels_all_us.size == 129050

labels_train_us = np.load('%s/labels_train_us.npy' % (save_dir))
assert labels_train_us.size == 90335

labels_val = np.load('%s/labels_val.npy' % (save_dir))
assert labels_val.size == 10535