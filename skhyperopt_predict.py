#! /usr/bin/python
import sys

import numpy as np
from clint.textui import puts
import skll
import hyperopt.tpe
import hpsklearn
import hpsklearn.demo_support

import load
from skhyperopt import preprocessing, cv_estimator
import utils


def main():
    train_predictions_path = sys.argv[1]
    valid_predictions_path = sys.argv[2]
    data_loader = load.DataLoader(predict_mode="ordinal_regression")

    train_X = np.load(train_predictions_path)
    train_Y = utils.predict_ordinal(data_loader.labels_train, thresh=0.5)

    test_X = np.load(valid_predictions_path)
    test_Y = utils.predict_ordinal(data_loader.labels_valid, thresh=0.5)

    estimator = cv_estimator( #hpsklearn.HyperoptEstimator(
        preprocessing=preprocessing('pp'),
        classifier=hpsklearn.components.any_classifier('clf'),
        algo=hyperopt.tpe.suggest,
        trial_timeout=60.0 * 5, # seconds
        max_evals=1000,
        verbose=1,
        )

    #estimator.fit(train_X, train_Y)
    fit_iterator = estimator.fit_iter_val(train_X, train_Y, test_X, test_Y)
    fit_iterator.next()
    while len(estimator.trials.trials) < estimator.max_evals:
        fit_iterator.send(1) # -- try one more model
        last_loss = estimator.trials.losses()[-1]
        print "Iteration %d loss: %s, best: %f" % (len(estimator.trials.trials), str(last_loss), estimator._best_loss)
        print "best model: %s" % (estimator.best_model(),)

    estimator.retrain_best_model_on_full_data(train_X, train_Y)

    print 'Best preprocessing pipeline:'
    best_model = estimator.best_model()
    for pp in best_model['preprocs']:
        print pp
    print
    print 'Best classifier:\n', best_model['classifier']

    def class_error(predicted, expected):
        return sum( int(predicted[i]) != expected[i] for i in range(len(expected))) / float(len(expected))

    def print_prediction_stats(data_set, cat_outputs, cat_labels):
        puts()
        puts("Results for %s" % (data_set,))
        puts("error: %f" % (class_error(cat_outputs, cat_labels)))
        puts("kappa: %f" % (skll.kappa(cat_outputs, cat_labels, 'quadratic')))
        puts("Confusion Matrix:")
        puts(str(utils.conf_matrix(cat_outputs, cat_labels, 5)))

    print_prediction_stats("train model", estimator.predict(train_X), train_Y)
    print_prediction_stats("train thresh", utils.predict_ordinal(train_X, thresh=0.5), train_Y)
    print_prediction_stats("valid model", estimator.predict(test_X), test_Y)
    print_prediction_stats("valid thresh", utils.predict_ordinal(test_X, thresh=0.5), test_Y)


    # TODO: use the best parameters, retrain on the whole training set and predict the actual test set.
    #clf.fit(np.vstack((train_X, test_X)), np.concatenate((train_Y, test_Y)))
    return 0

if __name__ == '__main__':
    sys.exit(main())


"""
This is the best model found with 1000 iterations with the following command line:
OMP_NUM_THREADS=1 python skhyperopt_predict.py predictions/train_noaug--cnn_batchnormdense_3channel_255patch_ordinal_l2--cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best--avg-probs.npy predictions/valid_noaug--cnn_batchnormdense_3channel_255patch_ordinal_l2--cnn_batchnormdense_3channel_255patch_ordinal_l2-turing-20150604-201827_best--avg-probs.npy
Which is the best single prediction, as of 7/15. The output improved the prediction on valid by ~.017 over using 0.5 threshold
The best was found on iteration 75. So in hind-sight running 1000 iterations was overkill. Next time I'll run 100 or so, and 50 if
in a hurry.

{'classifier': SGDClassifier(alpha=0.00432923706076, class_weight=None, epsilon=0.1,
       eta0=0.000559970368666, fit_intercept=True, l1_ratio=0.432812887847,
       learning_rate='invscaling', loss='log', n_iter=5, n_jobs=1,
       penalty='l1', power_t=0.918350690955, random_state=None,
       shuffle=False, verbose=False, warm_start=False), 'preprocs': (StandardScaler(copy=True, with_mean=True, with_std=True),)}

Results for train model
error: 0.198749
kappa: 0.932367
Confusion Matrix:
[[16615  1237   197     8    10]
 [ 9591  7930   546     0     0]
 [ 2737  2968 11834   509    19]
 [    0     0    82 17935    50]
 [    0     0     0     0 18067]]

Results for train thresh
error: 0.186152
kappa: 0.939111
Confusion Matrix:
[[15875  1888   290     8     6]
 [ 8063  9582   422     0     0]
 [ 1955  3565 12333   211     3]
 [    0     0   405 17662     0]
 [    0     0     0     0 18067]]
Transforming X of shape (10535, 5)
Predicting X of shape (10535, 5)

Results for valid model
error: 0.224680
kappa: 0.755576
Confusion Matrix:
[[6929  631  153   14   16]
 [ 498  172   60    1    1]
 [ 331  295  792  133   36]
 [  10   10   74  143   24]
 [   8    5   44   23  132]]

Results for valid thresh
error: 0.251258
kappa: 0.738451
Confusion Matrix:
[[6566  937  220    9   11]
 [ 438  230   64    0    0]
 [ 271  345  857   90   24]
 [   7   14  110  114   16]
 [   9    5   56   21  121]]

"""

